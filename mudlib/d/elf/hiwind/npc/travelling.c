// travelling.c
// written by hiwind

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
    int HP= 15, SP= 50;

    set_name("�෬���ȫ�", ({ "traveler" }));
    set("lv", 1);
    set("class", WARRIOR);
    set("race", HUMAN);
    set("describe","�෬���ȫ�.");
    set("age", 25);
    set("element", FIRE);
    set("mind", -500);

    set("maxHP", HP);
    set("currentHP", HP);
    set("maxSP", SP);
    set("currentSP", SP);
    set("dem", 10);
    set("def", 4);
    set("spirit", 14);
    set("spe", 6);
    set("exp", 6000000);
    setup();
}
