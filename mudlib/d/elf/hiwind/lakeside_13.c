//lakeside_13.c
//write by hiwind

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1m��[m[1;36m��[m[1m��[m");
        set("long",@LONG
�o��s��[1;36m��[m,�A�@�Q�즹���T�h�F�@�U,�P��O[1;31m��[m�L��¶,�o�O
��[1;36m��[m,�O�W�Ѫ���N�w����...�A�]���T���M,���L�o��O�@��
�ר��i�ʪ��n�a��,������F�̳����p��������O?�����G�Q��
��M,�j���O���_�������y�J�����Y�a!
�j�۵M���𮧤��_����¶�b�A����,�uı�o��~�κZ.
LONG
        );

        set("exits",
        ([
        "east" : __DIR__ + "street_1",
        "west" : __DIR__ + "lakeside_11",
        "north" : __DIR__ + "lakeside_15",
        ])
        );

        set("objects",
        ([
                __DIR__ + "npc/frog":2
        ])
        );

        set("no_fight", 0);
        setup();

}
