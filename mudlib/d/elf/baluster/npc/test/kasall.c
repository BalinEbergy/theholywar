// kasall.c
// written by Hudson
// 卡薩爾•多倫

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
        int HP= 600000, MP= 2000, SP= 32000;

        set_name("旅客", ({ "traveller", "kasall doren", "kasall", "doren" }));
        set("lv", 84);
        set("class",MINSTREL);
        set("race",OGRE);
        set("gender","男");
        set("describe","一位背著大鼓,身材高壯的旅客.");
        set("pre","背著大鼓的");

        // 老師資訊
        set("teacher", ({
                11, 14, 15,
                }));
        set("favorite", ({
                MINSTREL, WIZARD,
                }));
        set("teach_mind", "225 ~ 1000");
        set("teach_msg_evil", "[1;36m旅客說道:[33m 邪惡的人啊...你還想從我這裡學到東西嗎...[m");
        set("lv_up", "[1;36m旅客哈哈的笑道:[33m 好了, 等級提升了吧![m\n");
        set("no_enu_money", "[1;36m旅客呵呵的笑道:[33m 就算是我也不做白工的喔...[m");
        set("no_enu_lv", "[1;36m旅客笑道:[33m 你的等級還不夠喔...[m");
        set("top_lv", "[1;36m旅客呵呵的笑道:[33m 我已經不能再教你什麼囉...[m");
        set("teach_message", "[1;36m旅客哈哈的笑道:[33m 想找我學技能啊! 你想學什麼嗎?[m");
        // ---------

        set("age", 379);
        set("element",EARTH);
        set("mind", 739);
        set("spirit", 50);

        set("mixHP", HP);
        set("currentHP", HP);
        set("mixMP", MP);
        set("currentMP", MP);
        set("mixSP", SP);
        set("currentSP", SP);
        set("dem", 29000);
        set("def", 15000);
        set("mdem", 12000);
        set("mdef", 28533);
        set("spe", 1797);
        set("exp", 243328);

        carry_object( OB_DRUM+ "giant_drum")->wear();

        set("chat_chance", 13);
        set("chat_msg", ({
                "\n旅客愉快的唱著歌~\n",
                "\n旅客哈哈大笑著.\n",
                (: this_object(), "random_move" :),
                }));

        set_skill_level( 11, 90);
        set_skill_level( 14, 100);
        set_skill_level( 15, 80);
        set("skill", ({
                "11:forest sound", "11:sky end",
                }));
        set("skill_chance", 20);

        setup();
}

void init()
{
        object obj;

        if( !(obj= previous_object())
                || !userp( obj))
                return;

        call_out("ki", 100, obj);
        call_out("action", 20, obj);
}

void action( object obj)
{
        if( this_object()->is_fighting()) return;
        command("hihi 這兒的環境真不錯...");
}

void ki( object obj)
{
        if( this_object()->is_fighting()) return;
        if( obj->query("attacker")) kill_ob( obj);
}
