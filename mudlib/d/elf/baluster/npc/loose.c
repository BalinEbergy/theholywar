// loose.c
// written by Hudson
// 魯斯

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
    int HP= 307823, MP= 51321, SP= 3000;

    set_name("魯斯", ({ "loose" }));
    set("lv", 71);
    set("class",WIZARD);
    set("race",ELF);
    set("gender",1);
    set("describe","一位著天藍色長袍的巫師.");
    set("pre","傳送使");

    set("age", 318);
    set("element",COSMOS);
    set("mind", 215);

    set("maxHP", HP);
    set("currentHP", HP);
    set("maxMP", MP);
    set("currentMP", MP);
    set("maxSP", SP);
    set("currentSP", SP);
    set("strength", 9340);
    set("armor", 20000);
    set("magic", 20000);
    set("spi", 12);
    set("speed", 540);
    set("exp", 6200);

    set("trans", ({ "chogall", "saint kruce", "great" }));

    setup();
}

void init()
{
    if( !previous_object()
        || !userp(previous_object()))
        return;
    call_out("mess", 1);
}

void mess()
{
    command("say 呵呵呵... 這裡是魔法公會傳送的中心喔...");
}
