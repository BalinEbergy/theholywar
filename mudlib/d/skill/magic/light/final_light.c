// final_light.c
// written by hiwind

#include <skill.h>

int main( object me, object obj)
{
        int demage, i, damage;
        
        if( random( 5)+ ( (1.5- ( me->query("currentHP")/ me->query("mixHP")))* me->query("spirit")) <25)
        {
                write("你並沒有必死的決心.\n");
        } else
        {
                tell_object( obj, "只覺得[1;31m敵人[m身上不斷發出[1;32m清[m[1;36m亮[m的光芒,似乎要[1;37m拼死一鬥[m.\n");

                for( i=1; i<= 16; i++)
                {
                if( me->query("spirit")+ random( 20) > obj->query("spirit")+ random( 20))
                {
                        if( me->query("currentMP")<= 50)
                        {
                                write("\n你沒有足夠的法力了.\n");
                        } else
                        {

                                me->add("currentMP", -50);
                        
                                tell_object( obj, "\n敵人強大的意志力,你無法與其抗衡.\n");
                                write("\n敵人被你不顧一切的氣勢所震撼.\n");        

                                        demage= ( random( 2500)+ (me->query("mdem")* me->query("spirit")/ 300));
                
                                obj->add("currentHP", -demage);

                                if( obj->query("currentHP")<= 0) return demage;

                                message_vision("$N使出[1;31m極招[m對$n" + COMBAT_D->underattack( demage, obj)
                                + "([1;31m" + ((demage>= 100000)? ((demage>= 400000)? "[33;41m!!!!!!": "[37;41m*****") : demage)
                                + "[m)\n", me, obj);
                        }
                }
                else if( me->query("spirit")+ random( 20) == obj->query("spirit")+ random( 20))
                {
                        message_vision("\n$N使出畢生之所學,不顧一切的向$n攻擊,但兩人勢均力敵,都絲毫沒有損傷.\n", me, obj);
                } else
                {
                        if( obj->query("currentMP")<= 50)
                        {
                                tell_object( obj, "\n你沒有足夠的法力了.\n");
                        } else
                        {

                                obj->add("currentMP", -50);

                                write("\n你的決心不足,反而使敵人對你[1;31m痛擊[m.\n");
                                tell_object( obj, "\n敵人的攻勢沒有威力,反而被你回擊.\n");

                                        demage= ( random( 2500)+ (obj->query("mdem")* obj->query("spirit")/ 300));

                                me->add("currentHP", -demage);

                                message_vision("$N破解了$n的招式,並給予[1;33m反擊[m對$n" + COMBAT_D->underattack( demage, obj)
                                + "([1;31m" + ((demage>= 100000)? ((demage>= 400000)? "[33;41m!!!!!!": "[37;41m*****") : demage)
                                + "[m)\n", obj, me);

                                if( me->query("currentHP")<= 0)
                                {
                                        COMBAT_D->die( obj, me);
                                        return 0;
                                }
                        }
                }
                }

                me->set("no_fight");
                obj->set("no_fight");
                message_vision("\n忽然間,$N和$n都停止了攻勢," +
                "取而代之的是一場[1;36m寂靜[m的[1;37m對峙[m.\n" +
                "突然間,天色漸漸變暗,一股奇怪的霧氣圍繞著$N與$n.\n", me, obj);
                call_out("end",2, me, obj, demage, damage);
                me->set("delay", time()+ 100);
                obj->set("delay", time()+ 100);
                environment( me)->set("unmoveable", 1);
                return -1;
        }
}
        void end( object me, object obj, int demage, int damage)
        {
                environment( me)->delete("unmoveable");
                me->delete("delay");
                obj->delete("delay");
                message_vision("忽然一聲如雷巨響,圍繞著$N與$n的煙霧散開了.\n", me, obj);

                tell_object( obj, "\n你只覺得萬道[1;5;33;43m光芒[m貫穿了你.\n");
                tell_object( me, "\n你只覺得敵人的魔法如[1;5;36;46m海浪[m般的濤濤不絕得襲來.\n");

                demage= ((obj->query("currentMP")* 10)+ obj->query("mdem")
                        + (obj->query("magic")* me->query("spirit")*6));

                me->add("currentHP", -demage);

                if( me->query("currentHP")<=0)        
                        COMBAT_D->die( obj, me);

                obj->add("currentMP", -500);

                if( obj->query("currentMP")<0)
                        obj->set("currentMP", 0);

                damage= ((me->query("currentMP")* 10)+ me->query("mdem")
                        + (me->query("magic")* me->query("spirit")*6));

                obj->add("currentHP", -damage);

                me->delete("no_fight");
                obj->delete("no_fight");
                COMBAT_D->skill_msg( me, obj, "迴光返照", "none", damage);
                COMBAT_D->skill_msg( obj, me, "迴光返照", "none", demage);
        }


string name( int select)
{
        switch( select)
        {
                case 1: return "迴光返照";
                case 2: return "final light";
                case 3: return "final_light";
        }
}

int type() { return LIGHT; }
int lv() { return 40; }
int attackful()
{
        return 1;
}

string mes( object me)
{
        return "集中[1;37m精神[m,將全部的[1;31m魔力[m逐漸匯聚,而在身旁散發出[1;32m清[m\n[1;36m亮[m的[1;37m光芒[m.\n";
}

int usesp() { return 0; }
int usemp() { return 500; }
int successful() { return 60; }
int delay() { return 1; }
int all() { return 0; }
string element() { return "light"; }

int check( object me)
{
        return 1;
}
