// mageguildm2.c
// written by Hudson

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[36m魔法師公會會所[m");
        set("long",@LONG
這兒是魔法師公會會所東方,在空中飄著一團綠色的火,你輕
輕的將手伸進去...咦?居然不會燙手?這使你感到非常的奇怪
,難道這是魔法造的火嗎?
LONG
        );

        set("exits", ([
                "north"  : __DIR__ + "mageguildn3",
                "west"   : __DIR__ + "mageguildc",
                "south"  : __DIR__ + "mageguilds3",
                ]));
}
