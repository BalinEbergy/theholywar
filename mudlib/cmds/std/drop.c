// drop.c
// written by Hudson

int do_drop( object me, object obj);

int main( object me, int argc, string *argv)
{
    int i;
    object obj, obj2, *inv;

/*    if( me->query("age")< 20)
        write("\n你還太年輕囉...等成年再說吧!\n");*/
    if( argc< 2)
        write("你要丟棄什麼東西?\n");
    else if( argv[1]== "all")
    {
        inv= all_inventory( me);
        for( i= 0; i< sizeof( inv); i++)
        {
            if( !inv[i]->equipped())
                do_drop( me, inv[i]);
        }
        write("Ok.\n");
    } else if( !objectp( obj= find( argv[1], me)))
        write("你身上沒有這樣東西.\n");
    else do_drop( me, obj);

    return 1;
}

int do_drop( object me, object obj)
{
    mixed no_drop;

    if( no_drop= obj->query("no_drop"))
        write( stringp( no_drop)? no_drop: "這樣東西無法丟棄.\n");
    else if( !obj->move( environment( me)))
        message_vision( sprintf("$N丟下一%s%s.\n", obj->query("unit"), obj->name()), me);
    else write("你必須先解除這個裝備.\n");

    return 1;
}

int help( object me)
{
    write( @HELP
指令格式 : drop <物品名稱>
 
這個指令可以讓你丟下你所攜帶的物品.
HELP
    );

  return 1;
}
 
