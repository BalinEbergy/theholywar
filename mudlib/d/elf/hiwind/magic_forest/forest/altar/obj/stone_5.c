// stone_5.c
// written by hiwind

inherit ITEM;

void create()
{
        set_name("精靈族人的石雕", ({ "statue" }));
        set("long", @LONG
這是一個精靈族人的石雕.刻劃得十分精細,栩栩如生.只見她
穿著傳說中有著強大魔力的法袍,手拿封印之杖,她的神情是安
詳的,帶著神密的微笑,似乎....
LONG
                );
        set("unsac", 1);
        set("no_get", 1);
        set("weight", 30000);
}
