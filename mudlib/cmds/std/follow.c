// follow.c
// written by Hudson

int main( object me, int argc, string *argv)
{
    object obj;

    if( argc== 1)
        if( me->get_follow()== me)
            write("你已經沒有跟隨什麼人了~\n");
        else me->follow();
    else if( !obj= find( argv[1], environment( me)))
        write("這裡沒有這個人喔...\n");
    else if( !obj->is_character())
        write("你不能跟隨一個物品.\n");
    else if( obj== me)
        write("你打算跟隨自己嗎?\n");
    else me->follow( obj);

    return 1;
}

int help( object me)
{
    write( @HELP
指令格式: follow [人物]

讓你跟隨一個人, 或者取消跟隨.
HELP
        );

    return 1;
}
