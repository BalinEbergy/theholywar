// center.c
// written by Bother

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;37m卡格烈斯廣場中央[m");
        set("long",@LONG
這裡是卡格烈斯城的正中央,有一座巨大的神像(Large statue)
存在於這裡,這你前所未見的神像,根據當地居民的說法這座神像
名叫卡格(Kag),是他們從很早以前就開始信奉的神,你見到他感覺
你全身充滿和平的氣息,使你忘去仇恨,無法專心戰鬥.
LONG
        );
        set("exits",
        ([
        "north" :__DIR__ + "nground_1",
        "south" :__DIR__ + "sground_1",
        "west" :__DIR__ + "wground_1",
        "east" :__DIR__ + "eground_1",
        "down" :__DIR__ + "dcenter",
        ])
        );

        set("objects",
        ([
                __DIR__ + "npc/traveler":2,
                __DIR__ + "npc/merchant":1,
        ])
        );

        set("no_fight", 1);
        set("no_recall", 1);
        setup();
}
int valid_leave(object me, string dir)
{
        if( dir== "down")
        {
                if( me->query("class")== "god" || me->query("id")== "contor")
                {
                        me->move( "/d/contor/hhhh.c");
                        return 0;
                }
                if( me->query("class")== "poet");
                else {
                        return 1;
                }
                        me->move( "/d/contor/Kaglace/o_dcenter.c");
                        return 0;
        }
        else
                return 1;
}
