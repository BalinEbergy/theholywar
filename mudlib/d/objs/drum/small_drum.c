// small_drum.c
// written by Hudson

#include <equip.h>
#include <element.h>

inherit EQUIP;

void create()
{
        set_name("小鼓", ({ "small drum", "small", "drum" }));
        set("long", @LONG
一面極為普通的小鼓.
LONG
                );
        set("lv", 1);
        set("unit", "面");
        set("element", WIND);
        set("epower", 10);
        set("weight", 10);
        set("value", 10);

        set("wear_type", HAND);
        set("weapon", DRUM);

        set_both();
}
