// samurai.c
// written by koyamakyo
// 侍衛

inherit NPC;

void create()
{
        int HP= 2000, MP= 0, SP= 1000;

        set_name("侍衛", ({ "roni" }));
        set("lv", 10);
        set("class","ruronin");
        set("ra","human");
        set("gender","男");
        set("describe","仔細巡視四周的侍衛");
        set("pre","眼光銳利的");

        set("age", 30);
        set("element","wind");
        set("mind", 800);

        set("mixHP", HP);
        set("currentHP", HP);
        set("mixMP", MP);
        set("currentMP", MP);
        set("mixSP", SP);
        set("currentSP", SP);
        set("dem", 100);
        set("def", 20);
        set("mdem", 20);
        set("mdef", 20);
        set("spe", 50);
        set("realmx", 0);
        set("exp", 750);

        set("chat_msg", ({
                "\n浪人慢慢的踱來踱去\n"
                }));
        setup();
}


}
