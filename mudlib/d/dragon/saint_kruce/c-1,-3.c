//c-1,-3.c
//written by zz

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;37m騎士隊總部營區[m");
        set("long",@LONG
你來到了騎士隊總部營區內.皇家騎士隊中,所有騎士都是從這
個總部訓練出來的,這裡的訓練非常的嚴格.你看見四周都是正
在接受訓練的見習騎士,看他們的動作每一個都是有模有樣的.
北邊是總部的休息室
LONG
        );
        set("exits", ([
                "north" : __DIR__ + "c-1,-2.c" ,
                "south" : __DIR__ + "c-1,-4.c" ,
                "east" : __DIR__ + "c0,-3.c" ,
                "west" : __DIR__ + "c-2,-3.c" ,
                ]));
        setup();
}
