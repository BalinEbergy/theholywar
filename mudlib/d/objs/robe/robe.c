// robe.c
// written by hiwind

#include <equip.h>
#include <element.h>

inherit EQUIP;

void create()
{
    set_name("[33m�b��[m", ({ "robe" }));
    set("long", @LONG
�@��A�Ҭ諸�b��
LONG
        );
    set("lv", 10);
    set("unit", "��");
    set("element", COSMOS);
    set("value", 700);

    set("wear_type", BODY);
    set("def_eff",100);
    set("defand_eff", 300);
    set("mdem_eff", 300);
    set("eff", ({ "def_eff", "defand_eff", "mdem_eff" }));

    set_weight( 100);
}
