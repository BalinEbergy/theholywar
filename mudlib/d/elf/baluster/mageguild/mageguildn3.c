// mageguildn3.c
// written by Hudson

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[36m魔法師公會會所[m");
        set("long",@LONG
這兒是魔法師公會北方,你面前的牆壁閃著奇異的亮光,你用
手摸了摸...唉呀!你被電到了,原來這是一面帶有電的牆壁?
LONG
        );

        set("exits", ([
                "south" : __DIR__ + "mageguildm2",
                "west"  : __DIR__ + "mageguildn2",
                ]));
}
