// quit.c

int main( object me, int argc, string *argv)
{
    if( me->query("lv")> 1)
        me->save();

    message_vision("$N離開了遊戲.\n", me);

    destruct( me);

    return 1;
}
int help()
{
    write( @HELP
指令格式: quit

離開遊戲
HELP
    );

    return 1;
}

