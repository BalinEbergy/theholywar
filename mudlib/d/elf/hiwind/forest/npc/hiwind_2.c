// hiwind_2.c
// written by hiwind

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
    int HP= 50000, MP= 25000 ,SP= 10000;

    set_name("[1;33m��[m[1;37m��ù�t��[m", ({ "hiwind" }));
    set("lv", 60);
    set("class",WARRIOR);
    set("race",HUMAN);
    set("describe","�n�ߪ��H.");
    set("age", 16);
    set("element", WIND);
    set("goodat", EARTH);
    set("weakat", THUNDER);
    set("mind", 750);
    set("gender", 1);

    set("maxHP", HP);
    set("currentHP", HP);
    set("maxMP", MP);
    set("currentMP", MP);
    set("maxSP", SP);
    set("currentSP", SP);
    set("dem", 10000);
    set("def", 15000);
    set("spirit", 10);
    set("spe", 1000);
    set("exp", 0);

    set("chat_chance", 10);
    set("chat_msg", ({
        "ù�t�ѥͮ�a��۶˲��ֲ֪����@��.\n"
        }));
    setup();
}
void init()
{
    ::init();
    if( !previous_object()
        || !userp(previous_object()))
        return;
    call_out("action", 1, previous_object());
}

void action( object obj)
{
    if( !obj || !present(obj) || this_object()->is_fighting()) return;
    if( obj->is_fighting()) kill_ob(obj);
    command("say ���n�����A�e�i�F,���M�U����L�@��.");
    setup();
}
