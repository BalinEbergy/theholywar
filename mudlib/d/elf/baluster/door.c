// door.c
// written by Hudson

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;32m巴路斯特城門口[m");
        set("long",@LONG
經過了一段長途的跋涉,你終於來到傳說中的精靈與魔法師之
城－巴路斯特,光站在城門口,你就感覺到陣陣的魔力向你滲
透而來,面前豎著一面高大的告示牌.
LONG
        );
        set("exits",
        ([
                "north" : __DIR__ + "sroad9",
                "south" : "/d/elf/thyeis/kaigic12",
        ])
        );
        set("objects", ([
                __DIR__ + "npc/magican": 2,
                OB_SIGN + "baluster": 1,
                ]));

        setup();
}
