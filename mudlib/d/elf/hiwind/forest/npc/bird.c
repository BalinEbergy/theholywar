// bird.c
// written by hiwind

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
    int HP= 1800, MP= 500,SP= 2000;

    set_name("在天空飛的大鳥", ({ "bird" }));
    set("lv", 25);
    set("race", ANIMAL);
    set("describe"," 大鳥.");
    set("age", 20);
    set("element", WIND);
    set("mind", 0);
    set("fly", 1);

    set("maxHP", HP);
    set("currentHP", HP);
    set("maxMP", MP);
    set("currentMP", MP);
    set("maxSP", SP);
    set("currentSP", SP);
    set("strength", 1300);
    set("armor", 450);
    set("magic", 300);
    set("spi", 2);
    set("speed", 1800);
    set("exp", 1250);
    setup();
    set("chat_chance", 10);
    set("chat_msg", ({
        (: random_move :)}));

}


