// wroad5.c
// written by Hudson

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;32m�ڸ����S��D[m");
        set("long",@LONG
�F��O�t�e�Ͱ���,�M���������e���q�A���Ǭy�L,���G�~��
�F�A�ȳ~���h��;���W��B�i������a,��W�����]�s�s��
�媺�s��,�A���Tı�o�o�઺�T�O�ӥ@�~�緽.
LONG
        );

        set("exits",
        ([
        "south" : __DIR__ + "wroad8",
        "east"  : __DIR__ + "wroad4",
        ])
        );

        set("objects", ([
                __DIR__ + "npc/lightning_elf":1,
                __DIR__ + "npc/water_elf":1,
                __DIR__ + "npc/wind_elf":1,
                ]));

        setup();
}
