// slash void
// written by Mondale

#include <skill.h>

string namel( int i);
int select_skill( object me, object obj);
int first( object me, object obj);
int second( object me, object obj);
int third( object me, object obj);
int forth( object me, object obj);
int fifth( object me, object obj);

int main( object me, object obj)
{
        string w_name= me->query_temp( me->query_temp("sla"))->name();
        int k,e,damage, dam_all = me->query("dem") + me->query("dem_eff");
        int def_all = obj->query("def") + obj->query("def_eff");
        if (!me->query("svoidt"))
        {
        me->set("svoid", 0);
        me->set("svoidt", 1);
        } 
        else
        {
        me->add("svoidt",1);
        if(me->query("svoidt") == 101) me->set("svoid", 1);
        if(me->query("svoidt") == 251) me->set("svoid", 2);
        if(me->query("svoidt") == 500) me->set("svoid", 3);
        if(me->query("svoidt") == 800) ;
        }
        damage = select_skill(me,obj);
        k = me->query("svoid");
        if (me->query_temp("svoids") >0) k = me->query_temp("svoids");
        return  COMBAT_D->skill_msg( me, obj, namel( k), w_name, damage);
}

string name( int select)
{
        switch( select)
        {
                case 1: return "虛空劍法";
                case 2: return "slash void";
                case 3: return "void";
        }
}

string namel( int select)
{
        switch( select)
        {
                case 0: return "靜嵐之刃-牙";
                case 1: return "混沌之刃-空";
                case 2: return "破碎之刃-裁";
                case 3: return "虛無之刃-塵";

        }
}

int lv() { return 90; }
int attackful()
{
        return 1;
}
int type() { return SLASH; }
int usesp() { return 70; }
int usemp() { return 0; }
int successful() { return 90; }
int delay() { return 3; }
int all() { return 0; }
int check( object me)
{
        return 1;
}

int select_skill(object me,object obj)
{
int temp;
int a;
a = me->query("svoid");
if (me->query_temp("svoids") > 0) a = me->query_temp("svoids");
        switch(a)
        {
        case 0:
        temp = first(me,obj);
        break;
        case 1:
        temp = second(me,obj);
        break;
        case 2:
        temp = third(me,obj);
        break;
        case 3:
        temp = forth(me,obj);
        break;
        }
if (temp < -1) temp = 0;
obj->add("currentHP",-temp);
return temp;
}

int first( object me, object obj)
{
        string w_name= me->query_temp( me->query_temp("sla"))->name();
        int damage, dam_all = me->query("dem") + me->query("dem_eff");
        int def_all = obj->query("def") + obj->query("def_eff");
        tell_object(me,
        "\n你凝視著手中的長劍,開始向自己心中最深處探索....\n" +
          "漸漸的,你的心完全靜了下來,就像一片毫無波瀾的[1;36m清水[0m...");
        tell_object(obj,"\n你被"+me->name()+"冷冷的目光掃過,不禁起了一身雞皮疙瘩...");
        message_vision("\n$N發出一聲長嘯：「虛空劍法第一式 『[1;37m靜嵐之刃-牙[0m』!!」\n",me);
        tell_object(me,
        "你反持長劍,直接衝到"+obj->name()+"面前,身形也隨著開始旋轉.\n" +
        "手中長劍夾著一道道[1;37;41m利風[0m與[1;37;44m劍氣[0m向"+obj->name()+"襲去.\n");
        tell_object(obj,
        "你看到"+me->name()+"倒持長劍向你衝來,當你想到要招架的時候\n"+
        "卻只覺得一陣勁風撲面,你已經被[1;37;44m劍光[0m所籠罩,只能感覺到\n" +
        "完全的[36m無力感[0m與全身被劍氣撕裂的[1;7;37;40m痛楚[0m.\n");
        damage = (me->skill_level(SLASH)*200) + dam_all + (me->query("str")*me->query("spe")/5) - def_all + random(10000);
        return damage;
}

int second( object me, object obj)
{
        string w_name= me->query_temp( me->query_temp("sla"))->name();
        int damage, dam_all = me->query("dem") + me->query("dem_eff");
        int def_all = obj->query("def") + obj->query("def_eff");
        damage = (me->skill_level(SLASH)*300) + dam_all + (me->query("str")*me->query("spe")/5) - def_all + random(10000);
        tell_object(me,"\n你緊緊握住手中的長劍,將一切影響心的想法沉澱下來.");
        message_vision("\n$N向空中高高躍起,並大喝一聲：虛空劍法第二式 「[37;1m混沌之刃-空[0m」\n"+
        "$N一劍劈落在$n面前,揚起一陣塵埃.\n",me,obj);
        tell_object(me,
        "雖然這一劍並沒有直接命中"+obj->name()+",但是劍氣卻凝聚\n"        
        "起來,形成一個力場,將"+obj->name()+"[31;1m吞噬[0m進去...\n");
        tell_object(obj,
        "雖然這一劍並沒有砍中你,但是[1;36m劍氣[0m卻將你包裹住,\n" +
        "使得你無法動彈,只能乖乖承受這[36;1m劍氣[0m所帶來的[1;7;37;40m劇痛[0m..\n");
        return damage;
}

int third( object me, object obj)
{
        string w_name= me->query_temp( me->query_temp("sla"))->name();
        int damage, dam_all = me->query("dem") + me->query("dem_eff");
        int def_all = obj->query("def") + obj->query("def_eff");
        damage = (me->skill_level(SLASH)*450) + dam_all + (me->query("str")*me->query("spe")/5) - def_all + random(10000);
        tell_object(me,"\n你手中握著長劍,心中已沒有任何疑惑,只剩下克敵制勝的意念.");
        tell_object(obj,"\n你看到"+me->name()+"眼中精光大盛,一陣強烈的壓迫感向你襲來.");
        message_vision("\n$N大喝一聲：「虛空劍法第三式 『[37;1m破碎之刃-裁[0m』!!」\n",me);
        tell_object(me,
        "你將強烈的真力運至劍上,將劍向"+obj->name()+"擲去,只見\n" +
        "長劍化作一條[33;1m光柱[0m擊向" + obj->name() +".\n");
        tell_object(obj,
        me->name()+"運起元功,將手中長劍化作一道猛烈的劍氣向你擲來.\n"+
        "你猝不及防,只能眼睜睜的看著這道[1;36m凜冽[0m的[1;33;45m劍氣[0m將自己貫穿.\n");
        return damage;
}

int forth( object me, object obj)
{
        string w_name= me->query_temp( me->query_temp("sla"))->name();
        int damage, dam_all = me->query("dem") + me->query("dem_eff");
        int def_all = obj->query("def") + obj->query("def_eff");
        damage = (me->skill_level(SLASH)*600) + dam_all + (me->query("str")*me->query("spe")/5) - def_all + random(10000);
        tell_object(me,
        "\n你手中握住長劍,閉上眼睛,只聽到自己心中傳出聲音.\n"+
        "「還等什麼？上吧...[37;1m已經沒有能夠阻止我的東西了[0m...」\n" +
        "你睜開眼睛,「是的...來做個了結吧」,你將長劍指向"+obj->name()+".\n");
        tell_object(obj,
        "\n"+me->name()+"用手中的長劍指著你.眼中透出一股股的殺氣.\n");
        message_vision(
        "\n   「來吧...."+obj->name()+"....我們來做個了結吧...」"+
        "\n   「虛空劍 終極破壞奧義 [37;1m心之劍-『虛無之刃-塵』[0m!!」\n"+
        "\n$N在喊出這句話的同時,手中的長劍已經攻向$n.\n",me,obj);
        tell_object(me,
        "你將長劍向"+obj->name()+"刺去,突然長劍發出耀眼的[33;1m金光[0m,從劍身\n" +
        "激射出一道道的[1;33;44m劍光[0m,將阻擋在前面的東西都化作塵埃.\n");
        tell_object(obj,"你只覺得一陣光芒閃過,一道道的[1;33;44m劍光[0m就已經穿身而過.\n");
        return damage;
}

int fifth( object me, object obj){}

