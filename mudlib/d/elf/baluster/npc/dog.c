// dog.c
// written by Hudson

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
        int HP= 500, MP= 0, SP= 1000;

        set_name("狗", ({ "dog"}));
        set("lv", 5);
        set("race", ANIMAL);
        set("gender", random(1));
        set("describe","一隻狗.");
        set("pre","四處晃動的");
        set("age", 3);
        set("element",EARTH);
        set("mind", 10);

        set("maxHP", HP);
        set("currentHP", HP);
        set("maxMP", MP);
        set("currentMP", MP);
        set("maxSP", SP);
        set("currentSP", SP);
        set("strength", 200);
        set("armor", 110);
        set("magic", 0);
        set("spi", 1);
        set("speed", 150);
        set("exp", 300);

        set("chat_chance", 12);
        set("chat_msg", ({
                (: random_move :)
                }));

        setup();
}
