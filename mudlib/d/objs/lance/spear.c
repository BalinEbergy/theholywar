// spear.c
// written by zz

#include <equip.h>
#include <element.h>

inherit EQUIP;

void create()
{
        set_name("長矛", ({ "spear" }));
        set("long", @LONG
一把堅硬耐用的長矛

LONG
                );
        set("lv", 10);
        set("unit", "把");
        set("element", EARTH);
        set("epower", 10);
        set("weight", 50);
        set("value", 30);

        set("wear_type", HAND);
        set("weapon", LANCE);
        set("dem_eff", 25);
        set("spe_eff", 5);
        set("eff", ({ "dem_eff", "spe_eff" }));
}
