// element.h

#ifndef __ELEMENT__
#define __ELEMENT__

#define HOLY            0
#define LIGHT           1
#define COSMOS          2
#define WIND            3
#define THUNDER         4
#define FIRE            5
#define WATER           6
#define EARTH           7
#define DARK            8
#define EVIL            9

#endif
