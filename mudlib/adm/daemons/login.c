// login.c

#include <class.h>
#include <command.h>
#include <element.h>
#include <login.h>
#include <race.h>

#define BANTIME 3

int check_legal_id( string id);
int check_legal_name( string name);
void enter_world( object ob);
object find_body( string name);
void make_body( object ob);

void create()
{
    seteuid( getuid());
}

void login( object ob)
{
    object *user;
    int i, ppl_cnt, login_cnt;

    printf( read_file( DIR_HELP+ "login/login_1"));

    user= users();

    ppl_cnt= 0;
    login_cnt= 0;
    for( i= 0; i< sizeof( user); i++)
        if( !environment( user[i])) login_cnt++;
        else ppl_cnt++;

    printf("%s�ثe�� [1;33m%d[m �Ӫ��a�b�u�W, �� [33m%d[m ��ϥΪ̹��ճs�u��.\n",
        CMD_UPTIME->time_des(), ppl_cnt, login_cnt);
    printf("�п�J ID: ");

    input_to("get_id", ob, 0);
}

private void get_id( string arg, object ob, int times)
{
    if( times> BANTIME)
    {
        printf("�z�w�g�չL�Ӧh���F��, �U���A�ӧa!\n");
        destruct( ob);
        return;
    }

    arg= lower_case( arg);
    if( !check_legal_id( arg))
    {
        printf("�п�J ID: ");
        input_to("get_id", ob, times++);
        return;
    }

    ob->set("id", arg);

    if( file_size( ob->query_save_file()+ __SAVE_EXTENSION__)>= 0)
    {
        if( ob->restore())
        {
            printf("%s %s�z�n, �п�J�K�X: ",
                ob->query("name"), ob->query("gender")? "����": "�p�j");
            input_to("get_passwd", 1, ob);
            return;
        }
        printf("�藍�_, �z���H���i��X�{���D, �гq���Ův�B�z.\n");
        destruct( ob);
        return;
    }

    printf("�ϥ� %s �o�ӦW�r�N�|�гy�@�ӷs���H��, �z�T�w��(y/[n])? ", ob->query("id"));
    input_to("confirm_id", ob);
}

private void get_passwd( string pass, object ob)
{
    string my_pass;
    object user;

    printf("\n");
    my_pass= ob->query("password");
    if( crypt( pass, my_pass)!= my_pass)
    {
        printf("�K�¿��~!\n");
        destruct( ob);
        return;
    }

    // Here will be to check if the character is playing
    user= find_body( ob->query("id"));
    if( user)
    {
        if( user->query_temp("netdead"))
        {
            exec( user, ob);
            destruct( ob);
            user->reconnect();
            return;
        }
        printf("�z�n�N�t�@�ӳs�u�����ۦP�H�����X�h, ���ӥN����? ([y]/n)");
        input_to("confirm_relogin", ob, user);
        return;
    }

    write_file( DIR_LOG+ "USAGE", sprintf("�ϥΪ� %s �q %s �n�J. %s\n",
        ob->query("id"), query_ip_name( ob), ctime( time())));
    enter_world( ob);
}

private void confirm_relogin( string yn, object ob, object user)
{
    object temp= new( O_USER);

    if( yn== "" || yn[0]!= 'n' && yn[0]!= 'N')
    {
        tell_object( user, "���H�q�O�B ("+ query_ip_number( ob)+ ") �s�u���N�A���H��.\n");
        write_file( DIR_LOG+ "USAGE", sprintf("�ϥΪ� %s �� %s �Ҩ��N. %s\n",
            user->query("id"), query_ip_name( ob), ctime( time())));
        exec( temp, user);
        exec( user, ob);
        destruct( ob);
        destruct( temp);
    } else {
        printf("��, �w��U���A��~\n");
        destruct( ob);
    }
}

private void confirm_id( string yn, object ob)
{
    if( yn[0]!= 'y' && yn[0]!= 'Y')
    {
        printf("�п�J ID: ");
        input_to("get_id", ob, 0);
        return;
    }

    printf("�г]�w�K�X: ");
    input_to("new_passwd", 1, ob);
}

private void new_passwd( string pass, object ob)
{
    printf("\n");
    if( strlen( pass)< 5)
    {
        printf("�K�X���צܤ֭n���Ӧr��, �Э��s��J�K�X: ");
        input_to("new_passwd", 1, ob);
        return;
    }

    ob->set("password", crypt( pass, 0));
    printf("�ЦA��J�@���K�X: ");
    input_to("confirm_passwd", 1, ob);
}

private void confirm_passwd( string pass, object ob)
{
    string old_pass;

    printf("\n");
    old_pass= ob->query("password");
    if( crypt( pass, old_pass)!= old_pass)
    {
        printf("�z���K�X�⦸�ä��@��, �ЦA���]�@���K�X: ");
        input_to("new_passwd", 1, ob);
        return;
    }

    printf("�п�J����m�W: ");
    input_to("get_name", ob);
}

private void get_name( string name, object ob)
{
    if( !check_legal_name( name))
    {
        printf("�п�J����m�W: ");
        input_to("get_name", ob);
        return;
    }

    ob->set("name", name);

    printf("�аݱz���ʧO(�km/�kf)? ");
    input_to("get_gender", ob);
}

private void get_gender( string gen, object ob)
{
    int gender;

    if( gen[0]== 'm' || gen[0]== 'M')
        gender= 1;
    else if( gen[0]== 'f' || gen[0]== 'F')
        gender= 0;
    else {
        printf("�аݱz���ʧO(�km/�kf)? ");
        input_to("get_gender", ob);
        return;
    }

    ob->set("gender", gender);

    printf("\n");
    printf( read_file( DIR_HELP+ "race/intro"));
    printf("�п�ܺر�(h/e/d/o): ");
    input_to("get_race", ob);
}

private void get_race( string ra, object ob)
{
    int race;

    switch( ra[0])
    {
        case 'h': case 'H': race= HUMAN; break;
        case 'e': case 'E': race= ELF; break;
        case 'd': case 'D': race= DRAGON; break;
        case 'o': case 'O': race= OGRE; break;
        default:
            printf("�п�ܺر�(h/e/d/o): ");
            input_to("get_race", ob);
            return;
    }

    ob->set("race", race);

    printf("\n");
    printf( read_file( DIR_HELP+ "element/intro"));
    printf("�п���ݩ�(l/w/c/t/f/a/e/d): ");
    input_to("get_element", ob);
}

private void get_element( string ele, object ob)
{
    int element;

    switch( ele[0])
    {
        case 'l': case 'L': element= LIGHT; break;
        case 'w': case 'W': element= WIND; break;
        case 'c': case 'C': element= COSMOS; break;
        case 't': case 'T': element= THUNDER; break;
        case 'f': case 'F': element= FIRE; break;
        case 'a': case 'A': element= WATER; break;
        case 'e': case 'E': element= EARTH; break;
        case 'd': case 'D': element= DARK; break;
        default:
            printf("�п���ݩ�(l/w/c/t/f/a/e/d): ");
            input_to("get_element", ob);
            return;
    }

    ob->set("element", element);

    make_body( ob);

    enter_world( ob);
}

void make_body( object user)
{
    object ob;

    int race;
    int *race_point= ({
        8, 8, 13, 8, 13,
        13, 9, 8, 12, 8,
        10, 10, 10, 10, 10,
        12, 13, 9, 9, 7
        });
    int *race_stat= ({
        50, 200, 400, 100, 200, 200, 800, 50
        });

    race= user->query("race");
    race-= 2;

    user->set("lv", 1);
    user->set("class", COMMONER);
    user->set("title", "�@�륭��");
    user->set("describe", "�@�Ӵ��q�������Ѧʩm");
    user->set("str", race_point[ race* 5]);
    user->set("con", race_point[ race* 5+ 1]);
    user->set("int", race_point[ race* 5+ 2]);
    user->set("spi", race_point[ race* 5+ 3]);
    user->set("dex", race_point[ race* 5+ 4]);

    user->set("maxHP", race_point[ race* 5+ 1]* 12+ 120);
    user->set("currentHP", user->query("maxHP"));
    user->set("maxMP", race_point[ race* 5+ 3]* 18+ 60);
    user->set("currentMP", user->query("maxMP"));
    user->set("maxSP", race_point[ race* 5+ 4]* 6+ 180);
    user->set("currentSP", user->query("maxSP"));

    user->set("strength", race_point[ race* 5]* 6+ 60);
    user->set("magic", race_point[ race* 5+ 2]* 9+ 30);
    user->set("armor", race_point[ race* 5+ 1]* 3+ 64);
    user->set("speed", race_point[ race* 5+ 4]* 6+ 60);

    user->set("maxHungry", race_stat[ race* 2]);
    user->set("currentHungry", race_stat[ race* 2]);
    user->set("maxThirsty", race_stat[ race* 2+ 1]);
    user->set("currentThirsty", race_stat[ race* 2+ 1]);

    user->set("lpoint", 4);
    user->set("addpoint", 3);
    user->set("money", 100);
    user->set("recallroom", race);

    switch( user->query("race"))
    {
        case OGRE:
            ob= new( OB_AXE+ "lv1axe"); break;
        case DRAGON:
            ob= new( OB_LANCE+ "lv1lance"); break;
        case ELF:
            ob= new( OB_SWORD+ "lv1sword"); break;
        case HUMAN:
            ob= new( OB_BLADE+ "lv1blade"); break;
    }
    ob->move( user);
}

void enter_world( object user)
{
    printf( read_file( DIR_HELP+ "BULLETIN")+ "�� �Ы��U return �~�� �� ");
    input_to("enter", user);
}

private void enter( string trash, object user)
{
    object ob;
    string startroom;

    user->setup();

    // Temporary -->
    if( !sizeof( all_inventory( user)))
    {
    switch( user->query("race"))
    {
        case OGRE:
            ob= new( OB_AXE+ "lv1axe"); break;
        case DRAGON:
            ob= new( OB_LANCE+ "lv1lance"); break;
        case ELF:
            ob= new( OB_SWORD+ "lv1sword"); break;
        case HUMAN:
            ob= new( OB_BLADE+ "lv1blade"); break;
    }
    ob->move( user);
    }

    user->initial_for_old();
    // <-- Temporary

    if( !stringp( startroom= user->query("startroom")))
        startroom= D_TRANS->transport_room( user->query("recallroom"));

    user->move( startroom);
}

int check_legal_id( string id)
{
    int i;

    if(( i= sizeof( id))< 3 || i> 13)
    {
        printf("�z�� ID ���O 3 �� 13 �ӭ^��r��.\n");
        return 0;
    } else {
        while( i--)
            if( id[i]> 'z' || id[i]< 'a')
            {
                printf("�z�� ID ���O 3 �� 13 �ӭ^��r��.\n");
                return 0;
            }
    }

    return 1;
}

int check_legal_name( string name)
{
    int i;

    if(( i= sizeof( name))< 4 || i> 12 || i% 2)
    {
        printf("�z���W�r���O 2 �� 6 �Ӥ���r.\n");
        return 0;
    } else {
/*        while( i--)
            if( i% 2 && ( name[i]< 128 || name[i]> 256))
            {
                printf("�z���W�r���O 2 �� 6 �Ӥ���r.\n");
                return 0;
            }*/
    }

    return 1;
}

object find_body( string name)
{
    int i;
    object ob, *body;

    if( objectp( ob= find_player( name)))
        return ob;

    body= children( O_USER);
    for( i= 0; i< sizeof( body); i++)
        if( clonep( body[i]) && getuid( body[i])== name )
            return body[i];

    return 0;
}
