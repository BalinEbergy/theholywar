// swground_1.c
// written by Bother

#include <room.h>


inherit ROOM;

void create()
{
        set("short","[1;37m卡格烈斯廣場[m");
        set("long",@LONG
這裡是卡格烈斯城的廣場,東北方向就是廣場的正中心,代表著
這個城市的象徵,廣闊的場所你可以在這兒嘻戲遊樂,它存在著
歡樂的氣氛,代表著這裡的風氣.
LONG
        );

        set("exits",
        ([
        "north" :__DIR__ + "wground_1",
        "east" :__DIR__ + "sground_1",
        ])
        );
}
