// suenus.c
// written by Hudson
// 蘇雅娜絲

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
    int HP= 287000, MP= 29000, SP= 3000;

    set_name("蘇雅娜絲", ({ "suenus" }));
    set("lv", 70);
    set("class",WIZARD);
    set("race",ELF);
    set("gender",0);
    set("describe","你一見到她就覺得一點也不討厭,有一種熟悉的感覺.");
    set("pre","共和國長老");

    set("age", 329);
    set("element",WATER);
    set("mind", 800);

    set("maxHP", HP);
    set("currentHP", HP);
    set("maxMP", MP);
    set("currentMP", MP);
    set("maxSP", SP);
    set("currentSP", SP);
    set("strength", 5340);
    set("armor", 30000);
    set("magic", 28000);
    set("spi", 23);
    set("speed", 1740);
    set("exp", 7000);

    set("skill_chance", 12);

    setup();
}

void init()
{
    if( !previous_object()
        || !userp(previous_object()))
        return;
    call_out("ki", 5, previous_object());
}

void ki( object obj)
{
    if( obj->is_fighting()) kill_ob( obj);
}
