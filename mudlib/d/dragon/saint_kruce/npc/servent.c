// servent.c
// written by zz

#include <class.h>
#include <element.h>
#include <race.h>

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
        int HP= 20000, MP= 0, SP= 100000;

        set_name("服務生", ({ "servent" }));
        set("lv", 30);
        set("race", HUMAN);
        set("gender",1);
        set("describe","一位身穿整齊服裝的服務生.");
        set("pre","飯店的");

        set("age", 27);
        set("element", EARTH);
        set("mind", 0);

        set("maxHP", HP);
        set("currentHP", HP);
        set("maxMP", MP);
        set("currentMP", MP);
        set("maxSP", SP);
        set("currentSP", SP);
        set("dem", 3000);
        set("def", 1000);
        set("mdem", 1000);
        set("mdef", 1000);
        set("spe", 100);
        set("realmx", 10);
        set("exp", 20000);

        set("trading", 201);
        set("buy_mind", "-1000~1000");

        set("selling", ({
                "/d/objs/food/hawaii_pizza",
                }));

        setup();
}
