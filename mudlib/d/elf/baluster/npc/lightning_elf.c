// lightning_elf.c
// written by Hudson

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
    int HP= 900, MP= 500, SP= 7000;

    set_name("雷小精靈", ({ "lightning elf" }));
    set("lv", 8);
    set("class", WARRIOR);
    set("race", ELF);
    set("gender", 1);
    set("describe","一個小精靈.");

    set("age", 20);
    set("element", THUNDER);
    set("mind", 133);

    set("maxHP", HP);
    set("currentHP", HP);
    set("maxMP", MP);
    set("currentMP", MP);
    set("maxSP", SP);
    set("currentSP", SP);
    set("strength", 300);
    set("armor", 150);
    set("spi", 5);
    set("speed", 140);
    set("exp", 800);

    set("chat_chance", 13);
    set("chat_msg", ({
        "雷精快樂的跑跳著\n",
        (: random_move :)
        }));

    setup();
}

void init()
{
    object obj;

    if( !( obj= previous_object()) || !userp( obj))
        return;

    call_out("ki", 10, obj);
}

void ki( object obj)
{
    if( obj->is_fighting()) kill_ob( obj);
}
