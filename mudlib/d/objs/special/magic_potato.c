// magic_potato.c
// written by Hudson

#include <element.h>

inherit FOOD;

void create()
{
    object obj;
    set_name("魔法馬鈴薯", ({ "magic potato", "magic", "potato" }));
    set("long", @LONG
用魔法造成的馬鈴薯.
LONG
        );
    set("unit", "顆");
    set("lv", 1);
    set("no_save", 1);

    set_amount( 2);

    set_weight( 20);
}

void disappear()
{
    message_vision("$N腐敗了.", this_object());
    destruct( this_object());
}
