// attack.c

int ghost;
int is_ghost() { return ghost; }

static object *enemies= ({ });

varargs int is_fighting( object ob)
{
    if( !ob) return sizeof( enemies);
    else return member_array( ob, enemies)!= -1;
}

object *query_enemies() { return enemies; }

void kill_ob( object ob)
{
    int i;
    object *group;

    set_heart_beat(1);

    this_object()->delete("rest");

    if( !ob ||
        ob== this_object() ||
        ob->is_ghost() ||
        member_array( ob, enemies)!= -1)
        return;

    enemies+= ({ ob });
    ob->kill_ob( this_object());
}

void clean_up_enemy()
{
    if( sizeof( enemies))
    {
        for( int i= 0; i< sizeof( enemies); i++)
            if( !objectp( enemies[i]) ||
                environment( enemies[i])!= environment() ||
                enemies[i]->is_ghost())
                enemies[i]= 0;
        enemies-= ({ 0 });
    }
}

void remove_enemy( object ob)
{
    enemies-= ({ ob });
}

void remove_all_enemy()
{
    int i;

    for( i= 0; i< sizeof( enemies); i++)
    {
        if( objectp( enemies[i]))
            enemies[i]->remove_enemy( this_object());
        enemies[i]= 0;
    }

    enemies= ({ });
}

void attack()
{
    clean_up_enemy();

    if( sizeof( enemies))
    {
        this_object()->combat_action();
        D_COMBAT->fight( this_object(), enemies[0]);
    }
}

object make_corpse()
{
    int i;
    object corpse, *inv;

    if( this_object()->query("no_corpse"))
        return 0;

    corpse= new( OB+ "corpse");
    corpse->set_name( this_object()->name()+
        "("+ this_object()->query("id")+ ")"+ "������", ({ "corpse" }));
    corpse->set("element", this_object()->query("element"));
    corpse->set("epower", this_object()->query("lv"));
    corpse->set_weight( this_object()->query_weight());
    corpse->move( environment());

    if( userp( this_object()))
    {
        corpse->set("playercor", this_object()->query("id"));
        this_object()->set_temp("corpse", corpse);
    }

    inv= all_inventory( this_object());
    i= sizeof( inv);
    while( i--)
    {
        if( inv[i]->equipped())
            inv[i]->unequip(1);
        inv[i]->move( corpse);
    }

    return corpse;
}

void die( object killer)
{
    int i, exp, lv;
    object env, *group;

    remove_all_enemy();
    this_object()->set("currentHP", 1);
    this_object()->set("currentMP", 1);
    this_object()->set("currentSP", 1);
    this_object()->set("invis", 1);

    if( is_ghost()) return;
    else ghost= 1;

    this_object()->death_msg( killer);
    message_vision("\n$N[1;5;31m�����F[m$n!!\n", killer, this_object());
    tell_object( this_object(),
        "\n[1;33m���|�a~~~~~~~~~~~~~~~~~~~[m\n"+
        "[1;31m�A~~~~~~~~~~~~~~~~~[m[1;31;5m��[m[1;31m~~~~~~~~�F~~~~~~~~~~~~[m\n");
    if( userp( this_object()))
        message("channel", sprintf(
            "\n[1;37m�z������!!!......."
            "\n�@�D�n����}�Ѫ�, [31m%s [1;37m"
            "�Q [33m%s [1;37m�b [m[36m%s [1;37m�F���F!!!!!!!!!!!!!![m\n\n",
            this_object()->name(), killer->name(), environment( killer)->query("short")), users());

    make_corpse();

    if( userp( this_object()))
    {
        exp= D_LEVEL->query_exp( this_object()->query("lv"))/ 25;
        this_object()->add("exp", -exp);
    } else exp= this_object()->query("exp");

    if( userp( killer) && killer!= this_object())
    {
        env= environment( killer);
        group= killer->get_leader()->get_group();

        i= sizeof( group);
        exp= ( exp* ( 0.8+ 0.2* i)+ i)/ i;
        while( i--)
            if( environment( group[i])== env)
                D_LEVEL->add_exp( group[i], this_object()->query("lv"), exp);
    }

    if( !userp( this_object()))
        destruct( this_object());
}

void revive()
{
    ghost= 0;
    this_object()->delete("invis");
}

void init()
{
}
