// final.c
// written by hiwind

inherit NPC;

void create()
{
        int HP= 1500000, MP= 8000 ,SP= 70000;

        set_name("毀天滅地", ({ "final" }));
        set("lv", 100);
        set("class","mage");
        set("ra","god");
        set("describe", "全身散發強大魔力的法師.");
        set("age", 150);
        set("mind", 1000);
        set("element", "hell");
        set("gender", "男");

        set("mixHP", HP);
        set("currentHP", HP);
        set("mixMP", MP);
        set("currentMP", MP);
        set("mixSP", SP);
        set("currentSP", SP);
        set("str", 10);
        set("dem", 5000);
        set("vir", 20);
        set("def", 15000);
        set("magic", 80);
        set("mdem", 75000);
        set("mdef", 25000);
        set("spirit", 30);
        set("speed", 10);
        set("spe", 1000);
        set("realmx", 150);
        set("exp", 0);

        set_skill_level( 3, 100);
        set_skill_level( 5, 100);
        set_skill_level( 8, 100);
        set("skill", ({ "8:angry sea", "5:limit", }));
        set("skill_chance", 75);
        setup();
}
void init()
{
        if( !previous_object()
                || !userp(previous_object()))
                return;
        call_out("action", 1, previous_object());
}

void action( object obj)
{
        if( !obj || !present(obj) || this_object()->is_fighting()) return;
        if( obj->is_fighting()) kill_ob(obj);

        if( !this_object()->query("follow"))
        {
                command("follow dream");
        } else
        {
                command("hihi 死吧!! 愚蠢的人....");
        }
}
