// fire_light.c
// written by Hudson

#include <skill.h>

inherit FIRE_MAGIC;

void create()
{
    set("name", "火光術");
    set("id", "fire light");
    set("lv", 1);
    set("attack", 1);
    set("usemp", 27);
    set("delay", 1);
    set("base_value/hp", 100);
    set("improve/hp", 10);
    set("cost", 40);
    set("exp", 42);

    setup();
}

string mes( object me)
{
    return "$N唸完咒語, 瞬間飛出一道火光!!";
}
