// square-e.c
// written by koyamakyo

#include <room.h>

inherit ROOM;

void create()
{
   set("short","城鎮廣場東側");
        set("long",@LONG
這裡是大城城鎮廣場的東側.
LONG
        );
       set("exits",
        ([
        "east" : __DIR__ + "main_road-e1",
        "north" : __DIR__ + "square-ne",
        "south" : __DIR__ + "square-se",
        "west" : __DIR__ + "center",
        ])
        );

        set("objects",  ([
                __DIR__ + "npc/roni":2,
                ]));
        setup();

}
