// akas.c
// written by Hudson
// 阿卡斯

#include <class.h>
#include <element.h>
#include <race.h>

inherit TEACHER;

void create()
{
    int HP= 120000, MP= 14000, SP= 1000;

    set_name("阿卡斯", ({ "akas" }));
    set("lv", 40);
    set("class",WIZARD);
    set("race",ELF);
    set("gender",1);
    set("describe","一位嚴肅的魔導士.");
    set("pre","魔導士");
    set("age", 132);
    set("element",WIND);
    set("mind", 100);

    set_favorite(({
        COMMONER, WIZARD
        }));
    set("lv_up", "好了, 等級提升了");
    set("no_enu_money", "景仰魔法的人啊! 你的錢不夠喔...");
    set("no_enu_lv", "你的等級還不夠!");
    set("top_lv", "恭喜了, 你的能力已經超越我了");
    set("teach_message", "學魔法嗎? 你想學哪項呢?");

    set("maxHP", HP);
    set("currentHP", HP);
    set("maxMP", MP);
    set("currentMP", MP);
    set("maxSP", SP);
    set("currentSP", SP);
    set("strength", 7000);
    set("armor", 10000);
    set("magic", 17000);
    set("spi", 25);
    set("speed", 1030);
    set("exp", 6000);

    add_skill_type(([
        "fire": 30, "thunder": 30
        ]));
    add_skill(({
        "fire light", "flame arrow", "lightning light",
        }));

    setup();
}

void init()
{
    if( !previous_object()
        || !userp(previous_object()))
        return;
    call_out("action", 0, previous_object());
}

void action( object obj)
{
    if( obj->query("lv")<= 5)
        command("tell "+ obj->query("id")+ " 想要學初級法術嗎? 找我就對了喔...");
}
