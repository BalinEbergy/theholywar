// call_win.c
// written by Hudson

#include <skill.h>

void create() { seteuid( getuid()); }

int main( object me, object obj)
{
        object wind;

        wind= new( OB_SPECIAL+ "wind");
        wind->set("lv", me->skill_level( SUMMON));
        wind->move( environment( me));
        wind->set("epower", wind->query("lv"));
        call_out( "disappear", me->skill_level( SUMMON)* 20, wind);

        tell_room( environment( me), "大地上捲起陣陣狂風!!\n");

        return 1;
}

void disappear( object wind)
{
        wind->disappear( wind);
}

string name( int select)
{
        switch( select)
        {
                case 1: return "風召喚術";
                case 2: return "winden";
                case 3: return "call_win";
        }
}

int type() { return SUMMON; }
int lv() { return 3; }
int attackful()
{
        return 0;
}

string mes( object me)
{
        return "口中唸著古老的咒語...";
}

int usesp() { return 0; }
int usemp() { return 10; }
int successful() { return 100; }
int delay() { return 1; }
int all() { return 0; }
string element() { return "wind"; }

int check( object me)
{
        int i= 1;
        object wind;

        while( wind= present("wind "+ i, environment( me)))
        {
                if( wind->query("epower"))
                {
                        tell_object( me, "\n已經有風存在了...\n");
                        return 0;
                }
                i++;
        }

        return 1;
}
