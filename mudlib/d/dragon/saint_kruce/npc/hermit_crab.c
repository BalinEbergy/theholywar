// hermit crab.c
// written by zz

#include <class.h>
#include <element.h>
#include <race.h>

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
        int HP= 2000, MP= 200, SP= 1000;

        set_name("寄居蟹", ({ "hermit crab" , "hermit" , "crab"}));
        set("lv", 7);
        set("race","animal");
        set("gender","動物");
        set("describe","寄居蟹.");
        set("pre","從殼鑽進鑽出的");
        set("age", 3);
        set("element",EARTH);
        set("mind", 10);

        set("maxHP", HP);
        set("currentHP", HP);
        set("maxMP", MP);
        set("currentMP", MP);
        set("maxSP", SP);
        set("currentSP", SP);
        set("dem", 53);
        set("def", 40);
        set("mdem", 17);
        set("mdef", 20);
        set("spe", 40);
        set("exp", 980);

        set("chat_chance", 10);
        set("chat_msg", ({
                (: random_move :)
                "寄居蟹迅速的鑽進牠的殼裡.\n",
                "寄居蟹從殼裡探出頭來.\n",
                }));
        setup();
}
