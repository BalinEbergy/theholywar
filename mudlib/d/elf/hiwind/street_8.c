// street_8.c
// written by hiwind

#include <room.h>


inherit ROOM;

void create()
{
        set("short","[1m�p�|[m");
        set("long",@LONG
�b�p�|�W,���۰ʪ�,�ڻ��u�ۦ������N�O�i�h[1;36m��[m�����P����
�L�F,�䤤�R���F�j�۵M����.���L�o�������G�����K,����
�D�|������Ƶo��,�٬O�p�ߤ@�I.
�A�w�g�v���b����[1;36m��[m�����P����F.
LONG
        );

        set("exits",
        ([
        "west" : __DIR__ + "street_7",
        "east" : __DIR__ + "street_9",
        ])
        );

        set("objects",
        ([
                __DIR__ + "forest/npc/bird":2,
        ])
        );

        set("no_fight", 0);
        setup();
}
