// blade_practice.c
// written by Hiwind

#include <skill.h>

int main( object me, object obj)
{
        int i;
        i= random( 10);
        if( i> 8)
        {
                message_vision("$N以極快的速度揮舞著刀,似乎有所領悟.\n", me);
                me->add_skill_exp( 1.1* me->query("spirit")* me->query("lv")/
                        me->skill_level( BLADE));
                me->add("currentSP", -3);
                return 1;
        } else if( i> 5)
        {
                message_vision("$N奮力的揮舞著刀,累得滿身是汗.\n", me);
                me->add_skill_exp( 0.9* me->query("spirit")* me->query("lv")/
                        me->skill_level( BLADE));
                me->add("currentSP", -1);
                return 1;
        } else if( i> 1)
        {
                message_vision("$N奮力的揮舞著刀,但沒有進展.\n", me);
                me->add_skill_exp( 0.7* me->query("spirit")* me->query("lv")/
                        me->skill_level( BLADE));
                return 1;
        } else
        {
                message_vision("$N沒力般揮舞著刀,似乎是太勞累了.\n", me);
                me->add_skill_exp( 0.5* me->query("spirit")* me->query("lv")/
                        me->skill_level( BLADE));
                me->add("currentHP", -2);
                return 1;
        }
}

string name( int select)
{
        switch( select)
        {
                case 1: return "揮刀練習";
                case 2: return "blade practice";
                case 3: return "blade_practice";
        }
}

int lv() { return 1; }
int attackful()
{
        return 0;
}

string mes( object me)
{
        return "舉起手中的長刀,迅速的揮舞起來.\n";
}

int type() { return BLADE; }
int usesp() { return 5; }
int usemp() { return 0; }
int successful() { return 100; }
int delay() { return 1; }
int all() { return 0; }
int check( object me)
{
        me->delete_temp("bla");
        return 1;

}
void temp(object me)
{
printf(".");
}
