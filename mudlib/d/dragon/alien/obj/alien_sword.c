// alien_sword.c
// written by Mondale

inherit EQUIP;

void create()
{
        set_name("長劍", ({ "sword" }));
        set("long", @LONG
一把普通的長劍.
LONG
                );
        set("lv", 1);
        set("unit", "把");
        set("element", "sky");
        set("epower", 0);
        set("weight", 100);
        set("value",5);

        set("wear_type", "hand");
        set("weapon", "sword");
        set("dem_eff", 400);
        set("eff", ({ "dem_eff" }));
}
