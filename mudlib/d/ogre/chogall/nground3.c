// nground3.c
// written by Hudson

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;31m卡格爾廣場邊緣[m");
        set("long",@LONG
這裡是卡格爾廣場,來往的巨魔身上散發著殘暴之氣,令你擔
心是否下一刻便會成為他們的食物.由此轉向東方,是卡格爾
的第二條幹線－薩霸多大道,而大道的遠處,則是武士公會總
部.
LONG
        );

        set("exits", ([
                "east"  : __DIR__ + "sabado1",
                "south" : __DIR__ + "nground2",
                ]));
}
