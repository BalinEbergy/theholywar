//test2.c
//written by zz

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;37mtest2[m");
        set("long",@LONG
test Lv21-30
LONG
        );
        set("exits", ([
                "north" : __DIR__ + "test1.c" ,
                "south" : __DIR__ + "test3.c" ,
                ]));
        set("objects",  ([
                __DIR__ + "npc/test/lv21" :1 ,
                __DIR__ + "npc/test/lv22" :1 ,
                __DIR__ + "npc/test/lv23" :1 ,
                __DIR__ + "npc/test/lv24" :1 ,
                __DIR__ + "npc/test/lv25" :1 ,
                __DIR__ + "npc/test/lv26" :1 ,
                __DIR__ + "npc/test/lv27" :1 ,
                __DIR__ + "npc/test/lv28" :1 ,
                __DIR__ + "npc/test/lv29" :1 ,
                __DIR__ + "npc/test/lv30" :1 ,
        ]));
        set("no_recall", 1);
        setup();
}
