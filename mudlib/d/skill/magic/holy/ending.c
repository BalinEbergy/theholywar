// ending.c
// written by hiwind

#include <skill.h>

int main( object me, object obj)
{
        int demage, i, mdem, spirit, mdef;
        object *group;
        
        group= ( me->query("leader"))->query("group");
        i= sizeof( group);

        mdef= obj->query("mdef")+ obj->query("mdef_eff");
        mdem= group[1]->query("mdem")+ group[1]->query("mdem_eff")
                + group[0]->query("mdem")+ group[0]->query("mdem_eff");

        demage= mdem* 10000;

        tell_object( obj, "�A��F��[1;33m��[1;36m��[m���O�q��?\n");

        obj->add("currentHP", -demage);

        return demage;
}

string name( int select)
{
        switch( select)
        {
                case 1: return "�Ѧa�׵�";
                case 2: return "ending";
                case 3: return "ending";
        }
}

int type() { return HOLY; }
int lv() { return 40; }
int attackful()
{
        return 1;
}

string mes( object me)
{
        return "����[1;37m�믫[m,�N������[1;31m�]�O[m�v���׻E,�Ӧb���Ǵ��o�X[1;32m�M[m\n[1;36m�G[m��[1;37m���~[m.\n";
}

int usesp() { return 0; }
int usemp() { return 100; }
int successful() { return 100; }
int delay() { return 1; }
int all() { return 0; }
string element() { return "holy"; }

int check( object me)
{
        int i;
        object *group;

        group= ( me->query("leader"))->query("group");
        i= sizeof( group);

        if( me->query("element")== "light")
        {
                me->set("list", 3);

                if( me->query("group"))
                {
                        if( i> 2)
                        {
                                write("�L�h���H�}�a�F[1;33m��[1;36m��[m��[1;37m�M[1;36m��[m.\n");
                                me->delete("list");
                                return 0;
                        } else if( i== 2)
                        {
                                while( i--)
                                {
                                        if( group[i]->query("list")== 10)
                                        {

                                        group[i]->delete("list");

                                        return 1;

                                        }
                                        else;
                                }
                                        write("�A�ݭn[1;35m�t��[m���O�q.\n");
                                        return 0;
                        } else
                        {
                                write("�t�W���H�L�k�⮩[1;33m��[1;36m��[m��[1;37m���q[m.\n");
                                me->delete("list");
                                return 0;
                        }
                } else
                {
                        write("�A�ä��O����.\n");
                        return 0;
                }
        } else if( me->query("element")== "dark")
        {
                me->set("list", 10);

                if( me->query("group"))
                {
                        if( i> 2)
                        {
                                write("�L�h���H�}�a�F[1;33m��[1;36m��[m��[1;37m�M[1;36m��[m.\n");
                                me->delete("list");
                                return 0;
                        } else if( i== 2)
                        {
                                while( i--)
                                {
                                        if( group[i]->query("list")== 3)
                                        {

                                        group[i]->delete("list");

                                        return 1;

                                        }
                                        else;
                                }
                                        write("�A�ݭn[1;33m����[m���O�q.\n");
                                        return 0;
                        } else
                        {
                                write("�t�W���H�L�k�⮩[1;33m��[1;36m��[m��[1;37m���q[m.\n");
                                me->delete("list");
                                return 0;
                        }
                } else
                {
                        write("�A�ä��O����.\n");
                        return 0;
                }
        } else
        {
                write("�A���O�Q[1;33m��[1;36m��[m�ҬD�諸[1;37m�H[m.\n");
                return 0;
        }
}
