// simul_efun.h

#ifndef __SIM_EFUN__
#define __SIM_EFUN__

#define SIM_ATOI        "/adm/system/simul_efun/atoi"
#define SIM_FILE        "/adm/system/simul_efun/file"
#define SIM_MESSAGE     "/adm/system/simul_efun/message"
#define SIM_OBJECT      "/adm/system/simul_efun/object"
#define SIM_SEARCH      "/adm/system/simul_efun/search"

#endif
