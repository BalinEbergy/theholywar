// butterfly.c
// written by Mondale
// 蝴蝶

inherit NPC;

void create()
{
        int HP= 100, MP= 0, SP= 100;

        set_name("蝴蝶", ({ "butterfly" }));
        set("lv", 1);
        set("class","warrior");
        set("ra","蝴蝶");
        set("gender","女");
        set("describe","正在花叢中穿梭的蝴蝶.");
        set("pre","翩翩飛舞的");

        set("age", 1);
        set("element","wind");
        set("mind", 0);

        set("mixHP", HP);
        set("currentHP", HP);
        set("mixMP", MP);
        set("currentMP", MP);
        set("mixSP", SP);
        set("currentSP", SP);
        set("dem", 30);
        set("def", 10);
        set("mdem", 30);
        set("mdef", 30);
        set("spe", 30);
        set("realmx", 1);
        set("exp", 100);

        set("chat_chance", 20);
        set("chat_msg", ({
                (: this_object(), "random_move" :),
                "\n蝴蝶優雅的飛來飛去\n"
                }));
        setup();
}

void init()
{
        ::init();
        if( !previous_object()
                || !userp(previous_object()))
                return;
       
}
