// forg.c
// written by Hudson

#include <element.h>

inherit ITEM;

void create()
{
    set_name("霧", ({ "fog" }));
    set("long", @LONG
一陣大霧.
LONG
        );
    set("lv", 1);
    set("element", WATER);
    set("epower", 100);

    set("unsac", 1);
    set("no_get", 1);
}

void disappear()
{
    tell_room( environment( this_object()), "濃濃的大霧消失了!\n");
    environment( this_object())->delete("low_reach");
    destruct( this_object());
}
