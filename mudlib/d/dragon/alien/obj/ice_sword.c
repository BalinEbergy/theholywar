// ice_sword.c
// written by Mondale

inherit EQUIP;

void create()
{
        set_name("[1;36;44m�B�S��v�C[m", ({ "ice sword", "ice", "sword" }));
        set("long", @LONG
�@��C�b���G�p[1;36m���[0m���_�C,�W�������x�۩_����[1;34m�ť�[0m.
LONG
                );
        set("lv", 100);
        set("unit", "��");
        set("element", "water");
        set("epower", 100);
        set("weight", 500);
        set("color", 3);

        set("wear_type", "hand");
        set("weapon", "sword");
        set("dem_eff", 10000);
        set("spe_eff", 300);
        set("def_eff", 5000);
        set("eff", ({ "dem_eff", "spe_eff", "def_eff" }));
}
