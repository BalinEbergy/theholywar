// npc.c
// From es2 definition

inherit CHARACTER;

void add_skill_type( mapping s)
{
    int i;
    string *sks;

    i= sizeof( sks= keys( s));
    while( i--)
        ::add_skill_type( sks[i], s[ sks[i]]);
}

int add_skill( string *skill)
{
    int i;

    i= sizeof( skill);
    while( i--)
        ::add_skill( skill[i]);

    return 1;
}

object carry_object( string file)
{
    object ob;

    seteuid( getuid());
    if( !objectp( ob= new( file)))
        return 0;
    ob->move( this_object());

    return ob;
}

int return_home( object home)
{
    return 1;
}

int chat()
{
    string *msg;
    int chance, rnd;

    if( !environment()) return 0;

    if( !chance= (int) query("chat_chance"))
        return 0;

    if( arrayp( msg= query("chat_msg")))
    {
        if( random( 100)< chance)
        {
            rnd= random( sizeof( msg));
            if( stringp( msg[rnd]))
                message_vision( msg[rnd], this_object());
            else if( functionp( msg[rnd]))
                return evaluate( msg[rnd]);
        }
        return 1;
    }
}

void combat_action()
{
    mapping act;
    mixed actions;
    int i, chance, rnd;

    if( mapp( act= query("combat_action")))
    {
        actions= keys( act);
        rnd= random( 100);
        i= sizeof( actions);
        chance= 0;
        while( i--)
            if( rnd< chance+= act[ actions[i]])
            {
                evaluate( actions[i]);
                break;
            }
    }
}

int random_move()
{
    int i;
    mapping exits;
    string *dirs;

    if( !mapp( exits= environment()->query("exits"))) return 0;
    dirs= keys( exits);

    command("go " + dirs[ random( sizeof( dirs))]);
}
