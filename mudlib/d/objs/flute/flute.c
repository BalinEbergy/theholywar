// flute.c
// written by Bother

#include <equip.h>
#include <element.h>

inherit EQUIP;

void create()
{
        set_name("竹笛", ({ "flute" }));
        set("long", @LONG
一枝青竹做成的笛子
LONG
                );
        set("lv", 1);
        set("unit", "枝");
        set("element", COSMOS);
        set("epower", 10);
        set("weight", 30);

        set("value", 10);
        set("wear_type", HAND);
        set("weapon", FLUTE);
        set("dem_eff", 20);
        set("eff", ({ "dem_eff" }));

        set_both();
}
