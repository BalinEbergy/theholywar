// rovecub.c
// written by Hudson
// 洛夫克貝

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
    int HP= 401245, MP= 33792, SP= 6000;

    set_name("洛夫克貝", ({ "rovecub" }));
    set("lv", 68);
    set("class", WIZARD);
    set("race", OGRE);
    set("gender", 1);
    set("describe", "一位穿著黑色短衫, 身型矮壯的年輕人, 粗獷的外觀讓你\n"
        "很難想像他居然會是公會的傳送使者.");
    set("pre","傳送使");

    set("age", 43);
    set("element", FIRE);
    set("mind", 192);

    set("maxHP", HP);
    set("currentHP", HP);
    set("maxMP", MP);
    set("currentMP", MP);
    set("maxSP", SP);
    set("currentSP", SP);
    set("dem", 13375);
    set("def", 19984);
    set("mdem", 17636);
    set("mdef", 16533);
    set("spe", 500);
    set("exp", 5800);

    set("chat_chance", 10);
    set("chat_msg", ({
        "洛夫克貝騷騷頭, 兩隻大眼瞪著你看.\n" }));

    set("trans", ({ "baluster" }));

    setup();
}

void init()
{
    if( !previous_object()
        || !userp(previous_object()))
        return;
    call_out("mess", 1);
}

void mess()
{
    command("say 真不好意思, 施工期間我會待在廣場裡, 請大家多包涵 ^^\"");
}
