// cat.c
// written by Mondale
// 小貓

inherit NPC;

void create()
{
        int HP= 600, MP= 0, SP= 100;

        set_name("小貓", ({ "cat" }));
        set("lv", 5);
        set("class","warrior");
        set("ra","cat");
        set("gender","女");
        set("describe","一隻令人疼愛的小貓.");
        set("pre","一隻可愛的");

        set("age", 1);
        set("element","earth");
        set("mind", 0);

        set("mixHP", HP);
        set("currentHP", HP);
        set("mixMP", MP);
        set("currentMP", MP);
        set("mixSP", SP);
        set("currentSP", SP);
        set("dem", 60);
        set("def", 60);
        set("mdem", 60);
        set("mdef", 60);
        set("spe", 30);
        set("realmx", 1);
        set("exp", 200);

        set("chat_chance", 20);
        set("chat_msg", ({
                (: this_object(), "random_move" :),
                "\n小貓喵喵的叫著\n"
                }));
        setup();
        setup();
}

void init()
{
        ::init();
        if( !previous_object()
                || !userp(previous_object()))
                return;
        
}

void action( object obj)
{
        setup();
}
