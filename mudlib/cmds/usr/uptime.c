// uptime.c

#include <mudlib.h>

string time_des()
{
    int t, d, h, m, s;
    string time;

    t = uptime();
    s = t % 60;       t /= 60;
    m = t % 60;       t /= 60;
    h = t % 24;       t /= 24;
    d = t;

    if( d) time= d+ " 天 ";
    else time = "";

    if( h) time+= h+ " 小時 ";
    if( m) time+= m+ " 分 ";
    time+= s+ " 秒 ";

    return ( "[1;33m"+ MUD_NAME+ "[m已經執行了 "+ time+
        "[版本 [36m"+ VERSION+ "[m]\n");
}

int main( object me, int argc, string *argv)
{
    write( time_des()+ "\n");

    return 1;
}

int help( object me)
{
  write( @HELP
指令格式: uptime
 
HELP
+ "這個指令告訴你"+ MUD_NAME+ "已經連續執行了多久.\n"
    );

  return 1;
}
