// laina.c
// written by zz

#include <class.h>
#include <element.h>
#include <race.h>

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
        int HP=60000, MP= 0, SP= 8000;

        set_name("萊娜", ({ "laina" }));
        set("lv", 40);
        set("class",MINSTREL);
        set("race",HUMAN);
        set("gender",0);
        set("describe","道具店老闆萊娜.");
        set("pre", "道具店老闆");

        set("age", 25);
        set("element", WATER);
        set("mind", 100);

        set("maxHP", HP);
        set("currentHP", HP);
        set("maxSP", SP);
        set("currentSP", SP);
        set("dem", 10000);
        set("def", 8000);
        set("mdem", 1200);
        set("mdef", 5000);
        set("spe", 990);
        set("exp", 2500);

        set("trading", 101);
        set("buy_mind", "-1000~1000");
        set("selling", ({
                OB_OTHERS + "water_skin.c",
                }));

        setup();
}
