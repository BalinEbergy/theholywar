//tree_4.c
//write by hiwind

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;37m�L�ϲ`�B[m");
        set("long",@LONG
���M�����B�������u��,����^�ɹ�b�޲�,�s���e����
�ȳ������F,�]�����B�����F����s�D���������M�F,��
�O�p�ߤ@�I�n�F.
LONG
        );

        set("exits",
        ([
        "north" : __DIR__ + "tree_6",
        "east" : __DIR__ + "tree_4",
        ])
        );

        set("objects",
        ([
                __DIR__ + "npc/rabbit":1,
                __DIR__ + "npc/shrewmouse":2,
        ])
        );

        set("no_fight", 0);
        setup();
}
