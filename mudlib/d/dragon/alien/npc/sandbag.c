// sandbag.c
// written by Mondale
// 沙包

inherit NPC;

void create()
{
        int HP= 100000000, MP= 0, SP= 0;

        set_name("沙包", ({ "sandbag" }));
        set("lv", 1);
        set("class","warrior");
        set("ra","沙包");
        set("gender","無");
        set("describe","就是給你打的...");
        set("pre","看起來很欠揍的");

        set("age", 1);
        set("element","sky");
        set("mind", 0);

        set("mixHP", HP);
        set("currentHP", HP);
        set("mixMP", MP);
        set("currentMP", MP);
        set("mixSP", SP);
        set("currentSP", SP);
        set("dem", 0);
        set("def", 0);
        set("mdem", 0);
        set("mdef", 0);
        set("spe", 0);
        set("realmx", 0);
        set("exp", 100000);

        set("chat_chance", 0);
        set("chat_msg", ({
                (: this_object(), "random_move" :),
                "\n\n"
                }));
        setup();
}

void init()
{
        ::init();
        if( !previous_object()
                || !userp(previous_object()))
                return;
}

void action( object obj)
{

        setup();
}
