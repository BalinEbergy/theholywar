// moon.c
// written by hiwind

#include <skill.h>

int main( object me, object obj)
{
        int demage, dem, def, me_spe, obj_spe, i, damage, hit, vir;
        string weapon_name;

        weapon_name=( me->query_temp( me->query_temp("bla")))->query("name");
        dem= me->query("dem")+ me->query("dem_eff");
        def= obj->query("def")+ obj->query("def_eff");
        me_spe= ( me->query("spe")+ me->query("spe_eff"))* 2.5;
        obj_spe= obj->query("spe")+ me->query("spe_eff");
        vir= me->query("vir");

        if( me->query("mind")> 500)
        {
        demage= ( 1+ ( me->query("blade_fast")/ 10) )* 0.7
                * ( dem* ( ( vir/ 20)+ 1)- ( def* obj_spe/ me_spe)) 
                -obj->query("defand_eff")+ random( 1000);
        } else if( me->query("mind")> 0)
        {
        demage= ( 1+ ( me->query("blade_fast")/ 10) )* 0.8
                * ( dem* ( ( vir/ 20)+ 1)- ( def* obj_spe/ me_spe))
                -obj->query("defand_eff")+ random( 1000);
        } else if( me->query("mind")> -500)
        {
        demage= ( 1+ ( me->query("blade_fast")/ 10) )* 0.9
                * ( dem* ( ( vir/ 15)+ 1)- ( def* obj_spe/ me_spe))
                -obj->query("defand_eff")+ random( 1000);
        } else
        {
        demage= ( 1+ ( me->query("blade_fast")/ 10) ) 
                * ( dem* ( ( vir/ 15)+ 1)- ( def* obj_spe/ me_spe))
                -obj->query("defand_eff")+ random( 1000);
        }

        if( demage> 1000)
        damage= demage+ random( me_spe);
        else
        damage= random( me_spe);

        message_vision("$N鼓起全身的力量,欲拼死一擊.\n那壯烈的氣勢,使得天空都暗了下來\n", me);

        obj->add("currentHP", - damage);
        return COMBAT_D->skill_msg( me, obj, "闇月逆天殺", weapon_name, damage);
}

string name( int select)
{
        switch( select)
        {
                case 1: return "闇月逆天殺";
                case 2: return "moon";
                case 3: return "moon";
        }
}

int lv() { return 85; }
int attackful()
{
        return 1;
}

string mes( object me)
{
        return "將全身的力量灌注在武器上.\n";
}

int type() { return BLADE; }
int usesp() { return 0; }
int usemp() { return 0; }
int successful() { return 100; }
int delay() { return 3; }
int all() { return 0; }
int check( object me)
{
        int me_sp, vir ;

        vir= me->query("vir");
        me_sp= 4000/ vir;
        if( me->query("currentSP")< me_sp)
        {
                write("\n你[5;33m沒力[m了....\n");
                return 0;
        } else
        {
                me->add("currentSP", -me_sp);
                if( me->query("gender")== "男")
                {
                message_vision("[1;36m$N說道: [1;33m我心中的憤怒,要你用血來補償啦!!.[m\n", me);
                return 1;
                } else
                {
                message_vision("[1;35m$N說道: [1;33m我心中的憤怒,要你用血來補償啦!!.\n", me);
                return 1;
                }
        }
}
void temp(object me)
{
printf(".");
}
