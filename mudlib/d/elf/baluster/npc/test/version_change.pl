#! /usr/bin/perl -w

# The Holy War version change

my $content;
my $temp;
my $filename;
my @files;

open( LIST, "list");
@files= ( <LIST>);

foreach $filename ( @files) {
    chomp( $filename);
    print $filename;
    if( open( FILE, $filename)) {
        $content= <FILE>;
        while( defined( $temp= <FILE>)) {
            if( $temp=~ /\/\//) {
                $content.= $temp;
            } else {
                $content.= "\n#include <class.h>\n#include <element.h>\n#include <race.h>\n";
                $content.= $temp;
                last;
            }
        }
        while( defined( $temp= <FILE>)) {
            $content.= $temp;
        }
        close( FILE);

        $content=~ s/"ra"/"\"race\""/eg;
        $content=~ s/"dragon"/DRAGON/eg;
        $content=~ s/"elf"/ELF/eg;
        $content=~ s/"human"/HUMAN/eg;
        $content=~ s/"ogre"/OGRE/eg;
        $content=~ s/"light"/LIGHT/eg;
        $content=~ s/"wind"/WIND/eg;
        $content=~ s/"air"/COSMOS/eg;
        $content=~ s/"lightning"/THUNDER/eg;
        $content=~ s/"fire"/FIRE/eg;
        $content=~ s/"water"/WATER/eg;
        $content=~ s/"earth"/EARTH/eg;
        $content=~ s/"dark"/DARK/eg;
        $content=~ s/"holy"/HOLY/eg;
        $content=~ s/"hell"/EVIL/eg;
        $content=~ s/"mage"/WIZARD/eg;
        $content=~ s/"warrior"/WARRIOR/eg;
        $content=~ s/"poet"/MINSTREL/eg;

        open( FILE, ">$filename");
        print FILE $content;
    }
}
