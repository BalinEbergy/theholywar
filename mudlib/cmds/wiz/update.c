// update.c

void create() { seteuid( getuid()); }

int main( object me, int argc, string *argv)
{
    mapping usual_objects= ([
        "login":        "/adm/daemons/login",
        "command":      "/adm/daemons/command",
        "combat":       "/adm/daemons/combat",
        "level":        "/adm/daemons/level",
        "skill":        "/adm/daemons/skill", 
        "attack":       "/features/char/attack",
        "group":        "/features/char/group",
        "char":         "/objs/char",
        "user":         "/objs/user",
        "update":       "/cmds/wiz/update",
        "cmds":         "/cmds/usr/cmds",
        "move":         "/cmds/wiz/move",
        "score":        "/cmds/usr/score",
        "t":            "/cmds/usr/transport",
        "test":         "/cmds/usr/test"
        ]);

    if( argc< 2)
        write("你要更新哪個物件呢?\n");
    else {
        if( usual_objects[ argv[1]])
            argv[1]= usual_objects[ argv[1]];
        argv[1]+= ".c";

        if( file_size( argv[1])< 0)
            write("沒有這個檔案\n");
        else {
            if( find_object( argv[1]))
                destruct( find_object( argv[1]));
            load_object( argv[1]);

            write("Update "+ argv[1]+ " successfully.\n");
        }
    }

    return 1;
}

int help( object me)
{
    write( @HELP
指令格式: update <檔案> | <物件>

login       登入物件    attack      攻擊屬性
command     指令物件    char        角色物件
combat      戰鬥物件    user        玩家物件
level       升級物件    group       團隊屬性
update      更新指令    skill       技能物件
cmds        查詢指令
move        移動指令
score       自身指令
test        測試指令

更新檔案(物件)
HELP
        );
    return 1;
}
