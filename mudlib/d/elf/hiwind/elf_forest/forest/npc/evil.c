// evil.c
// written by Hiwind

inherit NPC;

void create()
{
        int HP= 250000, MP= 500, SP= 3000;

        set_name("暗黑忍者", ({ "evil" }));
        set("lv", 70);
        set("class","ruronin");
        set("ra","human");
        set("gender","男");
        set("describe","一個殘忍的殺手.");
        set("age", 40);
        set("element","earth");
        set("mind", -500);

        set("teacher", ({
                23,
                }));
        set("favorite", ({
                "ruronin",
                }));
        set("teach_mind", "-1000 ~ -500");
        set("lv_up", "[1;36m暗黑忍者說道:[33m 好了, 等級提升了[m\n");
        set("teach_msg_pure", "[1;36m暗黑忍者說道:[33m 賤人....滾[m\n");
        set("no_enu_money", "[1;36m暗黑忍者說道:[33m 你想死嗎..[m");
        set("no_enu_lv", "[1;36m暗黑忍者說道:[33m 你這隻笨豬[m");
        set("top_lv", "[1;36m暗黑忍者道:[33m 唉!!人老了!!不行了!![m");
        set("teach_message", "[1;36m暗黑忍者說道:[33m 學刀法嗎?[m");

        set("mixHP", HP);
        set("currentHP", HP);
        set("mixMP", MP);
        set("currentMP", MP);
        set("mixSP", SP);
        set("currentSP", SP);
        set("dem", 15000);
        set("def", 5000);
        set("mdem", 2000);
        set("mdef", 1000);
        set("spe", 900);
        set("exp", 9000);

        set_skill_level( 23, 70);
        set("skill", ({ "23:aculeus", "23:gale" }));
        set("skill_chance", 100);

        setup();
}

void init()
{
        if( !previous_object()
                || !userp(previous_object()))
                return;
        call_out("ki", 10, previous_object());
}

void ki( object obj)
{
        if( obj->is_fighting()) kill_ob(obj);
}
