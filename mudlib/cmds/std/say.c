// say.c

int main( object me, int argc, string *argv)
{
    int i;
    string str;

    if( argc== 1) {
        write("你想說什麼呢?\n");
        return 1;
    }

    if( me->query("gender"))
        str= "[1;36m";
    else str= "[1;35m";

    str+= "$N說道:[33m";

    for( i= 1; i< argc; i++)
        str+= " "+ argv[i];
    str+= "[m\n";

    message_vision( str, me);

    return 1;
}

int help()
{
    write( @HELP
指令格式: say <句子>

對整個房間的人說話
HELP
    );

    return 1;
}
