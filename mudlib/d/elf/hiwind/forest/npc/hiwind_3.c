// hiwind_3.c
// written by hiwind

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
    int HP= 100000, MP= 30000 ,SP= 50000;

    set_name("[1;33m�g��[m[1;37m��ù�t��[m", ({ "hiwind" }));
    set("lv", 100);
    set("class",WARRIOR);
    set("race",HUMAN);
    set("describe","�ƨg���H.");
    set("age", 16);
    set("mind", -500);
    set("gender", 1);

    set("maxHP", HP);
    set("currentHP", HP);
    set("maxMP", MP);
    set("currentMP", MP);
    set("maxSP", SP);
    set("currentSP", SP);
    set("dem", 32000);
    set("def", 0);
    set("spirit", 10);
    set("spe", 1500);
    set("exp", 0);

    set("chat_chance", 100);
    set("chat_msg", ({
        "ù�t�ѩ��㪺�ݵۧA.\n"
        }));
    setup();
}
void init()
{
    if( !previous_object()
        || !userp(previous_object()))
        return;
    call_out("action", 1, previous_object());
    kill_ob( previous_object());
}

void action( object obj)
{
    if( !obj || !present(obj) || this_object()->is_fighting()) return;
    command("say ��ť��!!!�����ީM���@�����o�٤����G��??.");
    setup();
}
