// square-s.c
// written by koyamakyo

#include <room.h>

inherit ROOM;

void create()
{
   set("short","城鎮廣場南側");
        set("long",@LONG
這裡是大城城鎮廣場的南側.
LONG
        );
       set("exits",
        ([
        "east" : __DIR__ + "square-se",
        "north" : __DIR__ + "center",
        "south" : __DIR__ + "main_road-s1",
        "west" : __DIR__ + "square-sw",
        ])
        );

        set("objects",  ([
                __DIR__ + "npc/roni":2,
                ]));
        setup();

}
