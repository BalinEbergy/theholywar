//forest_10.c
//write by hiwind

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;32m�L��[m");
        set("long",@LONG
�����G�~ı�o[1;31m��[m�L���G�����F,�S�Q�즹�B���٦��@�����p
������L,���L�٬Oı�o�ܿ��,�����٬O�Ӥp�F,���L�o����
�a���G���Ӧ��H�|��,�ҥH�ܦw�R,��ܦ��j�۵M�����,�S���Q
�ȤH���C�U�ҦìV,�O�ӭר��i�ʪ��n�a��.
�F�n����G�i�h�@�B[1;37m�����a[m,�ܷQ�h�_�I.
LONG
        );

        set("exits",
        ([
        "west" : __DIR__ + "forest_5",
        "east" : __DIR__ + "forest_13",
        "south" : __DIR__ + "forest_9",
        "north" : __DIR__ + "forest_11",
        ])
        );

        set("objects",
        ([
                __DIR__ + "npc/bird":1,
        ])
        );

        set("no_fight", 0);
        setup();
}
