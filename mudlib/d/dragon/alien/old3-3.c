// old3-3.c
// written by Mondale

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;37m練金術士公會舊址[m");
        set("long",@LONG
這裡的空氣不只是難聞而已，還像是凝固了一般，令人
難以呼吸。看起來這邊所有的怪物，都應該是因為這種
奇怪的藥品所造成的結果，看來再深入會更危險了。
這裡有座往下的樓梯。
LONG
        );

        set("exits",
        ([
        "north" : __DIR__ + "old2-3",
        "west" : __DIR__ + "old3-2",
        "down" : __DIR__ + "oldb13-3",
        ])
        );

        set("objects",  ([
                ]));
        setup();
}
