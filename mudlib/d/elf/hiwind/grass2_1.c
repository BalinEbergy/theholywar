// grass2_1.c
// written by hiwind

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;32m���[m");
        set("long",@LONG
�A�~��V�e��,�A�ݨ�@�Ӥ@��L�ڪ����,��B�����F��
(grass)�A�w�g�w�Y��V,�u���D���n���G�i�H�^��[1;33m���y[m��,��
���_���G�O�h[1;5;31m�۷�[m[1m��[m[1;36m��[m,�ڻ������O�@��H�ү�h��,�٬O�O
�����e��,�ڻ������B�������@��[1;36m��[m����,�i�H�h��ť�@�U��
��.
LONG
        );

        set("exits",
        ([
        "east" : __DIR__ + "sand_16",
        "north" : __DIR__ + "grass2_2",
        "south" : __DIR__ + "sand_13",
        ])
        );
}
