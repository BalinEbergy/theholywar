// grass_5.c
// written by hiwind

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;32m���[m");
        set("long",@LONG
�A�~��V�e��,�A�ݨ�@�Ӥ@��L�ڪ����,��B�����F��
(grass)�A�w�g�L�k�^�Y,�u���D������G�i�H�^��[1;36m��[m����,
�ө��F���G�O�h���䪺�F�y,��!�٬O�����X���a!
LONG
        );

        set("exits",
        ([
        "east" : __DIR__ + "sand_7",
        "west" : __DIR__ + "grass_3",
        "south" : __DIR__ + "sand_4",
        ])
        );

        set("objects",
        ([
                __DIR__ + "forest/npc/bird":1,
        ])
        );

        set("no_fight", 0);
        setup();
}
