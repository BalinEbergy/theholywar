// ice_sword.c
// written by Mondale

#include <equip.h>
#include <element.h>

inherit EQUIP;

void create()
{
    set_name("[1;36;44m�B�S��v�C[m", ({ "ice sword", "ice", "sword" }));
    set("long", @LONG
�@��C�b���G�p[1;36m���[0m���_�C,�W�������x�۩_����[1;34m�ť�[0m.
LONG
        );
    set("lv", 100);
    set("unit", "��");
    set("element", WATER);
    set("epower", 100);
    set("color", 3);
    set("value",1000);

    set("wear_type", HAND);
    set("weapon", SWORD);
    set("dem_eff", 10000);
    set("spe_eff", 300);
    set("def_eff", 5000);
    set("eff", ({ "dem_eff", "spe_eff", "def_eff" }));

    set_weight( 500);
}
