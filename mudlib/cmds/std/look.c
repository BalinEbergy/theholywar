// look.c

mapping default_dirs = ([
        "north" :       "[1;31m�_��[m",
        "south" :       "[1;34m�n��[m",
        "east"  :       "[1;33m�F��[m",
        "west"  :       "[1;32m���[m",
        "up"    :       "[1;37m�W��[m",
        "down"  :       "[1;36m�U��[m",
]);
string *dir_order= ({ "down", "up", "north", "south", "west", "east" });

int look_living( object me, object obj);
int look_item( object me, object obj);
int look_room( object me, object env);

void create() { seteuid( getuid()); }

int main( object me, int argc, string *argv)
{
    object ob;

    if( argc== 1)
        return look_room( me, environment( me));
    else if( ob= find( argv[1], environment( me)) || ob= find( argv[1], me))
    {
        if( ob->is_character()) look_living( me, ob);
        else look_item( me, ob);
    } else write("�䤣��o�ӪF��...\n");

    return 1;
}

int look_item( object me, object obj)
{
    int i;
    string str;
    object *inv;

    str= " \n"+ obj->short();
    if( D_WIZARD->wiz_level( me)> 1)
        str+= " [1;36;44m"+ base_name( obj)+ "[m";
    str+= "\n\n"+ obj->long()+ "\n";

    if( obj->query_max_carried_object())
    {
        inv= all_inventory( obj);
        if( i= sizeof( inv))
        {
            str+= "���Y��:\n";
            while( i--)
                str+= "\t"+ inv[i]->short()+ "\n";
        } else str+= "�o���Y�S���F��.\n";
    }

    tell_object( me, str);

    return 1;
}

int look_living( object me, object obj)
{
    int lv;
    string str= " \n", it;

    if( me== obj)
    {
        write("�ݦۤv��? �� score �|����n...:P\n");
        return 1;
    }

    message_vision("$N�����$n, �����D�b������D�N.\n", me, obj);

    lv= me->query("lv")- obj->query("lv");
    it= obj->query("gender")? "�L": "�o";

    str+= "[1;33m"+ obj->name()+ "[m"+
        "�E"+ ( obj->query("gender")? "�k": "�k");

    if( D_WIZARD->wiz_level( me)> 1)
        str+= " [1;36;44m"+ base_name( obj)+ "[m";
    if( obj->query("nick"))
        str+= "\n�H�١E"+ obj->query("nick");
    str+= "\n\n"+ obj->query("describe")+ "\n";
    str+= D_STATUS->ra( obj->query("race"))+ ", "+ it+ "�ݨӬ�[1;35m "+
        ( obj->query("age")- random( obj->query("age")/ 10)+
        random( obj->query("age")/ 9))+ " [m��\n";
    str+= it;
    if( lv>= 20) str+= "�z�줣��F.";
    else if( lv>= 10) str+= "²������P�A�ۤ�.";
    else if( lv>= 2) str+= "���G��A�t�F�@��.";
    else if( lv>= -2) str+= "�P�A�������.";
    else if( lv>= -10) str+= "�n����A�j�F�@�I.";
    else if( lv>= -20) str+= "�ݨӮ��F�`��, �٬O�O�Q��L�����n.";
    else str+= "�W�űj.";
    str+= "\n";
    switch( obj->query("currentHP")* 5/ obj->query("maxHP"))
    {
        case 5: str+= "[1;36m�ثe���骬�p�}�n[m"; break;
        case 4: str+= "[1;32m�ثe���F�@�I��[m"; break;
        case 3: str+= "[1;37m�ثe�˶դ��p[m"; break;
        case 2: str+= "[1;33m�ثe�˶ի��Y��[m"; break;
        case 1: str+= "[1;31m�ثe�֭n����F[m"; break;
        default: str+= "[1;41m�w�g����F[m";
    }
    str+= "\n";

    tell_object( me, str);

    return 1;
}

int look_room( object me, object env)
{
    int i, j;
    mapping exit;
    string str, lives, objs, *dirs, *dirtemp;
    object *invs;

    str= " \n"+ env->query("short")+ " - [";
    if( mapp( exit= env->query("exits")))
    {
        dirtemp= keys( exit);

        /* sort */
        i= sizeof( dirtemp);
        j= 5;
        while( i-- && j+ 1)
            if( dirtemp[i]== dir_order[j]) {
                if( !dirs) dirs= ({ dir_order[j] });
                else dirs+= ({ dir_order[j] });
                dirtemp-= ({ dirtemp[i] });
                j--;
                i= sizeof( dirtemp);
            } else if( !i && i= sizeof( dirtemp)) j--;

        if( !j= sizeof( dirs)) str+= "[36m�L[m";
        else str+= default_dirs[dirs[0]];
        for( i= 1; i< j; i++)
            str+= " "+ default_dirs[dirs[i]];
        str+= "]\n";
    } else str+= "[36m�L[m]\n";

    if( D_WIZARD->wiz_level( me)> 1)
        str+= "\n[1;36;44m"+ base_name( env)+ "[m\n";

    str+= "\n"+ ( present("fog", env)?
        "�|�P�@���j��": env->query("long"))+ "\n";

    invs= all_inventory( env);
    i= sizeof( invs);

    lives= objs= "";
    while( i--)
        if( !invs[i]->is_character())
            objs+= "     "+ invs[i]->short()+ "\n";
        else if( invs[i]!= me)
        {
            lives+= invs[i]->short();
            if( invs[i]->is_fighting()) lives+= " ���E�P�a�԰���!!\n";
            else if( invs[i]->query("rest")== 2) lives+= " ���wí���ε�...\n";
            else if( invs[i]->query("rest")== 3) lives+= " ���B��߷Q���A...\n";
            else lives+= "\n";
        }

    tell_object( me, str+ objs+ lives);

    return 1;
}

int help()
{
    write( @HELP
���O�榡: look [�ؼ�]

�[��~���[�ݩж�
HELP
    );

    return 1;
}
