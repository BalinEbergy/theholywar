// kris.c
// written by Mondale
// �J�R��

inherit NPC;

void create()
{
        int HP= 250000, MP= 500, SP= 60000;

        set_name("[m[1m�J�R��[m", ({ "kris" }));
        set("lv", 100);
        set("class","knight");
        set("ra","human");
        set("gender","�k");
        set("describe","�o�O�o�䪺�u�ö���.");
        set("pre","[m[1;36m�J[32m��[m");

        set("age", 17);
        set("element","water");
        set("mind", 1000);

        set("mixHP", HP);
        set("currentHP", HP);
        set("mixMP", MP);
        set("currentMP", MP);
        set("mixSP", SP);
        set("currentSP", SP);
        set("dem", 25000);
        set("def", 20000);
        set("mdem", 20000);
        set("mdef", 20000);
        set("spe",1500);
        set("realmx", 0);
        set("exp", 19999);
        set("svoid", 3);
        set("svoidt", 1);
        set_skill_level( 20, 100);
        set("skill_chance", 50);
        set("skill", ({
                "20:slash moon",
                "20:slash void",
                }));
        carry_object(OB_SWORD +"lv1sword")->wear("b");
        set("chat_chance", 0);
        set("chat_msg", ({
                "\n�J�R���C\n"
                }));
        set("trading", 1);
        set("buy_mind", "-1000~1000");

        set("selling", ({
                OB_SWORD +"lv1sword",
                }));
        setup();
}

void init()
{
        object obj;

        if( !(obj= previous_object())
                || !userp( obj))
                return;
        if(!obj->is_fighting())
        {
        call_out("action", 1, obj);
        }
        else {
        call_out("ki",3,obj);
        }
}

void action( object obj)
{
        command("hihi �Ф��n�b�����S�Ƴ�...");
}

void ki( object obj)
{
        kill_ob(obj);
        command("hihi �b�����x�ưڡH�p�l�H");
        command("hihi ���N���A�}�}���ɧa....���Ѥ@�U�ڪ�����...");
        command("slash moon to" + obj->query("id"));
}
