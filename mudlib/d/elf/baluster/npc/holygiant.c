// holygiant.c
// written by Hudson

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
    int HP= 340000, MP= 6000, SP= 25000;

    set_name("神聖巨人", ({ "holy giant",HOLY,"giant" }));
    set("lv", 70);
    set("class", WARRIOR);
    set("race", ELEMENT);
    set("gender", 1);
    set("describe","一個身上散發著神聖光芒的巨人.");

    set("age", 1033);
    set("element", HOLY);
    set("mind", 1000);

    set("maxHP", HP);
    set("currentHP", HP);
    set("maxMP", MP);
    set("currentMP", MP);
    set("maxSP", SP);
    set("currentSP", SP);
    set("strength", 20000);
    set("armor", 21000);
    set("magic", 9000);
    set("spi", 50);
    set("speed", 2750);
    set("exp", 200000);

    setup();
}

void init()
{
    object obj;

    if( !(obj= previous_object())
        || !userp( obj))
        return;

    call_out("ki", 10, obj);
}

void ki( object obj)
{
    if( obj->is_fighting()) kill_ob( obj);
}
