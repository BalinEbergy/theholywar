// mageguilds1.c
// written by Hudson

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[36m魔法師公會會所[m");
        set("long",@LONG
這兒是魔法師公會會所南方,你仔細看看牆壁上的紋路,全都
是無意義的幾何圖案,但當你靠近的時候,又覺得有一股魔力
緩緩注入你的身體,讓你覺得暖哄哄的.
LONG
        );

        set("exits", ([
                "north"  : __DIR__ + "mageguildm1",
                "east"  : __DIR__ + "mageguilds2",
                ]));
        set("objects", ([
                "d/elf/baluster/npc/loose":1,
                ]));

        setup();
}
