// road_3.c
// written by hiwind

#include <room.h>


inherit ROOM;

void create()
{
        set("short","[1;37m���L�D��[m");
        set("long",@LONG
�b���L�D���W,���۰ʪ��M�ȫ�,���֫_�I�̦b�o���D���W��
�ʵ�,�u�ۦ������N�O�i�h[1;36m��[m�����P�t�@�Ӵ˪L�F,�䤤�R��
�F�j�۵M����.
�b���B���@���e��,�i�H�h���F�˪L.
LONG
        );

        set("exits",
        ([
        "east" : __DIR__ + "road_4",
        "west" : __DIR__ + "road_2",
        "north" : __DIR__ + "elf_forest/road_1",
        ])
        );
}
