// sword.c
// written by Hudson

#include <equip.h>
#include <element.h>

inherit EQUIP;

void create()
{
    set_name("[36;1m�B[33;1m�s[37;1m���C[0m", ({ "ice sword", "ice", "sword"}));
    set("long", @LONG
�@�ⴲ�o�۱j�P�H�𪺪��C.
LONG
        );
    set("lv", 100);
    set("unit", "��");
    set("element", WATER);
    set("epower", 100);
    set("value",300);

    set("wear_type", HAND);
    set("weapon", SWORD);
    set("dem_eff", 5000);
    set("def_eff", 5000);
    set("spe_eff", 500);
    set("eff", ({ "dem_eff", "spe_eff", "def_eff" }));

    set_weight( 100);
}
