// save.c

string query_save_file()
{
    string path, id;

    path= "/data/user/";
    id= this_object()->query("id");

    return path+ lower_case( id[0..0])+ "/"+ id;
}

int save()
{
    string file;

    this_object()->set("startroom", base_name( environment()));
    if( !stringp( file= this_object()->query_save_file())) return 0;

    return save_object( file);
}

int restore()
{
    string file;

    if( !stringp( file= this_object()->query_save_file())) return 0;

    return restore_object( file);
}
