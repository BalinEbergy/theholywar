// transport.c
// written by Hudson

#include <map.h>

mapping name= ([
    "chogall"       :       "[1;31m����j��[m - �d�溸�� (Chogall)",
    "baluster"      :       "[1;32m�㺸�ܤj��[m - �ڸ����S�� (Baluster)",
    "saint kruce"   :       "[1;34m�h�Ԯڤj��[m - �t�J�|���� (Saint Kruce)",
    "great"         :       "[1;37m��Ҥj��[m - �j�� (Great)"
    ]);
mapping path= ([
    "chogall"       :       CHOGALL+ "center",
    "baluster"      :       BALUSTER+ "center",
    "saint kruce"   :       SAINT_KRUCE+ "center",
    "great"         :       GREAT+ "center"
    ]);

int main( object me, int argc, string *argv)
{
    int i, j, price;
    string str, goal, *place;
    object *objs;

    if( argc< 2)
    {
        write("�A�n�ШD�ǰe����O?\n");
        return 1;
    }

    i= sizeof( objs= all_inventory( environment( me)));

    while( i--)
    {
        if( arrayp( place= objs[i]->query("trans")))
            break;
    }

    if( i!= -1)
    {
        price= me->query("lv")* me->query("lv");
        if( argv[1]== "list")
        {
            objs[i]->command("tell "+ me->query("id")+ " �ڥi�H�����ǰe��o�Ǧa��:");
            j= sizeof( place);
            str= " \n    "+ name[ place[ --j]]+ "\n";
            while( j--)
                str+= "    "+ name[ place[j]]+ "\n";
            write( str+ "\n�ǰe�N�� [1;33m$ "+ price+ "[m\n\n");
        } else {
/*
            if( me->query("money")< price)
            {
                objs[i]->command("tell "+ me->query("id")+ " �A����������!\n");
                return 1;
            }
            */

            if( goal= find_string( argv[1], place))
                if( goal= path[ goal])
                {
                    // Transport there
//                    me->add("money", -price);
                    me->move( goal);
                } else write("�X�{���D, �гq���Ův.\n");
            else objs[i]->command("tell "+ me->query("id")+ " �ڤ��t�d�o�Ӧa���~");
        }
    } else write("�o�Ӧa��S���ǰe�ϳ�.\n");

    return 1;
}

int help( object me)
{
    write( @HELP
���O�榡: transport <list> | <�ؼ�>

�b�ǰe�Ϧb���a��, �i�H�ϥγo�ӫ��O�H�@�ǿ��ӽШD�ǰe.
HELP
        );

    return 1;
}
