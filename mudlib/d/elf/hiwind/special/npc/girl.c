// girl.c
// written by hiwind

inherit NPC;

void create()
{
        int HP= 1000000, MP= 10000 ,SP= 100000;

        set_name("少女", ({ "girl" }));
        set("lv", 100);
        set("class","mage");
        set("ra","god");
        set("describe", "一位美麗的少女.");
        set("age", 25);
        set("mind", 1000);
        set("element", "holy");
        set("gender", "女");

        set("mixHP", HP);
        set("currentHP", HP);
        set("mixMP", MP);
        set("currentMP", MP);
        set("mixSP", SP);
        set("currentSP", SP);
        set("str", 40);
        set("dem", 35000);
        set("vir", 15);
        set("def", 10000);
        set("magic", 50);
        set("mdem", 45000);
        set("mdef", 20000);
        set("spirit", 25);
        set("speed", 20);
        set("spe", 2000);
        set("realmx", 200);
        set("exp", 0);

        set_skill_level( 3, 100);
        set_skill_level( 5, 100);
        set("skill", ({  "5:dream", "3:advent of kaigic", }));
        set("skill_chance", 50);
        setup();
}
void init()
{
        if( !previous_object()
                || !userp(previous_object()))
                return;
        call_out("action", 1, previous_object());
}

void action( object obj)
{
        if( !obj || !present(obj) || this_object()->is_fighting()) return;
        if( obj->is_fighting()) kill_ob(obj);

        if( obj->query("mind") < 0)
        {
                command("kill " + obj->query("id"));
        } else
        {
                if( !this_object()->query("follow"))
                {
                        command("follow " + obj->query("id"));
                } else
                {
                        command("hihi 我可是很可愛的喔^^");
                }
        }
}
