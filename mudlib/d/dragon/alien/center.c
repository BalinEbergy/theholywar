// center.c
// written by Mondale

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;37m亞利恩城中心廣場[m");
        set("long",@LONG
這裡是亞利恩城的中心廣場，有一座噴水池在這裡，
這個城感覺上沒有什麼活力，連中心廣場都沒有什麼
人在。一想起這個城從前的繁華，與現在的落寞，真
令人不禁唏噓。
LONG
        );

        set("exits",
        ([
        "north" : __DIR__ + "nroad1",
        "south" : __DIR__ + "sroad1",
        "east" : __DIR__ + "eroad1",
        "west" : __DIR__ + "wroad1",
        ])
        );

        set("objects",  ([
                __DIR__ + "npc/tourist":1,
                __DIR__ + "npc/dog":2,
                __DIR__ + "obj/fountain":1,
                ]));
        set("no_fight", 1);
        setup();
}
