// commercial_association.c
// written by Hudson

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;37m商業公會巴路斯特分會[m");
        set("long",@LONG
你一進到這裡,面前的牆上掛著一幅公會聖龍的圖騰,四面都
是乾淨的白色牆壁,這裡就是世界上最龐大的公會－商業公會
在巴路斯特的分部,你見到眼前站著兩位笑容可掬的服務員,
覺得異常親切.
公會可說是很多商家的命脈啊!
LONG
        );

        set("exits",
        ([
        "west"  : __DIR__ + "nroad4",
        ])
        );

        set("objects", ([
                __DIR__ + "npc/steward": 1,
                __DIR__ + "npc/stewardI": 1,
                ]));

        setup();
}
