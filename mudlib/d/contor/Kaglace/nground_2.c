// nground_2.c
// written by Bother

#include <room.h>


inherit ROOM;

void create()
{
        set("short","[1;37m卡格烈斯廣場[m");
        set("long",@LONG
這裡是卡格烈斯城的廣場,再往北走就是卡格烈斯山區邊境,若你
對文化的東西非常有興趣,你可以繼續往前走再往西前進就可以
進入圖書館(library)了,裡面的文獻資料可以增加你的文學氣質.
LONG
        );

        set("exits",
        ([
        "north" :__DIR__ + "city_45",
        "south" :__DIR__ + "nground_1",
        "west" :__DIR__ + "nwground_3",
        "east" :__DIR__ + "neground_2",
        ])
        );

}
