// wear.c

int main( object me, int argc, string *argv)
{
    object obj;

    if( argc< 2 || !objectp( obj= find( argv[1], me)))
        write("你想要裝備什麼呢?\n");
    else if( !obj->equip())
        write("你不能裝備此樣物品.\n");
    else message_vision("$N將"+ obj->name()+ "裝備在身上.\n", me);

    return 1;
}

int help( object me)
{
    write( @HELP
指令格式: wear <裝備>

這個指令可以讓你裝備身上可裝備的物品.
HELP
        );

    return 1;
}
