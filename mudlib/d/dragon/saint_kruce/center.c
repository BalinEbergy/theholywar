//center.c
//written by zz

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;37;44m聖克魯斯城中央廣場[m");
        set("long",@LONG
這裡就是聖克魯斯城的正中心,四周是寬大的廣場,佇立在你
眼前的,是[1;37m銀騎士凱斯特[m的高聳雕像,手中拿著銀劍,腳下踩
著[31m惡魔[m的頭顱,象徵著一切的罪惡都將在此被洗滌殆盡.
LONG
        );
        set("exits", ([
                "north" : __DIR__ + "c0,1.c" ,
                "south" : __DIR__ + "c0,-1.c" ,
                "east" : __DIR__ + "c1,0.c" ,
                "west" : __DIR__ + "c-1,0.c" ,
                ]));
        set("objects",  ([
                __DIR__ + "obj/fountain" :1 ,
                __DIR__ + "obj/statue" :1 ,
                __DIR__ + "npc/child" :5 ,
                __DIR__ + "npc/caster" :1 ,
                __DIR__ + "npc/lin": 1
        ]));
//        set("no_fight", 1);

        setup();
}
