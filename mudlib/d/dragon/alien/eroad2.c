// eroad2.c
// written by Mondale

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[m[1;33m����[m[1;37m�j�D[m");
        set("long",@LONG
�o���j�D��V���F�A�O�{�b�ȧQ�������D�n�D���A�D
�����|�P���ۤ@�ǰӮa�A�]���@�ǥ��СA���L�٬O�S
���q�e���c�ءC���W�u���۴X�W���H�C
�_�䦳�@�a�Z�����C
�n�䦳�@���p���q�����v�ϡC
LONG
        );

        set("exits",
        ([
        "north" : __DIR__ + "weaponshop",
        "south" : __DIR__ + "wstreet1",
        "east" : __DIR__ + "eroad3",
        "west" : __DIR__ + "eroad1",
        ])
        );

        set("objects",  ([
                __DIR__ + "npc/tourist":1,
                __DIR__ + "npc/cat":2,
                ]));
        setup();
}
