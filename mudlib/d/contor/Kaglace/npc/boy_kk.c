// boy_kk.c
// written by Hudson
// 玩童_kk

inherit NPC;

void create()
{
        int HP= 200, MP= 50, SP= 100;

        set_name("玩童", ({ "boy" }));
        set("lv", 3);
        set("class","worrior");
        set("ra","human");
        set("gender","男");
        set("describe","一個好奇心重的男孩.");
        set("pre","好奇心重的");

        set("age", 10);
        set("element","fire");
        set("mind", 10);

        set("mixHP", HP);
        set("currentHP", HP);
        set("mixMP", MP);
        set("currentMP", MP);
        set("mixSP", SP);
        set("currentSP", SP);
        set("dem", 20);
        set("def", 25);
        set("mdem", 0);
        set("mdef", 0);
        set("spe", 20);
        set("realmx", 0);
        set("exp", 100);
        set("money", 100);

        set("chat_chance", 15);
        set("chat_msg", ({
                "\n你要和我玩捉迷藏嗎??\n"
                }));

        setup();
}
