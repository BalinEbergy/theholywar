// time.c
// written by Hudson

#include <skill.h>

int main( object me, object obj)
{
        if( me->skill_level(AIR)< 10)
                write( NATURE_D->game_time(1));
        else if( me->skill_level(AIR)< 20)
                write( NATURE_D->game_time(2));
        else if( me->skill_level(AIR)< 40)
                write( NATURE_D->game_time(3));
        else write( NATURE_D->game_time(4));

        return 1;
}

string name( int select)
{
        switch( select)
        {
                case 1: return "顯示時間術";
                case 2: return "know time";
                case 3: return "time";
        }
}

int type() { return AIR; }
int lv() { return 1; }
int attackful()
{
        return 0;
}

string mes( object me)
{
        return "唸出一段咒語,一行文字從空中顯現出來.";
}

int usesp() { return 0; }
int usemp() { return 10; }
int successful() { return 100; }
int delay() { return 3; }
int all() { return 0; }
string element() { return "air"; }

int check( object me)
{
        return 1;
}
