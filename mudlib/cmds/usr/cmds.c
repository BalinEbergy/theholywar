// cmds.c

string *cmds, wizcmds;

int help();

void create()
{
    cmds= ({
        read_file( DIR_HELP+ "cmds/USER"),
        read_file( DIR_HELP+ "cmds/ACTION"),
        read_file( DIR_HELP+ "cmds/COMBAT"),
        read_file( DIR_HELP+ "cmds/HELP")
        });
        
    wizcmds= read_file( DIR_HELP+ "cmds/WIZCMDS");
}

int main ( object me, int argc, string *argv)
{
    int type;

    if( argc< 2) help();
    else if(( type= atoi( argv[1]))> 0 && type< 5)
    {
        write("\n[1;33m"+ MUD_NAME+ "[m目前開放指令...\n\n"+
            cmds[ type- 1]+ ( D_WIZARD->wiz_level( me)< 3? "": wizcmds)+
            "\n每個指令的用法請輸入 help <指令名> 查詢.\n\n");
    } else write("沒有這個類別喔!\n");

    return 1;
}

int help()
{
    write( @HELP
指令格式: cmds [類別]

玩家指令 [1]
行動指令 [2]
戰鬥指令 [3]
說明指令 [4]

列出目前開放的指令
HELP
    );

    return 1;
}
