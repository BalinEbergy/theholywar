// m_shield.c
// written by Hudson

#include <skill.h>

int main( object me, object obj)
{
        string *effects;

        if( obj->query("m_shieldt")) return 0;
        else
        {
                effects= obj->query("effect");
                obj->set("m_shield", ({
                        "wind_eff:"+ 10, "lightning_eff:"+ 10, "fire_eff:"+ 10,
                        "water_eff:"+ 10, "earth_eff:"+ 10
                        }));
                obj->set("m_shieldt", me->skill_level( 4)/ 2+ 1);
                obj->add("wind_eff", 10);
                obj->add("lightning_eff", 10);
                obj->add("fire_eff", 10);
                obj->add("water_eff", 10);
                obj->add("earth_eff", 10);

                if( effects)
                        effects+= ({ "m_shield" });
                else effects= ({ "m_shield" });
                obj->set("effect", effects);
        }

        tell_object( obj, "\n�A������¶�_[1;32m�@�}����[m.\n");

        return 1;
}

string name( int select)
{
        switch( select)
        {
                case 1: return "�]�k����";
                case 2: return "magic shield";
                case 3: return "m_shield";
        }
}

int type() { return WIND; }
int lv() { return 1; }
int attackful()
{
        return 0;
}

string mes( object me)
{
        return "�᧹�G�y,�����@�}���q�}�U�Ͱ_.";
}

int usesp() { return 0; }
int usemp() { return 30; }
int successful() { return 100; }
int delay() { return 2; }
int all() { return 0; }
string element() { return "wind"; }

int check( object me)
{
        return 1;
}
