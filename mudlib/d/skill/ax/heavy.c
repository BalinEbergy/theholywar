// heavy.c
// written by Hudson

#include <skill.h>

string name( int i);

int main( object me, object obj)
{
        int damage, dem, eff, vir, hp;
        string w_name;

        w_name= me->query_temp( me->query_temp("ax"))->name();
        if( random( obj->query("spe")* 2)> me->query("spe"))
                damage= -1;
        else
        {
                dem= me->query("dem")+ me->query("dem_eff");
                eff= me->query("damage_eff");
                damage= random( 2* dem)+ eff;

                obj->add("currentHP", -damage);
        }

        return COMBAT_D->skill_msg( me, obj, name( 1), w_name, damage);
}

string name( int select)
{
        switch( select)
        {
                case 1: return "重劈";
                case 2: return "heavy";
                case 3: return "heavy";
        }
}

int type() { return AX; }
int lv() { return 5; }
int attackful()
{
        return 1;
}

string mes( object me)
{
        return "大喝一聲,手中的"+ me->query_temp( me->query_temp("ax"))->name()+
                "沉重的往敵人砍去!!";
}

int usesp() { return 30; }
int usemp() { return 0; }
int successful() { return 64; }
int delay() { return 2; }
int all() { return 0; }

int check( object me)
{
        return 1;
}
