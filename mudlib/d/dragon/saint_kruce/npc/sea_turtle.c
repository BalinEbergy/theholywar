// sea turtule.c
// written by zz

#include <class.h>
#include <element.h>
#include <race.h>

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
        int HP= 10000, MP= 200, SP= 10000;

        set_name("大海龜", ({ "sea turtle" , "sea" , "turtle"}));
        set("lv", 17);
        set("race","animal");
        set("gender","動物");
        set("describe","一隻大海龜.");
        set("pre","停在沙灘旁的");
        set("age", 20);
        set("element",WATER);
        set("mind", 10);

        set("maxHP", HP);
        set("currentHP", HP);
        set("maxMP", MP);
        set("currentMP", MP);
        set("maxSP", SP);
        set("currentSP", SP);
        set("dem", 200);
        set("def", 300);
        set("mdem", 30);
        set("mdef", 30);
        set("spe", 40);
        set("exp", 1700);

        set("chat_chance", 2);
        set("chat_msg", ({
                (: random_move :)
                }));
        setup();
}
