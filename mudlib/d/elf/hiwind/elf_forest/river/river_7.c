// river_7.c
// written by hiwind

#include <room.h>


inherit ROOM;

void create()
{
        set("short","[1;36m�p��[m");
        set("long",@LONG
�o�O�@���M�����p��,�O���F�˪L���@�Ӵ�y���u��,�u���e��
�w�w�o�V�n��y�h,�γ\�]���O�������Y,�|�P���۲H�H����
,�ϱo����a���a�g����.
����O�@�����Z����a.
LONG
        );

        set("exits",
        ([
        "west" : "d/elf/hiwind/elf_forest/grass_5",
        "south" : __DIR__ + "river_6",
        "north" : __DIR__ + "river_8",
        ])
        );

        set("objects",
        ([
                __DIR__ + "npc/fish1":2,
                __DIR__ + "npc/fish2":2,
        ])
        );
        set("no_fight", 0);
        setup();
}
