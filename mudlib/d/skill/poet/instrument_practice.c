// instrument_practice.c
// written by Bother

#include <skill.h>

int main( object me, object obj)
{
        string weapon_name, han;
        object weapon;

        han = "hand" + "b";
        weapon = me->query_temp( han);
        weapon_name = weapon->query("weapon");

        message_vision("$N演奏著"+ weapon->name()+ "傳出陣陣的音樂\n", me);
        me->add_skill_exp( BARD, me->query("spirit")* me->query("lv")/
                me->skill_level( BARD)/ 2);
        switch( weapon_name)
        {
                case "harp":
                        me->add_skill_exp( HARP, me->query("spirit")*
                                me->query("lv")/ me->skill_level( HARP));
                        break;
                case "flute":
                        me->add_skill_exp( FLUTE, me->query("spirit")*
                                me->query("lv")/ me->skill_level( FLUTE));
                        break;
                case "drum":
                        me->add_skill_exp( DRUM, me->query("spirit")*
                                me->query("lv")/ me->skill_level( DRUM));
                        break;
        }

        return 1;
}

string name( int select)
{
        switch( select)
        {
                case 1: return "樂器練習";
                case 2: return "instrument practice";
                case 3: return "practice";
        }
}

int lv() { return 1; }
int attackful()
{
        return 0;
}

string mes( object me)
{
        return "演奏著美妙的音樂,感動著大眾.";
}

int type() { return BARD; }
int usesp() { return 5; }
int usemp() { return 0; }
int successful() { return 100; }
int delay() { return 2; }
int all() { return 0; }
int check( object me)
{
        return 1;
}
