// squirrel.c
// written by Mondale

inherit NPC;

void create()
{
        int HP= 5200, SP= 700;

        set_name("松鼠", ({ "squirrel", "squirrel" }));
        set("lv", 15);
        set("class","warrior");
        set("ra","squirrel");
        set("describe","住在這裡的松鼠.");
        set("pre","一隻可愛的");

        set("age", 5);
        set("element", "earth");
        set("goodat", "wind");
        set("weakat", "fire");
        set("mind", 50);
        set("gender", "男");

        set("mixHP", HP);
        set("currentHP", HP);
        set("mixSP", SP);
        set("currentSP", SP);
        set("dem", 450);
        set("def", 300);
        set("spirit", 100);
        set("spe", 470);
        set("exp", 10000);

        set("chat_chance", 20);
        set("chat_msg", ({
                (: this_object(), "random_move" :),
                "\n小松鼠看見你，就啾的叫了一聲.\n"
                }));
        setup();
}
