// caeser_weapon.c
// written by Hudson

#include <room.h>

inherit ROOM;

void create()
{
        set("short", "[1;30m凱撒武器巴路斯特分店[m");
        set("long",@LONG
這裡是巴路斯特唯一的一家武器店,因為受到魔法師公會的協
助,因此店內有販賣屬於魔法師專用的法杖及水晶球等.是世
界上少數可取得魔法師用品的地方.四周掛著許多附有魔法的
武器,亦是世界除了米德加德外唯一可取得魔法武器之處.
LONG
        );

        set("exits",
        ([
        "west"  : __DIR__ + "sroad5",
        ])
        );

        set("objects", ([
                __DIR__ + "npc/dean": 1,
                ]));

        setup();
}
