// search.c

string find_string( string verb, string *strs)
{
    int i, j, k, size, length;

    if( verb== 0) return 0;

    size= sizeof( strs);
    length= sizeof( verb);

    for( i= 0; i< size; i++)
    {
        for( j= 0; j< length; j++)
            if( strs[i][j]!= verb[j])
                break;
        if( j== length)
            return strs[i];
    }

    return 0;
}

