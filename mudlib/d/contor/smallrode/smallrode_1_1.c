// smallrode_1_1.c
// written by Bother

#include <room.h>


inherit ROOM;

void create()
{
        set("short","[36m不知名的小路[m");
        set("long",@LONG
你走著走著來到了一條不知名的小路,你不知道他通往哪你,
如果你不擔心後果的話,它是個不錯的冒險地點.
LONG
        );

        set("exits",
        ([
        "west" : "/d/contor/grassland/grassland_7_2",
        ])
        );
}
