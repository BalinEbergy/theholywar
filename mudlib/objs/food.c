// food.c

#include <race.h>

inherit ITEM;

static int amount= 1, recover= 100;
static int rough;

int is_food() { return environment()->query("race")== OGRE? 1: !rough; }
void set_amount( int a) { amount= a; }
void set_rec( int r) { recover= r; }
void set_rough() { rough= 1; }

void eat()
{
    int cur, max;
    object eater;

    eater= environment();
    cur= eater->query("currentHungry");
    max= eater->query("maxHungry");

    cur+= recover* ( random( 20)+ 90)/ 100;
    if( cur> max) cur= max;
    eater->set("currentHungry", cur);

    amount--;

    if( !amount)
    {
        tell_object( eater, "�A�Y���F"+ this_object()->name()+ ".\n");
        destruct( this_object());
    }
}
