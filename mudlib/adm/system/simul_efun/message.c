// message.c

string trans_gender( int gender, int self)
{
    switch( gender)
    {
        case 0:
            if( self) return "�p";
            else return "�o";
        case 1:
            if( self) return "�A";
            else return "�L";
        case 2:
            return "�e";
        case 3:
            return "͢";
    }
}

varargs void message_vision( string str, object me, object you)
{
    int my_gender, your_gender;
    string my_name, your_name;
    string str1, str2, str3;

    my_name= me->query("name");
    my_gender= me->query("gender");
    str1= replace_string( str, "$P", trans_gender( my_gender, 1));
    str1= replace_string( str1, "$N", trans_gender( my_gender, 1));
    str3= replace_string( str, "$P", my_name);
    str3= replace_string( str3, "$N", my_name);
    if( you) {
        your_name= you->query("name");
        your_gender= you->query("gender");
        str1= replace_string( str1, "$p", trans_gender( your_gender, 0));
        str1= replace_string( str1, "$n", your_name);
        str3= replace_string( str3, "$p", your_name);
        str3= replace_string( str3, "$n", your_name);

        str2= replace_string( str, "$P", trans_gender( my_gender, 0));
        str2= replace_string( str2, "$N", my_name);
        str2= replace_string( str2, "$p", trans_gender( your_gender, 1));
        str2= replace_string( str2, "$n", trans_gender( your_gender, 1));
        if( you!= me)
            message("vision", str2, you);
    }

    message("vision", str1, me);
    message("vision", str3, environment( me), ({ me, you }));
}

void write( string str)
{
    if( this_player())
        message("write", str, this_player());
    else message("write", str, previous_object());
}

void tell_object( object ob, string str)
{
    message("tell_object", str, ob);
}

varargs void tell_room( mixed room, string str, object *exclude)
{
    message("tell_room", str, room, exclude);
}
