// new-e.c
// written by Mondale

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;37m練金術士公會robot練習場[m");
        set("long",@LONG
這裡是亞利恩城的鍊金術士公會的練習場，專門給練金
術士練習各種技巧，藉以提昇自己的實力。練習場的圍
牆都是特製的，不怕各種的爆破或侵蝕，這邊有著公會
所派駐的老師來教導練金術士們各種技能。
LONG
        );

        set("exits",
        ([
        "west" :__DIR__ + "newunion",        
        ])
        );

        set("objects",  ([
                __DIR__ + "obj/fountain":1,
                __DIR__ + "npc/keynes":1,
                ]));
        setup();
}
