// magic_apple.c
// written by Hudson

#include <element.h>

inherit FOOD;

void create()
{
    object obj;
    set_name("魔法蘋果", ({ "magic apple", "magic", "apple" }));
    set("long", @LONG
用魔法造成的蘋果.
LONG
        );
    set("unit", "顆");
    set("lv", 1);
    set("no_save", 1);

    set_weight( 13);
}

void disappear()
{
    message_vision("$N腐敗了.", this_object());
    destruct( this_object());
}
