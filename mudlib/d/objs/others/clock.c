// clock.c
// written by Bother

#include <localtime.h>

inherit ITEM;
int time= 0, min= 0;
string temp1, temp2;

void create()
{
        object obj;

        set_name("����", ({ "clock" }));
        set("long", time+ ":"+ min);
        set("unit", "��");
        set("lv", 0);
        set("value", 5);
        set("weight", 1000);
        set("no_get", 1);
        set("no_sac", 1);
        heart_beat(1);
}

void heart_beat()
{
        sscanf( NATURE_D->game_time(4), "%s*[1;44;37m%d:%d*[m%s"
                , temp1, time, min, temp2);

}
