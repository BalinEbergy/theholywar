// wind_ring.c
// written by Bother

#include <equip.h>
#include <element.h>

inherit EQUIP;

void create()
{
        set_name("[1;32m������[m", ({ "wind ring", "wind", "ring" }));
        set("long", @LONG
�@�ӻA��⪺�٫�.
LONG
                );
        set("lv", 100);
        set("unit", "��");
        set("element", WIND);
        set("epower", 100);
        set("weight", 1);

        set("wear_type", RING);
        set("mdef_eff", 1500);
        set("mdem_eff", 2000);
        set("eff", ({ "mdef_eff", "mdem_eff" }));
}
