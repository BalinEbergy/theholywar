//test9.c
//written by zz

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;37mtest9[m");
        set("long",@LONG
test Lv91-99
LONG
        );
        set("exits", ([
                "north" : __DIR__ + "test8.c" ,
                ]));
        set("objects",  ([
                __DIR__ + "npc/test/lv91" :1 ,
                __DIR__ + "npc/test/lv92" :1 ,
                __DIR__ + "npc/test/lv93" :1 ,
                __DIR__ + "npc/test/lv94" :1 ,
                __DIR__ + "npc/test/lv95" :1 ,
                __DIR__ + "npc/test/lv96" :1 ,
                __DIR__ + "npc/test/lv97" :1 ,
                __DIR__ + "npc/test/lv98" :1 ,
                __DIR__ + "npc/test/lv99" :1 ,
        ]));
        set("no_recall", 1);
        setup();
}
