// man.c
// written by hiwind

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
    int HP= 40000, MP= 500 ,SP= 10000;

    set_name("[1m�s�u�u��[m", ({ "guild" }));
    set("lv", 30);
    set("class", WARRIOR);
    set("race", ELF);
    set("describe","���F.");
    set("age", 30);
    set("element", WIND);
    set("mind", -250);
    set("gender", 1);

    set("maxHP", HP);
    set("currentHP", HP);
    set("maxSP", SP);
    set("currentSP", SP);
    set("strength", 2000);
    set("def", 500);
    set("mdem", 800);
    set("mdef", 200);
    set("spi", 10);
    set("speed", 75);
    set("exp", 0);

    set("chat_chance", 10);
    set("chat_msg", ({
        "�u�ï��۬ݵۧA.\n"
        }));
    setup();
}
void init()
{
    ::init();
    if( !previous_object()
        || !userp(previous_object()))
        return;
    call_out("action", 1, previous_object());
}

void action( object obj)
{
    if( !obj || !present(obj) || this_object()->is_fighting()) return;
    if( obj->is_fighting()) kill_ob(obj);
    command("say �p�ߧr!���L�̪�í");
    setup();
}
