// drink.c

int main( object me, int argc, string *argv)
{
    int i;
    object *inv;

    inv= all_inventory( environment( me));

    i= sizeof( inv);
    while( i--)
        if( inv[i]->query("drinkable"))
            break;

    if( i< 0) write("這裡沒有水可以喝唷~\n");
    else {
        message_vision("$N從"+ inv[i]->name()+ "中喝了一口水.\n", me);
        me->set("currentThirsty", me->query("maxThirsty"));
    }

    return 1;
}

int help( object me)
{
    write( @HELP
指令格式: drink

在有水池的地方可以使用這個指令來喝水.
HELP
        );
    return 1;
}
