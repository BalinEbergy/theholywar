//priest.c
//written by zz

#include <class.h>
#include <element.h>
#include <race.h>

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
        int HP= 340000, MP= 56000, SP= 5000;

        set_name("神父", ({ "priest" }));
        set("lv", 90);
        set("class",WIZARD);
        set("race",ELF);
        set("gender",1);
        set("describe","一位年長而又和藹的老者.");
        set("pre","聖克魯斯教堂的");

        set("age", 84);
        set("element",COSMOS);
        set("mind", 922);

        set("maxHP", HP);
        set("currentHP", HP);
        set("maxMP", MP);
        set("currentMP", MP);
        set("maxSP", SP);
        set("currentSP", SP);
        set("dem", 17000);
        set("def", 31000);
        set("mdem", 49000);
        set("mdef", 38533);
        set("spe", 2800);
        set("realmx", 183);
        set("exp", 250000);

        setup();
}

void action( object obj)
{
        if( !obj || !present(obj) || this_object()->is_fighting()) return;
        command("say 有什麼我可以幫忙的嗎?");
}

void ki( object obj)
{
        if( obj->query("attacker")) kill_ob( obj);
        if( present( obj->query("id"), environment( this_object())))
                call_out("ki", 10, obj);
}

int rebirth( object obj)
{
        if( !obj->query("ghost"))
        {
                if( obj->query("age")< this_object()->query("age"))
                command("hhhh "+ obj->query("id")+
                        " 別說笑了...小傢伙,你還沒死呢...");
                else command("hhhh "+ obj->query("id")+
                        " 呵呵,你還沒死呢!");
                return 1;
        }

        command("hhhh "+ obj->query("id")+ " "+ obj->name()+
                "呀... 你還想回到這個世界上嗎? ...");
        call_out("do_rebirth", 1, obj);

        return 1;
}

void do_rebirth( object obj)
{
        command("hhhh "+ obj->query("id")+
                " 好吧, 給我一些經驗, 幫你找回你的屍體吧...");
        command("cast life to "+ obj->query("id"));
        command("hhhh "+ obj->query("id")+ " 你現在已經重生了!");

        return;
}
