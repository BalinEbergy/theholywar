//forest_13.c
//write by hiwind

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;37m�L��[m");
        set("long",@LONG
�����F�L�����D,���i�F[1;33;44m���F[m[1;32m�˪L[m���L��,�N��o�w�R�h�F,�S
���D���W���n�x,�ʪ����X�S�]�����W�c,���B��[1;31m��[m[1;32m�L[m������
�j���ۦP,���M���O�p������,����o�Q�����A��.�ѩ�L�즳
�ǭZ�K,�ҥH�����u��q�@�ǵ}�\�����_���S�F�X��,�򤧫e
���L�����D��,�𽮤U�N��o�Q�������D.
���ӬO���񦳬y���a,���B�g���ۤ@�ѲH�H��[1;36m����[m,�ϱo�L��
������[1;37m�Y[1;5;37m��[m[1;37m�Y[1;5;37m�{[m,�ϩ��m���ڹ�,�O�p���������P���R.
LONG
        );

        set("exits",
        ([
        "west" : __DIR__ + "forest_8",
        "east" : __DIR__ +  "forest_18",
        "south" : "d/elf/hiwind/elf_forest/grass_1",
        "north" : __DIR__ + "forest_14",
        ])
        );
        set("objects",
        ([
                __DIR__ + "npc/bear":1,
        ])
        );
        set("no_fight", 0);
        setup();
}
