// sky_end.c
// written by Hudson

#include <skill.h>

int main( object me, object obj)
{
        int damage= 150000, lv, effect;

        effect= me->query("mdef")* 10/ ( obj->query("mdef")+ obj->query("def"));

        lv= me->skill_level( SONG);
        damage= damage* lv/ 50* (80+ random( 40))/ 100* effect/ 5;

        obj->add("currentHP", -damage);
        obj->add("currentMP", -(obj->query("currentMP")* lv/ 300));
        obj->add("currentSP", -(obj->query("currentSP")* lv/ 200));

        return damage;
}

string name( int select)
{
        switch( select)
        {
                case 1: return "天空終結之歌";
                case 2: return "sky end";
                case 3: return "sky_end";
        }
}

int type() { return SONG; }
int lv() { return 80; }
int attackful()
{
        return 1;
}

string mes( object me)
{
        return "的眼神透露出一絲悲傷, 口中微微傳出低沉的聲音, [1;5;37m突然間[m,\n" +
                "[1;37m天空化成一片[34m慘藍[m, 排山倒海而來的歌聲如[1;35m末日[m般的淒愴, 每個人這片歌聲所包圍,\n"+
                "[36m漸漸的感到無力...[m\n";
}

int usesp() { return 1300; }
int usemp() { return 0; }
int successful() { return 90; }
int delay() { return 5; }
int all() { return 1; }

int check( object me)
{
        return 1;
}
