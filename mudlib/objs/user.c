// user.c

#include <user.h>

inherit CHARACTER;
inherit F_AUTOLOAD;
inherit F_SAVE;

static int last_time_set;
static string previous_command= "";

void logon()
{
    seteuid( getuid());

    call_out("time_out", 300);
    D_LOGIN->login( this_object());
}

void time_out()
{
    if( environment())
        return;
    if( interactive( this_object()))
        printf("您花在連線進入手續的時間太久了喔, 請重新進入.\n");
    destruct( this_object());
}

string process_input( string str)
{
    int i;
    object *objs;

    if( str== "")
    {
        write("");
        return "";
    }

    switch( str)
    {
        case "": write(""); return "";
        case "/": str= "recall"; break;
        case "!": str= previous_command; break;
        case "u": str= "go up"; break;
        case "d": str= "go down"; break;
        case "w": str= "go west"; break;
        case "e": str= "go east"; break;
        case "n": str= "go north"; break;
        case "s": str= "go south"; break;
    }

    if( sscanf( str, "edit %s", str) == 1) ed(str);
    else if( !command( str))
        write("沒有這個指令. 請輸入 cmds 查詢.\n");
    else previous_command= str;

    return "";
}

void update_age()
{
    if( !last_time_set) last_time_set= time();
    add("mud_age", last_time_set- time());
    last_time_set= time();
    set("age", 17- query("mud_age")/ 86400);
}

int save()
{
    int ret;

    update_age();
    save_autoload();
    ret= ::save();
    clean_up_autoload();

    return ret;
}

void setup()
{
    update_age();
    ::setup();
    set_living_name( query("id"));
    restore_autoload();
}

private void user_dump( int type)
{
    switch( type)
    {
        case DUMP_NET_DEAD:
            tell_room( environment(), query("name") + "斷線超過 "
                + NET_DEAD_TIMEOUT/60 + " 分鐘, 自動退出這個世界.\n");
            command("quit");
            break;
        case DUMP_IDLE:
            tell_object( this_object(), "對不起, 您已經發呆超過 "
                + IDLE_TIMEOUT/60 + " 分鐘了, 請下次再來.\n");
            tell_room( environment(), "一陣風吹來, 將發呆中的" + query("name")
                + "化為一堆飛灰, 消失了。\n", ({ this_object() }));
            command("quit");
            break;
        default: return;
    }
}

private void net_dead()
{
    object link_ob;

    set_heart_beat(0);

    // Stop fighting, we'll be back soon.
    remove_all_enemy();

    set_temp("netdead", 1);
    if( userp( this_object()))
    {
        call_out("user_dump", NET_DEAD_TIMEOUT, DUMP_NET_DEAD);
        tell_room( environment(), query("name") + "斷線了.\n", ({ this_object() }));
    } else command("quit");
}

void reconnect()
{
    set_heart_beat(1);
    set_temp("netdead", 0);
    remove_call_out("user_dump");
    tell_object( this_object(), "重新連線完畢.\n");
}
