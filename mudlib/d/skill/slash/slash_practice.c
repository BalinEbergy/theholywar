// fire_light.c
// written by Hudson

#include <skill.h>

int main( object me, object obj)
{
        switch (random(3))
        {
        case 0:
        tell_object(me,"\n絤策,钩⊿ぐ或秈甶.\n");
        break;
        case 1:
        tell_object(me,"\n絤策糃膀セмォ.\n");
        break;
        case 2:
        tell_object(me,"\n羭癬糃絤策阑мォ.\n");
        break;
        case 3:
        tell_object(me,"\n羭癬糃絤策揣мォ.\n");
        }
}

string name( int select)
{
        switch( select)
        {
                case 1: return "糃絤策";
                case 2: return "slash practice";
                case 3: return "slash_practice";
        }
}

int lv() { return 1; }
int attackful()
{
        return 0;
}

int type() { return SLASH; }
int usesp() { return 5; }
int usemp() { return 0; }
int successful() { return 100; }
int delay() { return 1; }
int all() { return 0; }
int check( object me)
{
        return 1;
}
