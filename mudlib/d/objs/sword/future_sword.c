// future_sword.c
// written by Hudson

#include <equip.h>
#include <element.h>

inherit EQUIP;

void create()
{
    set_name("未來聖劍", ({ "future sword", "future", "sword" }));
    set("long", @LONG
一把透著藍光的劍,上面刻著一行字:

    未來天空之劍
LONG
        );
    set("lv", 100);
    set("unit", "把");
    set("element", COSMOS);
    set("epower", 100);

    set("wear_type", HAND);
    set("weapon", SWORD);
    set("dem_eff", 50000);
    set("spe_eff", 300);
    set("eff", ({ "dem_eff", "spe_eff" }));

    set_weight( 500);
}
