// fire_lance.c
// written by zz

#include <equip.h>
#include <element.h>

inherit EQUIP;

void create()
{
        set_name("炙焰烈刃戟", ({ "fire lance", "lance", "fire" }));
        set("long", @LONG
一把散發著炙熱光芒的長戟
LONG
                );
        set("lv", 100);
        set("unit", "把");
        set("element", FIRE);
        set("epower", 100);
        set("weight", 100);

        set("wear_type", HAND);
        set("weapon", LANCE);
        set("dem_eff", 60000);
        set("spe_eff", 100);
        set("eff", ({ "dem_eff", "spe_eff" }));
}
