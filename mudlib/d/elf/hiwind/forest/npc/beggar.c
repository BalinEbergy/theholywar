// beggar.c
// written by Hiwind

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
    int HP= 200000, MP= 500, SP= 2000;

    set_name("乞丐", ({ "beggar" }));
    set("lv", 50);
    set("class","ruronin");
    set("race",HUMAN);
    set("gender",1);
    set("describe","一個苦命的乞丐.");
    set("age", 40);
    set("element",EARTH);
    set("mind", 100);

    set("teacher", ({
        23,
        }));
    set("favorite", ({
        "ruronin",
        }));
    set("teach_mind", "-1000 ~ 1000");
    set("lv_up", "[1;36m乞丐說道:[33m 好了, 等級提升了[m\n");
    set("no_enu_money", "[1;36m乞丐說道:[33m 景仰刀法的人啊!可憐我乞丐吧..[m");
    set("no_enu_lv", "[1;36m乞丐說道:[33m 你的等級還不夠![m");
    set("top_lv", "[1;36m乞丐道:[33m 恭喜了,你的能力已經超越我了[m");
    set("teach_message", "[1;36m乞丐說道:[33m 學刀法嗎?[m");

    set("maxHP", HP);
    set("currentHP", HP);
    set("maxMP", MP);
    set("currentMP", MP);
    set("maxSP", SP);
    set("currentSP", SP);
    set("strength", 7000);
    set("armor", 3000);
    set("magic", 800);
    set("spi", 4);
    set("speed", 3000);
    set("exp", 3000);

    setup();
}

void init()
{
    if( !previous_object()
        || !userp(previous_object()))
        return;
    call_out("ki", 100, previous_object());
    call_out("action", 10, previous_object());
}

void ki( object obj)
{
    if( obj->is_fighting()) kill_ob(obj);
}

void action( object obj)
{
    command("say 好心的人施捨一點吧....");
}
