// akas.c
// written by Hudson
// 阿卡斯

inherit NPC;

void create()
{
        int HP= 120000, MP= 14000, SP= 1000;

        set_name("泅�卡斯", ({ "akas" }));
        set("lv", 40);
        set("class","mage");
        set("ra","elf");
        set("gender","男");
        set("describe","一位嚴肅的魔導士.");
        set("pre","魔導士");
        set("age", 132);
        set("element","wind");
        set("mind", 100);

        set("teacher", ({
                23,22,21,20, 15, 13, 12,11,9, 8,3, 1, 0,
                }));
        set("favorite", ({
                "mage", "summoner",
                }));
        set("teach_mind", "-1000 ~ 1000");
        set("lv_up", ";36m阿卡斯說道:3m 好了, 等級提升了m\n");
        set("no_enu_money", ";36m阿卡斯說道:3m 景仰魔法的人啊! 你的錢不夠喔...m");
        set("no_enu_lv", ";36m阿卡斯說道:3m 你的等級還不夠!m");
        set("top_lv", ";36m阿卡斯道:3m 恭喜了,你的能力已經超越我了m");
        set("teach_message", ";36m阿卡斯說道:3m 學魔法嗎? 你想學哪項呢?m");

        set("mixHP", HP);
        set("currentHP", HP);
        set("mixMP", MP);
        set("currentMP", MP);
        set("mixSP", SP);
        set("currentSP", SP);
        set("dem", 7000);
        set("def", 10000);
        set("mdem", 17000);
        set("mdef", 12533);
        set("spe", 1030);
        set("exp", 6000);

       set_skill_level( 0, 300);                                               
        set_skill_level( 1, 300);
        set_skill_level( 2, 300);
        set_skill_level( 3, 300);
        set_skill_level( 4, 300);
        set_skill_level( 5, 300);
        set_skill_level( 6, 300);
        set_skill_level( 7, 300);
        set_skill_level( 8, 300);
        set_skill_level( 9, 300);
        set_skill_level( 10, 300);
        set_skill_level( 11, 300);
        set_skill_level( 12, 300);
        set_skill_level( 13, 300);
        set_skill_level( 14, 300);
        set_skill_level( 15, 300);
        set_skill_level( 16, 300);
        set_skill_level( 19, 300);
        set_skill_level( 20, 300);
        set_skill_level( 21, 300);
        set_skill_level( 22, 300);
        set_skill_level( 23, 300);
        set_skill_level( 24, 300);
        set_skill_level( 25, 300);
        set_skill_level( 26, 300);
        set_skill_level( 27, 300);
        set_skill_level( 30, 300);
        set_skill_level( 31, 300);

        set("skill", ({ "6:lightning light", "7:fire light" }));
        set("skill_chance", 100);

        setup();
}

void init()
{
        if( !previous_object()
                || !userp(previous_object()))
                return;
        call_out("ki", 100, previous_object());
        call_out("action", 10, previous_object());
}

void ki( object obj)
{
        if( obj->is_fighting()) kill_ob(obj);
}

void action( object obj)
{
        command("hihi ...");
}
