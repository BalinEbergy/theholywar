// lain.c
// written by Mondale
// �­�

inherit NPC;

void create()
{
        int HP= 120000, MP= 50000, SP= 60100;

        set_name("[m[1m�­�[m", ({ "lain" }));
        set("lv", 150);
        set("class","magicsword");
        set("ra","�H��");
        set("gender","�k");
        set("describe","�@��l�}�G�o�O�H�����������k�l.");
        set("pre","[m[33m�v[1m��[36m�C[32m�t[m");

        set("age", 17);
        set("element","water");
        set("mind", 1000);

        set("teacher", ({
                20,
                }));
        set("favorite", ({
                "element maker", "worrior",
                }));
        set("teach_mind", "-1000 ~ 1000");
        set("lv_up", "[1;36m�­����D:[33m ��ھǼC�k��^^..ok..�A�����Ŵ��@�F��[m\n");
        set("no_enu_money", "[1;36m�­����D:[33m�N��O�ڤ]�n�L����...���Icoco�a...^^[0m\n");
        set("no_enu_lv", "[1;36m�­����D:[33m �A�������٤���![m");
        set("top_lv", "[1;36m�­��D:[33m�u�O�F�`��.�A����O�w�g�W�V�ڤF*[m");
        set("teach_message", "[1;36m�­����D:[33m �A�n��ھǤ����?[m\n");
        set_skill_level( 20, 100);
        set("skill",({
                "20:slash void",
                }));
        set("skill_chance",50);

        set("mixHP", HP);
        set("currentHP", HP);
        set("mixMP", MP);
        set("currentMP", MP);
        set("mixSP", SP);
        set("currentSP", SP);
        set("dem", 26000);
        set("def", 70000);
        set("mdem", 35000);
        set("mdef", 75000);
        set("spe",2000);
        set("realmx", 500);
        set("exp", 125000);
        set("chat_chance", 20);
        set("chat_msg", ({
                "\n�­���A�L�L�@���C\n"
                }));
        setup();
}

void init()
{
        ::init();
        if( !previous_object()
                || !userp(previous_object()))
                return;
        call_out("action", 1, previous_object());
}

void action( object obj)
{
        command("say �A��Ө�o�̫K�O���t�A�ڦ�������������ܡH");
        setup();
}
