// light_ball.c
// written by Hudson

#include <skill.h>

void create() { seteuid( getuid()); }

int main( object me, object obj)
{
        object ball;

        ball= new( OB_SPECIAL+ "light_ball");
        ball->move( environment( me));
        ball->set("epower", me->skill_level( LIGHT));
        call_out( "disappear", me->skill_level( LIGHT)* 15, ball);

        message_vision("一顆耀眼的光球出現在$N的眼前!!\n", me);

        return 1;
}

void disappear( object ball)
{
        ball->disappear( ball);
}

string name( int select)
{
        switch( select)
        {
                case 1: return "光球術";
                case 2: return "light ball";
                case 3: return "light_ball";
        }
}

int type() { return LIGHT; }
int lv() { return 1; }
int attackful()
{
        return 0;
}

string mes( object me)
{
        return "喃喃唸著一串咒語,身旁發出微微的亮光...";
}

int usesp() { return 0; }
int usemp() { return 15; }
int successful() { return 90; }
int delay() { return 2; }
int all() { return 0; }

int check( object me)
{
        return 1;
}
