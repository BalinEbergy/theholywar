// shutdown.c

void shut_down()
{
    seteuid( getuid());
    shutdown( 0);
}

int main( object me, int argc, string *argv)
{
    if( argc< 2)
        write("要在幾秒後關閉"+ MUD_NAME+ "?\n");
    else call_out("shut_down", atoi( argv[1]));

    return 1;
}

int help( object me)
{
    write( @HELP
指令格式: shutdown <秒數>

在<秒數>後關閉本 MUD
HELP
        );
    return 1;
}
