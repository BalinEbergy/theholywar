// bee.c
// written by Mondale
// 蜜蜂

inherit NPC;

void create()
{
        int HP= 40, MP= 0, SP= 100;

        set_name("蜜蜂", ({ "bee" }));
        set("lv", 1);
        set("class","warrior");
        set("ra","蜜蜂");
        set("gender","男");
        set("describe","正在努力採著花粉的蜜蜂.");
        set("pre","飛來飛去的");

        set("age", 1);
        set("element","wind");
        set("mind", 0);

        set("mixHP", HP);
        set("currentHP", HP);
        set("mixMP", MP);
        set("currentMP", MP);
        set("mixSP", SP);
        set("currentSP", SP);
        set("dem", 20);
        set("def", 0);
        set("mdem", 0);
        set("mdef", 0);
        set("spe", 20);
        set("realmx", 1);
        set("exp", 100);

        set("chat_chance", 20);
        set("chat_msg", ({
                (: this_object(), "random_move" :),
                "\n蜜蜂不停的飛來飛去\n"
                }));
        setup();
}

void init()
{
        ::init();
        if( !previous_object()
                || !userp(previous_object()))
                return;
       
}
