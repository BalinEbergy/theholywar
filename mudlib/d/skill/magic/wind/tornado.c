// tornado.c
// written by Hudson

#include <skill.h>

int main( object me, object obj)
{
        int default_d= 87, demage, eff, i= 2, mdem, mdef;

        mdem= me->query("mdem")+ me->query("mdem_eff");
        mdef= obj->query("mdef")+ obj->query("mdef_eff");

        default_d= default_d+ default_d* me->skill_level( WIND)/ 33;

        if( me->query("element")== "wind")
                demage= default_d* mdem* 1.2/ mdef;
        else
                demage= default_d* mdem/ mdef;

        if( obj->query("element")== "earth") demage= demage* 1.2;

        eff= obj->query("wind_eff");

        while( i--)
        {
                demage= demage* ( 70+ random( 60))/ 100*( 100- eff)/ 100;
                obj->add("currentHP", -demage);
                if( i)
                        COMBAT_D->skill_msg( me, obj, "龍捲風", "none", demage);
        }

        return demage;
}

string name( int select)
{
        switch( select)
        {
                case 1: return "龍捲風";
                case 2: return "torando";
                case 3: return "torando";
        }
}

int type() { return WIND; }
int lv() { return 40; }
int attackful()
{
        return 1;
}

string mes( object me)
{
        return "喃喃的唸出一段咒語,瞬間四周產生一陣強烈的[1;36m龍捲風!![m";
}

int usesp() { return 0; }
int usemp() { return 180; }
int successful() { return 65; }
int delay() { return 2; }
int all() { return 0; }
string element() { return "wind"; }

int check( object me)
{
        return 1;
}
