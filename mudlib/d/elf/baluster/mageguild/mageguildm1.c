// mageguildm1.c
// written by Hudson

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[36m魔法師公會會所[m");
        set("long",@LONG
這兒是魔法師公會會所西方,這兒放置著一杯水,水面看似已
經凸出來了,但是水卻一滴也沒有溢出來,你用手去推了推,但
液面似乎早已凍結了一般絲毫沒有影響.
LONG
        );

        set("exits", ([
                "north"  : __DIR__ + "mageguildn1",
                "east"   : __DIR__ + "mageguildc",
                "south"  : __DIR__ + "mageguilds1",
                ]));
}
