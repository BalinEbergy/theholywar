// long_blade.c
// written by Hiwind

#include <equip.h>
#include <element.h>

inherit EQUIP;

void create()
{
        set_name("封印之刀", ({ "sss blade", "sss", "blade" }));
        set("long", @LONG
傳說中為了封印傳說鬥士過於強大的力量而鍛造的刀,
據說有著強大的隱藏力量.
LONG
                );
        set("lv", 99);
        set("unit", "把");
        set("weight", 500);
        set("element", EVIL);
        set("epower", 700);

        set("wear_type", HAND);
        set("weapon", BLADE);
        set("dem_eff", -20000);
        set("spe_eff", 1500);
        set("eff", ({ "dem_eff", "spe_eff" }));
}
