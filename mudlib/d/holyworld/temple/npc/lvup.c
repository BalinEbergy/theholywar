// lvup.c
// written by Hudson
// �ɯť]

inherit NPC;

void create()
{
        set_name("�ɯť]", ({ "lvup" }));
        set("lv", 1);

        set("maxHP", 1);
        set("currentHP", 1);
        set("maxSP", 1);
        set("currentSP", 1);
        set("mind", 0);
        set("exp", 214748364);
        setup();
}
