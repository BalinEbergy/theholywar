// earth_shild.c
// written by Hudson

#include <skill.h>

int main( object me, object obj)
{
    string *effects;

    if( obj->query("e_shieldt")) return 0;
    else
    {
        effects= obj->query("effect");
        obj->set("e_shield", ({ "defand_eff:"+ me->skill_level( 9)* 10, }));
        obj->set("e_shieldt", me->skill_level( 9));
        obj->add("defand_eff", me->skill_level( 9)* 10);
        if( effects)
            effects+= ({ "e_shield" });
        else effects= ({ "e_shield" });
        obj->set("effect", effects);
    }

    tell_object( obj, "\n你覺得身體受到保護.\n");

    return 1;
}

string name( int select)
{
    switch( select)
    {
        case 1: return "土盾術";
        case 2: return "earth shield";
        case 3: return "earth_shild";
    }
}

int type() { return EARTH; }
int lv() { return 1; }
int attackful()
{
    return 0;
}

string mes( object me)
{
    return "唸完咒語,只見地上漸漸隆起,然後便是一陣[5m光芒!![m";
}

int usesp() { return 0; }
int usemp() { return 20; }
int successful() { return 70; }
int delay() { return 1; }
int all() { return 0; }
string element() { return "earth"; }

int check( object me)
{
    return 1;
}
