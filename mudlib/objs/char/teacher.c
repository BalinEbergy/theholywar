// teacher.c

inherit NPC;

int *favorite= ({ });

int is_teacher() { return sizeof( query_skill_list()); }

void set_favorite( int *f) { favorite= f; }
int *query_favorite() { return favorite; }

void setup()
{
    mapping dbase;
    ::setup();

    dbase= query_entire_dbase();
    if( undefinedp( dbase["teach_mind"])) dbase["teach_mind"]= "-1000 ~ 1000";
    if( undefinedp( dbase["teach_msg_pure"])) dbase["teach_msg_pure"]= "你不夠邪惡.\n";
    if( undefinedp( dbase["teach_msg_evil"])) dbase["teach_msg_evil"]= "你不夠善良.\n";
    if( undefinedp( dbase["lv_up"])) dbase["lv_up"]= "你的等級提昇了.\n";
    if( undefinedp( dbase["no_enu_money"])) dbase["no_enu_money"]= "錢不夠囉.\n";
    if( undefinedp( dbase["no_enu_lv"])) dbase["no_enu_lv"]= "你等級不夠.\n";
    if( undefinedp( dbase["top_lv"])) dbase["top_lv"]= "我不能再教你什麼了~\n";
    if( undefinedp( dbase["teach_message"])) dbase["teach_message"]= "我會這些技能:\n";
}

void list_skill()
{
    int i, j, times, cls;
    string *temp, *skill_type, *skills, sk_tmp;
    string str= "";

    cls= previous_object( 2)->query("class");
    i= sizeof( favorite);
    while( i--)
        if( favorite[i]== cls)
            break;
    if( i== -1) times= 10;
    else times= 1;

    skill_type= query_skill_list();
    i= sizeof( skill_type);
    while( i--)
    {
        str+= "\n[1;33m"+ D_SKILL->skill_title( skill_type[i])+ "[m -\n";
        temp= query_skills( skill_type[i]);
        j= sizeof( temp);
        while( j--)
        {
            sk_tmp= D_SKILL->get_skill( temp[j]);
            str+= sprintf("%-23s %-22s LV %3d   花費 %d\n",
                sk_tmp->name(), sk_tmp->query_id(), sk_tmp->query("lv"), sk_tmp->query("cost")* times);
        }
    }

    write( str+ "\n");
}

void teach( object obj, string arg)
{
    int i, lv, cls, times;
    string sk, *skill_type;

    cls= previous_object( 2)->query("class");
    i= sizeof( favorite);
    while( i--)
        if( favorite[i]== cls)
            break;
    if( i== -1) times= 10;
    else times= 1;

    if( lv= query_skill_level( arg))
    {
        if( lv== ( lv= obj->query_skill_level( arg)))
            command("tell "+ obj->query("id")+ " "+ query("top_lv"));
        else if( obj->query_skill_exp( arg)< lv* lv* 100)
            command("tell "+ obj->query("id")+ " "+ "你的經驗不夠!");
        else if( obj->query("money")< lv* lv* 10* times)
            command("tell "+ obj->query("id")+ " "+ query("no_enu_money"));
        else {
            if( !lv)
                if( obj->query("lpoint")== 0)
                {
                    tell_object( obj, "你記不起來新的技能了!\n");
                    return;
                } else obj->add("lpoint", -1);

            obj->add("money", -lv* lv* 10* times);
            obj->add_skill_level( arg);
            command("tell "+ obj->query("id")+ " "+ query("lv_up"));
        }

        return;
    }

    skill_type= query_skill_list();
    i= sizeof( skill_type);
    while( i--)
    {
        if( stringp(
            sk= D_SKILL->find_skill( arg,
                query_skills( skill_type[i])
                )
            )
        )
        {
            if( obj->query("money")< sk->query("cost")* times)
                command("tell "+ obj->query("id")+ " "+ query("no_enu_money"));
            else {
                switch( obj->add_skill( sk->query("id")))
                {
                    case 2:
                    case 3:
                        command("tell "+ obj->query("id")+ " "+ query("no_enu_lv"));
                        break;
                    case 4:
                        tell_object( obj, "你已經學過了"+ sk->name()+ "了唷!\n");
                        break;
                    default:
                        obj->add("money", -sk->query("cost")* times);
                        tell_object( obj,
                            name()+ "傳授了你"+ sk->name()+ "的精要, 你學會了"+ sk->name()+ "!!\n");
                }
            }

            return;
        }
    }

    command("tell "+ obj->query("id")+ " 我不會這個技能喔!");
}
