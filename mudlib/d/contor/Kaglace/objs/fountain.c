// fountain.c
// written by Bother

inherit ITEM;

void create()
{
        set_name("噴水池", ({ "fountain" }));
        set("long", @LONG
一座美麗壯觀的噴水池呈現在你面前.
LONG
                );
        set("unit", "座");
        set("element", "light");
        set("epower", 10);
        set("d_water", 1);
        set("no_get", 1);
        set("unsac", 1);
}
