// frog.c
// written by hiwind

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
    int HP= 3200, MP= 200,SP= 1000;

    set_name("���c���C��", ({ "frog" }));
    set("lv", 13);
    set("describe","�C��.");
    set("age", 5);
    set("race", ANIMAL);
    set("element", WATER);
    set("mind", -400);

    set("maxHP", HP);
    set("currentHP", HP);
    set("maxMP", MP);
    set("currentMP", MP);
    set("maxSP", SP);
    set("currentSP", SP);
    set("strength", 360);
    set("armor", 120);
    set("spi", 16);
    set("speed", 200);
    set("exp", 1600);
    setup();
}
