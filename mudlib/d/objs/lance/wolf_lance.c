// wolf_lance.c
// written by zz

#include <equip.h>
#include <element.h>

inherit EQUIP;

void create()
{
        set_name("[1;31m�ӭ���v�u[m", ({ "wolf lance", "lance", "wolf" }));
        set("long", @LONG
�@��[1;31mCable[m�M�Ϊ����u
�۶ǬO�ѯ��ҽ�A�@��L�Ī��j.... :)
LONG
                );
        set("lv", 100);
        set("unit", "��");
        set("element", EVIL);
        set("epower", 10000);

        set("wear_type", HAND);
        set("weapon", LANCE);
        set("dem_eff",10000);
        set("spe_eff", 1000);
        set("defand_eff", 2000000);
        set("mdem_eff", 500000);
        set("mdef_eff", 200000);
        set("eff", ({ "dem_eff", "spe_eff" }));

        set_weight( 1000);
}
