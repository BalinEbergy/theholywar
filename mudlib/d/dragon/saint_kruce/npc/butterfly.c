// butterfly.c
// written by zz

#include <class.h>
#include <element.h>
#include <race.h>

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
        int HP= 480, MP= 0, SP= 1000;

        set_name("蝴蝶", ({ "butterfly"}));
        set("lv", 2);
        set("race","animal");
        set("gender","動物");
        set("describe","一隻蝴蝶.");
        set("pre","輕巧飛舞的");
        set("age", 2);
        set("element",EARTH);
        set("mind", 10);

        set("maxHP", HP);
        set("currentHP", HP);
        set("maxMP", MP);
        set("currentMP", MP);
        set("maxSP", SP);
        set("currentSP", SP);
        set("dem", 40);
        set("def", 25);
        set("mdem", 25);
        set("mdef", 25);
        set("spe", 40);
        set("exp", 280);

        set("chat_chance", 5);
        set("chat_msg", ({
                (: random_move :)
                }));
        setup();
}
