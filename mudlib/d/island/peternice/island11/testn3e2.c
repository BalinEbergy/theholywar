// center.c
// written by peterenice

#include <room.h>

inherit ROOM;

void create()
{
        set("short","*[1;37m......*[m");
        set("long",@LONG
.........
LONG
        );

        set("exits",
        ([
        "north" : __DIR__ + "testn4e2",
        "west" : __DIR__ + "testn3e1", 
       ])
        );
