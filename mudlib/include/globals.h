// globals.h

// MUD name

#define MUD_NAME        "�Ѧa�t��"

// Directories

#define DIR_HELP        "/docs/"
#define DIR_LOG         "/log/"

// Daemons

#define D_CHANNEL       "/adm/daemons/channel"
#define D_COMBAT        "/adm/daemons/combat"
#define D_COMMAND       "/adm/daemons/command"
#define D_LEVEL         "/adm/daemons/level"
#define D_LOGIN         "/adm/daemons/login"
#define D_SKILL         "/adm/daemons/skill"
#define D_STATUS        "/adm/daemons/status"
#define D_TRANS         "/adm/daemons/transport"
#define D_WIZARD        "/adm/daemons/wizard"

// Features

#define F_ATTACK        "/features/char/attack"
#define F_AUTOLOAD      "/features/autoload"
#define F_COMMAND       "/features/char/command"
#define F_DBASE         "/features/dbase"
#define F_EQUIP         "/features/equip"
#define F_GROUP         "/features/char/group"
#define F_MESSAGE       "/features/char/message"
#define F_MOVE          "/features/move"
#define F_NAME          "/features/name"
#define F_SAVE          "/features/save"
#define F_SKILL         "/features/char/skill"
#define F_TREEMAP       "/features/treemap"

// Standard Objects

#define O_SIMUL_EFUN    "/adm/system/simul_efun"
#define O_VOID          "/d/holyworld/temple/center"
#define O_USER          "/objs/user"

// Main object types to be inherited

#define CHARACTER       "/objs/char"
#define EQUIP           "/objs/equip"
#define FOOD            "/objs/food"
#define ITEM            "/objs/item"
#define NPC             "/objs/char/npc"
#define TEACHER         "/objs/char/teacher"
#define ROOM            "/objs/room"

// Object directories

#define OB              "/d/objs/"
#define OB_SIGN         "/d/objs/sign/"
#define OB_SWORD        "/d/objs/sword/"
#define OB_AXE          "/d/objs/axe/"
#define OB_LANCE        "/d/objs/lance/"
#define OB_BLADE        "/d/objs/blade/"
#define OB_DRUM         "/d/objs/drum/"
#define OB_SHIELD       "/d/objs/shield/"
#define OB_ARMOR        "/d/objs/armor/"
#define OB_ROBE         "/d/objs/robe/"
#define OB_BOOT         "/d/objs/boot/"
#define OB_NECK         "/d/objs/neck/"
#define OB_SPECIAL      "/d/objs/special/"
#define OB_OTHER        "/d/objs/others/"
#define OB_SUM          "/d/objs/summoned-mob/"
#define OB_FOOD         "/d/objs/food/"
#define OB_KEY          "/d/objs/key/"
#define OB_BOX          "/d/objs/box/"
#define OB_MAP          "/d/objs/map/"

// Wizard

#define ADMIN           3
#define IMM             2
