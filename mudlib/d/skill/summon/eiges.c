// eiges.c
// written by Hudson

#include <skill.h>

void create() { seteuid( getuid()); }

int main( object me, object obj)
{
        int HP, MP, SP, lv;
        object sum;

        lv= me->skill_level( SUMMON);

        HP= 135+ lv* 500;
        MP= 135+ lv* 10;
        SP= MP;
        sum= new( OB_SUM+ "eiges");
        sum->set("lv", lv);
        sum->set("mixHP", HP);
        sum->set("currentHP", HP);
        sum->set("mixMP", MP);
        sum->set("currentMP", MP);
        sum->set("mixSP", SP);
        sum->set("currentSP", SP);
        sum->set("def", 10000+ lv* 100);
        sum->set("mdem", 135+ lv* 250);
        sum->set("mdef", 1000+ lv* 100);
        sum->set("spe", lv* 10);
        sum->set("realmx", lv);
        sum->set("weight", 337);
        sum->move( environment( me));
        FOLLOW->follow( sum, me);

        tell_room( environment( me),
                "�|�P�G�_[1;33m�ƹD���~[m,�ū׺����W��,���ΨȦN���X�{�b[1;33m���~[m����!!\n");

        return GROUP->bond( me, sum);
}

string name( int select)
{
        switch( select)
        {
                case 1: return "�ȦN���l��";
                case 2: return "eiges";
                case 3: return "eiges";
        }
}

int type() { return SUMMON; }
int lv() { return 7; }
int attackful()
{
        return 0;
}

string mes( object me)
{
        return "�f����ۥj�Ѫ��G�y...";
}

int usesp() { return 0; }
int usemp() { return 50; }
int successful() { return 70; }
int delay() { return 1; }
int all() { return 0; }
string element() { return "fire"; }

int check( object me)
{
        int i;
        object *sum;

        sum= me->query_temp("pet");
        i= sizeof( sum);
        while( i--)
        {
                if( sum[i]->query("id")== "eiges")
                        return 0;
        }

        return 1;
}
