// fish2.c
// writtten by hiwind

inherit NPC;

void create()
{
        int HP= 20000, MP= 1000 ,SP= 4000;

        set_name("���F��", ({ "fish" }));
        set("lv", 35);
        set("describe","��.");
        set("element", "water");
        set("ra", "animal");
        set("age", 5);
        set("mind", -500);

        set("mixHP", HP);
        set("currentHP", HP);
        set("mixMP", MP);
        set("currentMP", MP);
        set("mixSP", SP);
        set("currentSP", SP);
        set("dem", 1400);
        set("def", 400);
        set("mdem", 800);
        set("mdef", 300);
        set("spirit", 20);
        set("spe", 250);
        set("exp", 1500);
        setup();
}
