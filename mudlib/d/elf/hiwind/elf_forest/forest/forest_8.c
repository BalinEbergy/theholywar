//forest_8.c
//write by hiwind

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;37m�L��[m");
        set("long",@LONG
���i�F[1;33;44m���F[m[1;32m�˪L[m���L�ϲ`�B,�N��o��[�w�R�F,�ȫȪ��ܼv
�w�g�X�G�ݤ���F,�u���ʪ����X�S,���B��[1;31m��[m[1;32m�L[m������j��
�ۦP,���M���O�p������,����o�Q�����A��.�ѩ�L��Q���Z
�K,�ҥH�����u��q�@�ǵ}�\�����_���S�F�X��,���b�𽮤U
��o�Q�������D�P�ξA.
LONG
        );

        set("exits",
        ([
        "east" : __DIR__ +  "forest_13",
        "south" : __DIR__ + "forest_7",
        "north" : __DIR__ + "forest_9",
        ])
        );
        set("objects",
        ([
                __DIR__ + "npc/eagle":1,
                __DIR__ + "npc/deer":1,
                __DIR__ + "npc/bear":2,
        ])
        );
        set("no_fight", 0);
        setup();
}
