// bado.c
// written by Hudson

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
    int HP= 60000, MP= 1500, SP= 800;

    set_name("巴杜", ({ "bado" }));
    set("lv", 49);
    set("class", WIZARD);
    set("race", ELF);
    set("gender", 1);
    set("describe","一位有些發福的廚師,餐\館的負責人.");
    set("pre", "餐\廳老闆");

    set("age", 189);
    set("element", THUNDER);
    set("mind", 9);

    set("maxHP", HP);
    set("currentHP", HP);
    set("maxMP", MP);
    set("currentMP", MP);
    set("maxSP", SP);
    set("currentSP", SP);
    set("strength", 3000);
    set("armor", 8231);
    set("magic", 7200);
    set("spi", 8);
    set("speed", 930);
    set("exp", 2000);

    set("trading", 205);
    set("buy_mind", "-1000~1000");
    set("selling", ({
        OB_FOOD + "bring",
        OB_FOOD + "beef",
        OB_FOOD + "quick",
        OB_FOOD + "happy",
        }));

    setup();
}
