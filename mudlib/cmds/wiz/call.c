// call.c

int main( object me, int argc, string *argv)
{
    object obj;

    if( argc< 2)
        write("你想要召喚誰呢?\n");
    else if( !objectp( obj= find_player( argv[1])))
        write("沒有這個人.\n");
    else {
        message_vision("[36m$N喃喃念著彷彿不存在世間的咒語...\n[m", me);
        tell_object( obj, "你感到有一股[1;31m莫名的力量[m在召喚著你...\n");
        if( !obj->move( environment( me)))
            message_vision("[1;37m$N瞬間出現在$n的跟前!![m\n", obj, me);
    }

    return 1;
}

int help( object me)
{
    write( @HELP
指令格式: call <玩家>

將指定玩家召喚到面前.
HELP
        );

    return 1;
}
