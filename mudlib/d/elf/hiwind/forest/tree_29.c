//tree_29.c
//write by hiwind

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;37m�L�ϲ`�B[m");
        set("long",@LONG
�A�{�b�B��L�����`�B,�o��S�O���P��~�򪺴���,���e
�Ҭݨ���[1;31m��[m�R�]�]���L���Ӯ����F,���B�@���w�R,�ϧA�]
���o���_�C�֪��߱�,�µM�_�q,�o�i�O�m�\���n
�a��.�n�n�M�ߧa!!
LONG
        );

        set("exits",
        ([
        "west" : __DIR__ + "tree_24",
        "south" : __DIR__ + "tree_28",
        "north" : __DIR__ + "tree_30",
        "east" : __DIR__ + "tree_33",
        ])
        );

        set("objects",
        ([
                __DIR__ + "npc/mage":2,
                __DIR__ + "npc/wolf":1,
        ])
        );
        set("no_fight", 0);
        setup();
}
