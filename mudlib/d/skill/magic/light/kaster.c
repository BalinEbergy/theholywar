// kaster.c
// written by Hudson

#include <skill.h>

int main( object me, object obj)
{
        int default_d= 25, demage, eff, mdem, mdef;

        mdem= me->query("mdem")+ me->query("mdem_eff");
        mdef= obj->query("mdef")+ obj->query("mdef_eff");

        default_d= default_d+ default_d* me->skill_level( LIGHT)/ 33;

        if( me->query("element")== "light")
                demage= default_d* mdem* 1.2/ mdef;
        else
                demage= default_d* mdem/ mdef;

        if( obj->query("element")== "dark") demage= demage* 1.2;

        eff= obj->query("light_eff");

        demage= demage* ( 90+ random( 20))/ 100*( 100- eff)/ 100;

        obj->add("currentHP", -demage);

        return demage;
}

string name( int select)
{
        switch( select)
        {
                case 1: return "卡士特光球術";
                case 2: return "kaster light";
                case 3: return "kaster";
        }
}

int type() { return LIGHT; }
int lv() { return 5; }
int attackful()
{
        return 1;
}

string mes( object me)
{
        return "口中喃喃唸道: [1;33m高諸凱基哿的卡士特呀...請借給我力量...[m\n"+
                "就在這一瞬間,$N手上聚集出了極具攻擊性的光球!!";
}

int usesp() { return 0; }
int usemp() { return 40; }
int successful() { return 100; }
int delay() { return 1; }
int all() { return 0; }
string element() { return "light"; }

int check( object me)
{
        return 1;
}
