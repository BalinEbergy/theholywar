// sground3.c
// written by Hudson

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;32m巴路斯特廣場[m");
        set("long",@LONG
這裡是巴路斯特廣場,四周是由淡綠色的石板所鋪成,你覺得
精神舒服許多,往來的精靈們神態悠然,讓你覺得這裡的一切
都是那麼的祥和.
LONG
        );

        set("exits",
        ([
        "north" : __DIR__ + "eground",
	"west"	: __DIR__ + "sground1",
        ])
        );
}
