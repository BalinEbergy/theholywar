// sarlin.c
// written by Hudson
// 莎琳

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
    int HP= 213020, MP= 23000, SP= 1700;

    set_name("莎琳", ({ "sarlin" }));
    set("lv", 88);
    set("class", WIZARD);
    set("race", ELF);
    set("gender",0);
    set("describe","一位有著明澈雙眼的女巫.");
    set("pre","水法師");

    set("age", 130);
    set("element",WATER);
    set("mind", -599);

    set("maxHP", HP);
    set("currentHP", HP);
    set("maxMP", MP);
    set("currentMP", MP);
    set("maxSP", SP);
    set("currentSP", SP);
    set("strength", 3000);
    set("armor", 20323);
    set("magic", 32900);
    set("spi", 22);
    set("speed", 2535);
    set("exp", 247000);

    setup();
}

void init()
{
    object obj;

    if( !(obj= previous_object())
        || !userp( obj))
        return;

    call_out("ki", 10, obj);
    call_out("action", 20, obj);
}

void action( object obj)
{
    if( !obj || !present(obj) || this_object()->is_fighting()) return;
    command("say 水是滋養萬物的泉源, 下雨的天空, 最美麗了");
}

void ki( object obj)
{
    if( obj->is_fighting()) kill_ob( obj);
}
