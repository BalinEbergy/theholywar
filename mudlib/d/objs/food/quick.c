// quick.c
// written by Mondale

#include <element.h>

inherit FOOD;

void create()
{
    set_name("速食簡單套餐\", ({ "quick bar", "quick", "bar" }));
    set("long", @LONG
聖德斯平價的套餐,便宜又好吃,可惜保存不久,適合在店內食用.
LONG
        );
    set("unit", "份");
    set("lv", 5);
    set("value", 6);
    set("no_save", 1);

    set_amount( 4);

    set_weight( 13);
    call_out("disappear", 5000);
}

void disappear()
{
    tell_object( environment( this_object()), name()+ "腐敗了.\n");
    destruct( this_object());
}
