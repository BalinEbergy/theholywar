// kill.c

int main( object me, int argc, string *argv)
{
    object obj;
    string *killer, callname;

    if( environment(me)->query("no_fight") )
        write("這裡的氣氛讓你提不起勁兒來戰鬥.\n");
    else if( me->is_ghost())
        write("你無法戰鬥...\n");
    else if( me->is_fighting())
        write("你早就在戰鬥了!\n");
    else if( argc< 2)
        write("你想殺誰?\n");
    else if( !objectp( obj= find( argv[1], environment( me))) ||
        !obj->is_character() || obj->is_ghost())
        write("這裡沒有這個人.\n");
    else if( obj== me)
        write("你想殺自己嗎? @@\n");
    else if( userp( obj) && ( me->query("lv")- obj->query("lv")> 6 || obj->query("lv")< 10))
        write("你不能攻擊低等級玩家...\n");
    else {
        message_vision("$N對著$n喝道: " 
            + obj->query("name") + "! [1;37;5m去死吧你~[m\n\n", me, obj);
        me->kill_ob( obj);
    }

    return 1;
}

int help(object me)
{
    write( @HELP

指令格式: kill <人物>

戰鬥指令, 用來與玩家或 mob 戰鬥.

HELP
    );
    return 1;
}
 
