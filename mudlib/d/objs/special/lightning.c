// lightning.c
// written by Hudson

#include <element.h>

inherit ITEM;

void create()
{
    set_name("雷", ({ "lightning" }));
    set("long", @LONG
數道電光在空中閃爍.
LONG
        );
    set("lv", 1);
    set("element", THUNDER);
    set("epower", 100);

    set("unsac", 1);
    set("no_get", 1);
}

void disappear()
{
    message_vision("閃閃的$N電在空中消失了!\n", this_object());
    environment( this_object())->delete("lightning");
    destruct( this_object());
}
