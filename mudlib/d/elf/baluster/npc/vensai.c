// vensai.c
// written by Hudson
// 溫薩依

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
    int HP= 223400, MP= 34500, SP= 4200;

    set_name("溫薩依", ({ "vensai" }));
    set("lv", 88);
    set("class",WIZARD);
    set("race",DRAGON);
    set("gender",0);
    set("describe","一位清秀的女法師.");
    set("pre","風之法師");

    set("age", 211);
    set("element",WIND);
    set("mind", 799);

    set("teacher", ({
        4,
        }));
    set("favorite", ({
        WIZARD,
        }));
    set("teach_mind", "700 ~ 1000");
    set("teach_msg_evil", "[1;35m溫薩依說道:[33m 我是不教邪惡之徒的喔![m");
    set("lv_up", "[1;35m溫薩依說道:[33m 升級啦![m\n");
    set("no_enu_money", "[1;35m溫薩依說道:[33m 抱歉, 你帶的錢不夠呦.[m");
    set("no_enu_lv", "[1;35m溫薩依說道:[33m 你的能力還太差...[m");
    set("top_lv", "[1;35m溫薩依道:[33m 我沒有什麼能再教你嚕...恭喜啦![m");
    set("teach_message", "[1;35m溫薩依說道:[33m 想學大氣的魔法呀...[m");

    set("maxHP", HP);
    set("currentHP", HP);
    set("maxMP", MP);
    set("currentMP", MP);
    set("maxSP", SP);
    set("currentSP", SP);
    set("strength", 8000);
    set("armor", 20000);
    set("magic", 29000);
    set("spi", 34);
    set("speed", 3200);
    set("exp", 247000);

    setup();
}

void init()
{
    object obj;

    if( !(obj= previous_object())
        || !userp( obj))
        return;

    call_out("ki", 10, obj);
    call_out("action", 20, obj);
}

void action( object obj)
{
    if( !obj || !present(obj) || this_object()->is_fighting()) return;
    command("hihi 美麗的春天,最適合風的喜讚了...");
}

void ki( object obj)
{
    if( obj->is_fighting()) kill_ob( obj);
}

int teach_check( object me)
{
    if( me->query("neckr")== "mage permit");
    else if( me->query("neckl")== "mage permit");
    else
    {
        command("say 你還未成為真正的魔法師, 很抱歉我不能教你任何東西...");
        return 1;
    }

    return 0;
}
