// kasall.c
// written by Hudson
// 卡薩爾•多倫

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
    int HP= 600000, MP= 2000, SP= 32000;

    set_name("旅客", ({ "traveller", "kasall doren", "kasall", "doren" }));
    set("lv", 84);
    set("class", MINSTREL);
    set("race", OGRE);
    set("gender", 1);
    set("describe","一位背著大鼓,身材高壯的旅客.");
    set("pre","背著大鼓的");

    // 老師資訊
    set("teacher", ({
        11, 14, 15,
        }));
    set("favorite", ({
        MINSTREL, WIZARD,
        }));
    set("teach_mind", "225 ~ 1000");
    set("teach_msg_evil", "[1;36m旅客說道:[33m 邪惡的人啊...你還想從我這裡學到東西嗎...[m");
    set("lv_up", "[1;36m旅客哈哈的笑道:[33m 好了, 等級提升了吧![m\n");
    set("no_enu_money", "[1;36m旅客呵呵的笑道:[33m 就算是我也不做白工的喔...[m");
    set("no_enu_lv", "[1;36m旅客笑道:[33m 你的等級還不夠喔...[m");
    set("top_lv", "[1;36m旅客呵呵的笑道:[33m 我已經不能再教你什麼囉...[m");
    set("teach_message", "[1;36m旅客哈哈的笑道:[33m 想找我學技能啊! 你想學什麼嗎?[m");
    // ---------

    set("age", 379);
    set("element", EARTH);
    set("mind", 739);
    set("spirit", 50);

    set("maxHP", HP);
    set("currentHP", HP);
    set("maxMP", MP);
    set("currentMP", MP);
    set("maxSP", SP);
    set("currentSP", SP);
    set("strength", 29000);
    set("armor", 15000);
    set("magic", 12000);
    set("spi", 28);
    set("speed", 1797);
    set("exp", 243328);

    set("chat_chance", 13);
    set("chat_msg", ({
        "旅客愉快的唱著歌~\n",
        "旅客哈哈大笑著.\n",
        (: random_move :)
        }));

    setup();

    carry_object( OB_DRUM+ "giant_drum")->equip();

}

void init()
{
    object obj;

    if( !(obj= previous_object())
        || !userp( obj))
        return;

    call_out("ki", 100, obj);
    call_out("action", 20, obj);
}

void action( object obj)
{
    if( this_object()->is_fighting()) return;
    if( random(1)) command("say 這兒的環境真不錯...");
}

void ki( object obj)
{
    if( this_object()->is_fighting()) return;
    if( obj->is_fighting()) kill_ob( obj);
}
