//forest_37.c
//write by hiwind

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;37m�L��[m");
        set("long",@LONG
���}�F�L�����D,���i�F[1;33;44m���F[m[1;32m�˪L[m���L��,���B��[1;31m��[m[1;32m�L[m������
�j���ۦP,���M���O�p������,����o�Q�����A��.�ѩ�L�즳
�ǭZ�K,�ҥH�����u��q�@�ǵ}�\�����_���S�F�X��,�򤧫e
���L�������,�𽮤U�N��o�Q�������D.
���ӬO���񦳬y���a,���B�g���ۤ@�ѲH�H��[1;36m����[m,�ϱo�L��
������[1;37m�Y[1;5;37m��[m[1;37m�Y[1;5;37m�{[m,�ϩ��m���ڹ�,�O�p���������P���R.
����O�L�����D,�F�䦳�@���������m���Z�����.
LONG
        );

        set("exits",
        ([
        "west" : __DIR__ + "forest_28",
        "east" : "d/elf/hiwind/elf_forest/grass_8",
        "south" : __DIR__ + "forest_36",
        "north" : __DIR__ + "forest_38",
        ])
        );

        set("objects",
        ([
                __DIR__ + "npc/eagle":1,
                __DIR__ + "npc/bear":2
        ])
        );
        set("no_fight", 0);
        setup();
}
