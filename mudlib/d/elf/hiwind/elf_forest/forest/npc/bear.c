// eagle.c
// written by hiwind

inherit NPC;

void create()
{
        int HP= 130000, MP= 1000,SP= 3500;

        set_name("�@�����N", ({ "eagle" }));
        set("lv", 56);
        set("describe","�N.");
        set("age", 5);
        set("element", "air");
        set("mind", 0);

        set("mixHP", HP);
        set("currentHP", HP);
        set("mixMP", MP);
        set("currentMP", MP);
        set("mixSP", SP);
        set("currentSP", SP);
        set("dem", 6000);
        set("def", 1800);
        set("mdem", 200);
        set("mdef", 400);
        set("spirit", 20);
        set("spe", 260);
        set("exp", 3250);
        setup();
        set("chat_chance", 25);
        set("chat_msg", ({
                (: this_object(), "random_move" :)}));

}
