// wedgell.c
// written by Hudson
// 威吉爾

#include <class.h>
#include <element.h>
#include <race.h>

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
        int HP= 180000, MP= 100, SP= 5400;

        set_name("威吉爾", ({ "wedgell" }));
        set("lv", 50);
        set("class",WARRIOR);
        set("race",OGRE);
        set("gender",1);
        set("describe","卡格爾城下町警備隊隊長,看來有些肥胖,面容和藹.");
        set("pre","卡格爾城下警備隊長");
        set("age", 203);
        set("element",EARTH);
        set("mind", 200);

        set("teacher", ({
                20, 21, 22, 23,
                }));
        set("favorite", ({
                WARRIOR,
                }));
        set("teach_mind", "-1000 ~ 1000");
        set("lv_up", "[1;36m威吉爾說道:[33m 小兄弟, 你的等級提升啦![m\n");
        set("no_enu_money", "[1;36m威吉爾說道:[33m 小兄弟, 天下沒有白吃的午餐喔![m");
        set("no_enu_lv", "[1;36m威吉爾說道:[33m 小兄弟, 再勤練久些再來吧![m");
        set("top_lv", "[1;36m威吉爾說道:[33m 小兄弟, 我已經不能教你什麼啦![m");
        set("teach_message", "[1;36m威吉爾說道:[33m 想做個武士啊, 我可以教你些基本的東西[m");

        set("maxHP", HP);
        set("currentHP", HP);
        set("maxMP", MP);
        set("currentMP", MP);
        set("maxSP", SP);
        set("currentSP", SP);
        set("dem", 10000);
        set("def", 5000);
        set("mdem", 900);
        set("mdef", 1503);
        set("spe", 530);
        set("exp", 6000);

        setup();
}

void init()
{
        if( !previous_object()
                || !userp( previous_object()))
                return;
        call_out("ki", 2, previous_object());
}

void ki( object obj)
{
        if( obj->query("attacker"))
        {
                kill_ob( obj);
                return;
        }

        command("say 最近城內的亂事越來越多了...");
}
