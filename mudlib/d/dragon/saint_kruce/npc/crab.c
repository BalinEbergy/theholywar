// crab.c
// written by zz

#include <class.h>
#include <element.h>
#include <race.h>

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
        int HP= 500, MP= 0, SP= 100;

        set_name("螃蟹", ({ "crab"}));
        set("lv", 3);
        set("race","animal");
        set("gender","動物");
        set("describe","一隻螃蟹.");
        set("pre","四處爬動的");
        set("age", 1);
        set("element",EARTH);
        set("mind", 0);

        set("maxHP", HP);
        set("currentHP", HP);
        set("maxMP", MP);
        set("currentMP", MP);
        set("maxSP", SP);
        set("currentSP", SP);
        set("dem", 37);
        set("def", 30);
        set("mdem", 25);
        set("mdef", 25);
        set("spe", 37);
        set("exp", 470);

        set("chat_chance", 8);
        set("chat_msg", ({
                (: random_move :)
                }));
        setup();
}
