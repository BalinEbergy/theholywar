// blade.c
// written by Hudson

#include <equip.h>
#include <element.h>

inherit EQUIP;

void create()
{
        set_name("�g�����M", ({ "blade" }));
        set("long", @LONG
LONG
                );
        set("lv", 100);
        set("unit", "��");
        set("element", DARK);
        set("epower", 100);
        set("weight", 500);

        set("wear_type", HAND);
        set("weapon", BLADE);
        set("dem_eff", 40000);
        set("def_eff", 10000);
        set("spe_eff", 1000);
        set("eff", ({ "dem_eff", "spe_eff", "def_eff" }));
}
