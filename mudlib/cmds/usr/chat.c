// chat.c

void create() { seteuid( getuid()); }

int main( object me, int argc, string *argv)
{
    int i;
    string msg;

    if( argc< 2)
        write("你想要聊什麼呢?\n");
    else {
        msg= argv[1];

        for( i= 2; i< argc; i++)
            msg+= " "+ argv[i];

        if( msg!= "/p")
            D_CHANNEL->do_channel( me, "chat", msg);
        else D_CHANNEL->print_channel("chat");
    }

    return 1;
}

int help()
{
    write( @HELP
指令格式: chat <想說的話| /p >

這個指令可以跟全部的玩家聊天, /p 選項可以列出前十句話
HELP
    );
 
    return 1;
}
