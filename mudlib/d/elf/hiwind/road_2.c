// road_2.c
// written by hiwind

#include <room.h>


inherit ROOM;

void create()
{
        set("short","[1m���L�D��[m");
        set("long",@LONG
�b���L�D���W,���۰ʪ��M�ȫ�,���֫_�I�̦b�o���D���W��
�ʵ�,�u�ۦ������N�O�i�h[1;36m��[m�����P�t�@�Ӵ˪L�F,�䤤�R��
�F�j�۵M����.
LONG
        );

        set("exits",
        ([
        "east" : __DIR__ + "road_3",
        "south" : __DIR__ + "road_1",
        ])
        );

        set("objects",
        ([
                __DIR__ + "npc/dog":1
        ])
        );
        set("no_fight", 0);
        setup();
}
