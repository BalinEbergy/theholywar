// watasa.c
// written by Mondale
// ����

inherit NPC;

void create()
{
        int HP= 350000, MP= 5000, SP= 100000;

        set_name("[m[1m����[m", ({ "watasa" }));
        set("lv", 100);
        set("class","warrior");
        set("ra","human");
        set("gender","�k");
        set("describe","�@��y�����ۤ@�`���C,���H�Pı�L�����R���k�l.\n"+
                       "�o�O�t�¤��|���Z�N�`��,�]�O���}�ȧJ���̨ΧU��.\n");
        set("pre","[36;1m�M�y[0m���C");

        set("age", 17);
        set("element","water");
        set("mind", 300);

        set("mixHP", HP);
        set("currentHP", HP);
        set("mixMP", MP);
        set("currentMP", MP);
        set("mixSP", SP);
        set("currentSP", SP);
        set("dem", 30000);
        set("def", 30000);
        set("mdem", 10000);
        set("mdef", 15000);
        set("spe",2500);
        set("realmx", 10);
        set("exp", 45500);
        set("skill_chance" , 75);
        set_skill_level(20 ,100);
        set("skill" , ({
                "20:slash tide",
                "20:slash void",
                "20:slash moon",
                "20:slash multi",
                }));
        set("svoid" ,3);
        set("svoidt" , 1);
        carry_object( OB_SWORD + "dragonsword-ice")->wear( "b");
        set("chat_chance", 0);
        set("chat_msg", ({
                }));
        setup();
}

void init()
{
        ::init();
        if( !previous_object()
                || !userp(previous_object()))
                return;
        call_out("ki", 5, previous_object());
}

void ki( object obj)
{
        if( obj->is_fighting()) kill_ob( obj);
}

void action( object obj)
{
        command("say �A��Ө�o�̫K�O���t�A�ڦ�������������ܡH");
        setup();
}
