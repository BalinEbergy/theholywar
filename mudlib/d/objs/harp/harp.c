// harp.c
// written by Bother

#include <equip.h>
#include <element.h>

inherit EQUIP;

void create()
{
        set_name("木琴", ({ "harp" }));
        set("long", @LONG
一把木雕精細的琴
LONG
                );
        set("lv", 1);
        set("unit", "把");
        set("element", WIND);
        set("epower", 10);
        set("weight", 80);

        set("value", 10);
        set("wear_type", HAND);
        set("weapon", HARP);
        set("dem_eff", 50);
        set("eff", ({ "dem_eff" }));

        set_both();
}
