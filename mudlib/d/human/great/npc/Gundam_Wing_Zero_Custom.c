// Gundam_Wing_Zero_Custom.c
// written by koyamakyo
// 雙翼鋼彈零式特裝型

inherit NPC;

void create()
{
        int HP= 500000, MP= 200000, SP= 300000;

        set_name("雙翼鋼彈零式特裝型", ({ "Gundam Wing Zero Custom", "gundam", "wing", "zero", "custom" }));
        set("lv", 80);
        set("class","鋼彈");
        set("ra","MS");
        set("gender","");
        set("describe","背後擁有一對翅膀,像天使般的鋼彈");
        set("pre","");

        set("age", 5);
        set("element","holy");
        set("mind", 1000);

        set("mixHP", HP);
        set("currentHP", HP);
        set("mixMP", MP);
        set("currentMP", MP);
        set("mixSP", SP);
        set("currentSP", SP);
        set("dem", 25000);
        set("def", 20000);
        set("mdem", 20000);
        set("mdef", 20000);
        set("spe", 2000);
        set("realmx", 30);
        set("exp", 10000);

        /*set("chat_chance", 10);
        set("chat_msg", ({
                (: this_object(), "random_move" :),
                "\n浪人慢慢的踱來踱去\n"
                })); */
        setup();
}

void init()
{
        ::init();
        if( !previous_object()
                || !userp(previous_object()))
                return;

}
