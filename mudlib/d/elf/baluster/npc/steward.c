// steward.c
// written by Hudson

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
    int HP= 50000, MP= 300, SP= 5000;

    set_name("服務員", ({ "steward" }));
    set("lv", 40);
    set("class", WARRIOR);
    set("race", DRAGON);
    set("gender",1);
    set("describe","商業公會巴路斯特分會服務員,身上的制服繡著: 服務交易員");
    set("pre", "笑容可掬的");

    set("age", 133);
    set("element", COSMOS);
    set("mind", 0);

    set("maxHP", HP);
    set("currentHP", HP);
    set("maxMP", MP);
    set("currentMP", MP);
    set("maxSP", SP);
    set("currentSP", SP);
    set("strength", 7000);
    set("armor", 7000);
    set("magic", 3200);
    set("spi", 4);
    set("speed", 1000);
    set("exp", 2000);

    setup();
}
