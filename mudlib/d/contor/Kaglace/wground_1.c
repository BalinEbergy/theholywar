// wground_1.c
// written by Bother

#include <room.h>


inherit ROOM;

void create()
{
        set("short","[1;37m卡格烈斯廣場[m");
        set("long",@LONG
這裡是卡格烈斯城的廣場,往東走就是廣場的中心,往西走就是春
月街,街道上有一間武器店,傳說中老闆可是一位戰士,以前好像
還帶兵打仗過,廣場上人來人往,可以回想起古文明帝國的繁榮.
LONG
        );

        set("exits",
        ([
        "north" :__DIR__ + "nwground_1",
        "south" :__DIR__ + "swground_1",
        "west" :__DIR__ + "city_29",
        "east" :__DIR__ + "center",
        ])
        );
}
