// street_3.c
// written by contor

#include <room.h>


inherit ROOM;

void create()
{
        set("short","[1m�p�|[m");
        set("long",@LONG
�b�p�|�W,���۰ʪ�,�ڻ��u�ۦ������N�O�i�h[1;36m��[m�����P����
�L�F,�䤤�R���F�j�۵M����.���L�o�������G�����K,����
�D�|������Ƶo��,�٬O�p�ߤ@�I.
�A�w�g�v���b������[1;36m��[m�F
LONG
        );

        set("exits",
        ([
        "east" : __DIR__ + "street_5",
        "west" : __DIR__ + "street_2",
        ])
        );

        set("objects",
        ([
                __DIR__ + "npc/dog":2,
                __DIR__ + "forest/npc/bird":2,
        ])
        );

        set("no_fight", 0);
        setup();
}
