// eird.c
// written by Hudson
// 亞爾德

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
    int HP= 290000, MP= 21500, SP= 5000;

    set_name("亞爾德", ({ "eird" }));
    set("lv", 88);
    set("class",WIZARD);
    set("race",HUMAN);
    set("gender",1);
    set("describe","一位身著火紅袍子的巫師.");
    set("pre","炎之法師");
    set("age", 150);
    set("element",FIRE);
    set("mind", -599);

    set("teacher", ({
        7,
        }));
    set("favorite", ({
        WIZARD,
        }));
    set("teach_mind", "-1000 ~ -500");
    set("teach_msg_pure", "[1;36m亞爾德說道:[33m 我不教導邪惡之心不足的人![m");
    set("lv_up", "[1;36m亞爾德說道:[33m 你的魔術增強了![m\n");
    set("no_enu_money", "[1;36m亞爾德說道:[33m 你的錢不夠![m");
    set("no_enu_lv", "[1;36m亞爾德說道:[33m 你的能力不足![m");
    set("top_lv", "[1;36m亞爾德說道:[33m 你已經青出於藍了![m");
    set("teach_message", "[1;36m亞爾德說道:[33m 喔? 想學魔法?[m");

    set("maxHP", HP);
    set("currentHP", HP);
    set("maxMP", MP);
    set("currentMP", MP);
    set("maxSP", SP);
    set("currentSP", SP);
    set("strength", 18000);
    set("armor", 24999);
    set("magic", 29900);
    set("spi", 27);
    set("speed", 2007);
    set("exp", 247000);

    set("chat_chance", 15);
    set("chat_msg", ({
        (: command("say 太陽的光芒很亮吧... 火炎所賜予的, 是創造與毀滅呀!") :),
        }));

    setup();
}

void init()
{
    object obj;

    if( !(obj= previous_object())
        || !userp( obj))
        return;

    call_out("ki", 10, obj);
}

void ki( object obj)
{
    if( obj->is_fighting()) kill_ob( obj);
}

int teach_check( object me)
{
    if( me->query("neckr")== "mage permit");
    else if( me->query("neckl")== "mage permit");
    else
    {
        command("say 你還未成為真正的魔法師, 很抱歉我不能教你任何東西...");
        return 1;
    }

    return 0;
}
