// rabbit.c
// written by hiwind

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
    int HP= 390, MP= 50, SP= 500;

    set_name("可愛的小白兔", ({ "rabbit" }));
    set("lv", 4);
    set("describe","小白兔.");
    set("age", 10);
    set("element", WATER);
    set("race", ANIMAL);
    set("mind", 0);

    set("maxHP", HP);
    set("currentHP", HP);
    set("maxMP", MP);
    set("currentMP", MP);
    set("maxSP", SP);
    set("currentSP", SP);
    set("strength", 163);
    set("armor", 100);
    set("speed", 140);
    set("exp", 230);
    set("chat_chance", 10);
    set("chat_msg", ({
        (: random_move :)}));

    setup();
}
