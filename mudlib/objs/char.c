// char.c

#include <race.h>

inherit F_ATTACK;
inherit F_COMMAND;
inherit F_DBASE;
inherit F_GROUP;
inherit F_MESSAGE;
inherit F_MOVE;
inherit F_NAME;
inherit F_SKILL;

static int tick;

void create()
{
    seteuid(0);
    enable_commands();
    set("maxHP", 1);
    set("maxMP", 1);
    set("maxSP", 1);
    set_temp("block_msg/all", 1);
}

int is_character() { return 1; }

void setup()
{
    int *race_weight= ({
        100000, 100000, 25, 100, 50, 200, 40, 90, 0, 150, 0 });

    seteuid( getuid( this_object()));

    set_heart_beat(1);
    tick= 10+ random(10);

    if( !intp( query("race"))) set("race", MONSTER);
    if( !query("str")) set("str", 50);
    if( !query("con")) set("con", 50);
    if( !query("int")) set("int", 50);
    if( !query("spi")) set("spi", 1);
    if( !query("dex")) set("dex", 50);

    if( query("race")== DRAGON)
        add_temp("mdef_eff", 15);

    if( !userp( this_object()))
    {
        if( !query("currentHungry")) set("currentHungry", 100);
        if( !query("currentThirsty")) set("currentThirsty", 100);
        if( !query_temp("hit_eff")) set_temp("hit_eff", query("speed")+ query("speed")/ 2);
    }

    set_temp("magic_eff", ([ ]));

    delete_temp("block_msg/all");
    set_weight( race_weight[ query("race")]);
    init_group();
}

void heal_up()
{
    string msg= "";
    int rec_ratio, med, sit, hp_rec, mp_rec, sp_rec, lv;
    int curHP, curMP, curSP, maxHP, maxMP, maxSP;
    int curHun, curThi, race;
    int *race_reduce= ({
        0, 4, 8, 2, 4, 4, 18, 0
        });

    if( query("uncurable") || is_ghost())
        return;

    rec_ratio= 2;
    med= 0;
    sit= 0;
    lv= query("lv");
    curHun= query("currentHungry");
    curThi= query("currentThirsty");

    if( curHun== 0) rec_ratio/= 2;
    if( curThi== 0) rec_ratio/= 2;

    if( environment()->is_inn())
        rec_ratio*= 10;

    switch( query("rest"))
    {
        case 1: sit= rec_ratio; break;
        case 2: rec_ratio*= 2; break;
        case 3: rec_ratio*= 2; med= 1; break;
    }

    hp_rec= (( query("con")+ random(2))* ( 10+ lv)+ 200)* rec_ratio/ 40;
    mp_rec= (( query("spi")+ random(2))* ( 10+ lv)+ 200)* rec_ratio/ 80+
        ( query("spi")+ 10)* ( 30+ lv)/ 25* med;
    sp_rec= (( query("dex")+ random(2))* ( 10+ lv)+ 200)* rec_ratio/ 40;

    curHP= query("currentHP");
    curMP= query("currentMP");
    curSP= query("currentSP");
    maxHP= query("maxHP");
    maxMP= query("maxMP");
    maxSP= query("maxSP");

    // Effect region

    if( query("race")== OGRE)
        hp_rec+= ( maxHP- curHP)* rec_ratio/ 40;
    else if( query("race")== ELF)
        mp_rec+= mp_rec* rec_ratio/ 4;

    // Noiger tceffe

    curHP+= hp_rec;
    curMP+= mp_rec;
    curSP+= sp_rec;

    if( curHP> maxHP) curHP= maxHP;
    if( curMP> maxMP) curMP= maxMP;
    if( curSP> maxSP) curSP= maxSP;

    set("currentHP", curHP);
    set("currentMP", curMP);
    set("currentSP", curSP);

    // Deal with hungry and thirsty

    if( !userp( this_object()))
        return;

    race= query("race")- 2;
    curHun-= random( race_reduce[ race* 2]);
    curThi-= random( race_reduce[ race* 2+ 1]);

    if( curHun<= 0)
    {
        msg+= "你覺得自己快要餓死了.\n";
        curHun= 0;
    } else if( curHun< 25)
        msg+= "你覺得有點飢餓.\n";

    if( curThi<= 0)
    {
        msg+= "你覺得自己快要渴死了.\n";
        curThi= 0;
    } else if( curThi< 25)
        msg+= "你覺得有點口渴.\n";

    set("currentHungry", curHun);
    set("currentThirsty", curThi);

    write( msg);
}

void heart_beat()
{
    attack();

    if( tick--) return;
    else tick= 10+ random(10);

    if( !userp( this_object()))
    {
        this_object()->chat();
        if( !this_object()) return;
    }

    heal_up();

    if( !interactive( this_object())) return;

    this_object()->update_age();
}
