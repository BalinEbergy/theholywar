// sword.c
// written by Hudson

inherit EQUIP;

void create()
{
        set_name("�g�����C", ({ "sword" }));
        set("long", @LONG
LONG
                );
        set("lv", 100);
        set("unit", "��");
        set("element", "dark");
        set("epower", 100);
        set("weight", 500);

        set("wear_type", "hand");
        set("weapon", "sword");
        set("dem_eff", 40000);
        set("def_eff", 10000);
        set("spe_eff", 1000);
        set("eff", ({ "dem_eff", "spe_eff", "def_eff" }));
}
