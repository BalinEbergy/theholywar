// 090.c
// written by Mondale
// 090

inherit NPC;

void create()
{
        int HP= 900000, MP= 50000, SP= 60000;

        set_name("090", ({ "090" }));
        set("lv", 90);
        set("class","chemist");
        set("ra","人類");
        set("gender","男");
        set("describe","一位喜歡爆破的~~~~~.");
        set("pre","精通火箭爆破技術的");

        set("age", 16);
        set("element","fire");
        set("mind", 90);

        set("mixHP", HP);
        set("currentHP", HP);
        set("mixMP", MP);
        set("currentMP", MP);
        set("mixSP", SP);
        set("currentSP", SP);
        set("dem", 9090);
        set("def", 90900);
        set("mdem", 09009);
        set("mdef", 99900);
        set("spe",909);
        set("realmx", 909);
        set("exp", 909090);

        set("chat_chance", 20);
        set("chat_msg", ({
                "\n090邪惡的笑了一笑...嘿嘿嘿。\n"
                }));
        setup();
        setup();
}

void init()
{
        ::init();
        if( !previous_object()
                || !userp(previous_object()))
                return;
        call_out("action", 1, previous_object());
}

void action( object obj)
{
        command("hihi .....有什麼事嗎？");
        command("hihi 加入火箭社~~~不然~~~...");
        command("hihi 嘿嘿嘿~~~");
        setup();
}
