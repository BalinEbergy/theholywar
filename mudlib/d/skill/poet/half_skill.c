// half_skill.c
// written by Bother

#include <skill.h>

int main( object me, object obj)
{
        me->set("half_skill", 1);

        return 1;
}

string name( int select)
{
        switch( select)
        {
                case 1: return "精神昇華";
                case 2: return "half skill";
                case 3: return "half_skill";
        }
}

int type() { return BARD; }
int lv() { return 50; }
int attackful()
{
        return -1;
}

int usesp() { return 0; }
int usemp() { return 0; }
int successful() { return 0; }
int delay() { return 0; }
int all() { return 0; }
string element() { return "air"; }

int check( object me)
{
        return notify_fail("\n\n你辦不到...\n");
}
