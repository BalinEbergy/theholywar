// wroad1.c
// written by Hudson

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;32m�ڸ����S��D[m");
        set("long",@LONG
�o��O�s�������䪺��D,�|�P�ͪ��ۭZ�������,�[�W���
���y���q���y�L,��W�[�F���̪��۵M��,�ө����H��,���F
�~���~,�N�O�ǥ��}�]�k���H,�αR�|�۵M���ȫȤF.�F��O��
�~���|���ڸ����S���|.
LONG
        );

        set("exits",
        ([
        "east"  : __DIR__ + "commercial_association",
        "north" : __DIR__ + "nroad1",
        "south" : __DIR__ + "nroad7",
        ])
        );

        set("objects", ([
                __DIR__ + "npc/elf_citizen": 1,
                ]));
}
