// keynes.c
// written by zz
// 凱恩斯

inherit NPC;

void create()
{
        int HP= 320000, MP= 4000, SP= 10000;

        set_name("凱恩斯", ({ "keynes" }));
        set("lv", 90);
        set("class","knight");
        set("ra","dragon");
        set("gender","男");
        set("describe","皇家騎士隊隊長.");
        set("pre","騎士隊隊長");
        set("age", 32);
        set("element","lightning");
        set("mind", 500);

        set("teacher", ({
                22, 20,
                }));
        set("favorite", ({
                "knight",
                "element maker",
                }));
        set("teach_mind", "-1000 ~ 1000");
        set("lv_up", "[1;36m凱恩斯說道:[33m 恭喜你, 等級提升了![m\n");
        set("no_enu_money", "[1;36m凱恩斯說道:[33m 你的錢不太夠,再去賺點錢吧.[m\n");
        set("no_enu_exp", "[1;36m凱恩斯說道:[33m 你的經驗還不夠,回去多練習吧.[m\n");
        set("no_enu_lv", "[1;36m凱恩斯說道:[33m 抱歉,我還不能教你這項技能.[m\n");
        set("top_lv", "[1;36m凱恩斯說道:[33m 嗯...我已經不能在傳授你什麼了.[m\n");
        set("teach_message", "[1;36m凱恩斯說道:[33m 你想學什麼技能嗎?[m\n");

        set("mixHP", HP);
        set("currentHP", HP);
        set("mixMP", MP);
        set("currentMP", MP);
        set("mixSP", SP);
        set("currentSP", SP);
        set("dem", 20000);
        set("str", 70);
        set("def", 10000);
        set("mdem", 7000);
        set("mdef", 15533);
        set("spe", 2030);
        set("exp", 90000);

        set_skill_level( 20, 100);
        set_skill_level( 22, 100);
        set("skill", ({ "20:void" , "20:slash multi" }));
        set("skill_chance", 100);

        carry_object( OB_SWORD+ "ice_sword")->wear( "b");

        setup();
}

void init()
{
        if( !previous_object()
                || !userp(previous_object()))
                return;
        call_out("ki", 100, previous_object());
        call_out("action", 10, previous_object());
}

void ki( object obj)
{
        if( !obj || !present(obj) || this_object()->is_fighting()) return;
        if( obj->is_fighting()) kill_ob(obj);
}

void action( object obj)
{
        command("hihi 想學技能可以來找我喔!");
}
