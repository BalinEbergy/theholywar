// dog.c
// written by hiwind

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
    int HP= 10000, MP= 500,SP= 2000;

    set_name("�|�B�̪���", ({ "dog" }));
    set("lv", 21);
    set("race", ANIMAL);
    set("describe"," ��.");
    set("age", 10);
    set("element", THUNDER);
    set("mind", 0);

    set("maxHP", HP);
    set("currentHP", HP);
    set("maxMP", MP);
    set("currentMP", MP);
    set("maxSP", SP);
    set("currentSP", SP);
    set("strength", 570);
    set("armor", 300);
    set("magic", 200);
    set("spi", 10);
    set("speed", 260);
    set("exp", 2000);
    setup();
    set("chat_chance", 1);
    set("chat_msg", ({
        (: random_move :)}));
}
