// grass_1.c
// written by hiwind

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;32m�˪L[m[1;37m�����Ŧa[m");
        set("long",@LONG
���B���˪L����,�èS�����j�����,�u�������C�������O,
�ϱo���B���������}��,�]�]���p��,�ҥH,���B�`�`���H�|�b
���E�|,�Υ�,�ζ���,�Ʀܦ����ٷ|���C���U�a���ӤH.
LONG
        );

        set("exits",
        ([
        "west" : __DIR__ + "forest/forest_7",
        "east" : __DIR__ + "forest/forest_17",
        "south" : __DIR__ + "forest/forest_12",
        "north" : __DIR__ + "forest/forest_13",
        ])
        );

        set("objects",
        ([
                __DIR__ + "forest/npc/merchant":1
        ])
        );
        set("no_fight", 0);
        setup();
}
