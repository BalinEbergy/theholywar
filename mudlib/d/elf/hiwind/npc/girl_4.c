// girl_4.c
// written by hiwind

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
    int HP= 200000, MP= 20000 ,SP= 30000;

    set_name("[1;35m����[m[1;37m���V��[m", ({ "girl" }));
    set("lv", 80);
    set("class", WIZARD);
    set("race", ELF);
    set("describe","�@��S�O���k�l.");
    set("age", 16);
    set("mind", 800);
    set("gender", 0);

    set("maxHP", HP);
    set("currentHP", 125000);
    set("maxMP", MP);
    set("currentMP", MP);
    set("maxSP", SP);
    set("currentSP", SP);
    set("strength", 13000);
    set("def", 22000);
    set("mdem",7500);
    set("mdef", 15000);
    set("spirit", 75);
    set("speed", 1800);
    set("uncurable", 1);
    set("exp", 0);

    set("chat_chance", 10);
    set("chat_msg", ({
        "\�V���d�E�����_.\n"
        }));
    setup();
}
void init()
{
    if( !previous_object()
        || !userp(previous_object()))
        return;
    call_out("action", 10, previous_object());
}

void action( object obj)
{
    if( !obj || !present(obj) || this_object()->is_fighting()) return;
    if( obj->is_fighting()) kill_ob(obj);

    command("say �D�H....");
}
