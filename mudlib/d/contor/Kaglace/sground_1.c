// sground_1.c
// written by Bother

#include <room.h>


inherit ROOM;

void create()
{
        set("short","[1;37m卡格烈斯廣場[m");
        set("long",@LONG
這裡是卡格烈斯城的廣場,往北就是廣場的正中心,代表著這個
城市的象徵,卡格的石像就存在於前面,它有一股不可否認的魅
力把你吸引過去,這裡的行人比剛剛更多.
LONG
        );

        set("exits",
        ([
        "north" :__DIR__ + "center",
        "south" :__DIR__ + "city_23",
        "west" :__DIR__ + "swground_1",
        "east" :__DIR__ + "seground_1",
        ])
        );
}
