//forest_19.c
//write by hiwind

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;37m�L��[m");
        set("long",@LONG
���}�F�L�����D,���i�F[1;33;44m���F[m[1;32m�˪L[m���L��,���B��[1;31m��[m[1;32m�L[m������
�j���ۦP,���M���O�p������,����o�Q�����A��.�ѩ�L�즳
�ǭZ�K,�ҥH�����u��q�@�ǵ}�\�����_���S�F�X��,�򤧫e
���L�������,�𽮤U�N��o�Q�������D.
�F��O�L�����D,�n��O�����F�˪L���D��.
LONG
        );

        set("exits",
        ([
        "east" : "d/elf/hiwind/elf_forest/forest/forest_26",
        "south" : "d/elf/hiwind/elf_forest/road_4",
        "north" : __DIR__ + "forest_20",
        ])
        );


        set("objects",
        ([
                __DIR__ + "npc/eagle":2,
                __DIR__ + "npc/magic":1,
        ])
        );
        set("no_fight", 0);
        setup();
}
