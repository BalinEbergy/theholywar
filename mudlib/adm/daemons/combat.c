// combat.c

#include <element.h>
#include <equip.h>
#include <race.h>

int element_eff( int source, int target)
{
    mixed *eff= ({
        //  H   L   C   W   T   F   A   E   D   V  //
        ({ 10, 10, 10, 10, 10, 15, 15, 15, 15, 15 }),
        ({  8,  8, 10, 10, 10, 10, 10, 10, 12, 12 }),
        ({ 10, 10, 10, 10, 10, 10, 10, 10, 10, 10 }),
        ({  9, 10, 10,  8, 10, 10, 10, 12, 10, 11 }),
        ({  9, 10, 10, 12,  8, 10, 10, 10, 10, 11 }),
        ({ 11, 10, 10, 10, 12,  8, 10, 10, 10,  9 }),
        ({ 11, 10, 10, 10, 10, 12,  8, 10, 10,  9 }),
        ({ 11, 10, 10, 10, 10, 10, 12,  8, 10,  9 }),
        ({ 12, 12, 10, 10, 10, 10, 10, 10,  8,  8 }),
        ({ 15, 15, 15, 15, 15, 10, 10, 10, 10, 10 })
        });

    return eff[source][target];
}

string status( object victim)
{
    int percent;

    if(( percent= victim->query("currentHP")* 100/ victim->query("maxHP"))
        == 100) return "[1;32m一點兒傷也沒有...[m";
    if( percent>= 60) return "[1;33m受了一點兒傷...[m";
    if( percent>= 40) return "[1;35;44m受了重傷!!![m";
    if( percent>= 10) return "[1;36;41m快不行了~~~~[m";
    if( percent>=  0) return "[1;33;41m已經全身是[5m血[m[1;33;41m了!!!![m";

    return "[47;34m死了...[m";
}

string underattack( object obj, int damage)
{
    int percent;
    string *msg= ({
        "造成[1;5;31m致命一擊!!!!!![m",
        "造成[1;31m足以[5m致死[m[1;31m的傷害[m",
        "造成[1;35m嚴重的傷害[m",
        "造成[1;33m很大的傷害[m",
        "造成[36m不小的傷害[m",
        "造成[32m一點點的傷害[m",
        "[1;37m根本沒有影響[m"
        });
    string str;

    if( obj->query("currentHP")<= 0) str= msg[0];
    else if(( percent= damage* 100/ obj->query("maxHP"))> 50) str= msg[1];
    else if( percent> 20) str= msg[2];
    else if( percent> 15) str= msg[3];
    else if( percent> 10) str= msg[4];
    else if( percent>  5) str= msg[5];
    else str= msg[6];

    str+= "!!([1;31m"+ ( damage> 20000? "[33;41m*****[m": (string) damage)+ "[m)";

    return str;
}

varargs string attackstate( object me, int power, int light)
{
    int *sword_type= ({ 1, 2, 3, });
    int *axe_type= ({ 1, 3, 6, });
    int *blade_type= ({ 1, 3, 6 });
    string *default_type=({
        "擊", "砍", "刺", "劈", "射", "揮", "斬", "敲"
        });
    int temp;
    string str, type;
    object obj;

    obj= me->query_temp("weapon");

    if( obj)
    {
        switch( obj->query("weapon"))
        {
            case SWORD:
                type= default_type[ sword_type[ random( sizeof( sword_type))]];
                break;
            case AXE:
                type= default_type[ axe_type[ random( sizeof( axe_type))]];
                break;
            case BLADE:
                type= default_type[ blade_type[ random( sizeof( blade_type))]];
            default: type= default_type[0]; break;
        }
    } else type= default_type[0];

    if( obj) str= "手持"+ obj->name();
    else str= "揮起拳頭";

    if(( temp= me->query("race"))== ANIMAL || temp== SAINTRE)
        str= "";

    if( power> 25) str+= "[1;31m使盡全力一"+ type+ "[m";
    else if( power> 15) str+= "[1;35m奮力一"+ type+ "[m";
    else if( power< 5) str+= "[36m輕輕一"+ type+ "[m";
    else str+= "[31m用力的一"+ type+ "[m";

    if( light) str+= "[1;37m瞬間出現[33m一道[37;5m閃光!![m";

    return str;
}

varargs int dodge( object me, object obj, int hit_boost)
{
    return random( me->query("speed")+ me->query_temp("spe_eff")+ me->query_temp("hit_eff"))*
        ( 100- environment( me)->query("low_reach"))/ 100<
        obj->query("speed")+ obj->query_temp("spe_eff");
}

void fight( object me, object enemy)
{
    int power, damage, ap;
    object weapon;
    string str;

    if( environment( me)->query("no_fight"))
    {
        me->remove_all_enemy();
        return;
    } else if( me->query("currentSP")<= 1)
    {
        write("[5;33m你沒力了...[m\n\n"+ enemy->name()+ status( enemy)+ "\n");
        return;
    }

    me->add("currentSP", -2);
    power= random( 30);

    str= "$N"+ attackstate( me, power);

    if( dodge( me, enemy))
    {
        str+= "但是被$n[1;37m閃過了~[m";
    } else {
        weapon= me->query_temp("weapon");

        ap= ( me->query("strength")+ me->query_temp("dem_eff")+
            ( objectp( weapon)? weapon->query("damage")* ( 100- weapon->query("epower"))/ 100: 0))*
            element_eff( me->query("element"), enemy->query("element"))+
            ( objectp( weapon)? weapon->query("damage")* weapon->query("epower")/ 100*
            element_eff( weapon->query("element"), enemy->query("element")): 0);

        ap= ap* ( 85+ power)/ 1000;

        damage= ap- enemy->query("armor")- enemy->query_temp("def_eff");
        if( damage< 0)
            damage= random( 10);

        enemy->add("currentHP", -damage);
        str+= "對$n"+ underattack( enemy, damage);
    }

    message_vision( str+ "\n", me, enemy);
    tell_object( me, " \n"+ enemy->name()+ status( enemy)+ "\n");

    if( enemy->query("currentHP")< 0)
        enemy->die( me);
}
