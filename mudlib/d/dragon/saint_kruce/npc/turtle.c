// turtle.c
// written by zz

#include <class.h>
#include <element.h>
#include <race.h>

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
        int HP= 3000, MP= 200, SP= 10000;

        set_name("小烏龜", ({ "turtle"}));
        set("lv", 10);
        set("race","animal");
        set("gender","動物");
        set("describe","一隻小烏龜.");
        set("pre","緩緩爬動的");
        set("age", 3);
        set("element",EARTH);
        set("mind", 10);

        set("maxHP", HP);
        set("currentHP", HP);
        set("maxMP", MP);
        set("currentMP", MP);
        set("maxSP", SP);
        set("currentSP", SP);
        set("dem", 70);
        set("def", 80);
        set("mdem", 40);
        set("mdef", 48);
        set("spe", 40);
        set("exp", 1100);

        set("chat_chance", 4);
        set("chat_msg", ({
                (: random_move :)
                }));
        setup();
}
