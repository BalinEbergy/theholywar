// cachet_wand.c
// written by Hiwind

#include <equip.h>
#include <element.h>

inherit EQUIP;

void create()
{
    set_name("封印之杖", ({"cachet wand", "cachet", "wand" }));
    set("long", @LONG
一把散發強大魔力的法杖,傳說中"夢"的法杖.
杖上有著幾行小字:
    
    風隨我行;如幻似夢.
    
LONG
        );
    set("lv", 100);
    set("unit", "把");
    set("element", EVIL);
    set("epower", 100);

    set("wear_type", HAND);
    set("weapon", WAND);
    set("def_eff", 20000);
    set("mdem_eff", 50000);
    set("mdef_eff", 20000);
    set("spe_eff", 1000);
    set("eff", ({ "def_eff", "mdem_eff", "mdef_eff", "spe_eff" }));

    set_weight( 500);
}
