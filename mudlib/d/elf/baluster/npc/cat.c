// cat.c
// written by Hudson

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
        int HP= 260, MP= 0, SP= 900;

        set_name("貓", ({ "cat"}));
        set("lv", 2);
        set("race", ANIMAL);
        set("gender",0);
        set("describe","一隻貓.");
        set("pre","四處走動的");
        set("age", 2);
        set("element",WIND);
        set("mind", -10);

        set("maxHP", HP);
        set("currentHP", HP);
        set("maxMP", MP);
        set("currentMP", MP);
        set("maxSP", SP);
        set("currentSP", SP);
        set("strength", 130);
        set("armor", 100);
        set("spi", 5);
        set("speed", 130);
        set("exp", 200);

        set("chat_chance", 22);
        set("chat_msg", ({
                (: random_move :)
                }));

        setup();
}
