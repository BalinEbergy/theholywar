// time.c
// written by Bother

inherit ITEM;

int time, min, a, *time_f, *time_s, *ch, *cha;
string temp1, temp2; 

void create()
{
        object obj;

        set_name("看不見的報時器", ({ "look_time" }));
        set("long", @LONG
天神號的的報時器.
LONG
                );
        set("boat_name", "天神號");
        set("unit", "個");
        set("lv", 1);
        set("value", 0);
        set("weight", 100000);
        set("no_sac", 1);
        set("no_look", 1);
        set("board_time_f", ({ 8, 23}));
        set("board_time_s", ({ 10, 1}));
        set("haha", ({ 0, 0}));

        ch= this_object()->query("haha");
        cha= this_object()->query("haha");
        time_f= this_object()->query("board_time_f");
        time_s= this_object()->query("board_time_s");
        a=sizeof(time_f);
        sscanf( NATURE_D->game_time(4), "%s[1;44;37m%d:%d[m%s"
                , temp1, time, min, temp2);
        while( a--)        
        {
                if( time_f[a]> time_s[a])
                {
                        if( 60* time_f[a]> 60* time+ min)
                        {
                                ch[a]= (( 60* time_f[a])- ( 60* time+ min));
                                cha[a]= (( 60* time_s[a])- ( 60* time+ min));
                                call_out("come", ch[a], obj);
                                call_out("go", cha[a], obj);
                        }
                        if( 60* time_f[a]< 60* time+ min && 60* time+ min< 60 *time_s[a])
                        {
                                ch[a]= 720- (( 60* time+ min)- (60* time_f[a]));
                                cha[a]= (( 60* time_s[a])- ( 60* time+ min));
                                call_out("come", ch[a], obj);
                                call_out("go", cha[a], obj);
                        }
                        else
                        {
                                ch[a]= (( 60* time+ min)- ( 60* time_f[a]));
                                cha[a]= (( 60* time+ min)- ( 60* time_s[a]));
                                call_out("come", ch[a], obj);
                                call_out("go", cha[a], obj);
                        }
                }
                else
                {
                        if( 60* time_s[a]< 60* time+ min && 60* time+ min< 60* time_f[a])
                        {
                                ch[a]= (( 60* time+ min)- ( 60* time_f[a]));
                                cha[a]= 720- (( 60* time+ min)- ( 60* time_s[a]));
                                call_out("come", ch[a], obj);
                                call_out("go", cha[a], obj);
                        }
                        else
                        {
                                ch[a]= 720+ (( 60* time_f[a])- ( 60* time+ min));
                                cha[a]= 720+ (( 60* time_s[a])- ( 60* time+ min));
                                call_out("come", ch[a], obj);
                                call_out("go", cha[a], obj);
                        }
                }
        }
}
void come( object obj)
{
        message_vision("$N出航了\n", obj);
        call_out("come", 720, obj);
}
void go( object obj)
{
        message_vision("$N靠港了\n", obj);
        call_out("go", 720, obj);
} 
