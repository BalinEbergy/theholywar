// stone_1.c
// written by hiwind

inherit ITEM;

void create()
{
        set_name("精靈族人的石雕", ({ "statue" }));
        set("long", @LONG
這是一個精靈族人的石雕.刻劃得十分精細,栩栩如生.但奇怪
的是為何他的神情中卻帶著恐懼.
LONG
                );
        set("unsac", 1);
        set("no_get", 1);
        set("weight", 30000);
}
