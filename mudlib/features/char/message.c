// message.c

#include <dbase.h>

#define MAX_MSG_BUFFER 500

static string *msg_buffer= ({ });

void receive_message( mixed msg_class, string message)
{
    if( !interactive( this_object()))
        return;

    if( query_temp("block_msg/all") || query_temp("block_msg/"+ msg_class))
        return;

    switch( msg_class)
    {
        case "vision":
            if( query("rest")> 1)
                return;
    }

    if( query("no_prompt"))
    {
        receive( message);
        return;
    }

    message= "[K\n[K\n[K\n[K[3A"+ message+
        D_STATUS->user_under_prompt( this_object())+ "[3A[80D[K";

    receive( message);
}

void write_prompt() { printf(""); }
