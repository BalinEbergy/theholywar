//tree_31.c
//write by hiwind

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;37m�L��[m");
        set("long",@LONG
�A�{�b�B��L������t,�o��S�O���P��̭�������,���B
��[1;31m��[m��w�g���㤣�p���e���h�F,�i�H���O[1;31m��[m�L���X�f,�M
��[1;31m��[m�L�N�o�˵����F��?�A���G���ǯ��M,�o�Ӵ˪L���O��
�p��?�u�����O�����]�Ҿɦܪ�?�@�Q�즹�A�]���T�{��.
�u���_�観�@���p��,��ť��F�n���n,�i��O�X�f��,�i�H
�h�ݬ�.
LONG
        );

        set("exits",
        ([
        "west" : __DIR__ + "tree_26",
        "south" : __DIR__ + "tree_30",
        "north" :"d/elf/hiwind/road_1",
        ])
        );

        set("objects",
        ([
                __DIR__ + "npc/rabbit":1,
                __DIR__ + "npc/shrewmouse":1,
        ])
        );

        set("no_fight", 0);
        setup();
}
