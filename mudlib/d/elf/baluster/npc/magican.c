// magican.c
// written by Hudson

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
    int HP= 150000, MP= 9000, SP= 1000;

    set_name("魔法使", ({ "magic messenger" }));
    set("lv", 60);
    set("class", WIZARD);
    set("race", ELF);
    set("gender", 1);
    set("describe","一位神情和藹的法師.");
    set("pre","駐守城門的");
    set("age", 200);
    set("element", COSMOS);
    set("mind", 800);

    set("maxHP", HP);
    set("currentHP", HP);
    set("maxMP", MP);
    set("currentMP", MP);
    set("maxSP", SP);
    set("currentSP", SP);
    set("strength", 6000);
    set("armor", 14000);
    set("magic", 23000);
    set("api", 15);
    set("speed", 930);
    set("exp", 5000);

    setup();
}

void init()
{
    if( !previous_object()
        || !userp(previous_object()))
        return;
    call_out("ki", 10, previous_object());
    call_out("action", 10);
}

void ki( object obj)
{
    if( obj->query("attacker")) kill_ob(obj);
}

void action()
{
    if( !this_object()->fighting())
    command("say 歡迎來到巴路斯特~");
}
