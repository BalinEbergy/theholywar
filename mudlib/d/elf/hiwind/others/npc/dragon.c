// dragon.c
// written by hiwind

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
    int HP= 100000, MP= 40000 ,SP= 20000;

    set_name("[1;37m���s[m", ({ "dragon" }));
    set("lv", 75);
    set("class", WIZARD);
    set("race", DRAGON);
    set("describe","�@�����c���s.");
    set("age", 300);
    set("mind", 0);

    set("maxHP", HP);
    set("currentHP", HP);
    set("maxMP", MP);
    set("currentMP", MP);
    set("maxSP", SP);
    set("currentSP", SP);
    set("strength", 40000);
    set("def", 7500);
    set("mdem", 10000);
    set("mdef", 2500);
    set("spi", 60);
    set("speed", 1500);
    set("realmx", 100);
    set("exp", 1000);

    setup();
}

void init()
{
    ::init();
    if( !previous_object()
        || !userp(previous_object()))
        return;
    call_out("action", 10, previous_object());
}

void action( object obj)
{
    if( !obj || !present(obj) || this_object()->is_fighting()) return;
    if( obj->is_fighting()) kill_ob(obj);
    if( obj->query("mind")<0) kill_ob(obj);
    command("say �D�H�߱����n�O�n�o");
    setup();
}
