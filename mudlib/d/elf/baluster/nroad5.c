// nroad5.c
// written by Hudson

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;32m�ڸ����S��D[m");
        set("long",@LONG
�o��j���O�ڸ����S�ө��Ϥ��ߪ�����,�_�䦳���㩱,�n��
�����J,�Ȧ�,�Z������,���䦳�@�ɨu�����]�k�D���,�F��
�h���D�㩱,�ӷ~���|.
LONG
        );

        set("exits",
        ([
        "north" : __DIR__ + "nroad3",
        "south" : __DIR__ + "nroad8",
        "west"  : __DIR__ + "nroad6",
        ])
        );

        set("objects", ([
                __DIR__ + "npc/dog":2,
                __DIR__ + "npc/cat":1,
                ]));

        setup();
}
