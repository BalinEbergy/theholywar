// center.c
// written by peternice

#include <room.h>

inherit ROOM;

void create()
{
        set("short","*[1;37m天嵐之島_近陸*[m");
        set("west",@LONG
       總是有一堆瘦瘦長長又煩人的野草, 擋住你往前的道路, 你必需撥開這些叢生的雜
草才能繼續往前走, 一個不小心還會被長刺的草給劃破手掌, 煩死人了.
 LONG
        );

        set("exits",
        ([
        "west" : __DIR__ + "tests1e2",
        
        ])
        );
}
