// help.c

void create()
{
    seteuid( getuid());
}

int main( object me, int argc, string *argv)
{
    string file;

    if( argc== 1)
        write("請輸入您有興趣的主題.\n");
    else if(( file= D_COMMAND->find_command( argv[1], me))== "N")
        write("沒有這個主題.\n");
    else return file->help( me);

    return 1;
}

int help( object me)
{
    write( @HELP
指令格式: help <主題>

這個指令提供你針對某一主題的詳細說明文件.
HELP
    );

    return 1;
}
