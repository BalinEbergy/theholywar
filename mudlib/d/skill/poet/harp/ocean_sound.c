// ocean_sound.c
// written by Hudson

#include <skill.h>

int main( object me, object obj)
{
        int sppoint, i, q;

        sppoint= obj->query("mixSP")/20+ 20;
        i= me->skill_level( HARP)/10;
        for( q= 1; q<= i; q++)
        {
                sppoint+= 20;
        }
        tell_object( obj, "\n你感到一陣清爽!\n");

        return COMBAT_D->heal_func( obj, "SP", sppoint);
}

string name( int select)
{
        switch( select)
        {
                case 1: return "清新的海聲";
                case 2: return "ocean sound";
                case 3: return "ocean_sound";
        }
}

int type() { return HARP; }
int lv() { return 1; }
int attackful()
{
        return 0;
}

string mes( object me)
{
        return "[1;32m清[1;36m新[1;37m的琴聲使同伴重獲[1;30m新生[m.";
}

int usesp() { return 10; }
int usemp() { return 20; }
int successful() { return 90; }
int delay() { return 2; }
int all() { return 1; }

int check( object me)
{
        return 1;
}
