//center.c
//written by zz

#include <class.h>
#include <element.h>
#include <race.h>

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
        int HP= 7000 , MP= 0 , SP= 5200;

        set_name("小孩", ({ "child" }));
        set("lv", 15);
        set("race",HUMAN);
        set("describe","一個小孩");
        set("age", 9);
        set("pre", "正玩的起興的");
        set("element", EARTH);
        set("goodat", WATER);
        set("weakat", THUNDER);
        set("mind", 10);
        set("gender", 1);

        set("maxHP", HP);
        set("currentHP", HP);
        set("maxSP", SP);
        set("currentSP", SP);
        set("dem", 190);
        set("def", 120);
        set("mdem", 50);
        set("mdef", 50);
        set("spirit", 20);
        set("spe", 70);
        set("exp", 1500);

        set("chat_chance", 12);
        set("chat_msg", ({
                (: random_move :)
                }));

        setup();
}
