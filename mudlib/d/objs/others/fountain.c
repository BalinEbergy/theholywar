// fountain.c
// written by Bother
// rewritten by Hudson

#include <element.h>

inherit ITEM;

void create()
{
    set_name("噴水池", ({ "fountain" }));
    set("long", @LONG
一座美麗壯觀的噴水池呈現在你面前.
LONG
        );
    set("unit", "座");
    set("element", WATER);
    set("epower", 30);
    set("no_get", 1);
    set("unsac", 1);

    set("drinkable", 1);
}
