// steak.c
// written by Mondale

inherit ITEM;

void create()
{
        object obj;
        set_name("牛排", ({ "steak" }));
        set("long", @LONG
一份五分熟的牛排.這味道聞起來真誘人哪!
LONG
                );
        set("unit", "份");
        set("lv", 0);
        set("value", 0);
        set("c_water", 2);
        set("weight", 50);
        set("wwater", 1);
        call_out("disappear", 5000);
}

void disappear()
{
        object env;
        env= environment( this_object());
        env->add("under", -query("weight"));
        tell_object( env, "\n"+ name()+ "腐敗了.\n");
        destruct( this_object());
}
