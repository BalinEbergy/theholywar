// holy_blest.c
// written by Bother

#include <skill.h>

int main( object me, object obj)
{
        string *effects;

        if( obj->query("holy_bt")) {
                if( me->query("meditation")== 1) {
                        obj->remove_all_effect();
                        obj->set("meditation", 1);
                }
                else
                        obj->remove_all_effect();

                tell_object( obj, me->name()+ "�I�i[1;37m���t������[m,[1;33m�ϱo�A�����`���A�Ѱ��F[m\n");
                return 1;
        }
        else
        {
                if( me->query("meditation")== 1) {
                        obj->remove_all_effect();
                        obj->set("meditation", 1);
                }
                else
                        obj->remove_all_effect();
                tell_object( obj, me->name()+ "�I�i[1;37m���t������[m,[1;33m�ϱo�A�����`���A�Ѱ��F[m\n");
                effects= obj->query("effect");
                obj->set("holy_b", ({ "realmx_eff:"+ me->skill_level( FLUTE), }));
                obj->set("holy_bt", me->skill_level( FLUTE));
                obj->add("realmx_eff", me->skill_level( FLUTE));
                if( effects)
                        effects+= ({ "holy_b" });
                else effects= ({ "holy_b" });
                obj->set("effect", effects);
        }

        tell_object( obj, "�A���F�m�W�ɤF.\n");
        return 1;
}

string name( int select)
{
        switch( select)
        {
                case 1: return "���t������";
                case 2: return "holy blest";
                case 3: return "holy_blest";
        }
}

int type() { return FLUTE; }
int lv() { return 90; }
int attackful()
{
        return 0;
}

string mes( object me)
{
        return "�q�äl�̵o�X�@�q[1;37m���R���֦�[m\n"+
                "�ѤW�����������}�F,�@�D[1;33m���~[m�q�ѪŮg�F�U��\n"+
                "[1;36m�|�P��M�ŷx�F�_��[m,���W���t����������.\n";
}

int usesp() { return 120; }
int usemp() { return 20; }
int successful() { return 100; }
int delay() { return 4; }
int all() { return 1; }
string element() { return "air"; }

int check( object me)
{
        if( me->query("mind")< 700)
        {
                write("\n�A���G�������q��!\n");
                return 0;
        }

        return 1;
}
