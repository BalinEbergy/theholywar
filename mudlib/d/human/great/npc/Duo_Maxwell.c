// Duo_Maxwell.c
// written by koyamakyo
// 迪歐•麥斯威爾

inherit NPC;

void create()
{
        int HP= 500000, MP= 20000, SP= 30000;

        set_name("迪歐•麥斯威爾", ({ "Duo Maxwell","duo", "maxwell"}));
        set("lv", 80);
        set("class","MS駕駛員");
        set("ra","human");
        set("gender","男");
        set("describe","綁著棕色的辮子,看起來非常開朗的男孩子.");
        set("pre","");

        set("age", 15);
        set("element","dark");
        set("mind", 200);

        set("mixHP", HP);
        set("currentHP", HP);
        set("mixMP", MP);
        set("currentMP", MP);
        set("mixSP", SP);
        set("currentSP", SP);
        set("dem", 28000);
        set("def", 18000);
        set("mdem", 24000);
        set("mdef", 18000);
        set("spe", 4000);
        set("realmx", 20);
        set("exp", 15000);

        /*set("chat_chance", 10);
        set("chat_msg", ({
                (: this_object(), "random_move" :),
                "\n浪人慢慢的踱來踱去\n"
                })); */
        setup();
}

