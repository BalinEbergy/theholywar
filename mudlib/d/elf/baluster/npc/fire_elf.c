// fire_elf
// written by Hudson

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
    int HP= 930, MP= 300, SP= 5000;

    set_name("火炎小精靈", ({ "fire elf" }));
    set("lv", 9);
    set("class", WARRIOR);
    set("race", ELF);
    set("gender", 1);
    set("describe","一個小精靈.");

    set("age", 22);
    set("element", FIRE);
    set("mind", 127);

    set("maxHP", HP);
    set("currentHP", HP);
    set("maxMP", MP);
    set("currentMP", MP);
    set("maxSP", SP);
    set("currentSP", SP);
    set("strength", 320);
    set("armor", 200);
    set("spi", 5);
    set("speed", 130);
    set("exp", 1300);

    set("chat_chance", 15);
    set("chat_msg", ({
        "火精快樂的玩耍著\n",
        (: random_move :)
        }));

    set("combat_action", ([
        (: command("cast \"fire light\"") :) : 100 ]));
    add_skill_type(([ "fire": 10 ]));
    add_skill(({ "fire light" }));

    setup();
}

void init()
{
    object obj;

    if( !( obj= previous_object()) || !userp( obj))
        return;

    call_out("ki", 10, obj);
}

void ki( object obj)
{
    if( obj->is_fighting()) kill_ob( obj);
}
