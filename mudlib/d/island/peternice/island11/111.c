// center.c
// written by Hudson

#include <room.h>

inherit ROOM;

void create()
{
        set("short","*[1;37m巴路斯特廣場中心*[m");
        set("long",@LONG
你現在站在精靈之城--巴路斯特的中心,這裡是整個世界魔法
最為純粹的集中點,正中的艾爾芙水晶(Elf crystal)閃爍著
清亮的光輝,你覺得這裡的清靈洗滌了心中的雜念,精神也為
之一亮.
LONG
        );

        set("exits",
        ([
        "north" : __DIR__ + "nground1",
        ])
        );
