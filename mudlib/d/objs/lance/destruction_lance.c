// destroy_lance.c
// written by zz

#include <equip.h>
#include <element.h>

inherit EQUIP;

void create()
{
        set_name("[1;31m��[0;31m��[34m��[0;36m�u[m", ({ "destruction lance", "lance" , "destruction" , "zz"}));
        set("long",
@LONG
�@��[1;31m��[0;31m��[34m��[36m��[0m�M�Ϊ����u
LONG
                );
        set("lv", 100);
        set("unit", "��");
        set("element", THUNDER);
        set("epower", 1);
        set("weight", 1);

        set("wear_type", HAND);
        set("weapon", LANCE);
        set("dem_eff", 2000000);
        set("spe_eff", 100);
        set("eff", ({ "dem_eff", "spe_eff" }));
        setup();
}
