// create.c

void create()
{
    seteuid( getuid());
}

int main( object me, int argc, string *argv)
{
    object ob;

    if( argc< 2) write("你要創造什麼呢?\n");
    else {
        ob= new( argv[1]);
        if( ob) {
            message_vision(
                "[36m$N喃喃念著其他人都聽不懂的咒語...[m 周圍瞬間散發出[1;5;37m萬丈光芒!![m\n"+
                "一"+ ( ob->query("unit")? ob->query("unit"): "個")+ ob->name()+ "出現在$N的面前.\n", me);
            ob->move( environment( me));
        } else write("沒有這樣東西.\n");
    }

    return 1;
}

int help( object me)
{
    write( @HELP
指令格式: create <物品>

可讓巫師創造一個物品
HELP
        );

    return 1;
}
