// go.c
// rewritten by Hudson

mapping default_dirs = ([
        "north":                "北",
        "south":                "南",
        "east":                 "東",
        "west":                 "西",
        "up":                   "上",
        "down":                 "下",
]);

void follow( object me, object env, string arg);

void create() { seteuid( getuid()); }

int main( object me, int argc, string *argv)
{
    string arg;
    string dest, mout, min, dir;
    string npcdef, npcgo, temp, exits;
    object env, obj;
    mapping exit;

    if( argc== 1)
    {
        tell_object( me, "你要往哪個方向走?\n");
        return 1;
    }
    
    arg= argv[1];
    env= environment( me);

    if( me->is_fighting())
        tell_object( me, "你還在戰鬥耶!!\n");
    else if( !env)
        tell_object( me, "你哪裡也去不了.\n");
    else if( env->query("unmoveable"))
        tell_object( me, "\n有一股強大的魔力將你的身體禁錮,無法移動...\n");
    else if( !mapp( exit= env->query("exits")) || undefinedp( exit[arg]))
        tell_object( me, "這個方向沒有出路.\n");
    else if( me->query("currentSP")< 4 && !me->is_ghost() && userp( me))
        tell_object( me, "你累得走不動了.....\n");
    else {
        if( !me->is_ghost() && userp( me))
            me->add("currentSP", -4);

        dest= exit[arg];
        if( !( obj= find_object( dest)))
            call_other( dest, "???");
        if( !( obj= find_object( dest)))
        {
            tell_object( me, "無法移動.\n");
            return 1;
        }

        if( env->valid_leave( me, arg)) return 1;

        // 限制 mob 行動
        if( !userp( me) && !me->query("follow"))
        {
            sscanf( base_name( environment( me)), "%s/%s", npcdef, temp);
            while( sscanf( temp, "%s/%s", npcdef, temp)== 2);
            sscanf( base_name( obj), "%s/%s", npcgo, temp);
            while( sscanf( temp, "%s/%s", npcgo, temp)== 2);

            if( npcdef!= npcgo) return 1;
        }

        if( !undefinedp( default_dirs[arg]) )
            dir= default_dirs[arg];
        else dir= arg;

        mout= "往"+ dir+ "離開.\n";
        min= ( me->query("fly")? "飛": "走")+ "了過來.\n";

        if( !me->query("invis"))
            message("vision", me->name()+ mout, environment( me), ({ me }));

        if( !me->move( obj))
        {
            if( !me->query("invis"))
                message( "vision", me->name()+ min, environment( me), ({ me }));
            follow( me, env, arg);
        }
    }

    return 1;
}

void follow( object me, object env, string arg)
{
    int i;
    object *objs;

    objs= me->get_followed();

    i= sizeof( objs);
    while( i--)
    {
        if( environment( objs[i])== env)
        {
            tell_object( objs[i], "你嘗試跟隨著"+ me->name()+ "往"+
                default_dirs[ arg]+ "方走去.\n");
            objs[i]->command("go "+ arg);
        }
    }
}

int help( object me)
{
    write( @HELP
指令格式 : go <方向>
 
讓你往指定的方向移動。
 
HELP
    );
    return 1;
}
