// paper_3.c
// written by hiwind

inherit ITEM;

void create()
{
        set_name("秘訣", ({ "mystery" }));
        set("long", @LONG
各招皆有法師精通,然而2000年來最強也是最美的法師"夢"
曾經融會貫通所有招式:
HOLY       :....AJK#$^%(
AIR        :....!(%*#^#T
WIND       :....!%*^()#*
LIGHTNING  :....#JK!@%!@
FIRE       :..../??>@#$!
WATER      :....!@%#$#^:
EARTH      :....!@#%$^A%
DARK       :....>!@#%!$!
HELL       :....QWEKT@#$
LONG
                );
        set("unit", "卷");
        set("unsac", 0);
        set("no_get", 0);
        set("weight", 300);
}
