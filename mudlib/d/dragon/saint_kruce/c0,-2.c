//c0,-2.c
//written by zz

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;37m皇家騎士隊總部入口[m");
        set("long",@LONG
這裡是皇家騎士隊總部的入口.再往前走就可以進到營區內了.
皇家騎士隊是一支強大的軍隊,每個士兵都勇猛善戰.他們全都
是從這個總部訓練出來的,想接受訓練就趕快進去吧！
往南是總部營區,往北可以回到聖克魯斯廣場
LONG
        );
        set("exits", ([
                "north" : __DIR__ + "c0,-1.c" ,
                "south" : __DIR__ + "c0,-3.c" ,
                ]));
        setup();
}
