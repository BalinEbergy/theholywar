// ramuh.c
// written by Hudson
// 雷帝 拉姆

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
    int HP= 3000000, MP= 350000, SP= 20000;

    set_name("拉姆", ({ "ramuh" }));
    set("lv", 120);
    set("class", GOD);
    set("race", GODBEING);
    set("gender", 1);
    set("describe","一位留著白色長鬚,雙目炯炯有神的老者.");
    set("pre","雷帝");
    set("age", 1200);
    set("element", THUNDER);
    set("mind", 1000);

    set("teacher", ({
        30,
        }));
    set("favorite", ({
        "summoner",
        }));
    set("teach_mind", "-1000 ~ 1000");
    set("lv_up", "[1;36m拉姆說道:[33m 好了, 等級提升了[m\n");
    set("no_enu_money", "[1;36m拉姆說道:[33m 召喚師呀! 你的錢不夠[m");
    set("no_enu_lv", "[1;36m拉姆說道:[33m 你的等級還不夠![m");
    set("top_lv", "[1;36m拉姆說道:[33m 恭喜了,你的能力已經超越我了[m");
    set("teach_message", "[1;36m拉姆說道:[33m 召喚術嗎? 你想學哪項呢?[m");

    set("maxHP", HP);
    set("currentHP", HP);
    set("maxMP", MP);
    set("currentMP", MP);
    set("maxSP", SP);
    set("currentSP", SP);
    set("dem", 37000);
    set("def", 30000);
    set("mdem", 97000);
    set("mdef", 42533);
    set("spe", 3140);
    set("exp", 60000);

    setup();
}
