// neground_1.c
// written by Bother

#include <room.h>


inherit ROOM;

void create()
{
        set("short","[1;37m卡格烈斯廣場[m");
        set("long",@LONG
這裡是卡格烈斯城的廣場,西南方向前進就是廣場的正中心,代
表著這個城市的象徵,四周充滿著歡愉的嘻鬧聲,有幾個孩童在
附近嬉戲,你若是無聊可以到處走走散散心.
LONG
        );

        set("exits",
        ([
        "north" :__DIR__ + "neground_2",
        "south" :__DIR__ + "eground_1",
        "west" :__DIR__ + "nground_1",
        ])
        );

}
