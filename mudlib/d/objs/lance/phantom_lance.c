// phantom_lance.c
// written by zz

#include <equip.h>
#include <element.h>

inherit EQUIP;

void create()
{
        set_name("[0;36m����]��u[0m", ({ "phantom lance", "lance", "phantom" }));
        set("long", @LONG
�@�ⴲ�o�۶����]�𪺪��u
LONG
                );
        set("lv", 80);
        set("unit", "��");
        set("element", DARK);
        set("epower", 100);
        set("weight", 100);

        set("wear_type", HAND);
        set("weapon", LANCE);
        set("dem_eff", 4000);
        set("spe_eff", 100);
        set("eff", ({ "dem_eff", "spe_eff" }));
}
