// ending.c
// written by hiwind

#include <skill.h>

int main( object me, object obj)
{
        int demage, i, mdem, spirit, mdef;
        object *group;
        
        group= ( me->query("leader"))->query("group");
        i= sizeof( group);

        mdef= obj->query("mdef")+ obj->query("mdef_eff");
        mdem= group[1]->query("mdem")+ group[1]->query("mdem_eff")
                + group[0]->query("mdem")+ group[0]->query("mdem_eff");

        demage= mdem;

        if( me->query("element")== "light")
        {
                me->set("list", 3);

                if( me->query("group"))
                {
                        if( i> 2)
                        {
                                write("過多的人破壞了[1;33m時[1;36m空[m的[1;37m和[1;36m諧[m.\n");
                                me->delete("list");
                                return 1;
                        } else if( i== 2)
                        {
                                while( i--)
                                {
                                        if( group[i]->query("list")== 10)
                                        {

                                        group[i]->delete("list");

                                        obj->add("currentHP", -demage);

                                        tell_object( obj, "你能了解[1;33m時[1;36m空[m的力量嗎?\n");

                                        return demage;
                                        }
                                        else;
                                }
                                        write("你需要[1;35m暗黑[m的力量.\n");
                                        return 1;
                        } else
                        {
                                write("孤獨的人無法領悟[1;33m時[1;36m空[m的[1;37m奧義[m.\n");
                                me->delete("list");
                        }
                } else
                {
                        write("你並不是隊長.\n");
                }
        } else if( me->query("element")== "dark")
        {
                me->set("list", 10);

                if( me->query("group"))
                {
                        if( i> 2)
                        {
                                write("過多的人破壞了[1;33m時[1;36m空[m的[1;37m和[1;36m諧[m.\n");
                                me->delete("list");
                                return 1;
                        } else if( i== 2)
                        {
                                while( i--)
                                {
                                        if( group[i]->query("list")== 3)
                                        {

                                        group[i]->delete("list");

                                        obj->add("currentHP", -demage);

                                        tell_object( obj, "你能了解*[1;33m時*[1;36m空*[m的力量嗎?\n");

                                        return demage;
                                        }
                                        else;
                                }
                                        write("你需要[1;33m光明[m的力量.\n");
                                        return 1;
                        } else
                        {
                                write("孤獨的人無法領悟[1;33m時[1;36m空[m的[1;37m奧義[m.\n");
                                me->delete("list");
                        }
                } else
                {
                        write("你並不是隊長.\n");
                }
        } else
        {
                write("你不是被[1;33m時[1;36m空[m所挑選的[1;37m人[m.\n");
                return 1;
        }
}

string name( int select)
{
        switch( select)
        {
                case 1: return "天地終結";
                case 2: return "ending";
                case 3: return "ending";
        }
}

int type() { return HOLY; }
int lv() { return 40; }
int attackful()
{
        return 1;
}

string mes( object me)
{
        return "集中[1;37m精神[m,將全部的[1;31m魔力[m逐漸匯聚,而在身旁散發出[1;32m清[m\n[1;36m亮[m的[1;37m光芒[m.\n";
}

int usesp() { return 0; }
int usemp() { return 100; }
int successful() { return 100; }
int delay() { return 1; }
int all() { return 0; }
string element() { return "holy"; }

int check( object me)
{
        return 1;
}
