// thearn.c
// written by Hudson
// 謝倫

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
    int HP= 367000, MP= 6000, SP= 26000;

    set_name("謝倫", ({ "thearn" }));
    set("lv", 88);
    set("class", KNIGHT);
    set("race", ELF);
    set("gender",1);
    set("describe","一位眼神銳利,精神煥發的老者.");
    set("pre","共和國長老");
    set("selfsk", 1);

    set("age", 554);
    set("element", EARTH);
    set("mind", 857);

    set("maxHP", HP);
    set("currentHP", HP);
    set("maxMP", MP);
    set("currentMP", MP);
    set("maxSP", SP);
    set("currentSP", SP);
    set("strength", 30340);
    set("armor", 40000);
    set("mdem", 5000);
    set("api", 10);
    set("speed", 2540);
    set("exp", 8000);

    setup();

    carry_object( OB_LANCE + "elf_lance")->equip();
}

void init()
{
    if( !previous_object()
        || !userp(previous_object()))
        return;
    call_out("ki", 5, previous_object());
}

void ki( object obj)
{
    if( obj->is_fighting()) kill_ob( obj);
}
