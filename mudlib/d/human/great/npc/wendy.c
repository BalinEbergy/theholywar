// wendy.c
// written by Hudson
// 溫蒂

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
    int HP= 204697, MP= 44802, SP= 2000;

    set_name("溫蒂", ({ "wendy" }));
    set("lv", 66);
    set("class", WIZARD);
    set("race", DRAGON);
    set("gender", 0);
    set("describe", "一位穿著綠色長袍的龍族巫師.");
    set("pre","傳送使");

    set("age", 122);
    set("element", WIND);
    set("mind", 320);

    set("maxHP", HP);
    set("currentHP", HP);
    set("maxMP", MP);
    set("currentMP", MP);
    set("maxSP", SP);
    set("currentSP", SP);
    set("dem", 5275);
    set("def", 11984);
    set("mdem", 20636);
    set("mdef", 13533);
    set("spe", 600);
    set("exp", 5200);

    set("trans", ({ "baluster" }));

    setup();
}

void init()
{
    if( !previous_object()
        || !userp(previous_object()))
        return;
    call_out("mess", 1);
}

void mess()
{
    command("say 真不好意思, 施工期間我會待在廣場裡, 可來找我傳送唷~");
}
