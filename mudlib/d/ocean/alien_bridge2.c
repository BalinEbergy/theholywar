// alien_bridge2.c
// written by Hudson

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;36m�ȧQ���j��[m");
        set("long",@LONG
�A���b���j���ȧQ���j���W,�ɨ��ۥ˺��w���M�n������,��
�����@��L�ڪ�����,�ѪŤ@���Ŧ⪺�a��,�u���A�P��H��
���p,�ߤ��]���T�ɰ_�ǳ\�q�Ȥ���.
LONG
        );
        set("exits",
        ([
                "east"  : __DIR__ + "alien_bridge1",
                "west"  : __DIR__ + "alien_bridge3",
        ])
        );
}

int valid_leave( object me, string dir)
{
        int i;
        if( dir== "west")
        {
                i= (int)me->query_temp("long_road");
                i++;
                if( i== 5)
                {
                        me->delete_temp("long_road");
                        return 1;
                }
                else
                {
                        me->set_temp("long_road",i);
                        return notify_fail("\n����j�����F��u��.\n");
                }
        }
        else
        {
                if( dir== "east")
                {
                        i= (int)me->query_temp("long_road");
                        i--;
                        if( i== -5)
                        {
                                me->delete_temp("long_road");
                                return 1;
                        } else
                        {
                                me->set_temp("long_road",i);
                                return notify_fail("\n����j�����F��u��.\n");
                        }
                }
                return 1;
        }
}
