// Room: /d/wiz/courthouse.c

#include <ansi.h>

inherit ROOM;

void create()
{
	set("short", "法院");
	set("long", @LONG
這裡是東方故事的巫師審問機器人的法院﹐你一定是被人檢舉或是
行為太像機器人﹐才會來到這裡﹐如果你答不出審判官的三個問題﹐會
被做上記號﹐超過三次的人將會被處以極刑﹐好好回答吧。
LONG
	);
	set("no_fight", 1);
	set("no_magic", 1);
	set("no_spells", 1);
	set("objects", ([
		__DIR__"npc/judge":1 ]) );

	setup();
}

void test_me(object me)
{
	me->set_temp("old_startroom", me->query("startroom"));
	me->set("startroom", __FILE__);
	me->set_temp("last_location", base_name(environment(me)));
	message_vision("忽然一陣閃光罩住了$N。\n", me);
	me->move(this_object());
}
