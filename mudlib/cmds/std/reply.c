// reply.c

int main( object me, int argc, string *argv)
{
    int i;
    string msg;
    object obj;

    if( argc< 2)
        write("你要對他說什麼話呢?\n");
    else if( !objectp( obj= me->query_temp("reply_ob")))
        write("沒有人跟你說話.\n");
    else {
        msg= argv[1];
        for( i= 2; i< argc; i++)
            msg+= " "+ argv[i];

        tell_object( me, "[1;37m你告訴" + obj->name() + ": " + msg + "[m\n");
        tell_object( obj, sprintf("[1;37m%s告訴你: %s[m\n", me->name(), msg));

        if( userp( me))
            obj->set_temp("reply_ob", me);
    }

    return 1;
}

int help( object me)
{
    write( @HELP

指令格式﹕reply <訊息>

你可以用這個指令回應剛剛跟你說話的玩家.
HELP
    );

    return 1;
}
