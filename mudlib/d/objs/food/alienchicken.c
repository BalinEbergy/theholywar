// alienchicken.c
// written by Mondale

#include <element.h>

inherit FOOD;

void create()
{
    object obj;
    set_name("亞利恩香雞排", ({ "chicken" }));
    set("long", @LONG
亞利恩飯店的招牌菜.這味道聞起來真誘人哪!
LONG
        );
    set("unit", "份");
    set("lv", 0);
    set("value", 1);

    set_weight( 50);
}
