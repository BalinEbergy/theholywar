// elf_crystal.c
// written by Hudson

#include <element.h>

inherit ITEM;

void create()
{
        set_name("艾爾芙水晶", ({ "elf crystal", "elf", "crystal" }));
        set("long", @LONG
你往它望去,只覺得心中一陣平靜,澄澈明亮的柔和
光芒散發在水晶四周,你感到有一陣源源不絕的魔力
慢慢的散發出來.
LONG
                );
        set("lv", 100);
        set("unit", "顆");
        set("element", LIGHT);
        set("epower", 100);

        set("unsac", 1);

        set_weight( 30000);
}
