// center.c
// written by Hudson

#include <room.h>

inherit ROOM;

void create()
{
        set("short","*[1;37m........*[m");
        set("long",@LONG
.......
LONG
        );

        set("exits",
        ([
        "south" : __DIR__ + "testn1e1.c",
        "east" : __DIR__ + "testn2e2.c",
        ])
        );
