// kris.c
// written by Mondale
// �J�R��

inherit NPC;

void create()
{
        int HP= 250000, MP= 500, SP= 60000;

        set_name("[m[1m�J�R��[m", ({ "kris" }));
        set("lv", 150);
        set("class","magicsword");
        set("ra","�H��");
        set("gender","�k");
        set("describe","�o�n���O�o�䪺�u�ö���.");
        set("pre","[m[1;36m�J[32m��[m");

        set("age", 17);
        set("element","water");
        set("mind", 1000);

        set("mixHP", HP);
        set("currentHP", HP);
        set("mixMP", MP);
        set("currentMP", MP);
        set("mixSP", SP);
        set("currentSP", SP);
        set("dem", 95000);
        set("def", 0);
        set("mdem", 0);
        set("mdef", 0);
        set("spe",1500);
        set("realmx", 0);
        set("exp", 12500000);

        set("chat_chance", 20);
        set("chat_msg", ({
                "\n�J�R���V�|�B�i��ۡC\n"
                }));
        setup();
        setup();
}

void init()
{
        ::init();
        if( !previous_object()
                || !userp(previous_object()))
                return;
        call_out("action", 1, previous_object());
        kill_ob( previous_object());
}

void action( object obj)
{
        command("say �o��|���}��A�i�J�̦��I");
        setup();
}
