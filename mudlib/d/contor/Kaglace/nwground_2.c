// nwground_2.c
// written by Bother

#include <room.h>


inherit ROOM;

void create()
{
        set("short","[1;37m卡格烈斯廣場[m");
        set("long",@LONG
這裡是卡格烈斯城的廣場,西北方向前進就是廣場的噴水池,代
表著這個城市的象徵,清澈見底的水池你是否會想進去玩水,偶
爾你會看到有一群小孩在附近玩捉迷藏.
LONG
        );

        set("exits",
        ([
        "south" :__DIR__ + "nwground_1",
        "east" :__DIR__ + "nground_2",
        ])
        );

}
