// meditation.c
// written by Hudson

#include <skill.h>

int main( object me, object obj)
{
        me->set("meditation", 1);

        return 1;
}

string name( int select)
{
        switch( select)
        {
                case 1: return "冥想";
                case 2: return "meditation";
                case 3: return "meditation";
        }
}

int type() { return MAGIC; }
int lv() { return 1; }
int attackful()
{
        return -1;
}

int usesp() { return 0; }
int usemp() { return 0; }
int successful() { return 0; }
int delay() { return 0; }
int all() { return 0; }
string element() { return "air"; }

int check( object me)
{
        return notify_fail("\n\n你辦不到...\n");
}
