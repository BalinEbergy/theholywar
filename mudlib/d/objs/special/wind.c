// wind.c
// written by Hudson

#include <element.h>

inherit ITEM;

void create()
{
    set_name("風", ({ "wind" }));
    set("long", @LONG
一陣陣風在空中呼嘯著.
LONG
        );
    set("lv", 1);
    set("element", WIND);
    set("epower", 100);

    set("unsac", 1);
    set("no_get", 1);
}

void disappear()
{
    tell_room( environment( this_object()), "呼嘯著的風停了!\n");
    destruct( obj);
}
