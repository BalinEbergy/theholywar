// seamew.c
// written by zz

#include <class.h>
#include <element.h>
#include <race.h>

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
        int HP= 12000, MP= 200, SP= 10000;

        set_name("海鷗", ({ "seamew"}));
        set("lv", 20);
        set("race","animal");
        set("gender","動物");
        set("describe","一隻海鷗.");
        set("pre","遨遊在天空中的");
        set("age", 8);
        set("element",WIND);
        set("mind", 10);

        set("maxHP", HP);
        set("currentHP", HP);
        set("maxMP", MP);
        set("currentMP", MP);
        set("maxSP", SP);
        set("currentSP", SP);
        set("dem", 400);
        set("def", 150);
        set("mdem", 80);
        set("mdef", 80);
        set("spe", 120);
        set("exp", 2500);

        set("chat_chance", 12);
        set("chat_msg", ({
                (: random_move :)
                }));
        setup();
}
