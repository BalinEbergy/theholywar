// magic.c
// written by hiwind

inherit NPC;

void create()
{
        int HP= 80000, MP= 2000,SP= 1000;

        set_name("流浪法師", ({ "magic" }));
        set("lv", 50);
        set("ra", "elf");
        set("describe"," 法師.");
        set("age", 25);
        set("element", "dark");
        set("mind", 0);

        set("mixHP", HP);
        set("currentHP", HP);
        set("mixMP", MP);
        set("currentMP", MP);
        set("mixSP", SP);
        set("currentSP", SP);
        set("dem", 1000);
        set("def", 600);
        set("mdem", 8000);
        set("mdef", 3000);
        set("spirit", 25);
        set("spe", 200);
        set("exp", 3000);
        set_skill_level( 4, 50);
        set_skill_level( 8, 50);
        set("skill", ({ "4:tornado", "8:water slice"}));
        set("skill_chance", 100);
        setup();
        set("chat_chance", 5);
        set("chat_msg", ({
                (: this_object(), "random_move" :)}));

}
