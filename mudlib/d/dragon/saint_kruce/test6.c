//test6.c
//written by zz

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;37mtest6[m");
        set("long",@LONG
test Lv61-70
LONG
        );
        set("exits", ([
                "north" : __DIR__ + "test5.c" ,
                "south" : __DIR__ + "test7.c" ,
                ]));
        set("objects",  ([
                __DIR__ + "npc/test/lv61" :1 ,
                __DIR__ + "npc/test/lv62" :1 ,
                __DIR__ + "npc/test/lv63" :1 ,
                __DIR__ + "npc/test/lv64" :1 ,
                __DIR__ + "npc/test/lv65" :1 ,
                __DIR__ + "npc/test/lv66" :1 ,
                __DIR__ + "npc/test/lv67" :1 ,
                __DIR__ + "npc/test/lv68" :1 ,
                __DIR__ + "npc/test/lv69" :1 ,
                __DIR__ + "npc/test/lv70" :1 ,
        ]));
        set("no_recall", 1);
        setup();
}
