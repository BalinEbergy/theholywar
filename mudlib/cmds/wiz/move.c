// move.c

int main( object me, int argc, string *argv)
{
    object player;

    mapping important_place= ([
        "baluster":         "/d/elf/baluster/center",
        "saintkruce":       "/d/dragon/saint_kruce/center",
        "chogall":          "/d/ogre/chogall/center",
        "great":            "/d/human/great/center",
        "holy":             "/d/holyworld/temple/center"
        ]);

    if( argc< 2)
        write("你要到哪去呢?\n");
    else if( important_place[ argv[1]])
        me->move( important_place[ argv[1]]);
    else if( player= find_player( argv[1]))
        me->move( environment( player));
    else if( file_size( argv[1]+ ".c")< 0)
        write("沒有這個檔案.\n");
    else me->move( argv[1]);

    return 1;
}

int help( object me)
{
    write( @HELP
指令格式: move <目標|玩家>

baluster    巴路斯特
saintkruce  聖克魯斯
chogall     卡格爾
great       大城
holy        大神聖殿

移動到任何你想去的地方, 或者到某個玩家的身邊
HELP
        );
    return 1;
}
