// skill.c

#include <element.h>
#include <skill.h>

mapping skill_dir= ([
    //"holy": "magic/holy", "light": "magic/light", "cosmos": "magic/cosmos",
    /*"wind": "magic/wind", */"thunder": "magic/thunder", "fire": "magic/fire",
//    "water": "magic/water", "earth": "magic/earth"// , "magic/dark", "magic/evil"
    ]);
mapping skill_titles= ([
    "fire": "火系法術 (Fire)",
    "thunder": "雷系法術 (Thunder)",
    ]);
mapping skill= ([ ]);
mixed *eff= ({
    //  H   L   C   W   T   F   A   E   D   V  //
    ({  5, 10, 10, 10, 10, 15, 15, 15, 15, 15 }),
    ({  8,  5, 10, 10, 10, 10, 10, 10, 12, 12 }),
    ({ 10, 10,  5, 10, 10, 10, 10, 10, 10, 10 }),
    ({  9, 10, 10,  5,  8, 10, 10, 12, 10, 11 }),
    ({  9, 10, 10, 12,  5,  8, 10, 10, 10, 11 }),
    ({ 11, 10, 10, 10, 12,  5,  8, 10, 10,  9 }),
    ({ 11, 10, 10, 10, 10, 12,  5,  8, 10,  9 }),
    ({ 11, 10, 10,  8, 10, 10, 12,  5, 10,  9 }),
    ({ 12, 12, 10, 10, 10, 10, 10, 10,  5,  8 }),
    ({ 15, 15, 15, 15, 15, 10, 10, 10, 10,  5 })
    });

void create()
{
    int i, j;
    string *skill_types, *skill_objs;

    seteuid( getuid());
    i= sizeof( skill_types= keys( skill_dir));
    while( i--)
    {
        skill_objs= get_dir( DIR_SKILL( skill_dir[ skill_types[i]]));
        skill_objs-= ({ "." });
        j= sizeof( skill_objs);
        while( j--)
        {
            skill_objs[j]= DIR_SKILL( skill_dir[ skill_types[i]])+ skill_objs[j];
            skill[ skill_objs[j]->query("id")]= skill_objs[j];
        }
    }
}

string skill_title( string s) { return skill_titles[ s]; }
string get_skill( string name) { return skill[ name]; }
string find_skill( string name, string *skills)
{
    string cur_skill;

    if( stringp( cur_skill= find_string( name, skills)))
        cur_skill= skill[ cur_skill];

    return cur_skill;
}

void hit( object me, string skill, object obj)
{
    int *damage, sk_ele, me_ele, ob_ele;
    string str;

    damage= skill->damage( me, obj, 0);

    sk_ele= skill->query("element");
    me_ele= me->query("element");
    ob_ele= obj->query("element");
    for( int i= 0; i< 3; i++)
        damage[i]= damage[i]*
            eff[sk_ele][ob_ele]*
            ( me_ele== sk_ele? 11: 10)/ 100* 
            ( 100- skill->query("volatility")+ random( skill->query("volatility")* 2))/ 100;

    obj->add("currentHP", -damage[0]);
    obj->add("currentMP", -damage[1]);
    obj->add("currentSP", -damage[2]);
    if( skill->query("attack"))
    {
        str= "$N";
        if( !skill->no_weapon())
            str+= "手持"+ me->query_temp("weapon")->name();
        str+= "施展"+ skill->name()+ "對$n"+ D_COMBAT->underattack( obj, damage[0]);
        message_vision( str+ "\n", me, obj);
    }
}

varargs void cast_skill( object me, string skill, object obj)
{
    if( skill->query("attack"))
    {
        if( environment( me)->query("no_fight"))
        {
            tell_object( me, "這裡的氣氛讓你無法提起勁兒來戰鬥.\n");
            return;
        }

        if( !objectp( obj) && me->is_fighting())
            obj= me->query_enemies()[0];
    }

    if( objectp( obj))
    {
        if( userp( me) && userp( obj) && obj!= me && !me->is_fighting( obj))
            if( obj->query("lv")< 10 || obj->query("lv")+ 6< me->query("lv"))
            {
                write("你不能攻擊低等玩家.\n");
                return;
            }

        if( me->query("currentHP")< skill->usehp( me))
        {
            tell_object( me, "[1;37m你的生命值不夠了![m\n");
            return;
        } else if( me->query("currentMP")< skill->usemp( me))
        {
            tell_object( me, "[1;36m你的法力不夠了![m\n");
            return;
        } else if( me->query("currentSP")< skill->usesp( me))
        {
            tell_object( me, "[1;33m你的體力不夠了![m\n");
            return;
        } else if( !skill->check( me))
            return;

        me->add("currentHP", -skill->usehp( me));
        me->add("currentMP", -skill->usemp( me));
        me->add("currentSP", -skill->usesp( me));
        if( skill->delay( me))
            me->start_delay( skill->delay( me));

        if( random( 100)> skill->successful( me))
        {
            tell_object( me, "你失敗了.\n");
            me->add_skill_exp( skill->type(), skill->query("exp")/ 2);
            return;
        } else me->add_skill_exp( skill->type(), skill->query("exp"));

        if( obj!= me)
            obj->kill_ob( me);

        if( skill->mes( me))
            message_vision( skill->mes( me)+ "\n", me, obj);

        if( skill->query("special_process"))
            skill->main( me, obj);
        else hit( me, skill, obj);

        if( obj->query("currentHP")<= 0)
            obj->die( me);
    }
}
