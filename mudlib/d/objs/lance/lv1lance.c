// lv1lance.c
// written by Hudson

#include <equip.h>
#include <element.h>

inherit EQUIP;

void create()
{
    set_name("���ߵu�j", ({ "novice lance" }));
    set("long", @LONG
�@�`��s���u�j.
LONG
        );
    set("lv", 1);
    set("unit", "�`");
    set("element", COSMOS);
    set("value", 5);

    set("wear_type", HAND);
    set("weapon", LANCE);
    set("damage", 45);
    set("hit_eff", 180);
    set("eff", ({ "hit_eff" }));

    set_weight( 40);
}
