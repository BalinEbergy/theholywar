// stab_multi.c
// written by zz

#include <skill.h>

int main( object me, object obj)
{
        int damage, def_eff, dem_eff, dem, def, lv, hit, spirit, speed, ospeed;
        string weapon_name;

        def_eff= obj->query("defand_eff");
        def= obj->query("def")+ obj->query("def_eff");
        dem_eff= me->query("damage_eff");
        dem= me->query("dem")+ me->query("dem_eff");
        lv= ( 400- me->skill_level( STAB));
        hit= me->skill_level( STAB)/ 25+ 5;
        spirit= me->query("spirit");
        speed= me->query("spe");
        ospeed= obj->query("spe");
        weapon_name=( me->query_temp( me->query_temp("stb")))->query("name");

        while( hit--)
        {
                damage= ( dem*lv/400- def+ dem_eff- def_eff+ random( dem/ 5)+ random( dem/5))* 8/ 10;
                if( damage< 0) damage= 0;
                if( hit)
                if(( speed* 2- ospeed)* 30/ speed+ spirit/ 2> random( 100))
                {
                        obj->add("currentHP", -damage);
                        message_vision("$N�I�i[1;33m�g�ì��[m�뤤�F[31m���n����[m,��$n" + COMBAT_D->underattack( damage, obj)
                        + "([1;31m" + ((damage>= 100000)? ((damage>= 400000)? "[33;41m!!!!!!": "*[37;41m*****") : damage)
                        + "[m)\n", me, obj);
                        if( obj->query("currentHP")< 0)
                        {
                                return -1;
                                me->delete_temp("stb");
                        }
                } else
                {
                        if( spirit+ random( 20)> random( 100))
                        {
                                damage= damage/ 10+ random( damage/100);
                                obj->add("currentHP", -damage);
                                message_vision("$N�I�i[1;33m�g�ì��[m��[1;32m�S�����n�`[0m,��$n" +
                                COMBAT_D->underattack( damage, obj)+ "([1;31m" + ((damage>= 100000)? ((damage>= 400000)?
                                "[33;41m!!!!!!": "[37;41m*****") : damage)+ "[m)\n", me, obj);
                                if( obj->query("currentHP")< 0)
                                {
                                        return -1;
                                        me->delete_temp("stb");
                                }
                        } else
                        {
                                damage= damage/ 100+ random( damage/50);
                                obj->add("currentHP", -damage);
                                message_vision("$N�I�i[1;33m�g�ì��[m���O[36m�밾�F[0m,��$n" +
                                COMBAT_D->underattack( damage, obj)+ "([1;31m" + ((damage>= 100000)? ((damage>= 400000)?
                                "[33;41m!!!!!!": "[37;41m*****") : damage)+ "[m)\n", me, obj);
                                if( obj->query("currentHP")< 0)
                                {
                                        return -1;
                                        me->delete_temp("stb");
                                }
                        }
                }
        }
        damage= damage* 9/ 7;
        message_vision("\n$N��[0;31m���[m��F�F[1;36m�̰��I[0m,�����N���U��[1;33;41m���������K[0m�@��.\n", me);
        message_vision("$N�X�ĤH[1;35m�M�����[0m,�N�Ҧ�[0;31m���[m�b�@����[1;31m�z�o[0m�X��,�µ�$n��h.\n", me, obj);
        return damage;
}

string name( int select)
{
        switch( select)
        {
                case 1: return "�g�ì��";
                case 2: return "stab multi";
                case 3: return "multi";
        }
}

int type() { return STAB; }
int lv() { return 10; }
int attackful()
{
        return 1;
}

string mes( object me)
{
        return "�ߤ���M�E�_�@��[1;33m�j�j[0m��[1;31m���[0m,�V�ۼĤH[31m�ƨg[0m����h.\n";
}

int usesp() { return 20; }
int usemp() { return 0; }
int successful() { return 100; }
int delay() { return 2; }
int all() { return 0; }
string element() { return ""; }

int check( object me)
{
        if( me->query_temp("stb") != "handb")
        {
                write("\n�A�����j,�L�k�ϥX���O!\n");
                me->delete_temp("stb");
                return 0;
        }
        if( me->query("currentSP")< 20)
        {
                write("\n�A[5;33m�S�O[m�F....\n");
                me->delete_temp("stb");
                return 0;
        }
        return 1;
}
