// house1.c
// written by Mondale

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;37m公會會長的華宅[m");
        set("long",@LONG
走進來一看，裡面的裝潢卻不如想像中的華麗，這裡是
客廳，卻只有簡陋的傢俱....原來會長這麼節儉啊！
東邊有一扇門，是通往會長的圖書室的。
南邊也有一扇門，應該是通往會長房間的。
LONG
        );

        set("exits",
        ([
        "west" : __DIR__ + "estreet3-1",
        ])
        );

        set("objects",  ([
        ]));

        setup();
}
