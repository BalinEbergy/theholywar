// long_blade.c
// written by Hiwind

#include <equip.h>
#include <element.h>

inherit EQUIP;

void create()
{
        set_name("長刀", ({ "blade" }));
        set("long", @LONG
一般的長刀.
LONG
                );
        set("lv", 10);
        set("unit", "把");
        set("weight", 100);

        set("wear_type", HAND);
        set("weapon", BLADE);
        set("dem_eff", 5);
        set("eff", ({ "dem_eff" }));
}
