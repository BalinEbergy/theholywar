// girl_3.c
// written by hiwind

inherit NPC;

void create()
{
        int HP= 250000, MP= 100000 ,SP= 100000;

        set_name("[1;37m���_������[m", ({ "girl" }));
        set("lv", 90);
        set("class","mage");
        set("ra","human");
        set("describe","�@��Q���ˤߪ��k�l.");
        set("age", 16);
        set("mind", 750);
        set("gender", "�k");

        set("mixHP", HP);
        set("currentHP", HP);
        set("mixMP", MP);
        set("currentMP", MP);        
        set("mixSP", SP);
        set("currentSP", SP);
        set("dem", 25000);
        set("def", 25000);
        set("mdem", 10000);
        set("mdef", 5000);
        set("spirit", 60);
        set("spe", 2000);
        set("realmx", 400);
        set("exp", 100000);

        setup();
}
void init()
{
        if( !previous_object()
                || !userp(previous_object()))
                return;
        call_out("action", 10, previous_object());
}

void action( object obj)
{
        if( !obj || !present(obj) || this_object()->is_fighting()) return;
        if( obj->is_fighting()) kill_ob(obj);

        command("hihi �D�H....");
}
