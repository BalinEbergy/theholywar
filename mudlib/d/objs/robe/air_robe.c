// air_robe.c
// written by Hudson

#include <equip.h>
#include <element.h>

inherit EQUIP;

void create()
{
    set_name("天之袍", ({ "air robe", "air", "robe" }));
    set("long", @LONG
一件淡藍色的長袍,在內側繪著天空之鑰的法陣,
有一股魔力隱隱的透了出來.
LONG
        );
    set("lv", 90);
    set("unit", "件");
    set("element", COSMOS);
    set("epower", 60);
    set("value", 700);

    set("wear_type", BODY);
    set("def_eff", 9000);
    set("defand_eff", 3000);
    set("mdem_eff", 4000);
    set("eff", ({ "def_eff", "defand_eff", "mdem_eff" }));

    set_weight( 100);
}
