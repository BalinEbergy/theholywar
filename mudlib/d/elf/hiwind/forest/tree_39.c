//tree_39.c
//write by hiwind

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;31m��[m[1;37m��[1;31m��[m[1;32m�L[m");
        set("long",@LONG
�ܤ������A�O�����i�L�Ѫ��H,����ť�󤧫e�g�]�N�h��ĵ�i,
�i�J�F���a,���O�A�ݤF�ݥ|�P,�o�o�{�|�P���������`�޲�,��
�T�۫H�O�H�һ�����.
���B��[1;31m��[m���Ӭ��F,²������@��,�ө_�ǤF,�s���e���o�ܴr��
���p�ʪ��������F,�٬O�p�ߤ@��.
LONG
        );

        set("exits",
        ([
        "west" : __DIR__ + "tree_40",
        "south" : __DIR__ + "tree_38",
        ])
        );

        set("objects",
        ([
                __DIR__ + "npc/hiwind_2":1,
                __DIR__ + "npc/hiwind_7":1,
        ])
        );

        set("no_fight", 0);
        setup();
}
