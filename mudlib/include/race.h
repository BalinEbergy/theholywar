// race.h

#ifndef __RACE__
#define __RACE__

#define GODBEING        0
#define SAINTRE         1
#define ELF             2
#define DRAGON          3
#define HUMAN           4
#define OGRE            5
#define ANIMAL          6
#define MONSTER         7
#define GHOST           8
#define DEMON           9
#define ELEMENT         10

#endif
