// blade1.c
// written by Hiwind

#include <equip.h>
#include <element.h>

inherit EQUIP;

void create()
{
        set_name("�g�M", ({ "blade" }));
        set("long", @LONG
LONG
                );
        set("lv", 100);
        set("unit", "��");
        set("element", DARK);
        set("epower", 100);
        set("weight", 500);

        set("wear_type", HAND);
        set("weapon", BLADE);
        set("dem_eff", 20000);
        set("def_eff", 7500);
        set("eff", ({ "dem_eff", "def_eff" }));
}
