// class.h

#ifndef __CLASS__
#define __CLASS__

#define GOD             0
#define COMMONER        1
#define WIZARD          2
#define SUMMONER        3
#define SHAMAN          4
#define ALCHEMIST       5
#define WARRIOR         6
#define MINSTREL        7
#define KNIGHT          8

#endif
