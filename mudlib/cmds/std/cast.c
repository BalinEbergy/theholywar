// cast.c

string *magic= ({
    "holy", "light", "cosmos", "wind", "thunder", "fire", "water", "earth", "dark", "evil" });

void create() { seteuid( getuid()); }

int main( object me, int argc, string *argv)
{
    int i;
    string skill;
    object obj;

    if( argc< 2)
        write("你想要吟唱哪個魔法呢?\n");
    else {
        if( argc> 2)
            if( !objectp( obj= find( argv[2], environment( me))) ||
                !obj->is_character() || obj->is_ghost())
            {
                write("沒有這個人喔!\n");
                return 1;
            }

        for( i= 0; i< 10; i++)
            if( stringp(
                skill= D_SKILL->find_skill(
                    argv[1], me->query_skills(
                        magic[i])
                    )
                )
            )
                break;

        if( !stringp( skill))
            write("你不會這個法術喔!\n");
        else D_SKILL->cast_skill( me, skill, obj);
    }

    return 1;
}

int help( object me)
{
    write( @HELP
指令格式: cast <法術> [目標]

當你學過了魔法, 這個指令可以讓你展現你訓練的成果!
HELP
        );

    return 1;
}
