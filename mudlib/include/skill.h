#ifndef __SKILL__
#define __SKILL__

#include <element.h>

// Directories

#define DIR_SKILL(x)        ( "/d/skill/"+ x+ "/")

// Main skill objects to be inherited

#define SKILL           "/objs/skill/skill"
#define MAGIC           "/objs/skill/magic"
#define HOLY_MAGIC      "/objs/skill/magic/holy_magic"
#define LIGHT_MAGIC     "/objs/skill/magic/light_magic"
#define COSMOS_MAGIC    "/objs/skill/magic/cosmos_magic"
#define WIND_MAGIC      "/objs/skill/magic/wind_magic"
#define THUNDER_MAGIC   "/objs/skill/magic/thunder_magic"
#define FIRE_MAGIC      "/objs/skill/magic/fire_magic"
#define WATER_MAGIC     "/objs/skill/magic/water_magic"
#define EARTH_MAGIC     "/objs/skill/magic/earth_magic"
#define DARK_MAGIC      "/objs/skill/magic/dark_magic"
#define EVIL_MAGIC      "/objs/skill/magic/evil_magic"

#endif
