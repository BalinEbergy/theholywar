// eground1.c
// written by Hudson

#include <room.h>

inherit ROOM;

void create()
{
    set("short","[1;31m�d�溸�s��[m");
    set("long",@LONG
�o�̬O�d�溸�s��,�ө������]���W���o�۴ݼɤ���,�O�A��
�߬O�_�U�@��K�|�����L�̪�����.�e��i���쫰���������j
�D�ШȧB�N���j�D,�Ӥj�D�����Y,�K�O�������l����������
�~�������c�ШȧB�N���j��.
LONG
    );

    set("exits", ([
        "east"  : __DIR__ + "eground2",
        "west"  : __DIR__ + "center",
        ]));

    set("objects", ([
        __DIR__+ "npc/rovecub": 1
        ]));

    setup();
}
