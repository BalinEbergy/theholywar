// magic.c

#include <skill.h>

inherit SKILL;

int no_weapon() { return 1; }

int *damage( object me, object obj, mixed information)
{
    int magic, spirit;
    int *dam= allocate( 3);

    magic= me->query("magic")+ me->query_temp("mdem_eff");
    spirit= obj->query("spi")+ obj->query_temp("mdef_eff");
    dam[0]= (( query("base_value/hp")> magic? magic: query("base_value/hp"))+
        magic* query("improve/hp")/ 100)* ( 100- spirit)/ 100;

    if( query("attack"))
        dam[0]-= dam[0]* obj->query_temp("magic_eff")[ query("element")]/ 100;

    return dam;
}
