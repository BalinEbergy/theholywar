// girl_2.c
// written by hiwind

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
    int HP= 125000, MP= 20000 ,SP= 30000;

    set_name("[1;5;31m����[m[1m���^��[m", ({ "girl" }));
    set("lv", 80);
    set("class",WARRIOR);
    set("race",HUMAN);
    set("describe","�@��Q������誺�k�l.");
    set("age", 16);
    set("mind", 600);
    set("gender", 0);

    set("maxHP", HP);
    set("currentHP", HP);
    set("maxMP", MP);
    set("currentMP", MP);
    set("maxSP", SP);
    set("currentSP", SP);
    set("strength", 15000);
    set("def", 21000);
    set("mdem", 7500);
    set("mdef", 4000);
    set("spirit", 60);
    set("speed", 1700);
    set("exp", 0);

    set("chat_chance", 20);
    set("chat_msg", ({
        (: random_move :),
        "�^��L�U���F�i���.\n"
        }));
    setup();
}
void init()
{
    if( !previous_object()
        || !userp(previous_object()))
        return;
    call_out("action", 10, previous_object());
}

void action( object obj)
{
    if( !obj || !present(obj) || this_object()->is_fighting()) return;
    if( obj->is_fighting()) kill_ob(obj);

    command("say �D�H....����....�A�̦b����?");
}
