// crystal.c
// written by Hudson

#include <equip.h>
#include <element.h>

inherit EQUIP;

void create()
{
    set_name("[1;36m����[1;37m�y[m", ({ "crystal ball", "crystal", "ball" }));
    set("long", @LONG
�@�����o�L�L���~�������y.
LONG
        );
    set("color", 10);
    set("lv", 70);
    set("unit", "��");
    set("element", LIGHT);
    set("epower", 100);

    set("wear_type", EQ_LIGHT);
    set("spe_eff", 50);
    set("eff", ({ "spe_eff" }));

    set_weight( 70);
}
