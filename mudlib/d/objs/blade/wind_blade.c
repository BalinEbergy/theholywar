// long_blade.c
// written by Hiwind

#include <equip.h>
#include <element.h>

inherit EQUIP;

void create()
{
        set_name("風刃", ({ "wind blade", "wind", "blade" }));
        set("long", @LONG
一把十分古樸的刀.刀鋒散發出淡淡的光芒.
LONG
                );
        set("lv", 10);
        set("unit", "把");
        set("weight", 250);

        set("wear_type", HAND);
        set("weapon", BLADE);
        set("dem_eff", 20);
        set("def_eff", 5);
        set("eff", ({ "dem_eff", "def_eff" }));
}
