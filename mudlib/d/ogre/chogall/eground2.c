// eground2.c
// written by Hudson

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;31m卡格爾廣場邊緣[m");
        set("long",@LONG
再往前走,便是卡格爾的中央大道－亞伯吉斯大道,你見到面
前寬大的路面,不禁為卡格爾豪大的規模所震攝,遠方的盡頭
閃耀著紅色的光輝,便是亞伯吉斯大殿的冰山一隅吧?
LONG
        );

        set("exits", ([
                "east"  : __DIR__ + "firen1",
                "west"  : __DIR__ + "eground1",
                ]));
}
