// grass_1.c
// written by hiwind

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;32m���[m");
        set("long",@LONG
�A�~��V�e��,�A�ݨ�@�Ӥ@��L�ڪ����,��B�����F��
(grass)�A�w�g�L�k�^�Y,�u���D������G�i�H�^��[1;36m��[m����,
�ө��F���G�O�h���䪺�F�y,��!�٬O�����X���a!
LONG
        );

        set("exits",
        ([
        "east" : __DIR__ + "grass_3",
        "west" : __DIR__ + "street_9",
        "north" : __DIR__ + "grass_2",
        ])
        );

        set("objects",
        ([
                __DIR__ + "forest/npc/bird":2,
        ])
        );

        set("no_fight", 0);
        setup();
}
