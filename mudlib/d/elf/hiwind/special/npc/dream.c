// dream.c
// written by hiwind

inherit NPC;

void create()
{
        int HP= 3000000, MP= 10000 ,SP= 200000;

        set_name("夢", ({ "dream" }));
        set("lv", 100);
        set("class","mage");
        set("ra","elf");
        set("describe", "全身散發強大魔力的法師.");
        set("age", 25);
        set("mind", 1000);
        set("element", "holy");
        set("gender", "女");

        set("mixHP", HP);
        set("currentHP", HP);
        set("mixMP", MP);
        set("currentMP", MP);
        set("mixSP", SP);
        set("currentSP", SP);
        set("str", 10);
        set("dem", 5000);
        set("vir", 15);
        set("def", 10000);
        set("magic", 90);
        set("mdem", 85000);
        set("mdef", 35000);
        set("spirit", 40);
        set("speed", 20);
        set("spe", 2000);
        set("realmx", 150);
        set("exp", 0);

        set_skill_level( 3, 100);
        set_skill_level( 5, 100);
        set("skill", ({  "5:dream", "3:advent of kaigic", }));
        set("skill_chance", 75);
        setup();
}
void init()
{
        if( !previous_object()
                || !userp(previous_object()))
                return;
        call_out("action", 1, previous_object());
}

void action( object obj)
{
        if( !obj || !present(obj) || this_object()->is_fighting()) return;
        if( obj->is_fighting()) kill_ob(obj);

        if( !this_object()->query("group"))
        {
                command("group final");
        } else
        {
                command("hihi 讓你見識夢之回憶吧....");
        }
}
