// waiter.c
// written by Mondale
// 服務生

inherit NPC;

void create()
{
        int HP= 20000, MP= 0, SP= 100000;

        set_name("服務生", ({ "manager" }));
        set("lv", 20);
        set("class","warrior");
        set("ra", "human");
        set("gender","男");
        set("describe","一位笑容可掬的服務生.");
        set("pre","旅店的");

        set("age", 29);
        set("element", "earth");
        set("mind", 0);

        set("mixHP", HP);
        set("currentHP", HP);
        set("mixMP", MP);
        set("currentMP", MP);
        set("mixSP", SP);
        set("currentSP", SP);
        set("dem", 6000);
        set("def", 6000);
        set("mdem", 6000);
        set("mdef", 6000);
        set("spe", 600);
        set("realmx", 10);
        set("exp", 500);
        set("chat_chance", 20);
        set("trading", 1);
        set("buy_mind", "-1000~1000");
        set("selling", ({
                "/d/dragon/alien/obj/steak",
                }));

        setup();

}

void init()
{
        ::init();
        if( !previous_object()
                || !userp(previous_object()))
                return;
        call_out("ki",1,previous_object());
}

void ki(object obj)
{
command ("say 想要住宿嗎?" + obj->name() + "..只要打stay就可以了喔.");
}
