// channel.c

#define MAX_RECORD 10

mapping channel_records= ([
    "chat": ({ }),
    "rumor": ({ })
    ]);

mapping channels= ([
    "chat": "[1;32m�i����j[37m%s: [m%s\n",
    "rumor": "[1;31m�i�����j�Y�H: %s[m\n"
    ]);

int filter_listener( object ppl)
{
    if( !environment( ppl))
        return 0;

    return 1;
}

void do_channel( object me, string verb, string arg)
{
    object *ob;
    string who, msg;

    who= me->name()+ " ("+ me->query_id()+ ")";
    ob= filter_array( users(), "filter_listener", this_object());

    if( !arg) arg= "...";
    if( verb!= "rumor")
        message("channel:"+ verb, msg= sprintf( channels[verb], who, arg), ob);
    else message("channel:"+ verb, msg= sprintf( channels[verb], arg), ob);

    if( sizeof( channel_records[verb])== 10)
        channel_records[verb]= channel_records[verb][ 1.. 9];

    channel_records[verb]+= ({ msg });
}

void print_channel( string verb)
{
    int i, size;
    string str= "";

    size= sizeof( channel_records[verb]);
    for( i= 0; i< size; i++)
        str+= channel_records[verb][i];

    write( str);
}
