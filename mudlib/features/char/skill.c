// skill.c

#include <skill.h>

mapping skill_level= ([ ]);
mapping skill_exp= ([ ]);
mapping skills= ([ ]);

void initial_for_old()
{
    if( !mapp( skill_level))
        skill_level= ([ ]);
    if( !mapp( skill_exp))
        skill_exp= ([ ]);
    if( !mapp( skills))
        skills= ([ ]);
}
string *query_skill_list() { return keys( skill_level); }
int query_skill_level( string skill) { return skill_level[ skill]; }
string *query_skills( string skill) { return skills[ skill]; }
int query_skill_exp( string skill) { return skill_exp[ skill]; }

varargs void add_skill_type( string skill_type, int level)
{
    skill_level[ skill_type]= ( level? level: 1);
    skill_exp[ skill_type]= 0;
    skills[ skill_type]= ({ });
}

void add_skill_level( string skill_type)
{
    if( !skill_level[ skill_type])
        add_skill_type( skill_type);
    else {
        skill_level[ skill_type]++;
        skill_exp[ skill_type]= 0;
    }
}

void add_skill_exp( string skill_type, int exp)
{
    if( skill_level[ skill_type])
        skill_exp[ skill_type]+= exp;
}

int add_skill( string skill)
{
    int i;
    string type;

    seteuid( getuid());
    if( !stringp( skill= D_SKILL->get_skill( skill)))
        return 1;                       // No such skill
    type= skill->type();
    if( !skill_level[ type])
        return 2;                       // You haven't learned such skill type
    if( skill_level[ type]< skill->query("lv"))
        return 3;                       // You don't have enough level to learn this skill
    i= sizeof( skills[ type]);
    while( i--)
        if( skills[ type][i]== skill->query("id"))
            return 4;                   // You have learned this skill

    skills[ type]+= ({ skill->query("id") });

    return 0;
}
