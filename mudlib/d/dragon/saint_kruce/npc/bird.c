// bird.c
// written by zz

#include <class.h>
#include <element.h>
#include <race.h>

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
        int HP= 900, MP= 200, SP= 10000;

        set_name("小鳥", ({ "bird"}));
        set("lv", 5);
        set("race","animal");
        set("gender","動物");
        set("describe","一隻小鳥.");
        set("pre","飛來飛去的");
        set("age", 4);
        set("element",WIND);
        set("mind", 10);

        set("maxHP", HP);
        set("currentHP", HP);
        set("maxMP", MP);
        set("currentMP", MP);
        set("maxSP", SP);
        set("currentSP", SP);
        set("dem", 40);
        set("def", 30);
        set("mdem", 20);
        set("mdef", 20);
        set("spe", 40);
        set("exp", 700);

        set("chat_chance", 10);
        set("chat_msg", ({
                (: random_move :),
                "小鳥繞著你的身旁飛來飛去.\n"
                }));
        setup();
}
