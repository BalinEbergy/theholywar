// m_master.c
// written by Hudson

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
    int HP= 170000, MP= 10000, SP= 800;

    set_name("魔法使", ({ "magic messenger", "magic", "messenger" }));
    set("lv", 62);
    set("class", WIZARD);
    set("race", ELF);
    set("gender",1);
    set("describe","一位神情肅穆的巫師.");
    set("pre","巡視城內的");
    set("age", 198);
    set("element", FIRE);
    set("mind", 337);

    set("maxHP", HP);
    set("currentHP", HP);
    set("maxMP", MP);
    set("currentMP", MP);
    set("maxSP", SP);
    set("currentSP", SP);
    set("strength", 6000);
    set("armor", 12000);
    set("magic", 20000);
    set("spi", 20);
    set("speed", 830);
    set("exp", 4000);

    set("chat_chance", 10);
    set("chat_msg", ({ (: random_move :) }));

    setup();
}

void init()
{
    if( !previous_object()
        || !userp(previous_object()))
        return;
    call_out("ki", 10, previous_object());
}

void ki( object obj)
{
    if( obj->is_fighting()) kill_ob(obj);
}
