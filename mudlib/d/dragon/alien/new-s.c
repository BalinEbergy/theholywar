// new-e.c
// written by Mondale

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;37m公會會長辦公室[m");
        set("long",@LONG
這裡是練金術士公會會長辦公的地方，這間辦公室有一
半的位子被書架所佔據，書架上有一本「亞利恩城最大
的災難」的書(book)最為顯眼。能夠擁有如此多的書，
會長顯然是一位知識豐富的人。
LONG
        );

        set("exits",
        ([
        "north" :__DIR__ + "newunion",        
        ])
        );

        set("objects",  ([
                __DIR__ + "npc/diamondo":1,
                ]));
        setup();
}
