//test.c
//written by zz

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;37mtest[m");
        set("long",@LONG
test Lv1-10
LONG
        );
        set("exits", ([
                "south" : __DIR__ + "test1.c" ,
                ]));
        set("objects",  ([
                __DIR__ + "npc/test/up" :1 ,
                __DIR__ + "npc/test/lv1" :1 ,
                __DIR__ + "npc/test/lv2" :1 ,
                __DIR__ + "npc/test/lv3" :1 ,
                __DIR__ + "npc/test/lv4" :1 ,
                __DIR__ + "npc/test/lv5" :1 ,
                __DIR__ + "npc/test/lv6" :1 ,
                __DIR__ + "npc/test/lv7" :1 ,
                __DIR__ + "npc/test/lv8" :1 ,
                __DIR__ + "npc/test/lv9" :1 ,
                __DIR__ + "npc/test/lv10" :1 ,
        ]));
        set("no_recall", 1);
        setup();
}
