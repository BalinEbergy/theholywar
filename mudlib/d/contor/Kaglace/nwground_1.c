// nwground_1.c
// written by Bother

#include <room.h>


inherit ROOM;

void create()
{
        set("short","[1;37m卡格烈斯廣場[m");
        set("long",@LONG
這裡是卡格烈斯城的廣場,往東南方向前進就是廣場的正中心,
往東就可以看到一個噴水池,水池噴出來的水非常清澈,整個水
池都可見底,可說是透明純淨.
LONG
        );

        set("exits",
        ([
        "north" :__DIR__ + "nwground_2",
        "south" :__DIR__ + "wground_1",
        "east" :__DIR__ + "nground_1",
        ])
        );

}
