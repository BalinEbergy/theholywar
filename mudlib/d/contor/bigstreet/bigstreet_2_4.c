// bigstreet_2_4.c
// written by Bother

#include <room.h>


inherit ROOM;

void create()
{
        set("short","[33m卡特大道轉折點[m");
        set("long",@LONG
在這個轉折點你可以稍微休息一下,因為再往東北方前進
過不了多少時間就到了卡格列斯城了,往東前進有一條小
路,你可以去看看,不過不知道通往何處就是了.
LONG
        );

        set("exits",
        ([
        "south" :__DIR__ + "bigstreet_2_3",
        "west" :"d/contor/otherstreet/otherstreet_1",
        "east" :__DIR__ + "bigstreet_1_4",
        ])
        );
}
