// equipment.c

#include <equip.h>

string equip( object ob)
{
    int i, *che= ({ 0 });
    string str= "\n";
    string *equipp= ({
        "eq_light", "head", "neckr", "neckl", "body", "handr", "handl",
        "handb", "bracer", "bracel", "ringr", "ringl", "boot",
        });
    string *equip_name= ({
        "照明", "頭部", "項鍊", "項鍊", "身體", "右手", "左手", "雙手",
        "右腕", "左腕", "戒指", "戒指", " 鞋 ",
        });
    mapping equip= ([
        "default"     :     "none"
        ]);
    object eq;

    i= sizeof( equipp);
    while( i--)
        if( eq= ob->query_temp( equipp[i]))
        {
            equip[ equipp[i]]= eq->short();
            che+= ({ i });
        }

    i= sizeof( che);
    while( i--> 1)
    {
        if( stringp( equip[ equipp[ che[i]]]))
        str+= "    [[44m " + equip_name[ che[i]] + " [m]  "+ equip[ equipp[ che[i]]]+ "\n";
    }

    str+= "\n";

    return " \n[1;37;44m你現在的裝備                                     [m\n"+ str;
}

int main( object me, int argc, string *argv)
{
    write( equip( me));

    return 1;
}

int help( object me)
{
    write( @HELP
指令格式: equipment

顯示身上裝備.
HELP
        );

    return 1;
}
