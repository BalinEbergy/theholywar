// fish.c
// written by hiwind

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
    int HP= 9000, MP= 500 ,SP= 1000;

    set_name("���c����", ({ "fish" }));
    set("lv", 19);
    set("describe","��.");
    set("element", WATER);
    set("race", ANIMAL);
    set("age", 5);
    set("mind", -200);

    set("maxHP", HP);
    set("currentHP", HP);
    set("maxMP", MP);
    set("currentMP", MP);
    set("maxSP", SP);
    set("currentSP", SP);
    set("strength", 450);
    set("armor", 200);
    set("spi", 2);
    set("speed", 185);
    set("exp", 1800);
    setup();
}
