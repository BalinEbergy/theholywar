// street_10.c
// written by hiwind

#include <room.h>


inherit ROOM;

void create()
{
        set("short","[1m�p�|[m");
        set("long",@LONG
�b�p�|�W,���۰ʪ�,�ڻ��u�ۦ������N�O�i�h[1;36m��[m�����P����
�L�F,�䤤�R���F�j�۵M����.���L�o�������G�����K,����
�D�|������Ƶo��,�٬O�p�ߤ@�I.
�_��N�O[1;36m��[m�����F.
LONG
        );

        set("exits",
        ([
        "east" : __DIR__ + "grass_2",
        "south" : __DIR__ + "street_9",
        "north" : __DIR__ + "wind_8",
        ])
        );

        set("objects",
        ([
                __DIR__ + "npc/dog":1,
        ])
        );

        set("no_fight", 0);
        setup();
}
