// civilian.c
// written by Mondale
// 市民

inherit NPC;

void create()
{
        int HP= 1000, MP= 50, SP= 100;

        set_name("市民", ({ "civilian" }));
        set("lv", 10);
        set("class","warrior");
        set("ra","人類");
        set("gender","男");
        set("describe","住在亞利恩城的市民.");
        set("pre","");

        set("age", 23);
        set("element","earth");
        set("mind", 0);

        set("mixHP", HP);
        set("currentHP", HP);
        set("mixMP", MP);
        set("currentMP", MP);
        set("mixSP", SP);
        set("currentSP", SP);
        set("dem", 150);
        set("def", 0);
        set("mdem", 350);
        set("mdef", 250);
        set("spe",100);
        set("realmx", 1);
        set("exp", 1000);

        set("chat_chance", 20);
        set("chat_msg", ({
                (: this_object(), "random_move" :),
                "\n市民有氣無力的走著。\n"
                }));
        setup();
        setup();
}

void init()
{
        ::init();
        if( !previous_object()
                || !userp(previous_object()))
                return;
        call_out("action", 1, previous_object());
}

void action( object obj)
{
        setup();
}
