// stab_prac.c
// written by zz

#include <skill.h>

int main( object me, object obj)
{
        switch (random(4))
        {
        case 0:
        tell_object(me,"\n你練習了一下,不過好像沒什麼進展.\n");
        break;
        case 1:
        tell_object(me,"\n你練習刺擊的基本技巧.\n");
        break;
        case 2:
        tell_object(me,"\n你拿起長槍練習刺擊的技巧.\n");
        break;
        case 3:
        tell_object(me,"\n你拿起長槍奮力向前刺去.咻 ~ 咻 ~ 咻 ~\n");
        break;
        case 4:
        tell_object(me,"\n你手持著長槍四處揮舞著,虎虎生風的樣子.\n");
        }
}

string name( int select)
{
        switch( select)
        {
                case 1: return "刺擊練習";
                case 2: return "stab prac";
                case 3: return "stab_prac";
        }
}

int lv() { return 1; }
int attackful()
{
        return 0;
}

int type() { return STAB; }
int usesp() { return 5; }
int usemp() { return 0; }
int successful() { return 100; }
int delay() { return 1; }
int all() { return 0; }
int check( object me)
{
        return 1;
}
