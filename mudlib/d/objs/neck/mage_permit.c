// mage_permit.c
// written by Hudson

#include <class.h>
#include <equip.h>
#include <element.h>

inherit EQUIP;

void create()
{
        set_name("法師証明", ({ "mage permit", "mage", "permit" }));
        set("long", @LONG
一條淡銀色的項鍊,上頭鑲了一顆透著綠光的水晶,
背面則有法師公會會長福魯克親手簽下的魔法印記,
你覺得不停的有魔力滲透出來.
LONG
                );
        set("lv", 20);
        set("unit", "條");
        set("element", COSMOS);
        set("epower", 30);
        set("weight", 10);

        set("wear_type", NECK);
        set("wear_limit", ({ MAGE, WIZARD, SUMMONER }));
        set("maxMP", 100);
        set("eff", ({ "maxMP" }));
}
