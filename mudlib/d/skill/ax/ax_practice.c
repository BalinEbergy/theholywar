// ax_practice.c
// written by Hudson

#include <skill.h>

int main( object me, object obj)
{
        me->add_skill_exp( me->query("spirit")* me->query("lv")/
                me->skill_level( AX));
        return 1;
}

string name( int select)
{
        switch( select)
        {
                case 1: return "砍劈練習";
                case 2: return "ax practice";
                case 3: return "practice";
        }
}

int lv() { return 1; }
int attackful()
{
        return 0;
}

string mes( object me)
{
        return "舉起手中的斧頭,聲勢威猛的揮舞起來.\n";
}

int type() { return AX; }
int usesp() { return 6; }
int usemp() { return 0; }
int successful() { return 100; }
int delay() { return 1; }
int all() { return 0; }
int check( object me)
{
        if( me->query_temp("ax")!= "handb")
        {
                write("\n你必需以雙手拿斧喔!\n");
                return 0;
        }
        return 1;
}
