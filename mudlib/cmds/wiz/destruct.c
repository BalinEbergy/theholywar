// destruct.c

int main( object me, int argc, string *argv)
{
    object obj;

    if( argc< 2)
        write("你想要毀滅什麼呢?\n");
    else {
        if( objectp( obj= find( argv[1], environment( me))) ||
            objectp( obj= find( argv[1], me)))
        {
            message_vision("[36m$N喃喃唸著不屬於這個世界的咒文...[1;31m$n被毀滅了.\n", me, obj);
            destruct( obj);
        } else write("沒有這個東西.\n");
    }

    return 1;
}
