// thunder_armor.c
// written by Hudson

inherit EQUIP;

void create()
{
        set_name("�p�����Z", ({ "thunder armor", "thunder", "armor" }));
        set("long", @LONG
�@���J�観�p��  [1;44;5;7;30m�ԩi[m  �Ϯת��Z��.
LONG
                );
        set("lv", 100);
        set("unit", "��");
        set("element", "lightning");
        set("epower", 100);
        set("weight", 700);

        set("wear_type", "body");
        set("def_eff", 13000);
        set("defand_eff", 5000);
        set("eff", ({ "def_eff", "defand_eff" }));
}
