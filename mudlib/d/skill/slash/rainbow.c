// slash rainbow
// written by Mondale

#include <skill.h>

string name(int i);
int main( object me, object obj)
{
        int damage;
        string w_name= me->query_temp( me->query_temp("sla"))->name();
        message_vision("$N手腕陡然一翻，長劍化成一道[37;1m長虹[0m擊向$n.\n",me,obj);
        damage = ((me->query("dem") + me->query("dem_eff"))*(me->skill_level(SLASH)+250)/300) + random(50*me->skill_level(SLASH)) - ((obj->query("def") + obj->query("def_eff") + obj->query("defand_eff"))*7/10);
        if (damage < 0 ) return 0;
        obj->add("currentHP", -damage);
        return COMBAT_D->skill_msg( me, obj, name( 1), w_name, damage);;
}

string name( int select)
{
        switch( select)
        {
                case 1: return "白虹貫日";
                case 2: return "rainbow";
                case 3: return "rainbow";
        }
}

int lv() { return 50; }
int attackful()
{
        return 1;
}

string mes( object me)
{
        return "將長劍拉至身後,倒曳在地上,腳下開始依著八卦方位不停的移動.";
}

int type() { return SLASH; }
int usesp() { return 35; }
int usemp() { return 0; }
int successful() { return 100; }
int delay() { return 2; }
int all() { return 0; }
int check( object me)
{
        return 1;
}
