// wallson.c
// written by Hudson
// 沃爾森

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
    int HP= 330000, MP= 20500, SP= 5200;

    set_name("沃爾森", ({ "wallson" }));
    set("lv", 88);
    set("class",WIZARD);
    set("race",OGRE);
    set("gender",1);
    set("describe","一位胖胖的老先生.");
    set("pre","雷之法師");
    set("age", 450);
    set("element",THUNDER);
    set("mind", 599);

    set("teacher", ({
        6,
        }));
    set("favorite", ({
        WIZARD,
        }));
    set("teach_mind", "500 ~ 1000");
    set("teach_msg_evil", "[1;36m沃爾森說道:[33m 孩子,你太邪惡了,等你的罪惡消失之後再來吧![m");
    set("lv_up", "[1;36m沃爾森說道:[33m 呵呵, 你的等級提升囉![m\n");
    set("no_enu_money", "[1;36m沃爾森說道:[33m 哎呀,你帶的錢不太夠喔~[m");
    set("no_enu_lv", "[1;36m沃爾森說道:[33m 呵呵...你沒有足夠的等級喔![m");
    set("top_lv", "[1;36m沃爾森說道:[33m 呵呵...你已經從我這學會最高等級的魔法囉![m");
    set("teach_message", "[1;36m沃爾森說道:[33m 呵呵...想學魔法啊?[m");

    set("maxHP", HP);
    set("currentHP", HP);
    set("maxMP", MP);
    set("currentMP", MP);
    set("maxSP", SP);
    set("currentSP", SP);
    set("dem", 15000);
    set("strength", 26999);
    set("armor", 32900);
    set("magic", 33300);
    set("speed", 2207);
    set("exp", 247000);

    set("chat_chance", 15);
    set("chat_msg", ({
        (: command("say 呵呵... 天氣不錯... 天空的樂章正要演奏呢... 呵呵呵") :),
        (: command("say 呵呵... 我最喜歡看著這些小精靈們玩了") :)
        }));

    setup();
}

void init()
{
    object obj;

    if( !(obj= previous_object())
        || !userp( obj))
        return;

    call_out("ki", 10, obj);
}

void ki( object obj)
{
    if( obj->is_fighting()) kill_ob( obj);
}

int teach_check( object me)
{
    if( me->query("neckr")== "mage permit");
    else if( me->query("neckl")== "mage permit");
    else
    {
        command("say 你還未成為真正的魔法師, 很抱歉我不能教你任何東西...");
        return 1;
    }

    return 0;
}
