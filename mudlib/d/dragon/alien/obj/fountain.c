// fountain.c
// written by Bother
// rewritten by zz
// copy by mondale

inherit ITEM;

void create()
{
        set_name("[m噴水池", ({ "fountain" }));
        set("long", @LONG
一座美麗壯觀的噴水池呈現在你面前.
LONG
                );
        set("unit", "座");
        set("element", "water");
        set("epower", 30);
        set("d_water", 1);
        set("no_get", 1);
        set("unsac", 1);
}
