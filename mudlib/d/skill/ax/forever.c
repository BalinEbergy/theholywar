// forever.c
// written by Hudson

#include <skill.h>

string name( int i);

int main( object me, object obj)
{
        int damage, dem, eff, vir, hp;
        string w_name;

        hp= me->query("hp");
        vir= me->query("vir");
        dem= me->query("dem")+ me->query("dem_eff");
        eff= me->query("damage_eff");
        damage=( dem* ( 120- hp)/ 100+ random( dem /100* hp))/ 10* vir;
        obj->add("currentHP", -damage);

        tell_object( obj, "[1;33m"+ me->name()+ "身上猛烈的霸氣將你壓的喘不過氣來,那石破天驚的威勢使你根本無法防禦!![m\n");
//        tell_object( me, "\n傷害值 = "+ damage);
        message_vision("$N揮起手上的[1;33m"+( w_name= me->query_temp( me->query_temp("ax"))->name())+
                "[m挾帶著[5m狂暴的斧勢向$n砍去![m\n"+
                "四周的[1;5;37m大氣好似被$N劈裂[m,極大的風被裂縫吸入,接下來便是[1;5;33m一閃!![m\n" , me, obj);
        tell_object( obj, "[36m你只覺得身體好像不在存在,靈魂中的時間裂成兩半...[m\n");
        tell_room( environment( me), "\n");

        return COMBAT_D->skill_msg( me, obj, name( 1), w_name, damage);
}

string name( int select)
{
        switch( select)
        {
                case 1: return "[37m歸來永恆";
                case 2: return "return forever";
                case 3: return "forever";
        }
}

int type() { return AX; }
int lv() { return 100; }
int attackful()
{
        return 1;
}

string mes( object me)
{
        return "的手臂暴露青筋,身上散發狂暴的氣勢,令每個人感到極度的壓迫...";
}

int usesp() { return 500; }
int usemp() { return 0; }
int successful() { return 100; }
int delay() { return 6; }
int all() { return 0; }
string element() { return "time"; }

int check( object me)
{
        return 1;
}
