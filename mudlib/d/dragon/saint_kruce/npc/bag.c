// bag.c
// written by zz

#include <class.h>
#include <element.h>
#include <race.h>

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
        int HP= 2000000, MP= 0, SP= 1000;

        set_name("測試包", ({ "bag"}));
        set("lv", 8);
        set("race","animal");
        set("gender","動物");
        set("describe","一隻小貓.");
        set("pre","");
        set("age", 3);
        set("element",EARTH);
        set("mind", 10);

        set("maxHP", HP);
        set("currentHP", HP);
        set("maxMP", MP);
        set("currentMP", MP);
        set("maxSP", SP);
        set("currentSP", SP);
        set("dem", 65);
        set("def", 40);
        set("mdem", 25);
        set("mdef", 25);
        set("spe", 50);
        set("exp", 0);

        setup();
}
