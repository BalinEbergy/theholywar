//test7.c
//written by zz

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;37mtest7[m");
        set("long",@LONG
test Lv71-80
LONG
        );
        set("exits", ([
                "north" : __DIR__ + "test6.c" ,
                "south" : __DIR__ + "test8.c" ,
                ]));
        set("objects",  ([
                __DIR__ + "npc/test/lv71" :1 ,
                __DIR__ + "npc/test/lv72" :1 ,
                __DIR__ + "npc/test/lv73" :1 ,
                __DIR__ + "npc/test/lv74" :1 ,
                __DIR__ + "npc/test/lv75" :1 ,
                __DIR__ + "npc/test/lv76" :1 ,
                __DIR__ + "npc/test/lv77" :1 ,
                __DIR__ + "npc/test/lv78" :1 ,
                __DIR__ + "npc/test/lv79" :1 ,
                __DIR__ + "npc/test/lv80" :1 ,
        ]));
        set("no_recall", 1);
        setup();
}
