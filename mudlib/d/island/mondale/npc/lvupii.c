// lvup.c
// written by Hudson
// �ɯť]II

inherit NPC;

void create()
{
        set_name("�ɯť]II", ({ "lvupii" }));
        set("lv", 1);
        set("ra", "ogre");
        set("race", "���~");

        set("element", "earth");
        set("goodat", "none");
        set("weakat", "none");
        set("mixHP", 10);
        set("currentHP", 10);
        set("mixSP", 10);
        set("currentSP", 10);
        set("mind",  0);
        set("exp", 8000000);
        setup();
}
