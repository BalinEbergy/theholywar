// hirwedge.c
// written by Hudson
// 海爾崴吉

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
    int HP= 580000, MP= 27500, SP= 4000;

    set_name("海爾威吉", ({ "hirwedge" }));
    set("lv", 88);
    set("class", WIZARD);
    set("race", OGRE);
    set("gender",1);
    set("describe","一位身型頗大的巨漢,令人懷疑他是否真的是巫師...");
    set("pre","地之法師");

    set("age", 300);
    set("element", EARTH);
    set("mind", -799);

    set("maxHP", HP);
    set("currentHP", HP);
    set("maxMP", MP);
    set("currentMP", MP);
    set("maxSP", SP);
    set("currentSP", SP);
    set("strength", 17232);
    set("armor", 29883);
    set("magic", 22000);
    set("spi", 17);
    set("speed", 1735);
    set("exp", 247000);

    setup();
}

void init()
{
    object obj;

    if( !(obj= previous_object())
        || !userp( obj))
        return;

    call_out("ki", 10, obj);
    call_out("action", 20, obj);
}

void action( object obj)
{
    if( !obj || !present(obj) || this_object()->is_fighting()) return;
    command("say 哈哈哈哈...聽聽大地的聲音吧...你會發現,隱藏在創造之後的悲傷啊!");
}

void ki( object obj)
{
    if( obj->is_fighting()) kill_ob( obj);
}
