// rumor.c

void create() { seteuid( getuid()); }

int main( object me, int argc, string *argv)
{
    int i;
    string msg;

    if( argc< 2)
        write("你想要聊什麼呢?\n");
    else {
        msg= argv[1];

        for( i= 2; i< argc; i++)
            msg+= " "+ argv[i];

        if( msg!= "/p")
            D_CHANNEL->do_channel( me, "rumor", msg);
        else D_CHANNEL->print_channel("rumor");
    }

    return 1;
}

int help()
{
    write( @HELP
指令格式: rumor <想說的話| /p >

跟 chat 一樣, 不過是暱名聊天
HELP
    );
 
    return 1;
}
