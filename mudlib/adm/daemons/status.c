// status.c

#include <class.h>
#include <element.h>
#include <race.h>

string color( int percent)
{
    if( percent>= 100) return "[1;37m";
    if( percent>= 90) return "[37m";
    if( percent>= 80) return "[36m";
    if( percent>= 70) return "[32m";
    if( percent>= 60) return "[33m";
    if( percent>= 50) return "[1;36m";
    if( percent>= 40) return "[1;32m";
    if( percent>= 30) return "[1;33m";
    if( percent>= 20) return "[1;35m";
    if( percent>= 10) return "[1;31m";
    return "[1;34m";
}

string ra( int ra)
{
    switch( ra)
    {
        case OGRE:          return "[1;31m���][m";
        case ELF:           return "[1;32m���F[m";
        case HUMAN:         return "[1;37m�H��[m";
        case DRAGON:        return "[1;34m�s��[m";
        case SAINTRE:       return "[1;36m�t�~[m";
        case GODBEING:      return "[1;33m �� [m";
        case ANIMAL:        return "[33m�ʪ�[m";
        case ELEMENT:       return "[1;33m����[m";
        case GHOST:         return "����";
        case DEMON:         return "�c�]";
        case MONSTER:
        default:            return "���~";
    }

    return 0;
}

string ele( string element, int che)
{
    if( che)
        switch( element)
        {
            case HOLY: return "[1;37m�t[m";
            case EVIL: return "[1;35m��[m";

            case LIGHT: return "[1;33m��[m";
            case WIND: return "[1;32m��[m";
            case COSMOS: return "[1;36m��[m";
            case THUNDER: return "[1;33;44m�p[m";
            case FIRE: return "[1;31m��[m";
            case WATER: return "[1;37;44m��[m";
            case EARTH: return "[31m�a[m";
            case DARK: return "[1;30m��[m";
        }
    else
        switch( element)
        {
            case HOLY: return "[1;37m";
            case EVIL: return "[1;35m";

            case LIGHT: return "[1;33m";
            case WIND: return "[1;32m";
            case COSMOS: return "[1;36m";
            case THUNDER: return "[1;33;44m";
            case FIRE: return "[1;31m";
            case WATER: return "[1;37;44m";
            case EARTH: return "[31m";
            case DARK: return "[1;30m";
        }

    return "[m";
}

string mind( int mindnum)
{
    if( mindnum >900) return "[1;37m�t[m";
    if( mindnum >600) return "[1;33m��[m";
    if( mindnum >300) return "[1;32m��[m";
    if( mindnum <-900) return "[1;34m�][m";
    if( mindnum <-600) return "[1;35m��[m";
    if( mindnum <-300) return "[1;31m�c[m";

    return "[1;36m��[m";
}

string job( int cla)
{
    switch( cla)
    {
        case GOD:       return "��";
        case COMMONER:  return "����";
    }

    return 0;
}

string prin( int percent)
{
    string out;
    int j;

    out= "";
    if( percent% 4!= 0) percent= percent/ 4+1;
    else percent= percent/ 4;
    for( ; percent> 0; percent--) out+= " ";
    out+= "[m";

    return out;
}

string state_bar( int hp_ratio, int mp_ratio, int sp_ratio)
{
    int i, hp_out, sp_out;
    string str= "[1;4;33;41m";

    for( i= 0; i< 10; i++)
    {
        if( !hp_out && hp_ratio<= i)
        {
            hp_out= 1;
            str+= "[40m";
        }

        if( !sp_out && sp_ratio<= i)
        {
            sp_out= 1;
            if( hp_out)
                str+= "[0;1;33m";
            else str+= "[0;1;33;41m";
        }

        if( mp_ratio<= i)
            str+= "  ";
        else str+= "�w";
    }
    str+= "[m";

    return str;
}

string user_under_prompt( object obj)
{
    int hp_ratio, mp_ratio, sp_ratio;
    string str;
    mapping dbase;

    dbase= obj->query_entire_dbase();
    hp_ratio= dbase["currentHP"]* 100/ dbase["maxHP"];
    mp_ratio= dbase["currentMP"]* 100/ dbase["maxMP"];
    sp_ratio= dbase["currentSP"]* 100/ dbase["maxSP"];

    return sprintf(
        "\n[1;37m�� �������������������������������������������������������������������������� ��\n"
        "[m%s| %|16s |[37m %-29s[K\n"
        "%s "
        "[m[ HP %s%5d[m/[1;37m%5d "
        "[m| MP %s%5d[m/[1;36m%5d[m "
        "[m| SP %s%5d[m/[1;33m%5d[m ][K",
        color( hp_ratio),
        dbase["title"], dbase["name"]+ " ("+ obj->query_id()+ ")",
        state_bar( hp_ratio/ 10, mp_ratio/ 10, sp_ratio/ 10),
        color( hp_ratio), dbase["currentHP"], dbase["maxHP"],
        color( mp_ratio), dbase["currentMP"], dbase["maxMP"],
        color( sp_ratio), dbase["currentSP"], dbase["maxSP"]);
}
