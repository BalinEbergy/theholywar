// new-w.c
// written by Mondale

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;37m練金術士公會雜貨店[m");
        set("long",@LONG
這裡是亞利恩城的鍊金術士公會的雜貨店，專門賣東西
給練金術士。有賣各種的藥材、道具等等。
LONG
        );

        set("exits",
        ([
        "east" :__DIR__ + "newunion",        
        ])
        );

        set("objects",  ([
                ]));
        setup();
}
