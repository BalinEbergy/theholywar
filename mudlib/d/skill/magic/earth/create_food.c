// create_food.c
// written by Hudson

#include <skill.h>

string *foods= ({
    "magic_apple", "magic_mushroom", "magic_potato",
    });

void create()
{
    set("name", "食物創造術");
    set("id", "create food");
    set("lv", 3);
    set("attack", 0);
    set("usemp", 40);
    set("delay", 1);
    set("cost", 52);
    set("exp", 50);

    set("special_process", 1);

    setup();
}

int main( object me, object obj)
{
    object food;

    food= new( OB_SPECIAL+ foods[ random( sizeof( foods))]);
    food->move( environment( me));
    call_out("disappear", me->query_skill_level("earth")* 5, food);

    message_vision("一個" + food->name() + "出現在$N的眼前!!\n", me);

    return 1;
}

void disappear( object food) { food->disappear( food); }

string mes( object me)
{
    return "$N將手平舉, 唸出一段大地的咒文, 地面逐漸隆起...";
}
