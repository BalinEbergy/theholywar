// dio.c
// written by Hudson
// 迪奧

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
    int HP= 227000, MP= 46000, SP= 6000;

    set_name("迪奧", ({ "dio" }));
    set("lv", 90);
    set("class",WIZARD);
    set("race",ELF);
    set("gender",1);
    set("describe","一位神氣流轉的老者.");
    set("pre","共和國長老");

    set("age", 570);
    set("element",LIGHT);
    set("mind", 917);

    set("teacher",({ 30 }));
    set("favorite", ({
        WIZARD, "summoner",
        }));
    set("teach_mind", "-500 ~ 1000");
    set("teach_msg_evil", "[1;36m迪奧說道:[33m 在巴路斯特, 我不教邪惡之徒的...[m");
    set("lv_up", "[1;36m迪奧說道:[33m 好了, 你的能力增強了![m\n");
    set("no_enu_money", "[1;36m迪奧說道:[33m 呵... 你的錢不夠喔...[m");
    set("no_enu_lv", "[1;36m迪奧說道:[33m 你的能力不足![m");
    set("top_lv", "[1;36m迪奧說道:[33m 恭喜了,你在召喚術的造詣已經超越我了[m");
    set("teach_message", "[1;36m迪奧說道:[33m 召喚術吧... 這是一個禁斷的神術啊![m");

    set("maxHP", HP);
    set("currentHP", HP);
    set("maxMP", MP);
    set("currentMP", MP);
    set("maxSP", SP);
    set("currentSP", SP);
    set("strength", 7340);
    set("armor", 20000);
    set("magic", 50000);
    set("spi", 35);
    set("speed", 2050);
    set("exp", 9000);

    setup();
}

void init()
{
    if( !previous_object()
        || !userp(previous_object()))
        return;
    call_out("ki", 10, previous_object());
}

void ki( object obj)
{
    if( obj->is_fighting()) kill_ob( obj);
}
