// lake_2.c
// written by hiwind

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;36m��y[m");
        set("long",@LONG
[1;36m�p��[m�����Y�N�O[1;36m��y[m,�M�ӥѩ���`�O�j���۲H�H��[1;36m����[m,��
�H�`�O���H�ݱo���I�ҽk���M,��F��W,�~�o�{�����O�p����
�M��,�N�ϩ���l�@��.
���䦳�@���ƬO���Z�����.
LONG
        );

        set("exits",
        ([
        "west" : "d/elf/hiwind/elf_forest/grass_8",
        "east" : __DIR__ + "lake_5",
        "south" : __DIR__ + "lake_1",
        "north" : __DIR__ + "lake_3",
        ])
        );
}
