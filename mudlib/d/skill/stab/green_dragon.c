// green_dragon.c
// written by zz

#include <skill.h>

int main( object me, object obj)
{
        int damage, ra, def_eff, dem_eff, odem, dem, def, mdef, HP, mixHP, HPz, mixHPz, spe, spirit, omdef;
        string weapon_name;

        spe= me->query("speed");
        spirit= me->query("spirit");
        if( me->query("ra") == "dragon") ra= 1;
                else ra= 0;
        if( random( 100)> spe* 6/ 5+ spirit* 3/ 2+ ra*10)
        {
                message_vision("\n可是$N精神不夠集中,沒有命中$n!\n", me, obj);
                return -1;
        }

        def_eff= obj->query("defand_eff");
        def= me->query("def")+ me->query("def_eff");
        dem_eff= me->query("damage_eff");
        dem= me->query("dem")+ me->query("dem_eff");
        odem= obj->query("dem")+ obj->query("dem_eff");
        mdef= me->query("mdef");
        omdef= obj->query("mdef");
        mixHP= me->query("mixHP")/10;
        if( mixHP> 60000) mixHP= 60000;
        HP= me->query("currentHP")/10;
        if( HP> 60000) HP= 60000;
        HPz= HP/100;
        mixHPz= mixHP/100;
        HPz= mixHPz- HPz;
        weapon_name=( me->query_temp( me->query_temp("stb")))->query("name");

        damage= ( mixHP/ 2+ dem* 8/ 5+ odem+ def- omdef+ mdef* 2- 50000+ 500*
                HPz* HPz/ mixHPz/mixHPz* 100+ random( 10000)+
                random( 10000))* ( 100+ ra*15 )/ 100;

        if( damage< 0) damage= 0;

        obj->add("currentHP", -damage);

        message_vision(damage+ "\n天上的[1;37m雲朵[0m開始不斷的聚集.$N突然朝[36m天空[0m躍起,運用意念\n和手中的[1m"+ weapon_name+
                "[0m合為一體.[1;33m剎那間[m天上閃過一道[1;37m白光[0m,\n你像[1;32m青龍[1;36m破雲[0m而出,朝著[0;33m地上[m的$n衝去!\n", me, obj);

        return damage;
}

string name( int select)
{
        switch( select)
        {
                case 1: return "青龍破雲";
                case 2: return "stab dragon";
                case 3: return "stab dragon";
        }
}

int type() { return STAB; }
int lv() { return 100; }
int attackful()
{
        return 1;
}

string mes( object me)
{
        return "集中精神,回想起當時與[1;32m青龍[0m的那一戰.";
}

int usesp() { return 200; }
int usemp() { return 0; }
int successful() { return 100; }
int delay() { return 1; }
int all() { return 0; }
string element() { return ""; }

int check( object me)
{
        if( me->query_temp("stb") != "handb")
        {
                write("\n你單手持槍,無法使出全力!\n");
                me->delete_temp("stb");
                return 0;
        }
        if( me->query("currentSP")< 200)
        {
                write("\n你[5;33m沒力[m了....\n");
                me->delete_temp("stb");
                return 0;
        }
        return 1;
}
