// shell.c
// written by zz

#include <class.h>
#include <element.h>
#include <race.h>

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
        int HP= 400, MP= 0, SP= 1000;

        set_name("蚌殼", ({ "shell"}));
        set("lv", 1);
        set("race","animal");
        set("gender","動物");
        set("describe","一個蚌殼.");
        set("pre","隱藏在石頭之間的");
        set("age", 1);
        set("element",EARTH);
        set("mind", -50);

        set("maxHP", HP);
        set("currentHP", HP);
        set("maxMP", MP);
        set("currentMP", MP);
        set("maxSP", SP);
        set("currentSP", SP);
        set("dem", 35);
        set("def", 35);
        set("mdem", 25);
        set("mdef", 25);
        set("spe", 35);
        set("exp", 200);

        set("chat_chance", 5);
        set("chat_msg", ({
                (: random_move :)
                }));
        setup();
}
