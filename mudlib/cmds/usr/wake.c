// wake.c
// written by Hudson

int main( object me, string arg)
{
    if( me->query("rest")< 2)
        write("你並沒有在睡覺!!\n");
    else {
        me->delete("rest");
        message_vision("$N漸漸的清醒過來...\n", me);
    }

    return 1;
}

int help( object me)
{
    write(@HELP
指令格式: wake

甦醒,從睡眠或冥想狀態醒過來.
HELP
    );

    return 1;
}
