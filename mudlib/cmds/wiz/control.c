// control.c

int main( object me, int argc, string *argv)
{
    string verb;
    object obj;

    if( argc< 3)
        write("你想要控制誰做什麼事呢?\n");
    else if( !objectp( obj= find( argv[1], environment( me))))
        write("這裡沒有這個人.\n");
    else {
        verb= argv[2];
        for( int i= 3; i< sizeof( argv); i++)
            verb+= " "+ argv[i];

        message_vision("[36m$N喃喃念著彷彿不存在世間的咒語...\n[m", me);
        tell_object( obj, "你感到有一股[1;31m莫名的力量[m在控制著你...\n");
        tell_object( me, "Current command: "+ verb+ "\n");
        obj->command( verb);
    }

    return 1;
}

int help( object me)
{
    write( @HELP
指令格式: control <目標> <動作>

控制指定的目標做事
HELP
        );

    return 1;
}
