// deer.c
// written by hiwind

inherit NPC;

void create()
{
        int HP= 90000, MP= 1000,SP= 3000;

        set_name("一隻正在吃草的野鹿", ({ "deer" }));
        set("lv", 50);
        set("describe"," 鹿.");
        set("age", 25);
        set("element", "earth");
        set("mind", 0);

        set("mixHP", HP);
        set("currentHP", HP);
        set("mixMP", MP);
        set("currentMP", MP);
        set("mixSP", SP);
        set("currentSP", SP);
        set("dem", 4000);
        set("def", 1000);
        set("mdem", 600);
        set("mdef", 400);
        set("spirit", 20);
        set("spe", 500);
        set("exp", 2500);
        setup();
        set("chat_chance", 5);
        set("chat_msg", ({
                (: this_object(), "random_move" :)}));

}
