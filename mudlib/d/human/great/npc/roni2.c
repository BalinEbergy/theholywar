// roni2.c
// written by koyamakyo
// 浪人2

inherit NPC;

void create()
{
        int HP= 1, MP= 0, SP= 0;

        set_name("浪人2", ({ "roni2" }));
        set("lv", 10);
        set("class","ruroni");
        set("ra","human");
        set("gender","男");
        set("describe","四處晃蕩的浪人");
        set("pre","無所事事的");

        set("age", 30);
        set("element","wind");
        set("mind", -200);

        set("mixHP", HP);
        set("currentHP", HP);
        set("mixMP", MP);
        set("currentMP", MP);
        set("mixSP", SP);
        set("currentSP", SP);
        set("dem", 20);
        set("def", 0);
        set("mdem", 0);
        set("mdef", 0);
        set("spe", 20);
        set("realmx", 10);
        set("exp", 500);



}
void ki( object obj)
{
        if( obj->is_fighting()) kill_ob( obj);
}
