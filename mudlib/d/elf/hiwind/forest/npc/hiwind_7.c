// hiwind_7.c
// written by hiwind

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
    int HP= 50000, MP= 25000 ,SP= 10000;

    set_name("�����O[1;31m��[m[1m�����@��[m", ({ "contor" }));
    set("lv", 60);
    set("class",WARRIOR);
    set("race",HUMAN);
    set("describe","�A��è���H.");
    set("age", 16);
    set("element", WIND);
    set("goodat", EARTH);
    set("weakat", THUNDER);
    set("mind", 250);
    set("gender", 1);

    set("maxHP", HP);
    set("currentHP", 10000);
    set("maxSP", SP);
    set("currentSP", SP);
    set("dem", 10000);
    set("def", 15000);
    set("spirit", 1000);
    set("spe", 500);
    set("exp", 0);
    set("uncurable", 1);

    set("chat_chance", 10);
    set("chat_msg", ({
        "\���@���Υ��ꪺ�����ݵۧA.\n"
        }));
    setup();
}
void init()
{
    ::init();
    if( !previous_object()
        || !userp(previous_object()))
        return;
    call_out("action", 10, previous_object());
}

void action( object obj)
{
    if( !obj || !present(obj) || this_object()->is_fighting()) return;
    if( obj->is_fighting()) kill_ob(obj);
    command("say ���n����,���n�|��.���...");
    setup();
}
