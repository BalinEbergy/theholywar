// ogreguard.c
// written by Hudson

#include <class.h>
#include <element.h>
#include <race.h>

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
        int HP= 40000, SP= 7700;

        set_name("城門守衛", ({ "guard" }));
        set("lv", 40);
        set("class",WARRIOR);
        set("race",OGRE);
        set("describe","一位嚴密注意四周的守衛.");
        set("pre","卡格爾城");

        set("age", 90);
        set("element", FIRE);
        set("mind", 230);
        set("gender", 1);

        set("maxHP", HP);
        set("currentHP", HP);
        set("maxSP", SP);
        set("currentSP", SP);
        set("dem", 7000);
        set("def", 3000);
        set("mdef", 1000);
        set("spe", 650);
        set("exp", 7000);

        carry_object( OB_AXE + "huge_axe")->wear();

        setup();
}

void init()
{
        object obj;

        if( !(obj= previous_object())
                || !userp( obj))
                return;

        call_out("ki", 2, obj);
        call_out("action", 1, obj);
}

void ki( object obj)
{
        if( obj->query("attacker")) kill_ob( obj);
}

void action( object obj)
{
        if( !obj || this_object()->is_fighting()) return;

        command("say 歡迎來到卡格爾!");
}
