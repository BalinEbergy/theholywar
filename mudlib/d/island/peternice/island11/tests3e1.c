// center.c
// written by peternice

#include <room.h>

inherit ROOM;

void create()
{
        set("short","*[1;37m天嵐之島_內陸*[m");
        set("long",@LONG
    在披草斬棘之後, 你終於爬上了這個島稍微高一點的地方, 從這裡可以
俯看到這整個島的全貌, 果然不出所料, 光禿禿的一遍, 全是灰暗的岩石和叢
生的雜草, 阿! 你看到遙遠的北端似乎有些不一樣, 在灰暗與深綠的視野中,
似乎透露有奇異的光芒, 你開始覺的這個島可能不僅僅是這樣.
LONG
        );

        set("exits",
        ([
        "east" : __DIR__ + "tests3e2.c",
        ])
        );
}
