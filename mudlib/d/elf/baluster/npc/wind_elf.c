// wind_elf
// written by Hudson

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
    int HP= 500, MP= 200, SP= 2000;

    set_name("風小精靈", ({ "wind elf" }));
    set("lv", 6);
    set("class", MINSTREL);
    set("race", ELF);
    set("gender", 1);
    set("describe","一個小精靈.");

    set("age", 20);
    set("element", WIND);
    set("mind", 201);
    set("fly", 1);

    set("maxHP", HP);
    set("currentHP", HP);
    set("maxMP", MP);
    set("currentMP", MP);
    set("maxSP", SP);
    set("currentSP", SP);
    set("strength", 220);
    set("armor", 100);
    set("spi", 10);
    set("speed", 170);
    set("exp", 500);

    set("chat_chance", 13);
    set("chat_msg", ({
        "風精愉快的笑著\n",
        (: random_move :)
        }));

    setup();
}

void init()
{
    object obj;

    if( !( obj= previous_object()) || !userp( obj))
        return;

    call_out("ki", 10, obj);
}

void ki( object obj)
{
    if( obj->is_fighting()) kill_ob( obj);
}
