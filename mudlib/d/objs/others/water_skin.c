// water_skin.c
// written by Bother

inherit ITEM;

void create()
{
        int num=0;

        set_name("水袋", ({ "water skin", "water", "skin" }));
        set("long", @LONG
一個看似能裝很多水的袋子.
LONG
                );
        set("unit", "個");
        set("lv", 1);
        set("value", 9);
        set("weight", 10);
        set("c_drink", 1);
        set("wwater", num);
}
