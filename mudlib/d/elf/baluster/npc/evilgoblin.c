// evilgoblin.c
// written by Hudson

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
    int HP= 240000, MP= 12000, SP= 23000;

    set_name("邪惡小妖精", ({ "evil goblin","evil","goblin" }));
    set("lv", 70);
    set("class",MINSTREL);
    set("race","goblin");
    set("gender",1);
    set("describe","一個散發著邪氣的小妖精.");

    set("age", 533);
    set("element",EVIL);
    set("mind", -1000);

    set("maxHP", HP);
    set("currentHP", HP);
    set("maxMP", MP);
    set("currentMP", MP);
    set("maxSP", SP);
    set("currentSP", SP);
    set("strength", 10000);
    set("armor", 17000);
    set("magic", 19000);
    set("spi", 70);
    set("speed", 2113);
    set("exp", 200000);

    setup();
}

void init()
{
    object obj;

    if( !(obj= previous_object())
        || !userp( obj))
        return;

    call_out("ki", 10, obj);
}

void ki( object obj)
{
    if( obj->is_fighting()) kill_ob( obj);
}
