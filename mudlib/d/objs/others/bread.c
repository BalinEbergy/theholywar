// bread.c
// written by Bother

inherit ITEM;

void create()
{
        object obj;
        set_name("麵包", ({ "bread" }));
        set("long", @LONG
一塊好吃的麵包.
LONG
                );
        set("unit", "塊");
        set("lv", 0);
        set("value", 5);
        set("c_water", 2);
        set("weight", 10);
        set("wwater", 1);
}
