// tiger.c
// written by Bother

inherit NPC;

void create()
{
        int HP= 2250, MP=20, SP= 300;

        set_name("*[1;37m猛虎*[m", ({ "tiger" }));
        set("lv", 9);
        set("class","animal");
        set("ra","animal");
        set("describe","兇猛的一隻青眼老虎");
        set("pre","非常強壯的");

        set("age", 13);
        set("element", "earth");
        set("mind", 15);

        set("mixHP", HP);
        set("currentHP", HP);
        set("mixMP", MP);
        set("currentMP", MP);
        set("mixSP", SP);
        set("currentSP", SP);
        set("dem", 170);
        set("def", 100);
        set("mdem", 10);
        set("mdef", 120);
        set("spe", 51);
        set("realmx", 21);
        set("exp", 3800);

        set("chat_chance", 80);
        set("chat_msg", ({
                (: this_object(), "random_move" :),
                "\n*[1;37m猛虎*[m喉嚨發出一陣吼叫聲.\n"
                }));
        setup();
}
