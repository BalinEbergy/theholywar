// beef.c
// written by Mondale

#include <element.h>

inherit FOOD;

void create()
{
    object obj;
    set_name("炒牛肉", ({ "beef" }));
    set("long", @LONG
聖德斯特炒的牛肉,香氣逼人.
LONG
        );
    set("unit", "份");
    set("lv", 1);
    set("value", 3);
    set("no_save", 1);

    set_amount( 2);

    set_weight( 9);

    call_out("disappear", 5000);
}

void disappear()
{
    tell_object( environment( this_object()), name()+ "腐敗了.\n");
    destruct( this_object());
}
