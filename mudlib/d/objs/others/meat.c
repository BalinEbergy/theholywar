// meat.c
// written by Bother

inherit ITEM;

void create()
{
        object obj;
        set_name("牛肉", ({ "meat" }));
        set("long", @LONG
一塊看似能吃很多口的牛肉.
LONG
                );
        set("unit", "塊");
        set("lv", 0);
        set("value", 20);
        set("c_water", 2);
        set("weight", 15);
        set("wwater", 5+random( 5));
}

