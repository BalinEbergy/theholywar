//test1.c
//written by zz

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;37mtest1[m");
        set("long",@LONG
test Lv11-20
LONG
        );
        set("exits", ([
                "north" : __DIR__ + "test.c" ,
                "south" : __DIR__ + "test2.c" ,
                ]));
        set("objects",  ([
                __DIR__ + "npc/test/lv11" :1 ,
                __DIR__ + "npc/test/lv12" :1 ,
                __DIR__ + "npc/test/lv13" :1 ,
                __DIR__ + "npc/test/lv14" :1 ,
                __DIR__ + "npc/test/lv15" :1 ,
                __DIR__ + "npc/test/lv16" :1 ,
                __DIR__ + "npc/test/lv17" :1 ,
                __DIR__ + "npc/test/lv18" :1 ,
                __DIR__ + "npc/test/lv19" :1 ,
                __DIR__ + "npc/test/lv20" :1 ,
                __DIR__ + "npc/casterII" :10 ,
        ]));
        set("no_recall", 1);
        setup();
}
