// center.c
// written by Hudson

#include <room.h>

inherit ROOM;

void create()
{
    set("short","[1;32m巴路斯特廣場中心[m");
    set("long",@LONG
你現在站在精靈之城--巴路斯特的中心,這裡是整個世界魔法
最為純粹的集中點,正中的艾爾芙水晶(Elf crystal)閃爍著
清亮的光輝,你覺得這裡的清靈洗滌了心中的雜念,精神也為
之一亮.
上方是眾魔法師所組成的魔法公會會所.
LONG
    );

    set("exits", 
    ([
    "east"  : __DIR__ + "eground",
    "up"    : __DIR__ + "mageguild/mageguildc",
    "north" : __DIR__ + "nground1",
    "south" : __DIR__ + "sground1",
    "west"  : __DIR__ + "wground",
    ])
    );

    set("objects", ([
        OB_SPECIAL + "elf_crystal":1,
        __DIR__ + "npc/akas":1,
        __DIR__ + "npc/elf_citizen":1,
        __DIR__ + "npc/earth_elf":2,
        __DIR__ + "npc/m_master": 1,
        ]));

    set("no_fight", 1);

    setup();
}
