// sroad12.c
// written by Hudson

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;32m�ڸ����S��D[m");
        set("long",@LONG
�o�̬O�ڸ����S�ө����E���a,���D�㩱,�ȩ�,�Ψ��㩱,�\
�U����.���F�_�訫�i��F�ӷ~���|���ڸ����S���|.�|�P��
�����,���F�̪��@�ɵ��A�@���w�ֲ��M���P.
���F�O�ȩ��ЦN������.
LONG
        );
        set("exits",
        ([
        "north" : __DIR__ + "wroad6",
        "south" : __DIR__ + "sroad4",
        "west"  : __DIR__ + "sroad13",
        "east"  : __DIR__ + "gellways",
        ])
        );

        set("objects", ([
                __DIR__ + "npc/m_master": 2,
                ]));

        setup();
}
