// traveller.c
// written by Mondale
// 旅客

inherit NPC;

void create()
{
        int HP= 2000, MP= 250, SP= 1000;

        set_name("旅客", ({ "traveller" }));
        set("lv", 20);
        set("class","warrior");
        set("ra","人類");
        set("gender","男");
        set("describe","出來旅行，在亞利恩稍作停留的旅客.");
        set("pre","正在大快朵頤的");

        set("age", 30);
        set("element","earth");
        set("mind", 0);

        set("mixHP", HP);
        set("currentHP", HP);
        set("mixMP", MP);
        set("currentMP", MP);
        set("mixSP", SP);
        set("currentSP", SP);
        set("dem", 700);
        set("def", 650);
        set("mdem", 500);
        set("mdef", 123);
        set("spe", 200);
        set("realmx", 1);
        set("exp", 3000);

        set("chat_chance", 20);
        set("chat_msg", ({
                "\n旅客正津津有味的吃著。\n"
                }));
        setup();
        setup();
}

void init()
{
        ::init();
        if( !previous_object()
                || !userp(previous_object()))
                return;
        call_out("action", 1, previous_object());
}

void action( object obj)
{
        command("say 這裡的菜真是不錯啊！");
        setup();
}
