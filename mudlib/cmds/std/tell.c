// tell.c
// rewritten by Hudson

int main( object me, int argc, string *argv)
{
    int i;
    string msg;
    object obj;

    if( argc< 3)
        write("你要對誰說什麼話呢?\n");
    else {
        msg= argv[2];
        for( i= 3; i< argc; i++)
            msg+= " "+ argv[i];

        obj= present( argv[1], environment( me));
        if( !obj) obj= find_player( argv[1]);

        if( !obj)
        {
            write("沒有這個人耶...\n");
            return 1;
        }

        tell_object( me, "[1;37m你告訴" + obj->name() + ": " + msg + "[m\n");

        if( !userp( obj))
        {
            obj->receive( me, msg);
            return 1;
        }

        tell_object( obj, sprintf("[1;37m%s告訴你: %s[m\n",
            me->name(1), msg));
        if( userp( me))
            obj->set_temp("reply_ob", me);
    }

    return 1;
}

int help( object me)
{
    write( @HELP

指令格式﹕tell <某人> <訊息>

你可以用這個指令和其他地方的使用者說話或是與 NPC 交換訊息.
HELP
    );

    return 1;
}
