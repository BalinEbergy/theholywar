// eground_1.c
// written by Bother

#include <room.h>


inherit ROOM;

void create()
{
        set("short","[1;37m卡格烈斯廣場[m");
        set("long",@LONG
這裡是卡格烈斯城的廣場,往西走就是廣場的中心,往東就是松
風街,街上的店家可不少,若是你非常的空閒可以來這兒逛一下
午,街道的東南方向是一間富豪的家,外表裝飾的極為豪華,有
如皇宮一般,那富豪的祖先好像是一位貴族.
LONG
        );

        set("exits",
        ([
        "north" :__DIR__ + "neground_1",
        "south" :__DIR__ + "seground_1",
        "west" :__DIR__ + "center",
        "east" :__DIR__ + "songphon_12",
        ])
        );
}
