// eat.c

int main( object me, int argc, string *argv)
{
    string str;
    object food;

    if( argc< 2)
        write("你想吃什麼呢?\n");
    else if( !food= find( argv[1], me))
        write("你身上沒有這個東西唷~\n");
    else if( !food->is_food())
        write("這個東西不能吃喔!\n");
    else {
        message_vision("$N咬了一口"+ food->name()+ "\n", me);

        food->eat();

        switch( me->query("currentHungry")* 3/ me->query("maxHungry"))
        {
            case 0:
                write("你還是覺得有點餓.\n"); break;
            case 1:
                write("你覺得不餓了.\n"); break;
            default:
                write("你覺得吃飽了.\n"); break;
        }
    }

    return 1;
}

int help( object me)
{
    write( @HELP
指令格式: eat <食物>

這個指令可以讓你吃身上攜帶的食物, 餓著了對旅行會有不良影響唷!
HELP
        );

    return 1;
}
