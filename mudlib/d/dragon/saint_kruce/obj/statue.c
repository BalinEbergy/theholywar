// statue.c
// written by zz

inherit ITEM;

void create()
{
        set_name("[1;37m(泛銀白色光芒)[m銀騎士凱斯特的雕像", ({ "statue" }));
        set("pre","[1;37m(泛銀白色光芒)[m");
        set("long", @LONG
一座宏偉的雕像,在陽光下散發著銀色的光芒.
一個騎士騎在白馬上,彷彿天神下凡.
不禁想多看幾眼.
LONG
                );
        set("unsac", 1);
        set("no_get", 1);
        set("weight", 30000);
}
