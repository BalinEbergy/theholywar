// forest.c
// written by Hudson

#include <skill.h>

int main( object me, object obj)
{
        int curepoint= 200;

        curepoint+= obj->query("currentHP")/ 8;

        tell_object( obj, "你覺得身體的傷勢減輕了!\n");

        return COMBAT_D->heal_func( obj, "HP", curepoint);
}

string name( int select)
{
        switch( select)
        {
                case 1: return "森林之聲";
                case 2: return "forest sound";
                case 3: return "forest";
        }
}

int type() { return SONG; }
int lv() { return 2; }
int attackful()
{
        return 0;
}

string mes( object me)
{
        return "唱出一段優美的旋律,如同遙遠森林的聲音...";
}

int usesp() { return 20; }
int usemp() { return 0; }
int successful() { return 98; }
int delay() { return 3; }
int all() { return 1; }

int check( object me)
{
        return 1;
}
