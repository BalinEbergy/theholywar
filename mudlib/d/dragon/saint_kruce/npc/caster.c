//caster.c
//written by zz

#include <class.h>
#include <element.h>
#include <race.h>

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
        int HP= 1000000, SP= 100000;

        seteuid( getuid());

        set_name("銀騎士凱斯特的靈魂", ({ "caster", "cas" }));
        set("lv", 1000);
        set("class","knight");
        set("pre","[36m(半透明)[m");
        set("race",DRAGON);
        set("describe","全身發出銀色光芒的銀騎士凱斯特");
        set("age", 40);
        set("element", THUNDER);
        set("mind", 0);
        set("gender", 1);

        set("maxHP", HP);
        set("currentHP", HP);
        set("maxSP", SP);
        set("currentSP", SP);
        set("dem", 70000);
        set("def", 40000);
        set("mdem", 20000);
        set("mdef", 20000);
        set("spirit", 40);
        set("spe", 2000);
        set("speed", 40);
        set("exp", 10000);

        carry_object( OB_LANCE+ "phantom_lance")->wear( "b");

        set("chat_chance", 30);
        set("chat_msg", ({
                (: random_move :)
                }));

        setup();
}
