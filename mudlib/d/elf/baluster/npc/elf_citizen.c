// elf citizen
// written by Hudson

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
    int HP= 20000, MP= 3000, SP= 900;

    set_name("精靈居民", ({ "elf citizen", ELF }));
    set("lv", 29);
    set("class",WIZARD);
    set("race",ELF);
    set("gender",1);
    set("describe","往來於道路上的精靈.");
    set("pre","往來道路上的");

    set("age", 62);
    set("element",WIND);
    set("mind", 144);

    set("maxHP", HP);
    set("currentHP", HP);
    set("maxMP", MP);
    set("currentMP", MP);
    set("maxSP", SP);
    set("currentSP", SP);
    set("strength", 900);
    set("armor", 812);
    set("magic", 1600);
    set("spi", 5);
    set("speed", 410);
    set("exp", 2000);

    set("chat_chance", 15);
    set("chat_msg", ({
        (: random_move :)
        }));

    setup();
}

void init()
{
    object obj;

    if( !(obj= previous_object())
        || !userp( obj))
        return;

    call_out("ki", 10, obj);
}

void ki( object obj)
{
    if( obj->is_fighting()) kill_ob( obj);
}
