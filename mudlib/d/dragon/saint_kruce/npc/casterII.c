//casterII.c
//written by zz

#include <class.h>
#include <element.h>
#include <race.h>

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
        int HP= 4000000, MP= 10000, SP= 100000;

        set_name("銀騎士凱斯特的靈魂", ({ "caster", "cas" }));
        set("lv", 1000);
        set("class","knight");
        set("pre","[36m(半透明)[m");
        set("race",DRAGON);
        set("describe","全身發出銀色光芒的銀騎士凱斯特");
        set("age", 40);
        set("element", THUNDER);
        set("mind", 0);
        set("gender", 1);

        set("maxHP", HP);
        set("currentHP", HP);
        set("maxSP", SP);
        set("currentSP", SP);
        set("dem", 300000);
        set("def", 1000000);
        set("mdem", 50000);
        set("mdef", 10000);
        set("spirit", 40);
        set("spe", 1000);
        set("exp", 10000);

        setup();
}
void init()
{
        ::init();
        if( !previous_object()
                || !userp(previous_object()))
                return;
        call_out("action", 1, previous_object());
        kill_ob( previous_object());
}
