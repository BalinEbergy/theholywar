// blue_robe.c
// written by Hudson

#include <equip.h>
#include <element.h>

inherit EQUIP;

void create()
{
    set_name("藍色法袍", ({ "blue robe", "blue", "robe" }));
    set("long", @LONG
一件藍色的長袍,有著些許的魔力.
LONG
        );
    set("lv", 10);
    set("unit", "件");
    set("element", THUNDER);
    set("epower", 20);
    set("value", 10);

    set("wear_type", BODY);
    set("def_eff", 100);
    set("mdem_eff", 100);
    set("eff", ({ "def_eff", "mdem_eff" }));

    set_weight( 100);
}
