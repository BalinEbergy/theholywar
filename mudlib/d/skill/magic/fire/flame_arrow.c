// flame_shot.c
// written by Hudson

#include <skill.h>

inherit FIRE_MAGIC;

void create()
{
    set("name", "炎之箭");
    set("id", "flame arrow");
    set("lv", 10);
    set("attack", 1);
    set("usemp", 60);
    set("delay", 1);
    set("base_value/hp", 150);
    set("improve/hp", 21);
    set("cost", 80);
    set("exp", 80);

    setup();
}

string mes( object me)
{
    return "$N手呈拉弓狀, 忽然有一股[1;31m炎氣[m從$N手中發出!!!";
}
