// travelling_4.c
// written by hiwind

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
        int HP= 15, SP= 50;

        set_name("����", ({ "lv5" }));
        set("lv", 1);
        set("class","mage");
        set("ra","elf");
        set("describe","����.");
        set("age", 25);
        set("element", "fire");
        set("mind", 500);

        set("mixHP", HP);
        set("currentHP", HP);
        set("mixSP", SP);
        set("currentSP", SP);
        set("dem", 10);
        set("def", 4);
        set("spirit", 14);
        set("spe", 6);
        set("exp", 1016750);
        setup();
}
