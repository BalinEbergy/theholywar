// blade2.c
// written by Hudson

#include <equip.h>
#include <element.h>

inherit EQUIP;

void create()
{
        set_name("�����M", ({ "blade" }));
        set("long", @LONG
LONG
                );
        set("lv", 100);
        set("unit", "��");
        set("element", HOLY);
        set("epower", 100);
        set("weight", 500);

        set("wear_type", HAND);
        set("weapon", BLADE);
        set("dem_eff", 2000000000);
        set("def_eff", 2000000000);
        set("spe_eff", 1000000000);
        set("eff", ({ "dem_eff", "spe_eff", "def_eff" }));
}
