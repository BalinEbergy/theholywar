// dragon_lance.c
// written by zz

#include <equip.h>
#include <element.h>

inherit EQUIP;

void create()
{
        set_name("龍牙戟", ({ "dragon lance", "lance", "dragon" }));
        set("long", @LONG
一把以龍牙做成的的長戟
LONG
                );
        set("lv", 90);
        set("unit", "把");
        set("element", WIND);
        set("epower", 100);
        set("weight", 100);

        set("wear_type", HAND);
        set("weapon", LANCE);
        set("dem_eff", 10000);
        set("spe_eff", 100);
        set("eff", ({ "dem_eff", "spe_eff" }));
}
