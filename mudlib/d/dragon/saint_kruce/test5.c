//test5.c
//written by zz

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;37mtest5[m");
        set("long",@LONG
test Lv51-60
LONG
        );
        set("exits", ([
                "north" : __DIR__ + "test4.c" ,
                "south" : __DIR__ + "test6.c" ,
                ]));
        set("objects",  ([
                __DIR__ + "npc/test/lv51" :1 ,
                __DIR__ + "npc/test/lv52" :1 ,
                __DIR__ + "npc/test/lv53" :1 ,
                __DIR__ + "npc/test/lv54" :1 ,
                __DIR__ + "npc/test/lv55" :1 ,
                __DIR__ + "npc/test/lv56" :1 ,
                __DIR__ + "npc/test/lv57" :1 ,
                __DIR__ + "npc/test/lv58" :1 ,
                __DIR__ + "npc/test/lv59" :1 ,
                __DIR__ + "npc/test/lv60" :1 ,
        ]));
        set("no_recall", 1);
        setup();
}
