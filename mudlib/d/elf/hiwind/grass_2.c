// grass_2.c
// written by hiwind

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;32m���[m");
        set("long",@LONG
�A�~��V�e��,�A�ݨ�@�Ӥ@��L�ڪ����,��B�����F��
(grass)�A�w�g�L�k�^�Y,�u���D������G�i�H�^��[1;36m��[m����,
�ө��F���G�O�h���䪺�F�y,��!�٬O�����X���a!
LONG
        );

        set("exits",
        ([
        "east" : __DIR__ + "grass_4",
        "west" : __DIR__ + "street_10",
        "south" : __DIR__ + "grass_1",
        ])
        );
}
