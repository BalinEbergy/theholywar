// short_spear.c
// written by zz

#include <equip.h>
#include <element.h>

inherit EQUIP;

void create()
{
        set_name("短矛", ({ "short spear" , "short" , "spear" }));
        set("long", @LONG
一把練習用的短矛

LONG
                );
        set("lv", 1);
        set("unit", "把");
        set("element", EARTH);
        set("epower", 10);
        set("weight", 50);
        set("value", 5);

        set("wear_type", HAND);
        set("weapon", LANCE);
        set("dem_eff", 5);
        set("spe_eff", 1);
        set("eff", ({ "dem_eff", "spe_eff" }));
}
