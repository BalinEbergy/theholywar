// curse_sound.c
// written by Bother

#include <skill.h>

int main( object me, object obj)
{
        int dem= 0, def= 0, mdem= 0, mdef= 0, spe= 0, p= 5+ random( 5);
        int a= 0, b= 0, c= 0, d= 0, e= 0;
        string *effects;

        effects= obj->query("effect");

        if( !obj->query("curse_dem_st"))
        {
                if( random( 2)> 0) {
                        dem= -obj->query("dem");
                        obj->set("curse_dem_st", me->skill_level( FLUTE)/ 4);
                        message_vision("$N�t��[1;30m�A�G����[m��[36m$n[1;31m�����O[m�U���F.\n", me, obj);
                        a= 1;
                        obj->set("curse_dem_st", ({
                                "dem_eff:"+ dem/p,
                        }));
                        if( effects)
                                effects+= ({ "curse_dem_st" });
                        else effects= ({ "curse_dem_st" });
                }
                else;
        }
        if( !obj->query("curse_def_st"))
        {
                if( random( 2)> 0) {
                        def= -obj->query("def");
                        obj->set("curse_def_st", me->skill_level( FLUTE)/ 4);
                        message_vision("$N�t��[1;30m�A�G����[m��[36m$n[1;37m���m�O[m�U���F.\n", me, obj);
                        b= 1;
                        obj->set("curse_def_st", ({
                                "def_eff:"+ def/p,
                        }));
                        if( effects)
                                effects+= ({ "curse_def_st" });
                        else effects= ({ "curse_def_st" });
                }
                else;
        }
        if( !obj->query("curse_mdem_st"))
        {
                if( random( 2)> 0) {
                        mdem= -obj->query("mdem");
                        obj->set("curse_mdem_st", me->skill_level( FLUTE)/ 4);
                        message_vision("$N�t��[1;30m�A�G����[m��[36m$n[1;34m�]�O[m�U���F.\n", me, obj);
                        c= 1;
                        obj->set("curse_mdem_st", ({
                                "mdem_eff:"+ mdem/p,
                        }));
                        if( effects)
                                effects+= ({ "curse_mdem_st" });
                        else effects= ({ "curse_mdem_st" });
                }
                else;
        }
        if( !obj->query("curse_mdef_st"))
        {
                if( random( 2)> 0) {
                        mdef= -obj->query("mdef");
                        obj->set("curse_mdef_st", me->skill_level( FLUTE)/ 4);
                        message_vision("$N�t��[1;30m�A�G����[m��[36m$n[1;33m�F�O[m�U���F.\n", me, obj);
                        d= 1;
                        obj->set("curse_mdef_s", ({
                                "mdef_eff:"+ mdef/p,
                        }));
                        if( effects)
                                effects+= ({ "curse_mdef_st" });
                        else effects= ({ "curse_mdef_st" });
                }
                else;
        }
        if( !obj->query("curse_spe_st"))
        {
                if( random( 2)> 0) {
                        spe= -obj->query("spe");
                        obj->set("curse_spe_st", me->skill_level( FLUTE)/ 4);
                        message_vision("$N�t��[1;30m�A�G����[m��[36m$n[1;36m�t��[m�U���F.\n", me, obj);
                        e= 1;
                        obj->set("curse_spe_st", ({
                                "spe_eff:"+ spe/p,
                        }));
                        if( effects)
                                effects+= ({ "curse_spe_st" });
                        else effects= ({ "curse_spe_st" });
                }
                else;
        }
        if( a= 0 && b= 0 && c= 0 && d= 0 && e= 0)
                message_vision("$N�t��[1;30m�A�G����[m��[36m$n[m���y������v�T\n", me, obj);
        else 
                message_vision("$N�t��[1;30m�A�G����[m��[36m$n[m[1;33m�å����ͥ���v�T[m\n", me, obj);

                obj->set("curse_st", me->skill_level( FLUTE)/ 4);
                obj->set("no_recall", me->skill_level( FLUTE)/ 4);
                obj->add("dem_eff", dem/p);
                obj->add("def_eff", def/p);
                obj->add("mdem_eff", mdem/p);
                obj->add("mdef_eff", mdef/p);
                obj->add("spe_eff", spe/p);
                if( effects)
                        effects+= ({ "curse_s" , "no_recall" });
                else effects= ({ "curse_s" , "no_recall" });
                obj->set("effect", effects);

        tell_object( obj, "\n�Aı����F�A�G,����]��z�F�U��.\n");

        return -1;
}

string name( int select)
{
        switch( select)
        {
                case 1: return "�A�G����";
                case 2: return "curse sound";
                case 3: return "curse_sound";
        }
}

int type() { return FLUTE; }
int lv() { return 100; }
int attackful()
{
        return 1;
}

string mes( object me)
{
        return "��M�o�X�@�}[31m�s�z[m���n��,[1;5;30m�Ѫ������t�F�U��[m\n"+
                "�@�����|�B�ް_�F[35m�@��[m,[1;36m�}�}�Y�D�����ֱq�äl���ǥX[m\n"+
                "�i�J�F���H������,���p�@�ɭn[1;31m����[m�@��.\n";
}

int usesp() { return 80; }
int usemp() { return 40; }
int successful() { return 90; }
int delay() { return 4; }
int all() { return 1; }
string element() { return "hell"; }

int check( object me)
{
        if( me->query("mind")> -300)
        {
                write("\n�A���G�������c��!\n");
                return 0;
        }

        return 1;
}
