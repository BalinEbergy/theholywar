// traveler.c
// written by Bother

inherit NPC;

void create()
{
        int HP= 450, SP= 250;

        set_name("旅客", ({ "traveler" }));
        set("lv", 3);
        set("class","warrior");
        set("ra","human");
        set("describe","弱小的旅客.");

        set("age", 34);
        set("element", "fire");
        set("goodat", "water");
        set("weakat", "wind");
        set("mind", 45);
        set("gender", "男");

        set("mixHP", HP);
        set("currentHP", HP);
        set("mixSP", SP);
        set("currentSP", SP);
        set("dem", 28);
        set("def", 17);
        set("spirit", 24);
        set("spe", 12);
        set("exp", 700);

        set("chat_chance", 5);
        set("chat_msg", ({
                (: this_object(), "random_move" :),
                "\n旅客東看看西看看.\n"
                "\n[1;37m旅客[m說:[真是個好地方]\n"
                }));
        setup();
}
