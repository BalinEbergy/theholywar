// diamondo.c
// written by Mondale
// 迪亞曼多

inherit NPC;

void create()
{
        int HP= 800000, MP= 50000, SP= 60000;

        set_name("迪亞曼多", ({ "diamondo" }));
        set("lv", 90);
        set("class","e_maker");
        set("ra","human");
        set("gender","女");
        set("describe","一位飽讀書籍的學者.");
        set("pre","公會會長");

        // 老師資訊
        set("teacher", ({
                12,
                }));
        set("favorite", ({
                "poet", "mage",
                }));
        set("teach_mind", "-1000 ~ 1000");
        set("lv_up", "[1;36m迪亞曼多微笑道:[33m 好了, 等級提升了吧![m\n");
        set("no_enu_money", "[1;36m迪亞曼多體貼的說道:[33m我想你應該去多賺點錢吧^^.[0m,\n");
        set("no_enu_lv", "[1;36m迪亞曼多笑道:[33m 你的等級還不夠喔...*[m");
        set("top_lv", "[1;36m迪亞曼多呵呵的笑道:[33m 我已經不能再教你什麼囉...[m\n");
        set("teach_message", "[1;36m迪亞曼多微笑道:[33m 你可以跟我學練金技能喔![0m\n");
        // ---------
        set("age", 25);
        set("element","water");
        set("mind", 900);

       
        set("mixHP", HP);
        set("currentHP", HP);
        set("mixMP", MP);
        set("currentMP", MP);
        set("mixSP", SP);
        set("currentSP", SP);
        set("dem", 10000);
        set("def", 50000);
        set("mdem", 30000);
        set("mdef", 50000);
        set("spe",1000);
        set("spirit",50);
        set("realmx", 0);
        set("exp", 150000);

        set("chat_chance", 20);
        set("chat_msg", ({
                "\n迪亞曼多抬頭看了看你。\n"
                }));
        set_skill_level( 1, 80);
        set_skill_level(11, 100);
        set_skill_level(12, 100);
        set("skill", ({
                "11:final",
                "1:life",
                }));
        set("skill_chance", 1000);
        setup();
}

void init()
{
        ::init();
        if( !previous_object()
                || !userp(previous_object()))
                return;
        call_out("action", 10, previous_object());
//        if(previous_object->is_fighting() == 1)
//        kill_obj(previous_object);
}

void action( object obj)
{
        command("hihi 請問有什麼事嗎？");
        setup();
}


int rebirth( object obj)
{
        if( !obj->query("ghost"))
        {
                if( obj->query("age")< this_object()->query("age"))
                command("hhhh "+ obj->query("id")+
                        " 別說笑了...小傢伙,你還沒死呢...");
                else command("hhhh "+ obj->query("id")+
                        " 呵呵,你還沒死呢!");
                return 1;
        }

        command("hhhh "+ obj->query("id")+ " "+ obj->name()+
                "呀... 你還想回到這個世界上嗎? ...");
        call_out("do_rebirth", 1, obj);

        return 1;
}

void do_rebirth( object obj)
{
        command("hhhh "+ obj->query("id")+
                " 好吧, 給我一些經驗, 幫你找回你的屍體吧...");
        command("cast life to "+ obj->query("id"));
        command("hhhh "+ obj->query("id")+ " 你現在已經重生了!");

        return;
}
