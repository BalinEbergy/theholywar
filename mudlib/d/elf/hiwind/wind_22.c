// wind_22.c
// written by Hiwind

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;37m��o�p��[m");
        set("long",@LONG
���B�O�@����o�w�[���Ы�,�èS���S�O���a��,���ӬO�Y���F
�����v�a,�M��,�Τl�̦��w�����F����,�s�p�ʪ����|�b�䶡
�X�S,�ݨӯu���O�n�[�S���H�ӤF,���A�J���˹�F�@�U,���O
�����G���H�v�̰�,�u�O�_��,�٬O�h�ݰݤH�n�F.
LONG
        );
        set("exits",
        ([
        "west"  : __DIR__ + "wind_19",
        ])
        );

        set("objects",
        ([
                __DIR__ + "forest/npc/wolf":1,
                __DIR__ + "forest/npc/rabbit":2,
                __DIR__ + "forest/npc/shrewmouse":1,
        ])
        );

        set("no_fight", 0);
        setup();
}
