// thunder_magic.c
// written by Hudson

#include <skill.h>

inherit MAGIC;

void setup()
{
    set("element", THUNDER);
    set("successful", 75);
    set("target_number", 1);
    set("volatility", 98);
}

string type() { return "thunder"; }
