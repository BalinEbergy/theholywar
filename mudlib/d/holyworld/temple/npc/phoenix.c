// phoenix.c
// written by Hudson

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
    int HP= 400000, MP= 40000, SP= 40000;

    set_name("鳳凰", ({ "phoenix" }));
    set("lv", 400);
    set("describe","一尾全身金羽,身旁圍繞著數道火燄的鳳凰.");
    set("pre","全身圍繞火燄的");

    set("age", 4000);
    set("element", FIRE);
    set("race", SAINTRE);
    set("mind", 4000);
    set("gender", 1);

    set("maxHP", HP);
    set("currentHP", HP);
    set("maxMP", MP);
    set("currentMP", MP);
    set("maxSP", SP);
    set("currentSP", SP);
    set("str", 40);
    set("vir", 40);
    set("magic", 40);
    set("spirit", 40);
    set("speed", 40);
    set("dem", 40000);
    set("def", 40000);
    set("mdem", 40000);
    set("mdef", 40000);
    set("spe", 4000);
    set("exp", 400000);

    set("chat_chance", 10);
    set("chat_msg", ({
        "鳳凰揚了揚尾,幾道火燄往旁邊散去.\n",
        "鳳凰很奇異的看著你.\n",
        "鳳凰銳利的眼睛正瞪著你看,你覺得有點毛毛的.\n",
        }));

    setup();
}
