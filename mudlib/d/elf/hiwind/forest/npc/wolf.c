// wolf.c
// written by hiwind

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
    int HP= 12000, MP= 500,SP= 2500;

    set_name("�M���y�����T", ({ "wolf" }));
    set("lv", 20);
    set("describe"," ���T.");
    set("age", 10);
    set("element", WIND);
    set("race", ANIMAL);
    set("mind", -1000);

    set("maxHP", HP);
    set("currentHP", HP);
    set("maxMP", MP);
    set("currentMP", MP);
    set("maxSP", SP);
    set("currentSP", SP);
    set("strength", 440);
    set("spi", 15);
    set("speed", 160);
    set("exp", 1800);
    setup();
    set("chat_chance", 10);
    set("chat_msg", ({
        (: random_move :)}));

}
void action( object obj)
{
    if( !obj || !present(obj) || this_object()->is_fighting()) return;
    if( obj->is_fighting()) kill_ob(obj);
    setup();
}


