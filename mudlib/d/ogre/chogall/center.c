//center.c

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;31m卡格爾城廣場中央[m");
        set("long",@LONG
你踏著血紅的石板,來到了卡格爾城廣場中心,你往四周望去
只見一片的紅色,彷彿來到了地獄一般,眼前一把幾乎是你兩
倍高的巨斧半沒在地下,據說是從前酋長卡格爾(chogall)的
遺物,它散發著一股狂暴的氣息,似乎要將你吞食了似的.
LONG
        );

        set("exits", ([
        "east" : __DIR__ + "eground1",
        "north" : __DIR__ + "nground1",
        "south" : __DIR__ + "sground1",
        "west" : __DIR__ + "wground1",
        ]));

        set("objects",  ([
                __DIR__ + "npc/ogre": 2,
                __DIR__ + "npc/wedgell": 1,
                OB_SPECIAL+ "old_axe":1,
        ]));

        setup();
}
