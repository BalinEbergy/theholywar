// square-w.c
// written by koyamakyo

#include <room.h>

inherit ROOM;

void create()
{
   set("short","城鎮廣場西側");
        set("long",@LONG
這裡是大城城鎮廣場的西側.
LONG
        );
       set("exits",
        ([
        "east" : __DIR__ + "center",
        "north" : __DIR__ + "square-nw",
        "south" : __DIR__ + "square-sw",
        "west" : __DIR__ + "main_road-w1",
        ])
        );

        set("objects",  ([
                __DIR__ + "npc/roni":2,
                ]));
        setup();

}
