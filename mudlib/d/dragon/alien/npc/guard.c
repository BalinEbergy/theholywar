// guard.c
// written by Mondale
// �u��

inherit NPC;

void create()
{
        int HP= 100000, MP= 0, SP= 6000;

        set_name("[m[1m�u��[m", ({ "guard" }));
        set("lv", 70);
        set("class","warrior");
        set("ra","�s��");
        set("gender","�k");
        set("describe","�t�d�ȧQ�����v�w���u��.");
        set("pre","�ȧQ������");

        set("age", 32);
        set("element","earth");
        set("mind", 200);

        set("mixHP", HP);
        set("currentHP", HP);
        set("mixMP", MP);
        set("currentMP", MP);
        set("mixSP", SP);
        set("currentSP", SP);
        set("dem", 9000);
        set("def", 1000);
        set("mdem", 5000);
        set("mdef", 5000);
        set("spe",1000);
        set("realmx", 50);
        set("exp", 12500000);
        set("chat_chance", 10);
        set("chat_msg", ({
                (: this_object(), "random_move" :),       
                "\n�u�å��V�O�����޵�\n"
                }));
        setup();
}

void init()
{
        ::init();
        if( !previous_object()
                || !userp(previous_object()))
                return;
        call_out("action", 1, previous_object());
        if( previous_object()->is_fighting())
        call_out("ki", 10, previous_object());
}

void ki( object obj)
{
        if( obj->is_fighting()) kill_ob( obj);
}

void action( object obj)
{
        command("say �Ф��n�b�����x��,����...");
        setup();
}
