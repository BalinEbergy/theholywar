// group.c
// written by Hudson

#include <command.h>

int main( object me, int argc, string *argv)
{
    int i, j;
    object *group, *pets, obj;
    string list= "\n[1;37;44m隊伍成員                                                                       [m\n\n";

    if( argc< 2)
    {
        group= me->get_leader()->get_group();

        i= sizeof( group);
        while( i--)
        {
            list+= sprintf("%s %sLV %3d[m [%s] (%s) %s %-29s\n",
                D_STATUS->state_bar(
                    group[i]->query("currentHP")* 10/ group[i]->query("maxHP"),
                    group[i]->query("currentMP")* 10/ group[i]->query("maxMP"),
                    group[i]->query("currentSP")* 10/ group[i]->query("maxSP")),
                D_STATUS->ele( group[i]->query("element"), 0),
                group[i]->query("lv"), D_STATUS->ra( group[i]->query("race")),
                group[i]->query("gender")? "男": "女", group[i]->query("title"),
                group[i]->query("name")+ " ("+ group[i]->query("id")+ ")");

            if( pets= group[i]->query_temp("pet"))
            {
                j= sizeof( pets);
                while( j--)
                {
                    list+= sprintf("\n\t\t%sLV %3d[m [%s] (%s) %s %s(%s)\n"+
                        "\t\t[[1;37m %7d/ %-7d [m|[1;36m %7d/ %-7d [m|[1;33m %7d/ %-7d [m]",
                        D_STATUS->ele( pets[j]->query("element"), 0),
                        pets[j]->query("lv"), D_STATUS->ra( pets[j]->query("ra")),
                        pets[j]->query("gender"), pets[j]->query("pre"),
                        pets[j]->query("name"), pets[j]->query("id"),
                        pets[j]->query("currentHP"), pets[j]->query("mixHP"),
                        pets[j]->query("currentMP"), pets[j]->query("mixMP"),
                        pets[j]->query("currentSP"), pets[j]->query("mixSP"));
                }
            }
        }

        write( list+ "\n\n");

        return 1;
    }

    if( !objectp( obj= find( argv[1], environment( me))) || !obj->is_character())
        write("沒有這個人喔!\n");
    else {
        switch( me->group( obj))
        {
            case 1: write("只有隊長能組隊.\n"); break;
            case 2: write("這個傢伙並沒有跟隨你唷~\n"); break;
            case 3: write("他已經在隊上了.\n"); break;
            case 4: write("他已經是另一個隊伍的隊長了.\n"); break;
            case 5: write("他不能進入隊伍.\n"); break;
            case 6: write("你們的等級相差太多囉.\n"); break;
            case 7: write("隊伍人數太多囉.\n"); break;
        }
    }

    return 1;
}
