// travelling.c
// written by hiwind

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
    int HP= 400, MP= 200, SP= 400;

    set_name("�෬���ȫ�", ({ "traveler" }));
    set("lv", 1);
    set("class",WARRIOR);
    set("race",HUMAN);
    set("describe","�෬���ȫ�.");
    set("age", 100);
    set("element", FIRE);
    set("mind", 0);

    set("maxHP", HP);
    set("currentHP", HP);
    set("maxMP", MP);
    set("currentMP", MP);
    set("maxSP", SP);
    set("currentSP", SP);
    set("strength", 120);
    set("armor", 80);
    set("magic", 10);
    set("spi", 1);
    set("speed", 110);
    set("exp", 200);
    setup();
    set("money", 1);
    carry_object( "d/elf/hiwind/forest/obj/meat");
}
