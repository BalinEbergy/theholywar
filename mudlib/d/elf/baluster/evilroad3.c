// evilroad3.c
// written by Hudson

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[31m���c����[m");
        set("long",@LONG
�A��L�F�@���·t,�Ө�o��,���G�S���F�@�I����G,�A�oı
�|�P��¶�ۤ@��骺���P,�o��N�O�k�v�̥𮧪��a��F�a?
LONG
        );

        set("exits",
        ([
        "east"  : __DIR__ + "earthdoor",
        "down"  : __DIR__ + "helldoor",
        "up"    : __DIR__ + "firedoor",
        "north" : __DIR__ + "darkdoor",
        "south" : __DIR__ + "evilroad2",
        "west"  : __DIR__ + "waterdoor",
        ])
        );

        set("objects", ([
                __DIR__ + "npc/evilgoblin": 1,
                ]));

        setup();
}
