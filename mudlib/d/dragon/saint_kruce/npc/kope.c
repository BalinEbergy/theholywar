// kope.c
// written by zz

#include <class.h>
#include <element.h>
#include <race.h>

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
        int HP=60000, MP= 0, SP= 8000;

        set_name("�F��", ({ "kope" }));
        set("lv", 40);
        set("class",WARRIOR);
        set("race",HUMAN);
        set("gender",1);
        set("describe","���Ӫ��Z��������F��.");
        set("pre", "�Z��������");

        set("age", 25);
        set("element", FIRE);
        set("mind", 0);

        set("maxHP", HP);
        set("currentHP", HP);
        set("maxSP", SP);
        set("currentSP", SP);
        set("dem", 10000);
        set("def", 8000);
        set("mdem", 1200);
        set("mdef", 5000);
        set("spe", 990);
        set("exp", 2500);

        set("trading", 301);
        set("buy_mind", "-1000~1000");
        set("selling", ({
                OB_LANCE + "spear",
                OB_LANCE + "short_spear" ,
                }));

        setup();
}
