// uno.c
// written by Hudson

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
    int HP= 27000, MP= 0, SP= 2000;

    set_name("�տ�", ({ "uno" }));
    set("lv", 30);
    set("class", WARRIOR);
    set("race", ELF);
    set("gender", 1);
    set("describe","�N�������ȩ�����,�������I�G.");
    set("pre", "�ȩ�����");

    set("age", 400);
    set("element", LIGHT);
    set("mind", 113);

    set("maxHP", HP);
    set("currentHP", HP);
    set("maxSP", SP);
    set("currentSP", SP);
    set("strength", 1200);
    set("armor", 900);
    set("magic", 200);
    set("spi", 3);
    set("speed", 1100);
    set("exp", 2200);
    set("inn", 5);

    setup();

    carry_object( OB_LANCE + "spear")->equip();
}
