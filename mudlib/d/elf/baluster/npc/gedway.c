// gedway.c
// written by Hudson
// 吉德威

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
    int HP= 300000, MP= 31500, SP= 4300;

    set_name("吉德威", ({ "gedway" }));
    set("lv", 88);
    set("class", WIZARD);
    set("race", ELF);
    set("gender", 1);
    set("describe","一位英姿煥發的老者.");
    set("pre","天之法師");

    set("age", 533);
    set("element",COSMOS);
    set("mind", 599);

    set("teacher", ({
        5,
        }));
    set("favorite", ({
        WIZARD,
        }));
    set("teach_mind", "500 ~ 1000");
    set("teach_msg_evil", "[1;36m吉德威說道:[33m 我不教邪惡之徒的...[m");
    set("lv_up", "[1;36m吉德威說道:[33m 哈哈...等級提升了...[m\n");
    set("no_enu_money", "[1;36m吉德威說道:[33m 哈哈哈...要學東西之前得先有足夠的錢才行喔![m");
    set("no_enu_lv", "[1;36m吉德威說道:[33m 你目前的能力還學不會這套魔法喔![m");
    set("top_lv", "[1;36m吉德威說道:[33m 恭喜啦! 你的能力已經跟我不相上下了![m");
    set("teach_message", "[1;36m吉德威說道:[33m 哈哈...對天空的魔法有興趣嗎?[m");

    set("maxHP", HP);
    set("currentHP", HP);
    set("maxMP", MP);
    set("currentMP", MP);
    set("maxSP", SP);
    set("currentSP", SP);
    set("strength", 12000);
    set("armor", 27323);
    set("magic", 37900);
    set("spi", 21);
    set("speed", 2442);
    set("exp", 247000);

    setup();

    carry_object( OB_ROBE+ "air_robe")->equip();
}

void init()
{
    object obj;

    if( !(obj= previous_object())
        || !userp( obj))
        return;

    call_out("ki", 10, obj);
    call_out("action", 20, obj);
}

void action( object obj)
{
    if( !obj || !present(obj) || this_object()->is_fighting()) return;
    command("say 天空的歌聲,真美呀...你說,是不是呢?");
}

void ki( object obj)
{
    if( obj->is_fighting()) kill_ob( obj);
}

int teach_check( object me)
{
    if( me->query("neckr")== "mage permit");
    else if( me->query("neckl")== "mage permit");
    else
    {
        command("say 你還未成為真正的魔法師, 很抱歉我不能教你任何東西...");
        return 1;
    }

    return 0;
}
