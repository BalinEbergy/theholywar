// correct.c

int main( object me, int argc, string *argv)
{
    string verb;
    object obj;

    if( argc< 2)
        write("你想要更正誰呢?\n");
    else if( !objectp( obj= find( argv[1], environment( me))))
        write("這裡沒有這個人.\n");
    else {
        message_vision("[36m$N喃喃念著彷彿不存在世間的咒語...\n[m", me);
        tell_object( obj, "你感到有一股[1;31m莫名的力量[m在調整著你...\n");
        obj->initial_for_old();
    }

    return 1;
}

int help( object me)
{
    write( @HELP
指令格式: correct <目標>

更正指定的目標
HELP
        );

    return 1;
}
