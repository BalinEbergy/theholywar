// temple.c
// written by Hudson

#include <room.h>

inherit ROOM;

void create()
{
    set("short", "[1;33m�j��[m[1;37m�t[m[1;34m��[m[36m����[m");
    set("long", @LONG
�o��N�O�ǻ��������Х@�������t��,���ߪ�[1;37m�t[33m�F[m[1;34m����[m([1;37mHoly[m
[1;34mcrystal[m),�O�t�F�O���ӷ�,�A���Y���W�x�U�@�D[1;36m�X�M�����~[m
,�o�̰��b���M���𮧨ϧA�P������D�`���κZ.
LONG
    );
    set("exits", ([
        "down"  : __DIR__ + "down",
        ]));
    set("objects", ([
        __DIR__ + "npc/phoenix": 1,
//To Hudson
//
//�Ȯɲ���....�p��
//�n���@�өж����঳��ӦѮv��.....�ݬݦ��S����k��@�U
//�θ�ڻ��@�U��teach�������ɮ�
        __DIR__ + "npc/ramuh": 1,
        __DIR__ + "npc/confucius": 1,
        OB_SIGN + "light_word": 1,
        ]));
    set("no_fight", 1);

    setup();
}

int valid_leave( object me, string dir)
{
    if( dir== "down")
    {
        if( D_WIZARD->wiz_level( me)< 3)
        {
            write("�@���]�O�T�D�ۧA.\n", me);
            return 1;
        } else return 0;
    } else return 0;
}
