// mount_music.c
// written by Hudson

#include <skill.h>

int main( object me, object obj)
{
        int default_d= 20, m_mdef, o_mdef, damage, lv;

        if( !obj) return -1;

        m_mdef= me->query("mdef")+ me->query("mdef_eff");
        o_mdef= obj->query("mdef")+ obj->query("mdef_eff");
        lv= me->skill_level( DRUM);
        default_d= default_d+ default_d *lv/ 20;

        damage= default_d* m_mdef/ o_mdef;
        damage= damage/ 4* 3+ random( damage/ 2);

        obj->add("currentHP", -damage);

        return COMBAT_D->skill_msg( me, obj, "山之樂", "none", damage);
}

string name( int select)
{
        switch( select)
        {
                case 1: return "山之樂";
                case 2: return "mountain music";
                case 3: return "mount_music";
        }
}

int type() { return DRUM; }
int lv() { return 3; }
int attackful()
{
        return 1;
}

string mes( object me)
{
        return "手上的鼓棒漸趨緩慢,"+ me->query_temp("handb")->name()
                + "的聲音越來越低沉莊肅,聲音如雄偉的高山般將敵人壓在底下!!";
}

int usesp() { return 10; }
int usemp() { return 0; }
int successful() { return 80; }
int delay() { return 0; }
int all() { return 1; }

int check( object me)
{
        if( me->query_temp("drum")> 1)
        {
                write("\n你上一個動作還沒完成...\n");
                return 0;
        } else if( !me->query_temp("drum"))
        {
                write("\n你無法在現在的情況下打出山之樂...\n");
                return 0;
        }

        me->set_temp("drum", 2);
        call_out("drum_dis", 2, me);

        return 1;
}

void drum_dis( object me)
{
        if( me->query_temp("drum")== 2)
                me->delete_temp("drum");
}
