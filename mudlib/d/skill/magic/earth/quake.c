// quake.c
// written by Hudson

#include <skill.h>

int main( object me, object obj)
{
        int default_d= 42, demage, eff, mdem, mdef;

        mdem= me->query("mdem")+ me->query("mdem_eff");
        mdef= obj->query("mdef")+ obj->query("mdef_eff");

        default_d= default_d+ default_d* me->skill_level( EARTH)/ 33;

        if( me->query("element")== "earth")
                demage= default_d* mdem* 1.2/ mdef;
        else
                demage= default_d* mdem/ mdef;

        if( obj->query("element")== "water") demage= demage* 1.2;

        eff= obj->query("earth_eff");

        demage= demage* ( 50+ random( 100))/ 100*( 100- eff)/ 100;

        obj->add("currentHP", -demage);

        return demage;
}

string name( int select)
{
        switch( select)
        {
                case 1: return "地動咒";
                case 2: return "quake";
                case 3: return "quake";
        }
}

int type() { return EARTH; }
int lv() { return 10; }
int attackful()
{
        return 1;
}

string mes( object me)
{
        return "唸完咒語,地面微微的動著...瞬間[1;30m大地動搖,砂石震天![m";
}

int usesp() { return 0; }
int usemp() { return 100; }
int successful() { return 80; }
int delay() { return 1; }
int all() { return 1; }
string element() { return "earth"; }

int check( object me)
{
        return 1;
}
