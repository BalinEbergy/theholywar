// inventory.c
// written by Hudson

int main( object me, int argc, string *argv)
{
    int i, j, check, *num;
    string pre, str, *objs;
    object *inv;

    inv= all_inventory( me);

    i= sizeof( inv);
    num= ({ 1 });
    objs= ({ "\n" });

    while( i--)
    {
        if( inv[i]->equipped()) continue;
        check= 0;
        j= sizeof( objs);
        while( j--)
        {
            if( inv[i]->short()== objs[j])
            {
                num[j]++;
                check= 1;
            }
        }
        if( !check)
        {
            num+= ({ 1 });
            objs+= ({ inv[i]->short() });
        }
    }

    str= (( num[ i= sizeof( objs)-1]- 1)?
        sprintf("\n(%2d) %s", num[i], objs[i]):
        sprintf("\n     %s", objs[i]));

    while( i--)
        str+= (( num[i]- 1)?
            sprintf("\n(%2d) %s", num[i], objs[i]):
            sprintf("\n     %s", objs[i]));

    i= me->query_encumbrance()* 100/ me->query_max_encumbrance();
    if( i< 30)
        pre= "44";
    else if( i< 50)
        pre= "46";
    else if( i< 70)
        pre= "42";
    else if( i< 90)
        pre= "41";
    else pre= "45";

    write(" \n[1;37;" + pre + "m"+
        "你正攜帶著這些東西                               [m\n"+ str);

    return 1;
}

int help( object me)
{
    write( @HELP
指令格式: inventory

這個指令可用來觀看身上攜帶的東西
HELP
        );

    return 1;
}
