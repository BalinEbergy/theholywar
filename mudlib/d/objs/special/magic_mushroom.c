// magic_mushroom.c
// written by Hudson

#include <element.h>

inherit FOOD;

void create()
{
    object obj;
    set_name("魔法香菇", ({ "magic mushroom", "magic", "mushroom" }));
    set("long", @LONG
用魔法造成的香菇.
LONG
        );
    set("unit", "株");
    set("lv", 1);
    set("no_save", 1);

    set_weight( 5);
}

void disappear()
{
    message_vision("$N腐敗了.", this_object());
    destruct( this_object());
}
