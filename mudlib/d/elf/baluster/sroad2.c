// sroad2.c
// written by Hudson

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;32m�ڸ����S��D[m");
        set("long",@LONG
�A��۲H��⪺�۪O,�b�ڸ����S����D������,�|�P��¶��
���P���,�u�O�Ӧ۵M���P��,�����i�H�ݨ��@�Ǻ��F�̪�
����,�A�Pı��@�����M.
LONG
        );
        set("exits",
        ([
        "east"  : __DIR__ + "sgroad2",
        "south" : __DIR__ + "sroad6",
        "west"  : __DIR__ + "sroad3",
        ])
        );
        set("objects", ([
                __DIR__ + "npc/fire_elf": 1,
                ]));

        setup();
}
