// pond.c
// written by koyamakyo

#include <element.h>

inherit ITEM;

void create()
{
    set_name("泉水", ({ "pond" }));
    set("long", @LONG
一池清澈的泉水,看起來非常的清涼解渴.
LONG
        );
    set("unit", "池");
    set("element", WATER);
    set("epower", 30);

    set("drinkable", 1);
    set("no_get", 1);
    set("unsac", 1);
}
