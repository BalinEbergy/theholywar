// center.c
// written by peternice

#include <room.h>

inherit ROOM;

void create()
{
        set("short","*[1;37m天嵐之島_上坡*[m");
        set("long",@long
    往這條路走是微微的上坡, 有一堆瘦瘦長長又煩人的野草, 擋住你往前的
道路,你必需撥開這些叢生的雜草才能繼續往前走, 一個不小心還會被長刺的
草給劃破手掌, 景色依然不改荒涼
long
        );

        set("exits",
        ([
        "north" : __DIR__ + "tests2e2",
        "west" :__DIR__ + "tests3e1",
         ])
        );
}
