// final_punishment
// written by Mondale

#include <skill.h>

string name(int i);
int main( object me, object obj)
{
        int damage,a,b,dam_all;
        string w_name= me->query_temp( me->query_temp("sla"))->name();
        dam_all = me->query("dem") + me->query("dem_eff");
        damage = me->query("str") * me->query("spirit") * dam_all / 100 + random(dam_all) - obj->query("def") - obj->query("def_eff") - obj->query("defand");
        message_vision("$N忽然加速，倏的一下，長劍直取$n的心口!!!\n", me ,obj);
        if (damage < 0 ) damage = 0;
        if (random(20) < 2)
        {
        tell_object(obj,"你覺得一陣風響，一道[1;35;44m紫光[0m已經穿身而過.\n");
        tell_object(me,"你只見" + obj->name() + "緩緩倒臥於血泊之中.\n");
        obj->set("currentHP",0);
        return -1;
        }
        obj->add("currentHP", -damage);
        if ( me->query("luna") == 0 && random(20000) < 5) call_out("learn", 1,me);
        return  COMBAT_D->skill_msg( me, obj, name( 1), w_name, damage);
}

string name( int select)
{
        switch( select)
        {
                case 1: return "終焉之罰";
                case 2: return "slash final punishment";
                case 3: return "final_punishment";
        }
}

int lv() { return 100; }
int attackful()
{
        return 1;
}

string mes( object me)
{
        return "將長劍橫持在眼前，將全身的意志灌注在劍上，\n" +
               "漸漸的，劍身散出一陣陣的[1;35m紫氣[0m，右腳向後一步，\n" +
               "拉出[37;1m突刺[0m的架勢，準備以這一招來[1;7;36m終結[0m一切.\n";
}

int type() { return SLASH; }
int usesp() { return 300; }
int usemp() { return 15; }
int successful() { return 100; }
int delay() { return 4; }
int all() { return 0; }
int check( object me)
{
        if (me->query_temp("sla") == "handb") 
        {
        return 1;
        }
        tell_object(me,"\n你必須要雙手拿劍才行喔.\n");
        me->delete("delay");
        return 0;
}

void learn( object me)
{
        string sd= "20:slash moon";
        me->add("skill", ({
        sd
        }));
        me->set("luna",1);
        tell_object(me,"\n你對劍法有了更深一步的體驗...");
        tell_object(me,"你領悟了[33;1m荒天月空殘[0m!!!\n");
}
