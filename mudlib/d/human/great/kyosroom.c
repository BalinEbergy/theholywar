// kyosroom.c
// written by koyamakyo

#include <room.h>

inherit ROOM;

void create()
{
   set("short","小山京的房間");
        set("long",@LONG
你一進到這個地方,就發現到這個房間比普通的房間大了
非常多,除了右側的牆邊放滿了五大書櫃的漫畫,和左側牆
邊的一百吋大螢幕,各式的遊戲主機,軟體以外,最引人注
目的就是房間中央放置的五架真人般高的「新機動戰記
鋼彈W－Endless Waltz」版的五架鋼彈的模型,如果你
有興趣的話,可以上前去看看(look).
LONG
        );
       set("exits",
        ([
        "down" : __DIR__ + "center",
        ])
        );

        set("objects",  ([
                //__DIR__ + "npc/Gundam_Wing_Zero_Custom":1,
                __DIR__ + "npc/Duo_Maxwell":1,
                //__DIR__ + "npc/Gundam_Heave-Arms_Custom":1,
                //__DIR__ + "npc/Gundam_Sandrock_Custom":1,
                //__DIR__ + "npc/Gundam_Nataku":1,
                ]));
        setup();


}
