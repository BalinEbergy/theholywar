// square-ne.c
// written by koyamakyo

#include <room.h>

inherit ROOM;

void create()
{
   set("short","城鎮廣場東北側");
        set("long",@LONG
這裡是大城城鎮廣場的東北側.
LONG
        );
       set("exits",
        ([
        "south" : __DIR__ + "square-e",
        "west" : __DIR__ + "square-n",
        ])
        );

        set("objects",  ([
                __DIR__ + "npc/roni":2,
                ]));
        setup();

}
