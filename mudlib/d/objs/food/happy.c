// happy.c
// written by Mondale

#include <element.h>

inherit FOOD;

void create()
{
    set_name("歡樂麵", ({ "happy noodles", "happy", "noodles" }));
    set("long", @LONG
可保存很久的歡樂麵,適合旅行的人食用!
LONG
        );
    set("unit", "份");
    set("lv", 11);
    set("value", 6);

    set_amount( 3);

    set_weight( 13);
}
