// stab_sea.c
// written by zz

#include <skill.h>

int main( object me, object obj)
{
        int damage, def_eff, dem_eff, dem, def, spe, ospe, elementX, hit;
        string weapon_name;

        if( obj->query("element")== "water" ) elementX= 0;
        if( obj->query("element")== "fire" ) elementX= 200;
        elementX= 115;

        spe= me->query("spe")+ me->query("spe_eff");
        ospe= obj->query("spe")+ obj->query("spe_eff");
        hit= ( spe* 100- ospe* 50)/ spe;
        if( hit> 80) hit= 99;
        if( hit< 0) hit= 0;
        if( random( 100)+ 100> hit+ elementX)
        {
                message_vision("\n可是沒有命中$N!\n", obj);
                return -1;
        }

        def_eff= obj->query("defand_eff");
        def= obj->query("def")+ obj->query("def_eff");
        dem_eff= me->query("damage_eff");
        dem= me->query("dem")+ me->query("dem_eff");
        weapon_name=( me->query_temp( me->query_temp("stb")))->query("name");
        if( me->query("element")== "water") elementX= 125;
        elementX= 100;

        damage= dem*( me->skill_level( STAB))/ 2+ elementX+ ( me->query("spirit"))/ 100 -
                ( def+ def_eff- dem_eff);
        if( damage< 0) damage= 0;

        obj->add("currentHP", -damage);
        // me->delete_temp("stb");

        return damage;
}

string name( int select)
{
        switch( select)
        {
                case 1: return "驚濤駭浪";
                case 2: return "stab sea";
                case 3: return "stab sea";
        }
}

int type() { return STAB; }
int lv() { return 90; }
int attackful()
{
        return 1;
}

string mes( object me)
{
        return "的心中湧起一陣怒氣,想像自己是一片汪洋的大海.\n";
}

int usesp() { return 150; }
int usemp() { return 0; }
int successful() { return 100; }
int delay() { return 4; }
int all() { return 1; }
string element() { return ""; }

int check( object me)
{
        return 1;
}
