// stone_3.c
// written by hiwind

inherit ITEM;

void create()
{
        set_name("精靈族人的石雕", ({ "statue" }));
        set("long", @LONG
這是一個精靈族人的石雕.刻劃得十分精細,栩栩如生.但與其
他所不同的是,這一個臉上的表情卻是神聖的,安詳的.
LONG
                );
        set("unsac", 1);
        set("no_get", 1);
        set("weight", 30000);
}
