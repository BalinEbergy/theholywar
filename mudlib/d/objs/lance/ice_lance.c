// ice_lance.c
// written by zz

#include <equip.h>
#include <element.h>

inherit EQUIP;

void create()
{
        set_name("寒冰月戟", ({ "ice lance", "lance", "ice" }));
        set("long", @LONG
一把散發著冰冷寒氣的長戟
LONG
                );
        set("lv", 100);
        set("unit", "把");
        set("element", WATER);
        set("epower", 100);
        set("weight", 100);

        set("wear_type", HAND);
        set("weapon", LANCE);
        set("dem_eff", 6000);
        set("spe_eff", 100);
        set("eff", ({ "dem_eff", "spe_eff" }));
}
