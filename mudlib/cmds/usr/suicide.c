// suicide.c

void create()
{
    seteuid( getuid());
}

int main( object me)
{
    write("如果自殺了, 你就將永遠消失在這個世界上了...\n"
        "[1;31m<確定的話請輸入密碼>[m\n");
    input_to("get_passwd", 1, me);

    return 1;
}

void get_passwd( string pass, object me)
{
    string passwd;

    passwd= me->query("password");
    if( crypt( pass, passwd)!= passwd)
        write("密碼錯誤!\n");
    else {
        write("好吧... 永別了...\n");
        tell_room( environment( me), me->name()+ "自殺了, 你以後再也看不到這個人了.\n", ({ me }));
        rm( me->query_save_file()+ __SAVE_EXTENSION__);
        destruct( me);
    }
}

int help()
{
    write( @HELP
指令格式: suicide

自殺, 將會永遠離開這個世界
HELP
    );

    return 1;
}
