// dog.c
// written by zz

#include <class.h>
#include <element.h>
#include <race.h>

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
        int HP= 4000, MP= 0, SP= 1000;

        set_name("狗", ({ "dog"}));
        set("lv", 12);
        set("race","animal");
        set("gender","動物");
        set("describe","一隻狗.");
        set("pre","四處晃動的");
        set("age", 2);
        set("element",EARTH);
        set("mind", -50);

        set("maxHP", HP);
        set("currentHP", HP);
        set("maxMP", MP);
        set("currentMP", MP);
        set("maxSP", SP);
        set("currentSP", SP);
        set("dem", 120);
        set("def", 90);
        set("mdem", 90);
        set("mdef", 90);
        set("spe", 62);
        set("exp", 1200);

        set("chat_chance", 10);
        set("chat_msg", ({
                (: random_move :)
                }));
        setup();
}
