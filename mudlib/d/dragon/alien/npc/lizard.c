// lizard.c
// written by Mondale
// 蜥蜴

inherit NPC;

void create()
{
        int HP= 3000, MP= 0, SP= 100;

        set_name("蜥蜴", ({ "lizard" }));
        set("lv", 25);
        set("class","warrior");
        set("ra","蜥蜴");
        set("gender","男");
        set("describe","一隻向你張望著的變種蜥蜴.");
        set("pre","變種的");

        set("age", 1);
        set("element","earth");
        set("mind", 0);

        set("mixHP", HP);
        set("currentHP", HP);
        set("mixMP", MP);
        set("currentMP", MP);
        set("mixSP", SP);
        set("currentSP", SP);
        set("dem", 800);
        set("def", 700);
        set("mdem", 600);
        set("mdef", 300);
        set("spe", 150);
        set("realmx", 1);
        set("exp",  1000);

        set("chat_chance", 20);
        set("chat_msg", ({
                (: this_object(), "random_move" :),
                "\n蜥蜴懶懶的爬著\n"
                }));
        setup();
        setup();
}

void init()
{
        ::init();
        if( !previous_object()
                || !userp(previous_object()))
                return;
        
}

void action( object obj)
{
        setup();
}
