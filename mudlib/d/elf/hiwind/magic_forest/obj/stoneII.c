// stoneII.c
// written by hiwind

inherit ITEM;

void create()
{
        set_name("古老石碑", ({ "statue" }));
        set("long", @LONG
[1;37m��——————————————————————————╮
∣聖冥  說:                                           ∣
∣如果      所擊敗,那    將被毀滅,我精靈    不保,而上 ∣
∣天  了懲罰      的    與兇殘,所  在1000年後,天地將失∣
∣衡          ,然而上蒼並    要消滅我們,所以          ∣
∣一唯一的機會,將有        英雄降世,但由於擔心      魔∣
∣法失傳,    刻此碑.                                  ∣
∣<唯有  能生,當聖所  弱時,     能使重生.>            ∣
��——————————————————————————��[m
LONG
                );
        set("unsac", 1);
        set("no_get", 0);
        set("weight", 4000);
}
