// wind_.c
// written by hiwind

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
    int HP= 200000, MP= 50000 ,SP= 75000;

    set_name("��", ({ "wind" }));
    set("lv", 80);
    set("class", WIZARD);
    set("race", ELF);
    set("describe","[1;36m��[m��������.");
    set("age", 16);
    set("mind", 2000);
    set("gender", 1);

    set("maxHP", HP);
    set("currentHP", HP);
    set("maxMP", MP);
    set("currentMP", MP);
    set("maxSP", SP);
    set("currentSP", SP);
    set("strength", 8000);
    set("def", 4000);
    set("mdem", 30000);
    set("mdef", 8000);
    set("spi", 20);
    set("speed", 1000);
    set("exp", 10000);

    set("chat_chance", 20);
    set("chat_msg", ({
        "�������o�ĤF�@�f��.\n"
        }));
    setup();
}
void init()
{
    if( !previous_object()
        || !userp(previous_object()))
        return;
    call_out("action", 10, previous_object());
}

void action( object obj)
{
    if( !obj || !present(obj) || this_object()->is_fighting()) return;
    if( obj->is_fighting()) kill_ob(obj);

    command("say ��....�S������ƶ�?");
}
