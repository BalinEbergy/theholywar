// angry_sea.c
// written by hiwind

#include <skill.h>

int main( object me, object obj)
{
        int default_d= 2, demage;

        tell_object( obj, "你看見的敵人的神情中帶著[1;33m悲憤[m,突然間[1;31m魔法[m宛如[1;36m滔天巨浪[m向你襲來.\n");

        demage= default_d* ((1.5-(me->query("currentHP")/ me->query("mixHP")))
                * (me->query("mdem")+ me->query("mdem_eff")));

        obj->add("currentHP", -demage);

        return demage;
}

string name( int select)
{
        switch( select)
        {
                case 1: return "怒海狂濤";
                case 2: return "angry sea";
                case 3: return "angry_sea";          
        }
}

int type() { return WATER; }
int lv() { return 20; }
int attackful()
{
        return 1;
}

string mes( object me)
{
        return "你唸完咒語,心中的[1;31m怒意[m激盪而出,化為[1;36m滔天巨浪[m.\n";
}

int usesp() { return 0; }
int usemp() { return 250; }
int successful() { return 80; }
int delay() { return 2; }
int all() { return 1; }
string element() { return "water"; }

int check( object me)
{
        return 1;
}
