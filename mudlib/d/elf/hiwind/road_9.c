// road_9.c
// written by hiwind

#include <room.h>


inherit ROOM;

void create()
{
        set("short","[1m���L�D��[m");
        set("long",@LONG
�b���L�D���W,���۰ʪ��M�ȫ�,���֫_�I�̦b�o���D���W��
�ʵ�,�u�ۦ������N�O�i�h[1;36m��[m�����P�t�@�Ӵ˪L�F,�䤤�R��
�F�j�۵M����.�j�۵M���𮧤��_����¶�b�A����,�uı�o��
�~�κZ.�u�����B���@�y��,���U�N�O�y����[1;36m��[m�����դ�[1;34m�e[m,�@
���M�����p�˽w�w�V�n�ӥh.
�F��N�O�t�@�Ӵ˪L.
LONG
        );

        set("exits",
        ([
        "east" : __DIR__ + "forest/forest_2",
        "west" : __DIR__ + "road_7",
        "south" : __DIR__ + "river/river_12",
        "north" : __DIR__ + "river/river_13",
        ])
        );
}
