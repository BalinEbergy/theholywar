// travelling_1.c
// written by hiwind

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
    int HP= 15, SP= 50;

    set_name("����", ({ "lv1" }));
    set("lv", 1);
    set("class", WIZARD);
    set("race", ELF);
    set("describe","����.");
    set("age", 25);
    set("element", FIRE);
    set("mind", 500);

    set("maxHP", HP);
    set("currentHP", HP);
    set("maxSP", SP);
    set("currentSP", SP);
    set("dem", 10);
    set("def", 4);
    set("spirit", 14);
    set("spe", 6);
    set("exp", 25350);
    setup();
}
