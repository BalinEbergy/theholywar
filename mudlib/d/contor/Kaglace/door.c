// sdoor.c
// written by Bother

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;33m卡格烈斯城門口[m");
        set("long", @LONG
這裡就是古文化的遺跡－卡格烈斯城,傳說這裡曾經有過高度
的文明,但如經這裡和一般的城鎮比起來已經沒啥兩樣了,不
同的是在門上雕琢著傳說中的卡格大帝(Kag)統領時的盛世,
讓人不得肅然起敬.
LONG
        );

        set("exits", ([
                "north" : __DIR__ + "city_4",
                "south" : __DIR__ + "city_1",
                ]));

        set("no_fight", 1);
        setup();
}
