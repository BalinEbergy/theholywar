// lv1sword.c
// written by Hudson

#include <equip.h>
#include <element.h>

inherit EQUIP;

void create()
{
    set_name("見習小劍", ({ "novice sword" }));
    set("long", @LONG
一把青銅打製成的短劍.
LONG
        );
    set("lv", 1);
    set("unit", "把");
    set("element", COSMOS);
    set("value", 5);

    set("wear_type", HAND);
    set("weapon", SWORD);
    set("damage", 40);
    set("def_eff", 20);
    set("hit_eff", 300);
    set("eff", ({ "hit_eff", "def_eff" }));

    set_weight( 30);
}
