//tree_38.c
//write by hiwind

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;31m��[m[1;37m��[m[1;31m��[m[1;32m�L[m[1;37m�J�f[m");
        set("long",@LONG
�ܤ������A�O�����i�L�Ѫ��H,����ť�󤧫e�g�]�N�h��ĵ�i,
�i�J�F���a,���O�A�ݤF�ݥ|�P,�o�o�{�|�P���������`�޲�,��
�T�۫H�O�H�һ�����.
���B��[1;31m��[m���Ӭ��F,²������@��,�ө_�ǤF,�s���e���o�ܴr��
���p�ʪ��������F,�٬O�p�ߤ@��.
LONG
        );

        set("exits",
        ([
        "south" : __DIR__ + "tree_37",
        "north" : __DIR__ + "tree_39",
        ])
        );

        set("objects",
        ([
                __DIR__ + "npc/hiwind_1":1,
                __DIR__ + "npc/hiwind_6":1,
        ])
        );

        set("no_fight", 0);
        setup();
}
