// cloud.c
// written by Hudson

#include <equip.h>
#include <element.h>

inherit EQUIP;

void create()
{
    set_name("天界之雲", ({ "sky cloud", "sky", "cloud" }));
    set("long", @LONG
一朵飄散著霧氣的雲.
LONG
        );
    set("lv", 100);
    set("unit", "朵");
    set("element", WATER);
    set("epower", 100);

    set("wear_type", BOOT);
    set("def_eff", 1235);
    set("spe_eff", 500);
    set("eff", ({ "def_eff", "spe_eff" }));

    set_weight( 1);
}
