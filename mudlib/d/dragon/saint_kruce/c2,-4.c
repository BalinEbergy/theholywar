//c2,-4.c
//written by zz

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;37m騎士隊總部營區[m");
        set("long",@LONG
這裡是皇家騎士隊總部的營區.皇家騎士隊是一支強大的軍隊,
每一個騎士都勇猛善戰.所有騎士都是從這個總部訓練出來的,
你看見四周都是正在接受訓練的見習騎士,看他們有模有樣的,
自己也開始手養了,趕快找個老師學習吧！
LONG
        );
        set("exits", ([
                "north" : __DIR__ + "c2,-3.c" ,
                "west" : __DIR__ + "c1,-4.c" ,
                ]));
        set("objects", ([
                __DIR__ + "npc/bag":1
                ]));

        setup();
}
