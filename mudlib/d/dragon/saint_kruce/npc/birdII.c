// birdII.c
// written by zz

#include <class.h>
#include <element.h>
#include <race.h>

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
        int HP= 1300, MP= 200, SP= 10000;

        set_name("小鳥", ({ "bird"}));
        set("lv", 6);
        set("race","animal");
        set("gender","動物");
        set("describe","一隻小鳥.");
        set("pre","停在樹枝上的");
        set("age", 4);
        set("element",WIND);
        set("mind", 10);

        set("maxHP", HP);
        set("currentHP", HP);
        set("maxMP", MP);
        set("currentMP", MP);
        set("maxSP", SP);
        set("currentSP", SP);
        set("dem", 55);
        set("def", 35);
        set("mdem", 13);
        set("mdef", 13);
        set("spe", 60);
        set("exp", 950);

        set("chat_chance", 6);
        set("chat_msg", ({
                "小鳥高聲的吟唱著美麗的旋律.\n"
                }));
        setup();
}
