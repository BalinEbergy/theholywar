// train.c

void create() { seteuid( getuid()); }

void list_skill( object me)
{
    int i, j, lv, tmp;
    string *skill_type, *skill;
    string str, sk_tmp;

    str= sprintf("[1;37;44m%-80s[m\n\n", "◎ 你現在所會的技能 ◎");
    skill_type= me->query_skill_list();
    i= sizeof( skill_type);
    while( i--)
    {
        str+= sprintf("[1;33m%-46s[m LV %3d EXP %d%%\n",
            D_SKILL->skill_title( skill_type[i]),
            lv= me->query_skill_level( skill_type[i]),
            ( tmp= me->query_skill_exp( skill_type[i])/ lv/ lv)> 100? 100: tmp);
        skill= me->query_skills( skill_type[i]);
        for( j= 0; j< sizeof( skill); j++)
        {
            sk_tmp= D_SKILL->get_skill( skill[j]);
            str+= sprintf("%-23s %-22s LV %3d\n",
                sk_tmp->name(), sk_tmp->query_id(), sk_tmp->query("lv"));
        }
        str+= "\n";
    }

    write( str);
}

int main( object me, int argc, string *argv)
{
    int i;
    object *objs;

    if( argc< 2)
        list_skill( me);
    else {
        objs= all_inventory( environment( me));
        i= sizeof( objs);
        while( i--)
            if( objs[i]->is_teacher())
                break;

        if( i== -1)
            write("這裡沒有老師喔!\n");
        else if( argv[1]== "list")
        {
            objs[i]->command("tell "+ me->query("id")+ " "+ objs[i]->query("teach_message"));
            objs[i]->list_skill();
        } else objs[i]->teach( me, argv[1]);
    }

    return 1;
}

int help( object me)
{
    write( @HELP
指令格式: train [技能 | list]

可向老師學習技能, 如使用 list 可列出老師所會教授的技能.
直接打 train 可得知自己的技能狀況.
HELP
        );

    return 1;
}
