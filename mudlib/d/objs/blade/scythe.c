// scythe.c
// written by hiwind
// write for what
// modified by Hudson

#include <equip.h>
#include <element.h>

inherit EQUIP;

void create()
{
        set_name("[1;7m�I�M[m", ({ "scythe" }));
        set("long", @LONG
�@��A�ҩҨϥΪ��I�M
LONG
                );
        set("lv", 1);
        set("unit", "��");
        set("element", HOLY);
        set("epower", 100);
        set("weight", 50);

        set("wear_type", HAND);
        set("weapon", BLADE);
        set("damage", 200);
        set("def_eff", 60);
        set("hit_eff", 200);
        set("eff", ({ "def_eff", "hit_eff" }));
}
