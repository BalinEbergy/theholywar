// huge_axe.c
// written by Hudson

#include <equip.h>
#include <element.h>

inherit EQUIP;

void create()
{
    set_name("巨斧", ({ "huge axe", "huge", "axe" }));
    set("long", @LONG
一把極為巨大的斧頭.
LONG
        );
    set("lv", 43);
    set("unit", "把");
    set("element", EARTH);
    set("value", 100);

    set("wear_type", HAND);
    set("weapon", AXE);
    set("damage", 2000);
    set("spe_eff", -27);
    set("hit_eff", 300);
    set("eff", ({ "spe_eff", "hit_eff" }));

    set_weight( 300);
}
