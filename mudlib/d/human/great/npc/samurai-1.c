// samurai-1.c
// written by koyamakyo
// �ӫ��ͽ�

inherit NPC;

void create()
{
        int HP= 500000, MP= 1000, SP= 20000;

        set_name("�ӫ��ͽ�", ({ "samurai" }));
        set("lv", 80);
        set("class","samurai");
        set("ra","human");
        set("gender","�k");
        set("describe","�J�Ө����|�P���ͽ�");
        set("pre","�����U�Q��");

        set("age", 30);
        set("element","wind");
        set("mind", 800);

        set("mixHP", HP);
        set("currentHP", HP);
        set("mixMP", MP);
        set("currentMP", MP);
        set("mixSP", SP);
        set("currentSP", SP);
        set("dem", 30000);
        set("def", 20000);
        set("mdem", 10000);
        set("mdef", 15000);
        set("spe", 1500);
        set("realmx", 20);
        set("exp", 30000);

        setup();
}



