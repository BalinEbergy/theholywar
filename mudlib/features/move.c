// move.c

static int weight= 0;
static int encumb= 0;

static int carried_object= 0;

int query_carried_object() { return carried_object; }
int query_max_carried_object() { return this_object()->query("dex")+ 10; }
int add_carried_object( int c)
{
    carried_object+= c;
    if( carried_object> query_max_carried_object())
    {
        carried_object--;
        this_object()->over_carry();
        return 1;
    }

    return 0;
}

void over_carry()
{
    if( !interactive( this_object())) return;
    tell_object( this_object(), "[1;37m你身上的東西太多囉![m\n");
}

int query_encumbrance() { return encumb; }
int over_encumbranced() { return encumb> this_object()->query("strength"); }
int query_max_encumbrance() { return this_object()->query("strength"); }
void add_encumbrance( int w)
{
    encumb+= w;
    if( encumb> query_max_encumbrance())
        this_object()->over_encumbrance();
    if( environment())
        environment()->add_encumbrance( w);
}

void over_encumbrance()
{
    if( !interactive( this_object())) return;
    tell_object( this_object(), "[1;37m你的負荷過重了![m\n");
}

int query_weight() { return weight; }
void set_weight( int w)
{
    if( !environment())
    {
        weight= w;
        return;
    } else if( w!= weight)
        environment()->add_encumbrance( w- weight);

    weight= w;
}

int weight() { return weight+ encumb; }

int move( mixed goal)
{
    object env, ob;

    if( this_object()->equipped())
        return 1;                   // This object is equipped

    if( objectp( goal))
        ob= goal;
    else if( stringp( goal))
    {
        call_other( goal, "???");
        ob= find_object( goal);
        if( !ob) return -1;         // Goal is unavailable
    } else return -2;               // Goal has wrong type

    if( weight()> ob->query_max_encumbrance())
        return 2;                   // Goal will be overweight
    if( ob->add_carried_object( 1))
        return 3;                   // Goal is in over carry


    if( env= environment())
    {
        env->add_encumbrance( -weight());
        env->add_carried_object( -1);
    }
    move_object( ob);
    ob->add_encumbrance( weight());

    if( userp( this_object()))
        this_object()->command("look");

    return 0;
}

void remove( string euid)
{
    if( !previous_object() ||
        base_name( previous_object())!= O_SIMUL_EFUN)
        error("move: remove() can only be called by destruct() simul efun.\n");

    if( this_object()->equipped())
        this_object()->unequip( 1);

    if( environment())
    {
        environment()->add_encumbrance( -weight);
        environment()->add_carried_object( -1);
    }
}

int move_or_destruct( object dest)
{
    if( userp( this_object()))
    {
        tell_object( this_object(), "一陣時空的扭曲將你傳送到另一個地方....\n");
        move( O_VOID);
    }
}
