// save.c

int main( object me, int argc, string *argv)
{
    if( me->query("lv")== 1 && D_WIZARD->wiz_level( me)< 3)
        write("等級 1 無法儲存喔~ 快去升級吧!\n");
    else if( me->save()) write("儲存完畢.\n");
    else write("資料儲存失敗!!\n");

    return 1;
}

int help()
{
    write( @HELP
指令格式: save

儲存人物檔案
HELP
    );
 
    return 1;
}
