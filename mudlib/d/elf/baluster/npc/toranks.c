// toranks.c
// written by Hudson
// 多倫克斯•福魯克

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
    int HP= 340000, MP= 56000, SP= 5000;

    set_name("多倫克斯•福魯克", ({ "toranks frook" }));
    set("lv", 90);
    set("class", WIZARD);
    set("race", ELF);
    set("gender",1);
    set("describe","一位年長而又和藹的老者.");
    set("pre","精靈族大祭司長");

    set("age", 562);
    set("element", COSMOS);
    set("mind", 922);

    set("maxHP", HP);
    set("currentHP", HP);
    set("maxMP", MP);
    set("currentMP", MP);
    set("maxSP", SP);
    set("currentSP", SP);
    set("strength", 17000);
    set("armor", 31000);
    set("magic", 49000);
    set("spi", 38);
    set("speed", 2800);
    set("exp", 9000);

    setup();

    carry_object( OB_ROBE+ "air_robe")->equip();
}

void init()
{
    object obj;

    if( !(obj= previous_object())
        || !userp( obj))
        return;

    call_out("ki", 10, obj);
    call_out("action", 20, obj);
}

void action( object obj)
{
    if( !obj || !present(obj) || this_object()->is_fighting()) return;
    command("say 有什麼需要我們幫忙的嗎?");
}

void ki( object obj)
{
    if( obj->query("attacker")) kill_ob( obj);
    if( present( obj->query("id"), environment( this_object())))
        call_out("ki", 10, obj);
}

int rebirth( object obj)
{
    if( query("delay")> time())
    {
        command("tell "+ obj->query("id")+
            " 我正在忙著呢...稍等一下呵...");
        return 1;
    }

    if( !obj->query("ghost"))
    {
        if( obj->query("age")< this_object()->query("age"))
        command("tell "+ obj->query("id")+ 
            " 別說笑了...小傢伙,你還沒死呢...");
        else command("tell "+ obj->query("id")+
            " 呵呵,你還沒死呢!");
        return 1;
    }

    command("tell "+ obj->query("id")+ " "+ obj->name()+
        "呀... 你還想回到這個世界上嗎? ...");
    call_out("do_rebirth", 10, obj);

    set("delay", time()+ 10);
    environment( this_object())->set("unmoveable", 1);

    return 1;
}

void do_rebirth( object obj)
{
    command("tell "+ obj->query("id")+
        " 好吧, 給我一些經驗, 幫你找回你的屍體吧...");
    command("cast life to "+ obj->query("id"));
    command("tell "+ obj->query("id")+ " 你現在已經重生了!");

    delete("delay");
    environment( obj)->delete("unmoveable");

    return;
}

int death_msg( object obj)
{
    environment( obj)->delete("unmoveable");
}
