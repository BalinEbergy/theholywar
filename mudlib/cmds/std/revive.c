// revive.c

int main( object me, int argc, string *argv)
{
    me->revive();
    write("你復活了!\n");

    return 1;
}

int help( object me)
{
    write( @HELP
指令格式: revive

目前為無條件復活.
HELP
        );

    return 1;
}
