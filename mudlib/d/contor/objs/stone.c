// stone.c
// written by Bother

inherit ITEM;

void create()
{

        set_name("石頭", ({ "stone" }));
        set("long", @LONG
一個很重的石頭..
LONG
                );
        set("unit", "個");
        set("lv", 0);
        set("value", 20);
        set("weight", 10000);
}
