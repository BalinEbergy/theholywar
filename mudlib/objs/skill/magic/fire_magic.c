// fire_magic.c
// written by Hudson

#include <skill.h>

inherit MAGIC;

void setup()
{
    set("element", FIRE);
    set("successful", 80);
    set("target_number", 1);
    set("volatility", 10);
}

string type() { return "fire"; }
