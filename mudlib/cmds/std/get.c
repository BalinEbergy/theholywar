// get.c

int main( object me, int argc, string *argv)
{
    int i;
    object env, ob, *objs;
    string str= "", position;

    if( argc< 2)
        write("稱璶具癬ぐ或狥﹁㎡?\n");
    else if( argv[1]== "all")
    {
        objs= all_inventory( environment( me));

        i= sizeof( objs);
        while( i--)
        {
            if( objs[i]->query("no_get") || objs[i]->is_character())
                continue;

            if( !objs[i]->move( me))
            {
                if( argc== 2) position= "";
                str+= "$N眖"+ position+ "具癬"+ objs[i]->name()+ ".\n";
            }
        }
        message_vision( str, me);
    } else {
        env= environment( me);
        ob= find( argv[1], env);

        if( !objectp( ob) || ob->is_character())
            write("⊿Τ硂妓狥﹁.\n");
        else if( ob->query("no_get"))
            write("硂妓狥﹁ぃ砆具癬.\n");
        else if( !ob->move( me))
        {
            if( argc== 2) position= "";
            message_vision("$N眖"+ position+ "具癬"+ ob->name()+ ".\n", me);
        } else write("礚猭具癬硂妓狥﹁.\n");
    }

    return 1;
}

int help( object me)
{
    write( @HELP
Α: get <珇|all> [ㄓ方]

硂琵具癬珇┪眖ㄓ方い珇.
HELP
        );
    return 1;
}
