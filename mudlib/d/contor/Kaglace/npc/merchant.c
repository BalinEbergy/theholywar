// merchant.c
// written by Bother

inherit NPC;

void create()
{
        int HP= 150, SP= 20;

        set_name("商人", ({ "merchant" }));
        set("lv", 1);
        set("class","warrior");
        set("ra","human");
        set("gender","男");
        set("describe","一個買賣東西的商人");
        set("pre", "背負很多東西");

        set("age", 25);
        set("element", "fire");
        set("mind", 45);

        set("mixHP", HP);
        set("currentHP", HP);
        set("mixSP", SP);
        set("currentSP", SP);
        set("dem", 30);
        set("def", 15);
        set("mdem", 1);
        set("mdef", 1);
        set("spirit", 24);
        set("spe", 12);
        set("exp", 100);

        set("trading", 10);
        set("buy_mind", "-1~21");

        set("selling", ({
                OB_ROBE + "air_robe.c",
                "/d/contor/objs/water_skin.c",
                "/d/contor/objs/stone.c",
                }));
        setup();
}
