// contor.c
// written by hiwind

inherit NPC;

void create()
{
        int HP= 1000000, MP= 8000 ,SP= 80000;

        set_name("終極之光", ({ "contor" }));
        set("lv", 100);
        set("class","mage");
        set("ra","elf");
        set("describe", "全身散發強大魔力的法師.");
        set("age", 100);
        set("mind", 1000);
        set("element", "holy");
        set("gender", "男");

        set("mixHP", HP);
        set("currentHP", HP);
        set("mixMP", MP);
        set("currentMP", MP);
        set("mixSP", SP);
        set("currentSP", SP);
        set("str", 10);
        set("dem", 5000);
        set("vir", 20);
        set("def", 15000);
        set("magic", 70);
        set("mdem", 65000);
        set("mdef", 25000);
        set("spirit", 30);
        set("speed", 20);
        set("spe", 2000);
        set("realmx", 150);
        set("exp", 0);

        set_skill_level( 3, 100);
        set("skill_change", 100);
        set("skill", ({
                "3:light missile", "3:advent of kaigic", 
                }));
        setup();
}
void init()
{
        if( !previous_object()
                || !userp(previous_object()))
                return;
        call_out("action", 1, previous_object());
}

void action( object obj)
{
        if( !obj || !present(obj) || this_object()->is_fighting()) return;
        if( obj->is_fighting()) kill_ob(obj);

        command("hihi 見識神之法術吧....");
}
