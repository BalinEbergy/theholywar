// eillin.c
// written by Hudson
// 雅爾琳

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
    int HP= 200000, MP= 46000, SP= 3700;

    set_name("雅爾琳", ({ "eillin" }));
    set("lv", 88);
    set("class",WIZARD);
    set("race",HUMAN);
    set("gender",0);
    set("describe","一位面色溫柔的女法師.");
    set("pre","光明法師");

    set("age", 112);
    set("element",LIGHT);
    set("mind", 799);

    set("teacher", ({
        3,
        }));
    set("favorite", ({
        WIZARD,
        }));
    set("unteachable-3", ({ 50, }));
    set("teach_mind", "700 ~ 1000");
    set("teach_msg_evil", "[1;35m雅爾琳說道:[33m 你已經墮入了罪惡的深淵了...快些回頭吧![m");
    set("lv_up", "[1;35m雅爾琳說道:[33m 你的魔法等級又進了一級了![m\n");
    set("no_enu_money", "[1;35m雅爾琳說道:[33m 哎呀,你帶的錢不大夠喔~[m");
    set("no_enu_lv", "[1;35m雅爾琳說道:[33m 你的實力還不夠喔![m");
    set("top_lv", "[1;35m雅爾琳說道:[33m 恭喜恭喜! 你的能力已經可以獨當一面了![m");
    set("teach_message", "[1;35m雅爾琳說道:[33m 你要學魔法嗎? 想學什麼呢?[m");

    set("maxHP", HP);
    set("currentHP", HP);
    set("maxMP", MP);
    set("currentMP", MP);
    set("maxSP", SP);
    set("currentSP", SP);
    set("strength", 7000);
    set("armor", 22000);
    set("magic", 31000);
    set("spi", 24);
    set("speed", 2652);
    set("exp", 247000);

    setup();
}

void init()
{
    object obj;

    if( !(obj= previous_object())
        || !userp( obj))
        return;

    call_out("ki", 10, obj);
    call_out("action", 20, obj);
}

void action( object obj)
{
    if( !obj || !present(obj) || this_object()->is_fighting()) return;
    command("say 只要有希望,世界就是一片光明,要好好記住這一點...");
}

void ki( object obj)
{
    if( obj->is_fighting()) kill_ob( obj);
}

int teach_check( object me)
{
    if( me->query("neckr")== "mage permit");
    else if( me->query("neckl")== "mage permit");
    else
    {
        write("[1;35m雅爾琳說道:[33m 你還未成為真正的魔法師, 很抱歉我不能教你任何東西...[m\n");
        return 1;
    }

    return 0;
}
