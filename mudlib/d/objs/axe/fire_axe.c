// fire_axe.c
// written by Hudson

#include <equip.h>
#include <element.h>

inherit EQUIP;

void create()
{
    set_name("炎神巨斧", ({ "firen axe", "firen", "axe" }));
    set("long", @LONG
一把散發著炙熱炎氣的巨斧, 上面刻著一行字:

    炎神.亞伯吉斯之斧
LONG
        );
    set("lv", 100);
    set("unit", "把");
    set("element", FIRE);
    set("epower", 100);

    set("wear_type", HAND);
    set("weapon", AXE);
    set("dem_eff", 60000);
    set("spe_eff", -100);
    set("eff", ({ "dem_eff", "spe_eff" }));

    set_weight( 870);
}
