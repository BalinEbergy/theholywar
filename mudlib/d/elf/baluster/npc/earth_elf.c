// earth_elf
// written by Hudson

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
    int HP= 1000, MP= 130, SP= 1900;

    set_name("地小精靈", ({ "earth elf" }));
    set("lv", 10);
    set("class", WARRIOR);
    set("race", ELF);
    set("gender", 1);
    set("describe", "一個小精靈.");

    set("age", 25);
    set("element", EARTH);
    set("mind", 100);

    set("maxHP", HP);
    set("currentHP", HP);
    set("maxMP", MP);
    set("currentMP", MP);
    set("maxSP", SP);
    set("currentSP", SP);
    set("strength", 330);
    set("armor", 247);
    set("spi", 5);
    set("speed", 130);
    set("exp", 1500);

    set("chat_chance", 12);
    set("chat_msg", ({
        "土精哈哈的笑著\n",
        (: random_move :)
        }));

    setup();
}

void init()
{
    object obj;

    if( !( obj= previous_object()) || !userp( obj))
        return;

    call_out("ki", 10, obj);
}

void ki( object obj)
{
    if( obj->is_fighting()) kill_ob( obj);
}
