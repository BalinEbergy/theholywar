// seground_1.c
// written by Bother

#include <room.h>


inherit ROOM;

void create()
{
        set("short","[1;37m卡格烈斯廣場[m");
        set("long",@LONG
這裡是卡格烈斯城的廣場,西北方向前進就是廣場的正中心,代
表著這個城市的象徵,四周充滿著歡愉的嘻鬧聲,幾個孩子在廣
場玩捉迷藏,你是不是有股衝動想去加入他們阿.
LONG
        );

        set("exits",
        ([
        "north" :__DIR__ + "eground_1",
        "west" :__DIR__ + "sground_1",
        ])
        );

        set("objects",  ([
                __DIR__ + "npc/boy_kk":1,
                ]));
        setup();
}
