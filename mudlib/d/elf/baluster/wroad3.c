// wroad3.c
// written by Hudson

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;32m�ڸ����S��D[m");
        set("long",@LONG
�o��j���O�ڸ����S�ө��Ϥ��ߪ�����,�_�䦳���㩱,�n��
�����J,�Ȧ�,�Z������,���䦳�@�ɨu�����]�k�D���,�F��
�h���D�㩱,�ӷ~���|.
LONG
        );

        set("exits",
        ([
        "north" : __DIR__ + "nroad8",
        "east"  : __DIR__ + "wroad2",
        ])
        );

        set("objects", ([
                __DIR__ + "npc/elf_citizen":1,
                __DIR__ + "npc/m_master":1,
                ]));

        setup();
}
