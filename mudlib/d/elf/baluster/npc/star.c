// star.c
// written by Hudson
// 史達

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
    int HP= 380000, MP= 26000, SP= 1000;

    set_name("史達", ({ "star" }));
    set("lv", 70);
    set("class", WIZARD);
    set("race", ELF);
    set("gender",1);
    set("describe","一位神情嚴肅的中年人.");
    set("pre","魔法公會副會長");

    set("age", 262);
    set("element", FIRE);
    set("mind", 301);

    set("maxHP", HP);
    set("currentHP", HP);
    set("maxMP", MP);
    set("currentMP", MP);
    set("maxSP", SP);
    set("currentSP", SP);
    set("strength", 12000);
    set("armor", 13500);
    set("magic", 20000);
    set("api", 21);
    set("speed", 1570);
    set("exp", 5000);

    setup();
}

void init()
{
    if( !previous_object()
        || !userp(previous_object()))
        return;
    call_out("action", 10, previous_object());
}

void action( object obj)
{
    if( !obj || !present(obj) || this_object()->is_fighting()) return;
    if( obj->is_fighting())
    {
        kill_ob(obj);
        return;
    }

    command("say 歡迎你來到魔法師公會~");
}
