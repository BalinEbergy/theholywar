// water_elf
// written by Hudson

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
    int HP= 640, MP= 1000, SP= 1200;

    set_name("水小精靈", ({ "water elf" }));
    set("lv", 7);
    set("class", WIZARD);
    set("race", ELF);
    set("gender", 0);
    set("describe","一個小精靈.");

    set("age", 19);
    set("element", WATER);
    set("mind", 155);

    set("maxHP", HP);
    set("currentHP", HP);
    set("maxMP", MP);
    set("currentMP", MP);
    set("maxSP", SP);
    set("currentSP", SP);
    set("strength", 230);
    set("armor", 100);
    set("magic", 260);
    set("spi", 10);
    set("speed", 130);
    set("exp", 650);

    set("chat_chance", 10);
    set("chat_msg", ({
        "水精握著雙手不知道在做什麼\n",
        (: random_move :)
        }));

    setup();
}

void init()
{
    object obj;

    if( !( obj= previous_object()) || !userp( obj))
        return;

    call_out("ki", 10, obj);
}

void ki( object obj)
{
    if( obj->is_fighting()) kill_ob( obj);
}
