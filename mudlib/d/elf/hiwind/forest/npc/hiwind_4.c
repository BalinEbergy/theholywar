// hiwind_4.c
// written by hiwind

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
    int HP= 500000, MP= 100000 ,SP= 100000;

    set_name("[1;32mù�t��[m", ({ "templar" }));
    set("lv", 100);
    set("class",WARRIOR);
    set("race",HUMAN);
    set("describe","���c���H.");
    set("age", 16);
    set("mind", -750);
    set("gender", 1);

    set("maxHP", HP);
    set("currentHP", HP);
    set("maxMP", MP);
    set("currentMP", MP);
    set("maxSP", SP);
    set("currentSP", SP);
    set("dem", 25000);
    set("def", 22500);
    set("mdem", 12500);
    set("mdef", 7500);
    set("spirit", 10);
    set("spe", 1750);
    set("exp", 0);

    set("chat_chance", 5);
    set("chat_msg", ({
        "ù�t�Ѻôb���ݵۧA.\n"
        }));

    setup();
}

void init()
{
    if( !previous_object()
        || !userp(previous_object()))
        return;
    call_out("action", 1, previous_object());
    kill_ob( previous_object());
}

void action( object obj)
{
    if( !obj || !present(obj) || this_object()->is_fighting()) return;
    command("say �ӧr!�ӧr!�O�H�����ѴN�O�ڪ��ּ�!��!��!��!");
    setup();
}
