// light_ball.c
// written by Hudson

#include <equip.h>
#include <element.h>

inherit EQUIP;

void create()
{
    set_name("光球", ({ "light ball", "light", "ball" }));
    set("long", @LONG
一顆光明耀眼的溫暖光球.
LONG
        );
    set("lv", 1);
    set("unit", "顆");
    set("element", LIGHT);
    set("epower", 1);
    set("no_save", 1);

    set("wear_type", "eq_light");

    set_weight( 1);
}

void disappear()
{
    object env;

    env= environment( this_object());
    while( objectp( environment( env)))
        env= environment( env);

    tell_room( env, "光球消失了!\n");

    destruct( this_object());
}
