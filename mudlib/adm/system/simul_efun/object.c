// object.c

varargs object find( mixed str, object ob)
{
    int i, j, length;
    object *inv, res;
    string id;

    if( !ob) return efun::present( str);
    if( objectp( str))
        return efun::present( str, ob);

    length= sizeof( str);
    i= sizeof( inv= all_inventory( ob));
    while( i--)
    {
        id= inv[i]->query("id");
        for( j= 0; j< length; j++)
            if( id[j]!= str[j]) break;
        if( j== length)
            res= inv[i];
    }

    return res;
}

void destruct( object ob)
{
    if( ob)
    {
        ob->de_group();

        if( previous_object())
            ob->remove( geteuid( previous_object()));
        else ob->remove(0);
    }

    efun::destruct( ob);
}
