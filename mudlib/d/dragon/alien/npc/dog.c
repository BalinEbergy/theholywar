// dog.c
// written by Mondale
// 小狗

inherit NPC;

void create()
{
        int HP= 750, MP= 0, SP= 100;

        set_name("小狗", ({ "dog" }));
        set("lv", 5);
        set("class","warrior");
        set("ra","狗");
        set("gender","男");
        set("describe","一隻看起沒有什麼毛的小狗.");
        set("pre","一隻可愛的");

        set("age", 1);
        set("element","earth");
        set("mind", 0);

        set("mixHP", HP);
        set("currentHP", HP);
        set("mixMP", MP);
        set("currentMP", MP);
        set("mixSP", SP);
        set("currentSP", SP);
        set("dem", 70);
        set("def", 70);
        set("mdem", 70);
        set("mdef", 70);
        set("spe", 25);
        set("realmx", 1);
        set("exp", 210);

        set("chat_chance", 20);
        set("chat_msg", ({
                (: this_object(), "random_move" :),
                "\n小狗汪汪的叫著\n"
                }));
        setup();
        setup();
}

void init()
{
        ::init();
        if( !previous_object()
                || !userp(previous_object()))
                return;
        
}

void action( object obj)
{
        setup();
}
