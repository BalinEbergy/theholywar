// center.c
// written by Hudson

#include <room.h>

inherit ROOM;

void create()
{
        set("short","*[1;3.........*[m");
        set("long",@LONG
......
LONG
        );

        set("exits",
        ([
        "north" : __DIR__ + "testn4e1.c",
        "east" : __DIR__ + "testn3e2.c" 
       ])
        );
}
