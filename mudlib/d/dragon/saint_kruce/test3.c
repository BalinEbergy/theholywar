//test3.c
//written by zz

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;37mtest3[m");
        set("long",@LONG
test Lv31-40
LONG
        );
        set("exits", ([
                "north" : __DIR__ + "test2.c" ,
                "south" : __DIR__ + "test4.c" ,
                ]));
        set("objects",  ([
                __DIR__ + "npc/test/lv31" :1 ,
                __DIR__ + "npc/test/lv32" :1 ,
                __DIR__ + "npc/test/lv33" :1 ,
                __DIR__ + "npc/test/lv34" :1 ,
                __DIR__ + "npc/test/lv35" :1 ,
                __DIR__ + "npc/test/lv36" :1 ,
                __DIR__ + "npc/test/lv37" :1 ,
                __DIR__ + "npc/test/lv38" :1 ,
                __DIR__ + "npc/test/lv39" :1 ,
                __DIR__ + "npc/test/lv40" :1 ,
        ]));
        set("no_recall", 1);
        setup();
}
