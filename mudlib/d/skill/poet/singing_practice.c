// singing_practice.c
// written by Bother

#include <skill.h>

int main( object me, object obj)
{
        message_vision("$N發出音樂振奮人心.", me);
        me->add_skill_exp( me->query("spirit")* me->query("lv")/
                me->skill_level( BARD)); 
        me->add_skill_exp( HARP, me->query("spirit")* me->query("lv")/
               me->skill_level( SONG));
        return 1;
}

string name( int select)
{
        switch( select)
        {
                case 1: return "歌唱練習";
                case 2: return "singing practice";
                case 3: return "practice";
        }
}

int lv() { return 5; }
int attackful()
{
        return 0;
}

string mes( object me)
{
        return "唱著美妙的音樂,感動著大眾.";
}

int type() { return BARD; }
int usesp() { return 10; }
int usemp() { return 0; }
int successful() { return 100; }
int delay() { return 1; }
int all() { return 0; }
int check( object me)
{
        return 1;
}
