// dean.c
// written by Hudson

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
    int HP= 70000, MP= 8000, SP= 800;

    set_name("迪恩", ({ "dean" }));
    set("lv", 47);
    set("class",WIZARD);
    set("race",DRAGON);
    set("gender",1);
    set("describe","凱撒武器巴路斯特分店的老闆.");
    set("pre", "凱撒武器店老闆");

    set("age", 233);
    set("element", COSMOS);
    set("mind", 10);

    set("maxHP", HP);
    set("currentHP", HP);
    set("maxSP", MP);
    set("currentSP", MP);
    set("maxSP", SP);
    set("currentSP", SP);
    set("strength", 5000);
    set("armor", 4000);
    set("magic", 12000);
    set("spi", 10);
    set("speed", 690);
    set("exp", 2300);

    set("trading", 301);
    set("buy_mind", "-1000~1000");
    set("selling", ({
        OB + "wand/giant_wand.c",
        }));

    setup();
}
