// command.h

#ifndef __COMMAND__
#define __COMMAND__

#define WIZ_DIR         "/cmds/wiz/"
#define USR_DIR         "/cmds/usr/"
#define STD_DIR         "/cmds/std/"

#define CMD_GROUP       "/cmds/std/group"
#define CMD_SCORE       "/cmds/usr/score"
#define CMD_UPTIME      "/cmds/usr/uptime"

#endif
