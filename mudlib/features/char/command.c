// command.c

static int delay;
static string *delay_queue= ({ });

varargs int command( string arg)
{
    if( delay)
    {
        if( sizeof( delay_queue)< 10 && arg)
            delay_queue+= ({ arg });
    } else if( sizeof( delay_queue))
    {
        if( !D_COMMAND->exec_command( delay_queue[0], this_object()))
            tell_object( this_object(), "沒有這個指令. 請輸入 cmds 查詢.\n");
        if( sizeof( delay_queue)> 1)
            delay_queue= delay_queue[ 1.. sizeof( delay_queue)- 1];
        else delay_queue= ({ });
        command();
    } else if( arg)
        return D_COMMAND->exec_command( arg, this_object());

    return 1;
}

void start_delay( int sec)
{
    delay= 1;
    call_out("remove_delay", sec);
}

void remove_delay()
{
    delay= 0;
    command();
}
int is_delaying() { return delay; }
