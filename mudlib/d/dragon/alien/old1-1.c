// old1-1.c
// written by Mondale

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;37m練金術士公會舊址[m");
        set("long",@LONG
這裡是亞利恩城的鍊金術士公會舊址，這裡據說在十年
前，曾經因為一次的重大事故而廢棄。在那次的事件過
後，這裡已變成許多怪物棲息的地方了。進去一定要小
心，不然可能會有生命危險。
LONG
        );

        set("exits",
        ([
        "south" : __DIR__ + "old2-1",
        "east" : __DIR__ + "old1-2",
        ])
        );

        set("objects",  ([
                __DIR__ + "npc/lizard.c":1,
                ]));
        setup();
}
