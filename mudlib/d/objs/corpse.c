// corpse.c
// written by Hudson

inherit FOOD;

void create()
{
    set_name("����", ({ "corpse" }));
    set("long", "�@��V��[1;31m�A��[m������.");
    set("unit", "��");
    set("value", 100);
    set("no_save", 1);
    set_rough();
    if( clonep( this_object()))
        call_out("decay", ( random( 20)+ 40), 1);
}

string short() { return name(); }

void decay( int phase)
{
    if( this_object()->query("playercor")) return;

    switch( phase)
    {
        case 1:
            if( random( 9)> 2)
            {
                tell_object( environment( this_object()),
                    query("name") + "������[35m�G�ѤF[m.\n");
                set("long", "�@��[35m�G��[m������.");
                call_out("decay", random( 30)+ 50, 2);
            } else {
                tell_object( environment( this_object()),
                    query("name") + "�w�g�Q[33m�����F[m.\n");
                set("long", "�@��[33m�����F[m������.");
                call_out("decay", random( 40)+ 50, 2);
            }
            break;
        case 2:
            tell_object( environment( this_object()),
                query("name") + "�Ʀ��@��ǹЮ����F.\n");
            destruct( this_object());
            break;
    }
}
