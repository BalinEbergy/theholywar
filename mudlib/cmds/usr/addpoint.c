// addpoint.c

int main( object me, int argc, string *argv)
{
    mapping attributes= ([
        "str": ({ "strength", 9 }),
        "con": ({ "maxHP", 12 }),
        "int": ({ "magic", 6 }),
        "spi": ({ "maxMP", 18}),
        "dex": ({ "maxSP", 9})
        ]);
    mixed *type;

    if( argc== 1)
        write("你要增強哪項屬性呢?\n");
    else if( sizeof( type= attributes[ argv[1]]))
    {
        if( me->query("addpoint")== 0)
            write("你的額外點數不夠了!\n");
        else {
            me->add("addpoint", -1);
            me->add( argv[1], 1);
            me->add( type[0], type[1]);
            write("你覺得身體的某部分能力變強了.\n");
        }
    } else write("你要增強哪項屬性呢?\n");

    return 1;
}

int help( object me)
{
    write( @HELP
指令格式: addpoint <屬性>

str - 力量屬性  con - 體格屬性
int - 智慧屬性  spi - 精神屬性
dex - 敏捷屬性

使用額外點數來增強你的基本能力
HELP
    );

    return 1;
}
