// da-1.c
// written by koyamakyo

#include <room.h>

inherit ROOM;

void create()
{
   set("short","城鎮中心地下室");
        set("long",@LONG
這裡是大城中心點的地下室,空無一物,
看起來像是做測試用的.
LONG
        );
       set("exits",
        ([
        "up" : __DIR__ + "center",
        ])
        );

        set("objects",  ([
                __DIR__ + "npc/roni2":100,
                ]));
        setup();

}
