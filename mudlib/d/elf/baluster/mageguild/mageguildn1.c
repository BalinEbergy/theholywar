// mageguildn1.c
// written by Hudson

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[36m魔法師公會會所[m");
        set("long",@LONG
這兒是魔法師公會北方,咦?一陣風冷不防的從你身上吹過,你
感覺清爽多了,看看四周,似乎有著一股奇妙的感覺.
LONG
        );

        set("exits", ([
                "south" : __DIR__ + "mageguildm1",
                "east"  : __DIR__ + "mageguildn2",
                ]));
}
