// square-sw.c
// written by koyamakyo

#include <room.h>

inherit ROOM;

void create()
{
   set("short","城鎮廣場西南側");
        set("long",@LONG
這裡是大城城鎮廣場的西南側.
LONG
        );
       set("exits",
        ([
        "east" : __DIR__ + "square-s",
        "north" : __DIR__ + "square-w",
        ])
        );

        set("objects",  ([
                __DIR__ + "npc/roni":2,
                ]));
        setup();

}
