// akas.c
// written by Hudson
// 阿卡斯

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
        int HP= 120000, MP= 14000, SP= 1000;

        set_name("阿卡斯", ({ "akas" }));
        set("lv", 40);
        set("class",WIZARD);
        set("race",ELF);
        set("gender",1);
        set("describe","一位嚴肅的魔導士.");
        set("pre","魔導士");
        set("age", 132);
        set("element",WIND);
        set("mind", 100);

        set("teacher", ({
                23, 21, 9, 8, 7, 6, 5, 1, 0,
                }));
        set("favorite", ({
                WIZARD, "summoner", "ruronin",
                }));
        set("teach_mind", "-1000 ~ 1000");
        set("lv_up", "[1;36m阿卡司說道:[33m 好了, 等級提升了[m\n");
        set("no_enu_money", "[1;36m阿卡司說道:[33m 景仰魔法的人啊! 你的錢不夠喔...[m");
        set("no_enu_lv", "[1;36m阿卡司說道:[33m 你的等級還不夠![m");
        set("top_lv", "[1;36m阿卡司道:[33m 恭喜了,你的能力已經超越我了[m");
        set("teach_message", "[1;36m阿卡司說道:[33m 學魔法嗎? 你想學哪項呢?[m");

        set("maxHP", HP);
        set("currentHP", HP);
        set("maxMP", MP);
        set("currentMP", MP);
        set("maxSP", SP);
        set("currentSP", SP);
        set("dem", 7000);
        set("def", 10000);
        set("mdem", 17000);
        set("mdef", 12533);
        set("spe", 1030);
        set("exp", 6000);

        set_skill_level( 0, 100);
        set_skill_level( 1, 100);
        set_skill_level( 3, 100);
        set_skill_level( 5, 100);
        set_skill_level( 6, 100);
        set_skill_level( 7, 100);
        set_skill_level( 8, 100);
        set_skill_level( 9, 100);
        set_skill_level( 23, 100);
        set_skill_level( 21, 100);
        set("skill", ({ "6:lightning light", "7:fire light" }));
        set("skill_chance", 100);

        setup();
}

void init()
{
        if( !previous_object()
                || !userp(previous_object()))
                return;
        call_out("ki", 100, previous_object());
        call_out("action", 10, previous_object());
}

void ki( object obj)
{
        if( obj->is_fighting()) kill_ob(obj);
}

void action( object obj)
{
        command("hihi 想要學初級法術嗎?找我就對了喔...");
}
