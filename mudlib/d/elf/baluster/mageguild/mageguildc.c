// mageguildc.c
// written by Hudson

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[36m魔法師公會會所[m");
        set("long",@LONG
這兒是魔法師公會會所,所有的法師都要在此接受魔法師的試
煉,才會被承認為真正的魔法師,看看四周的人,有的已經是魔
法師的身份了,有些則是要來接受考試的,你不禁心想,自己是
否能通過試煉呢?
LONG
        );

        set("exits", ([
                "east"  : __DIR__ + "mageguildm2",
                "north" : __DIR__ + "mageguildn2",
                "south" : __DIR__ + "mageguilds2",
                "down"  : "d/elf/baluster/center",
                "west"  : __DIR__ + "mageguildm1",
                ]));
}
