// swimmer.c
// written by zz

#include <class.h>
#include <element.h>
#include <race.h>

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
        int HP= 10000, MP= 200, SP= 10000;

        set_name("泳客", ({ "swimmer"}));
        set("lv", 24);
        set("race",HUMAN);
        set("gender",1);
        set("describe","一位泳客.");
        set("pre","穿著泳褲的");
        set("age", 28);
        set("element",WATER);
        set("mind", 10);

        set("maxHP", HP);
        set("currentHP", HP);
        set("maxMP", MP);
        set("currentMP", MP);
        set("maxSP", SP);
        set("currentSP", SP);
        set("dem", 700);
        set("def", 400);
        set("mdem", 130);
        set("mdef", 130);
        set("spe", 150);
        set("exp", 4000);

        set("chat_chance", 10);
        set("chat_msg", ({
                (: random_move :)
                }));
        setup();
}
