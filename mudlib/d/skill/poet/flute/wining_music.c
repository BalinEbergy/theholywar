// wining_music.c
// written by Bother

#include <skill.h>

int main( object me, object obj)
{
        string *effects;

        if( obj->query("wining_mt")) return 0;
        else
        {
                effects= obj->query("effect");
                obj->set("wining_m", ({ 
                        "dem_eff:"+ obj->query("dem")/ 10, 
                        "spe_eff:"+ obj->query("spe")/ 10,
                }));
                obj->set("wining_mt", me->skill_level( FLUTE));
                obj->add("dem_eff", obj->query("dem")/ 10);
                obj->add("spe_eff", obj->query("spe")/ 10);

                if( effects)
                        effects+= ({ "wining_m" });
                else effects= ({ "wining_m" });
                obj->set("effect", effects);
        }

        tell_object( obj, "\n你覺得心情浮動了起來.\n");

        return 1;
}

string name( int select)
{
        switch( select)
        {
                case 1: return "勝利之樂";
                case 2: return "wining music";
                case 3: return "wining_music";
        }
}

int type() { return FLUTE; }
int lv() { return 2; }
int attackful()
{
        return 0;
}

string mes( object me)
{
        return "吹奏出[1;33m勝利之樂[m,使眾人的內心浮動了起來";
}

int usesp() { return 10; }
int usemp() { return 5; }
int successful() { return 80; }
int delay() { return 1; }
int all() { return 1; }
string element() { return "air"; }

int check( object me)
{
        return 1;
}
