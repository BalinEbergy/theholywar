// dark_moon.c
// written by Bother

#include <skill.h>

string name( int i);
void die( object me, object victim);

int main( object me, object obj)
{
        int damage, mdem, magic, spirit, mdef, hp, k;
        string w_name= "琴";

        mdem= me->query("mdem")+ me->query("mdem_eff");
        magic= me->query("magic");
        spirit= me->query("spirit");
        mdef= obj->query("mdef")+ obj->query("mdef_eff");
        hp= obj->query("currentHP");

        message_vision("[1;35m$N撥動著手中的琴逐漸加速[m\n", me);        

        if( me->query("lv")>= obj->query("lv")+ 80)
        {
                if( random( 9)< 8)
                {
                        tell_object( obj, "[33m"+ me->name()+ "的演奏漸漸加速,
你也漸漸感到暈眩,突然四周景物一片模糊[m\n");
                        message_vision("$N持續演奏,突然間$n倒了下來\n", me, obj);
                        tell_object( obj, "你只覺得身體好像不在存在似的,靈魂突然離開了你自己.\n");
                        message_vision("$N帶走了$n的[1;30m靈魂[m!!\n", me, obj);
                        obj->set("currentHP", 0);
                }
                else
                {
                        message_vision("$N奏出陣陣的[33m音樂[m傳進了$n的[31m耳[m裡\n", me, obj);
                        tell_object( obj, "音波的晃動使你覺得心脈具裂\n\n");
                        k= mdem/ mdef;
                        if( k<= 5)
                                damage= mdem* magic* spirit* 50/mdef;
                        else damage= 50000+ random(10000);
                        obj->add("currentHP", -damage);
                }
        }
        else if( me->query("lv")>= obj->query("lv")+ 20 || me->query("magic")+ me->query("spirit")>= obj->query("magic")+ obj->query("spirit")+ 50)
        {
                if( random( 9)> 8)
                {
                        tell_object( obj, me->name()+ "的演奏漸漸加速,
你也漸漸感到暈眩,突然四周景物一片模糊.\n");
                        message_vision("$N持續演奏,突然間$n倒了下來\n", me, obj);
                        tell_object( obj, "你只覺得身體好像不在存在似的,靈魂突然離開了你自己.\n");
                        message_vision("\$N帶走了$n的*[1;30m靈魂*[m!!\n", me, obj);
                        obj->set("currentHP", 0);
                }
                else
                {
                        message_vision("$N奏出陣陣的[33m音樂[m傳進了$n的[31m耳[m裡\n", me, obj);
                        tell_object( obj, "音波的晃動使你漸漸無法控制自己[1;32m突然間你晃了一下[1;5;37m當你恢復知覺時已經遍體淋傷.[m\n\n");
                        damage= obj->query("currentHP")/4+ random(10000);
                        obj->add("currentHP", -damage);
                }
        }
        else
        {
                if( random( 19)> 18)
                {
                        tell_object( obj, me->name()+ "的演奏漸漸加速,
你也漸漸感到暈眩,突然四周景物一片模糊\n");
                        message_vision("$N持續演奏,突然間$n倒了下來\n", me, obj);
                        tell_object( obj, "你只覺得身體好像不在存在似的,靈魂突然離開了你自己.\n");
                        message_vision("\$N帶走了$n的*[1;30m靈魂*[m!!\n", me, obj);
                        obj->set("currentHP", 0);
                }
                else
                {
                        message_vision("$N奏出陣陣的[33m音樂[m傳進了$n的[31m耳[m裡\n", me, obj);
                        tell_object( obj, "音波的晃動使你漸漸無法控制自己[1;32m突然間你晃了一下[1;5;37m當你恢復知覺時已經遍體淋傷.[m\n\n");
                        damage= obj->query("currentHP")/5+random(50000);
                        obj->add("currentHP", -damage);
                }
        }
        return COMBAT_D->skill_msg( me, obj, name( 1), w_name, damage);
}

string name( int select)
{
        switch( select)
        {
                case 1: return "闇月悲鳴";
                case 2: return "dark moon";
                case 3: return "dark_moon";
        }
}

int type() { return HARP; }
int lv() { return 100; }
int attackful()
{
        return 1;
}

string mes( object me)
{
        return "的表情漸漸嚴肅起來,一陣陣[1;30m詭[1;31m異[m的[1;33m音樂[m鑽入每個人的耳中";
}

int usesp() { return 200; }
int usemp() { return 120; }
int successful() { return 90; }
int delay() { return 4; }
int all() { return 1; }
string element() { return "time"; }

int check( object me)
{
        return 1;
}
