// test.c
// written by Mondale
// 函式測定的test招

#include <skill.h>


int main( object me, object obj)
{
        int demage,loop,times,totaldem=0;
        times=100;
        for(loop = 0;loop < times;loop++)
        {        
                demage = 92000* 1.5* 2- 25000;
                message_vision("[1;36m$N[0m對你評估了一下，做出了判斷...這將對[36;1m$n[0m造成([31;1m" + demage + "[0m)的影響!\n", me, obj);
                totaldem = totaldem + demage;
        }
        tell_object(me,"造成的總影響是「[1m" + totaldem + "[0m」.\n");
        tell_object(obj,"只是測試罷了....別擔心啦....^^\n");        
//        obj->set_skill_lv( 11,100);
//        obj->set("skill", ({
//        "11:test"
//        }));
}

string name( int select)
{
        switch( select)
        {
                case 1: return "試驗之瞳";
                case 2: return "test";
                case 3: return "test";
        }
}


int type() { return SONG; }
int lv() { return 1; }
int attackful()
{
        return 0;
}

string mes( object me)
{
        return "的眼睛逐漸轉為青藍,將所有的資訊連串起來.";
}

int usesp() { return 0; }
int usemp() { return 0; }
int successful() { return 100; }
int delay() { return 0; }
int all() { return 0; }

int check( object me)
{
        return 1;
}
