// merchant.c
// written by Bother

inherit NPC;

void create()
{
        int HP= 80000, MP= 2000, SP= 2000;

        set_name("商人", ({ "merchant" }));
        set("lv", 52);
        set("class","mage");
        set("ra","elf");
        set("gender","男");
        set("describe","一個買賣東西的商人");
        set("pre", "背負很多東西");

        set("age", 25);
        set("element", "holy");
        set("mind", 500);

        set("mixHP", HP);
        set("currentHP", HP);
        set("mixMP", MP);
        set("currentMP", MP);
        set("mixSP", SP);
        set("currentSP", SP);
        set("dem", 500);
        set("def", 300);
        set("mdem", 6000);
        set("mdef", 2500);
        set("spirit", 24);
        set("spe", 250);
        set("exp", 3000);

        set("trading", 20);
        set("buy_mind", "0~1000");

        set("selling", ({
                "/d/objs/others/water_skin.c",
                "/d/elf/hiwind/elf_forest/forest/obj/meat.c",
                }));
        setup();
}
