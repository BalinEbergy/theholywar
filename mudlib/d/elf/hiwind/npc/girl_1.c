// girl_1.c
// written by hiwind

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
    int HP= 200000, MP= 20000 ,SP= 30000;

    set_name("[1;5;31m����[m[1m������[m", ({ "girl" }));
    set("lv", 80);
    set("class", WIZARD);
    set("race", HUMAN);
    set("describe","�@��Q�����R���k�l.");
    set("age", 16);
    set("mind", 750);
    set("gender", 0);

    set("maxHP", HP);
    set("currentHP", 125000);
    set("maxSP", SP);
    set("currentSP", SP);
    set("strength", 13000);
    set("def", 22000);
    set("mdem",7500);
    set("mdef", 15000);
    set("spirit", 75);
    set("speed", 1800);
    set("uncurable", 1);
    set("exp", 0);

    set("chat_chance", 10);
    set("chat_msg", ({
        "\����d�E���ݵ�ù�t��.\n"
        }));

    set_skill_level( 11, 10);
    set("skill", ({
        "11:forest sound",
        }));
    set("skill_chance", 10);
    setup();
}
void init()
{
    if( !previous_object()
        || !userp(previous_object()))
        return;
    call_out("action", 10, previous_object());
}

void action( object obj)
{
    if( !obj || !present(obj) || this_object()->is_fighting()) return;
    if( obj->is_fighting()) kill_ob(obj);

    command("say �D�H....�n���j�����U�h");
}
