// city_4.c
// written by Bother

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[31m紅綿大道十字路口[m");
        set("long",@LONG
往南就是卡格烈斯城的大門,在這個交叉口上你可以好好的考
慮自己要去那兒,往西就是夏可街,延途經過旅館和食堂,往東
走就是松風街,或者你如果口渴可以繼續往前走,在廣場上有
一座噴水池供你飲用,這是個挺不一樣的城市,到處走走你會
發現些不同的地方.
LONG
        );

        set("exits",
        ([
        "north" :__DIR__ + "city_23",
        "south" :__DIR__ + "door",
        "west" :__DIR__ + "shak_1",
        "east" :__DIR__ + "songphon_1",
        ])
        );
}
