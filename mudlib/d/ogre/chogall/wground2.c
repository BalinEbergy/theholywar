// wground2.c
// written by Hudson

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;31m卡格爾廣場邊緣[m");
        set("long", @LONG
前方就是世界上最大的廣場,卡格爾廣場,你望望城內,四周都
是一片的紅色,背後宏偉的城門將廣場襯托得更為壯闊,廣場
上人來人往,真不愧為巨魔世界最大的城市.
LONG
        );

        set("exits", ([
                "east"  : __DIR__ + "wground1",
                "west"  : __DIR__ + "door",
                ]));
}
