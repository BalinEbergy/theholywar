// slash moon
// written by Mondale

#include <skill.h>
string name( int i);
int main( object me, object obj)
{
        int damage;
        string w_name= me->query_temp( me->query_temp("sla"))->name();
        message_vision("\n$N手中的長劍在半空中曳出一道[36;1m湛藍[0m的[1;7;47;36m劍光[0m.\n",me);
        message_vision("劍勢突然加速，幻化成三道[1;33;45m新月[0m型的[37;1m劍氣[0m向$N襲去!!!\n",obj);
        damage = ((me->query("dem") + me->query("dem_eff") + me->query("mdem"))*me->query("spirit") /30) - obj->query("def") - obj->query("defand") - obj->query("def_eff") + random(10000);
        for(int a=0;a<3;a++)
        {
        if (damage >= obj->query("currentHP")) break;
        if (damage < 0 ) damage = 0;
        COMBAT_D->skill_msg( me, obj, name( 1), w_name, damage);
        obj->add("currentHP", -damage);
        message_vision("[1;33;45m月形劍光[0m劃開時空，一併將$N切開!!!\n",obj);
        damage = obj->query("currentHP")*(50 + random(50))/(500 + random(200));
        }
        if (damage < 0 ) damage = 0;
        obj->add("currentHP", -damage);
        return COMBAT_D->skill_msg( me, obj, name( 1), w_name, damage);;
}

string name( int select)
{
        switch( select)
        {
                case 1: return "荒天月空殘";
                case 2: return "moon";
                case 3: return "moon";
        }
}

int lv() { return 101; }
int attackful()
{
        return 1;
}

string mes( object me)
{
        return "手持長劍,緩緩的在空中虛劃了一個半圓...";
}

int type() { return SLASH; }
int usesp() { return 300; }
int usemp() { return 300; }
int successful() { return 100; }
int delay() { return 6; }
int all() { return 0; }
int check( object me)
{
        return 1;
}
