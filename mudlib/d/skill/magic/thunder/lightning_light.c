// lighting_light.c
// written by Hudson

#include <skill.h>

inherit THUNDER_MAGIC;

void create()
{
    set("name", "電光術");
    set("id", "lightning light");
    set("lv", 1);
    set("attack", 1);
    set("usemp", 27);
    set("delay", 1);
    set("base_value/hp", 104);
    set("improve/hp", 11);
    set("cost", 40);
    set("exp", 40);

    setup();
}

string mes( object me)
{
    return "$N喃喃的唸出一段咒語, 手上瞬間發出一道[1;37m電光!!!![m";
}
