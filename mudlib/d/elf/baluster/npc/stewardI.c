// stewardI.c
// written by Hudson

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
    int HP= 51000, MP= 1300, SP= 1000;

    set_name("服務員", ({ "steward" }));
    set("lv", 50);
    set("class",WIZARD);
    set("race",ELF);
    set("gender",1);
    set("describe","商業公會巴路斯特分會服務員,身上的制服繡著: 服務交易員");
    set("pre", "笑容可掬的");

    set("age", 150);
    set("element", EARTH);
    set("mind", 20);

    set("maxHP", HP);
    set("currentHP", HP);
    set("maxMP", MP);
    set("currentMP", MP);
    set("maxSP", SP);
    set("currentSP", SP);
    set("strength", 3000);
    set("armor", 7000);
    set("magic", 4200);
    set("spi", 10);
    set("speed", 800);
    set("exp", 2300);

    setup();
}
