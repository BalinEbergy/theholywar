// old_axe.c
// written by Hudson

#include <equip.h>
#include <element.h>

inherit ITEM;

void create()
{
        set_name("卡格爾巨斧", ({ "chogall axe", "chogall", "axe" }));
        set("long", @LONG
一把極為巨大的斧頭,斧柄上已有些許刮痕,而整個斧身
也罩著一層炎紅色的灰,顯是已有一些年代了,站在它的旁邊,
你感到一股強烈而又龐大的狂暴之氣,面對這麼個巨大的斧頭,
你實在很難想像,當初酋長卡格爾應該是什麼樣子.
LONG
                );
        set("lv", 100);
        set("unit", "把");
        set("element", EARTH);
        set("epower", 100);

        set("unsac", 1);

        set_weight( 15000);
}
