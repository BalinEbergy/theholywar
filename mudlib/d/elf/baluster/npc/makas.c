// makas.c
// written by Hudson

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
    int HP=80000, MP= 0, SP= 8000;

    set_name("馬卡斯", ({ "makas" }));
    set("lv", 50);
    set("class", WARRIOR);
    set("race", ELF);
    set("gender", 1);
    set("describe","米契爾道具店巴路斯特總店的老闆.");
    set("pre", "米契爾道具店老闆");

    set("age", 325);
    set("element", WIND);
    set("mind", 0);

    set("maxHP", HP);
    set("currentHP", HP);
    set("maxSP", SP);
    set("currentSP", SP);
    set("strength", 10000);
    set("armor", 8000);
    set("magic", 1200);
    set("spi", 17);
    set("speed", 990);
    set("exp", 2500);

    set("trading", 101);
    set("buy_mind", "-999~1000");
    set("selling", ({
        OB_OTHER + "water_skin.c",
        OB_MAP   + "map_baluster.c",
        }));

    setup();

    carry_object( OB_AXE + "huge_axe")->equip();
}
