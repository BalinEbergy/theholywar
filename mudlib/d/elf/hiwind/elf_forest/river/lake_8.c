// lake_8.c
// written by hiwind

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;36m��y[m");
        set("long",@LONG
[1;36m�p��[m�����Y�N�O[1;36m��y[m,�M�ӥѩ���`�O�j���۲H�H��[1;36m����[m,��
�H�`�O���H�ݱo���I�ҽk���M,��F��W,�~�o�{�����O�p����
�M��,�N�ϩ���l�@��.
�F��P�_��O�@�K��[1;32m�˪L[m.
LONG
        );

        set("exits",
        ([
        "west" : __DIR__ + "lake_6",
        "south" : __DIR__ + "lake_7",
        ])
        );

        set("objects",
        ([
                __DIR__ + "npc/fish2":2
        ])
        );
        set("no_fight", 0);
        setup();
}
