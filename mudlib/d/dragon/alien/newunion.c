// newunion.c
// written by Mondale

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;37m練金術士公會[m");
        set("long",@LONG
這裡是亞利恩城的鍊金術士公會，公會也是現在亞利恩
城的市政中心，這邊不但負責培養新的鍊金術士，也幫
助執行市政。所以公會的會長，也就算是城內的行政首
長了。
南邊是會長辦公室。
東邊是練習場。
西邊是專門賣練金術士用具的雜貨店。
LONG
        );

        set("exits",
        ([
        "north" : __DIR__ + "wstreet5-3",
        "south" : __DIR__ + "new-s",
        "east" : __DIR__ + "new-e",
        "west" :__DIR__ + "new-w",        
        ])
        );

        set("objects",  ([
                ]));
        setup();
}
