// ogre.c
// written by Hudson

#include <class.h>
#include <element.h>
#include <race.h>

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
        int HP= 30000, SP= 1700;

        set_name("巨魔居民", ({ "ogre citizen", "ogre" }));
        set("lv", 25);
        set("class",WARRIOR);
        set("race",OGRE);
        set("describe","體格壯碩的巨魔.");
        set("pre","一個普通的");

        set("age", 90);
        set("element", EARTH);
        set("mind", -100);
        set("gender", 1);

        set("maxHP", HP);
        set("currentHP", HP);
        set("maxSP", SP);
        set("currentSP", SP);
        set("dem", 1300);
        set("def", 1900);
        set("mdef", 800);
        set("spe", 90);
        set("exp", 1000);

        set("chat_chance", 20);
        set("chat_msg", ({
                (: random_move :)
                }));

        carry_object( OB_AXE+ "big_axe")->wear();

        setup();
}
