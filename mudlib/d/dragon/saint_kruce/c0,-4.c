//c0,-4.c
//written by zz

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;37m騎士隊總部營區[m");
        set("long",@LONG
這裡是騎士隊總部營區.所有騎士都是從這個總部訓練出來的,
四周都是正在接受訓練的見習騎士,個個都努力的在學習技術.
你看見南方有棟建築,大概是總部內的辦公室吧！
往南可以到辦公大樓
LONG
        );
        set("exits", ([
                "north" : __DIR__ + "c0,-3.c" ,
                "south" : __DIR__ + "c0,-5.c" ,
                "east" : __DIR__ + "c1,-4.c" ,
                "west" : __DIR__ + "c-1,-4.c" ,
                ]));
        setup();
}
