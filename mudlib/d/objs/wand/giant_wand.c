// giant_wand.c
// written by Hudson

#include <equip.h>
#include <element.h>

inherit EQUIP;

void create()
{
    set_name("���H����", ({"giant wand", "giant", "wand" }));
    set("long", @LONG
LONG
        );
    set("lv", 30);
    set("unit", "��");
    set("element", FIRE);
    set("epower", 100);
    set("value", 50);

    set("wear_type", HAND);
    set("weapon", WAND);
    set("damage", 376);
    set("mdem_eff", 500);
    set("hit_eff", 250);
    set("eff", ({ "hit_eff", "mdem_eff", }));

    set_weight( 50);
}
