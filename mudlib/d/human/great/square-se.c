// square-se.c
// written by koyamakyo

#include <room.h>

inherit ROOM;

void create()
{
   set("short","城鎮廣場東南側");
        set("long",@LONG
這裡是大城城鎮廣場的東南側.
LONG
        );
       set("exits",
        ([
        "north" : __DIR__ + "square-e",
        "west" : __DIR__ + "square-s",
        ])
        );

        set("objects",  ([
                __DIR__ + "npc/roni":2,
                ]));
        setup();

}
