// ballman.c
// written by Hudson
// 巴爾曼

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
    int HP= 197000, MP= 44000, SP= 3200;

    set_name("巴爾曼", ({ "ballman" }));
    set("lv", 88);
    set("class",WIZARD);
    set("race",DRAGON);
    set("gender",1);
    set("describe","一位身著黑袍,眼神閃耀光芒的巫師.");
    set("pre","黑闇法師");

    set("age", 357);
    set("element",DARK);
    set("mind", -799);

    set("maxHP", HP);
    set("currentHP", HP);
    set("maxMP", MP);
    set("currentMP", MP);
    set("maxSP", SP);
    set("currentSP", SP);
    set("strength", 6300);
    set("armor", 24500);
    set("magic", 33200);
    set("spi", 27);
    set("speed", 2263);
    set("exp", 247000);

    setup();
}

void init()
{
    object obj;

    if( !(obj= previous_object())
        || !userp( obj))
        return;

    call_out("ki", 10, obj);
    call_out("action", 20, obj);
}

void action( object obj)
{
    if( !obj || !present(obj) || this_object()->is_fighting()) return;
    command("say 黑暗才是一切的根本啊...有來自於無,而無就是一片黑暗...");
}

void ki( object obj)
{
    if( obj->is_fighting()) kill_ob( obj);
}
