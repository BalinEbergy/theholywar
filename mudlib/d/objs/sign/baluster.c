// baluster.c
// written by Hudson

#include <element.h>

inherit ITEM;

void create()
{
    set_name("告示牌", ({ "sign" }));
    set("long", @LONG
�摃丐丐丐丐丐丐丐丐丐丐丐丐丐丐丐丐丐丐丐丐丐丐丐片�
��                                                ��
��           [1;32m巴      路      斯      特[m           ��
��                                                ��
��      歡迎光臨巴路斯特,                         ��
��      請各位景仰魔法的法師或旅客遵守            ��
��      城內的各項規定,亦勿破壞了自然的景觀.      ��
��                                                ��
��                                      [1;37m元老院[m    ��
��                                                ��
�聝丐丐丐丐丐丐丐丐丐丐丐丐丐丐丐丐丐丐丐丐丐丐丐片�
LONG
        );
    set("unit", "面");
    set("element", EARTH);
    set("epower", 5);

    set("no_get", 1);
    set("unsac", 1);
}
