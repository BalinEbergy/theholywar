// center.c
// written by Hudson

#include <room.h>

inherit ROOM;

void create()
{
        set("short","*[1;37m巴路斯特廣場中心*[m");
        set("long",@LONG
.....
LONG
        );

        set("exits",
        ([
        "west" : __DIR__ + "testn4e1.c",
        "down" : __DIR__ + "down-land.c"
        ])
        );
}
