// mageguildn2.c
// written by Hudson

#include <room.h>

inherit ROOM;

void create()
{
    set("short","[36m魔法師公會會所[m");
    set("long",@LONG
站在你面前的,就是魔法師公會會長,多倫克斯,他亦是巴路斯
特的大祭司長,從事國內有關魔法及古代典籍的研究,站在他
身旁的,是副會長史達.
LONG
    );

    set("exits", ([
        "south" : __DIR__ + "mageguildc",
        "east"  : __DIR__ + "mageguildn3",
        "west"  : __DIR__ + "mageguildn1",
        ]));

    set("objects", ([
        "d/elf/baluster/npc/toranks":1,
        "d/elf/baluster/npc/star":1,
        ]));

    setup();
}
