// bat.c
// written by Mondale

inherit NPC;

void create()
{
        int HP= 35000, SP= 1000;

        set_name("蝙蝠", ({ "bat", "bat" }));
        set("lv", 80);
        set("class","warrior");
        set("ra","bat");
        set("describe","性喜鮮血的蝙蝠.");
        set("pre","[m[30;47m暗夜的使者[m");

        set("age", 3);
        set("element", "wind");
        set("mind", -300);
        set("gender", "男");

        set("mixHP", HP);
        set("currentHP", HP);
        set("mixSP", SP);
        set("currentSP", SP);
        set("dem", 10000);
        set("def", 300);
        set("spirit", 100);
        set("spe", 1500);
        set("exp", 100000);
        setup();
}
void init()
{
        if( !previous_object()
                || !userp(previous_object()))
                return;
        call_out("action", 1, previous_object());
        kill_ob( previous_object());
}
void action( object obj)
{
        if( !obj || !present(obj) || this_object()->is_fighting()) return;
        if( obj->is_fighting()) kill_ob(obj);
        setup();
}
