// group.c

static object fol, *folled= ({ });
static object leader, *group;
static object boss, *pet;

void init_group()
{
    leader= fol= this_object();
    group= ({ leader });
}

object get_boss() { return boss; }

object get_leader() { return leader; }
void set_leader( object led) { leader= led; }
object *get_group() { return group; }

int group( object obj)
{
    int lv;

    if( leader!= this_object())
        return 1;                   // Group must be called by leader
    if( obj->get_follow()!= this_object())
        return 2;                   // Obj must follow me
    if( obj->get_leader()== this_object())
        return 3;                   // Obj has been a member of our group
    if( sizeof( obj->get_group())> 1)
        return 4;                   // Obj is a leader of another group
    if( obj->get_boss()== this_object())
        return 5;                   // Pets can't be grouped
    if(( lv= obj->query("lv")- this_object()->query("lv"))> 5 || lv< -5)
        return 6;                   // The level gap must be less than 6
    if( sizeof( group)> 7)
        return 7;                   // The group members reached limit

    tell_object( obj, "你加入了以"+ this_object()->name()+ "為首的隊伍.\n");
    message("group", obj->name()+ "加入了你的隊伍.\n", group);

    group+= ({ obj });
    obj->set_leader( this_object());

    return 0;
}

void remove_member( object obj)
{
    tell_object( obj, "你離開了以"+ this_object()->name()+ "為首的隊伍.\n");
    message("group", obj->name()+ "離開了你的隊伍.\n", group, obj);

    obj->set_leader( obj);
    group-= ({ obj });
}

void leave_group()
{
    int i;

    if( leader== this_object())
    {
        i= sizeof( group);
        while( i--> 1)
            remove_member( group[i]);
        tell_object( this_object(), "隊伍解散了.\n");
    } else leader->remove_member( this_object());
}

object get_follow() { return fol; }
object *get_followed() { return folled; }

varargs void follow( object obj)
{
    string str= "";

    if( obj== this_object())
        return;

    if( leader!= this_object() && leader!= obj)
        leave_group();

    if( fol!= this_object() && objectp( fol))
    {
        fol->remove_follow( this_object());
        str+= "你停止跟隨"+ fol->name()+ ".\n";
    }

    if( !objectp( obj))
        fol= this_object();
    else {
        fol= obj;
        fol->add_follow( this_object());
        str+= "你開始跟隨"+ fol->name()+ ".\n";
    }

    tell_object( this_object(), str+ "\n");
}

varargs void remove_follow( object obj)
{
    int i;

    if( objectp( obj))
    {
        folled-= ({ obj });
        tell_object( this_object(), obj->name()+ "停止跟隨你.\n");
    } else {
        i= sizeof( folled);
        while( i--)
            folled[i]->follow();
    }
}

void add_follow( object obj)
{
    folled+= ({ obj });
    tell_object( this_object(), obj->name()+ "開始跟隨你.\n");
}

void de_group()
{
    follow();
    remove_follow();
}
