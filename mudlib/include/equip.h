// equip.h

#define EQ_LIGHT    0
#define HEAD        1
#define NECK        2
#define BODY        3
#define HAND        4
#define BRACE       5
#define RING        6
#define BOOT        7

#define SWORD       1
#define BLADE       2
#define LANCE       3
#define AXE         4
#define WAND        5
#define DRUM        6
#define FLUTE       7
#define HARP        8
