// test_sword.c
// written by Hudson

#include <equip.h>
#include <element.h>

inherit EQUIP;

void create()
{
    set_name("測試神劍", ({ "test sword" }));
    set("long", @LONG
一把青銅打製成的短劍.
LONG
        );
    set("lv", 1);
    set("unit", "把");
    set("element", COSMOS);
    set("value", 5);

    set("wear_type", HAND);
    set("weapon", SWORD);
    set("damage", 400);
    set("hit_eff", 2000);
    set("eff", ({ "hit_eff" }));

    set_weight( 30);
}
