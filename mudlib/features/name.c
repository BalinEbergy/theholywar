// name.c
// From es2 definition

#include <element.h>
#include <dbase.h>

static string *my_id;

void set_name( string name, string *id)
{
    set("name", name);
    if( !query("id"))
        set("id", id[0]);
    my_id= id;
}

string name()
{
    return query("name");
}

string query_id()
{
    return capitalize( query("id"));
}

int id( string str)
{
    return str== query("id");
}

string short()
{
    int mind, element;
    string str;

    str= "";

    if( this_object()->is_character()) {
        mind= query("mind");
        if( mind> 9000) str+= "[1;37m(�t�F)[m";
        else if( mind> 6000) str+= "[1;33m(����)[m";
        else if( mind> 3000) str+= "[1;32m(����)[m";
        else if( mind< -9000) str+= "[1;34m(�]��)[m";
        else if( mind< -6000) str+= "[1;35m(����)[m";
        else if( mind< -3000) str+= "[1;31m(�c��)[m";

        if( query("element")== HOLY)
            str+= "[1;37m(���t���~)[m";
        else if( query("element")== EVIL)
            str+= "[1;30m(�¦���~)[m";

        if( mind> 3000 || mind< -3000) str+= " ";
        if( query("pre"))
            str+= query("pre");
        else if( query("title"))
            str+= query("title");
    } else {
        if( query("epower")> 80) {
            switch( query("element")) {
                case LIGHT: str= "[1;33m(�{��)[m"; break;
                case COSMOS: str= "[1;36m(����)[m"; break;
                case WIND: str= "[1;32m(���@�T)[m"; break;
                case THUNDER: str= "[1;37;44m(�q��)[m"; break;
                case WATER: str= "[1;37m(�H��)[m"; break;
                case FIRE: str= "[1;31m(����)[m"; break;
                case EARTH: str= "[31m(�L�L�̰�)[m"; break;
                case DARK: str= "[1;30m(�t��)[m"; break;
                case HOLY: str= "[1;37m(�t��)[m"; break;
                case EVIL: str= "[1;34m(�]��)[m"; break;
            }
            str+= " ";
        }
    }

    str+= query("name")+ " ("+
        query_id()+ ")";

    return str;
}

string long()
{
    string str;

    if( stringp( str= query("long"))) return str;

    return short();
}
