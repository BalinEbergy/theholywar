// hiwind_5.c
// written by hiwind

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
    int HP= 1000000, MP= 200000 ,SP= 500000;

    set_name("[1;5;31m����[m��[1;33mù�t��[m", ({ "darkwind" }));
    set("lv", 120);
    set("class", WARRIOR);
    set("race", HUMAN);
    set("describe","���˪��H.");
    set("age", 16);
    set("mind", 2000);
    set("gender", 1);

    set("maxHP", HP);
    set("currentHP", 200000);
    set("maxMP", MP);
    set("currentMP", 100000);
    set("maxSP", SP);
    set("currentSP", 250000);
    set("strength", 15000);
    set("def", 22500);
    set("mdem", 7500);
    set("mdef", 10000);
    set("spirit", 75);
    set("speed", 1800);
    set("uncurable", 1);
    set("exp", 0);

    set("chat_chance", 10);
    set("chat_msg", ({
        "ù�t�Ѵd�E���ݵۧA.\n"
        }));
    setup();
}
void init()
{
    if( !previous_object()
        || !userp(previous_object()))
        return;
    call_out("action", 10, previous_object());
}

void action( object obj)
{
    if( !obj || !present(obj) || this_object()->is_fighting()) return;
    if( obj->is_fighting()) kill_ob(obj);

    command("say �֯��A�ѧ�! ��...");
}
