// shrewmouse.c
// written by hiwind

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
    int HP= 980, MP= 500, SP= 1000;

    set_name("�a��", ({ "rat" }));
    set("lv", 10);
    set("describe"," �a��.");
    set("age", 10);
    set("element", EARTH);
    set("race", ANIMAL);
    set("mind", 0);

    set("maxHP", HP);
    set("currentHP", HP);
    set("maxMP", MP);
    set("currentMP", MP);
    set("maxSP", SP);
    set("currentSP", SP);
    set("strength", 300);
    set("armor", 240);
    set("magic", 60);
    set("spi", 2);
    set("speed", 135);
    set("exp", 1400);
    setup();
    set("chat_chance", 10);
    set("chat_msg", ({
        (: random_move :)}));
}
