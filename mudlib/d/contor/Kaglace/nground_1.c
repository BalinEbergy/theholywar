// nground_1.c
// written by Bother

#include <room.h>


inherit ROOM;

void create()
{
        set("short","[1;37m卡格烈斯廣場[m");
        set("long",@LONG
這裡是卡格烈斯城的廣場,往南走就是廣場的中心,你眼前看到不
少人來人往的情景,有不少鳥兒(bird)在天空飛翔,再往北走就是
卡格烈斯山區邊境,東北邊有一個圖書館(library)你可以試著去
看看,或許可以找到不少資料.
LONG
        );

        set("exits",
        ([
        "north" :__DIR__ + "nground_2",
        "south" :__DIR__ + "center",
        "west" :__DIR__ + "nwground_1",
        "east" :__DIR__ + "neground_1",
        ])
        );
        set("objects",
        ([
                __DIR__ + "objs/fountain":1
        ])
        );
        setup();
}
