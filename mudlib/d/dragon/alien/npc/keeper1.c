// keeper1.c
// written by Mondale
// 飯店的老闆

inherit NPC;

void create()
{
        int HP= 20000, MP= 0, SP= 100000;

        set_name("老闆", ({ "keeper" }));
        set("lv", 40);
        set("class","warrior");
        set("ra", "human");
        set("gender","男");
        set("describe","有著微微發福身段的飯店的老闆.");
        set("pre","飯店的");

        set("age", 47);
        set("element", "earth");
        set("mind", 0);

        set("mixHP", HP);
        set("currentHP", HP);
        set("mixMP", MP);
        set("currentMP", MP);
        set("mixSP", SP);
        set("currentSP", SP);
        set("dem", 3000);
        set("def", 1000);
        set("mdem", 1000);
        set("mdef", 1000);
        set("spe", 100);
        set("realmx", 10);
        set("exp", 50000);

        set("chat_chance", 20);
        set("trading", 1);
        set("buy_mind", "-1000~1000");
                                                                                
        set("selling", ({
                "/d/objs/food/alienchicken",
                }));

        setup();
}

void init()
{
        ::init();
        if( !previous_object()
                || !userp(previous_object()))
                return;
       
}
