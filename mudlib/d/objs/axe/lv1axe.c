// lv1axe.c
// written by Hudson

#include <equip.h>
#include <element.h>

inherit EQUIP;

void create()
{
    set_name("見習小斧", ({ "novice axe" }));
    set("long", @LONG
一把木柄小短斧.
LONG
        );
    set("lv", 1);
    set("unit", "把");
    set("element", COSMOS);
    set("value", 5);

    set("wear_type", HAND);
    set("weapon", AXE);
    set("damage", 100);
    set("hit_eff", 130);
    set("eff", ({ "hit_eff" }));

    set_weight( 50);
}
