// old2-1.c
// written by Mondale

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;37m練金術士公會舊址[m");
        set("long",@LONG
這裡越往內走就越來越昏暗了，還不時有著奇怪的聲音
傳出。真是有點令人毛骨悚然，連一秒鐘也呆不下去了
。這裡究竟發生過什麼事呢？
LONG
        );

        set("exits",
        ([
        "north" : __DIR__ + "old1-1",
        "south" : __DIR__ + "old3-1",
        "east" : __DIR__ + "old2-2",
        ])
        );

        set("objects",  ([
                __DIR__ + "npc/lizard.c":1,
                ]));
        setup();
}
