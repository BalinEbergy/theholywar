// light_missile.c
// written by Hudson

#include <skill.h>

int main( object me, object obj)
{
        int default_d= 125, demage, eff, mdem, mdef;

        mdem= me->query("mdem")+ me->query("mdem_eff");
        mdef= obj->query("mdef")+ obj->query("mdef_eff");

        default_d= default_d+ default_d* me->skill_level( LIGHT)/ 20;

        if( me->query("element")== "light")
                demage= default_d* mdem* 1.2/ mdef;
        else
                demage= default_d* mdem/ mdef;

        if( obj->query("element")== "dark") demage= demage* 1.2;

        demage= demage* ( 90+ random( 30))/ 100*( 100- eff)/ 100;

        obj->add("currentHP", -demage);

        message_vision("[1;37m$N唸完咒語,手上的光團瞬間化作無數的飛彈往$n猛擊而去!!![m\n", me, obj);

        return demage;
}

string name( int select)
{
        switch( select)
        {
                case 1: return "聖光彈";
                case 2: return "light missile";
                case 3: return "light_missile";
        }
}

int type() { return LIGHT; }
int lv() { return 35; }
int attackful()
{
        return 1;
}

string mes( object me)
{
        return "手上聚集了數道[1;37m神聖的光芒!![m";
}

int usesp() { return 0; }
int usemp() { return 160; }
int successful() { return 100; }
int delay() { return 2; }
int all() { return 0; }
string element() { return "light"; }

int check( object me)
{
        if( me->query("mind")< 799)
        {
                write("\n你的邪念使得你無法凝聚出神聖之光...\n");
                return 0;
        }

        return 1;
}
