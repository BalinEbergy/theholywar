// call_forg.c
// written by Hudson

#include <skill.h>

void create() { seteuid( getuid()); }

int main( object me, object obj)
{
        object fog;

        fog= new( OB_SPECIAL+ "fog");
        fog->set("lv", me->skill_level( SUMMON));
        fog->set("epower", fog->query("lv"));
        fog->move( environment( me));
        call_out( "disappear", me->skill_level( SUMMON)* 20, fog);

        tell_room( environment( me), "四周飄起一陣濃霧!!\n");
        environment( me)->set("low_reach", me->skill_level( SUMMON)* 2);

        return 1;
}

void disappear( object fog)
{
        fog->disappear( fog);
}

string name( int select)
{
        switch( select)
        {
                case 1: return "霧召喚術";
                case 2: return "fog";
                case 3: return "call_fog";
        }
}

int type() { return SUMMON; }
int lv() { return 5; }
int attackful()
{
        return 0;
}

string mes( object me)
{
        return "口中唸著古老的咒語...";
}

int usesp() { return 0; }
int usemp() { return 10; }
int successful() { return 100; }
int delay() { return 1; }
int all() { return 0; }
string element() { return "water"; }

int check( object me)
{
        int i= 1;
        object fog;

        while( fog= present("fog "+ i, environment( me)))
        {
                if( fog->query("epower"))
                {
                        tell_object( me, "\n已經有霧存在了...\n");
                        return 0;
                }
                i++;
        }

        return 1;
}
