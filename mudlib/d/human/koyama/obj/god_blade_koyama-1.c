// god_blade_koyama-1.c
// written by koyamakyo

inherit EQUIP;

void create()
{
        set_name("[1;33m���M[1;36m�E[1;37m�p�s[m", ({ "god blade koyama", "god", "koyama", "blade", "god blade", "blade koyama" }));
        set("long", @LONG
�@��[1;37m�M�W[m�W�����z��[1;36m�H�~[m,
���Gĭ�õ�[1;37m�j�j�O�q[m��[1;33m���M[m.
�M�`�W�g��:

                 [1;33m[[1;37m�F�E��[m[1;33m][m
                                 �L�r
LONG
                );
        set("lv", 1);
        set("unit", "��");
        set("element", "lightning");
        set("epower", 100);
        set("weight", 500);

        set("wear_type", "hand");
        set("weapon", "blade");
        set("dem_eff", 50000);
        set("def_eff", 10000);
        set("mdem_eff", 2000);
        set("mdef_eff", 10000);
        set("spe_eff", 1000);
        set("realmx_eff", 50);
        set("eff", ({ "dem_eff", "def_eff", "mdem_eff", "mdef_eff", "spe_eff", "realmx_eff" }));
}
