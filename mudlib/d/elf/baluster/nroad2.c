// nroad2.c
// written by Hudson

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;32m�ڸ����S��D[m");
        set("long",@LONG
��o��X�G�O��Ӥڸ����S�̥_�誺�a�I�F,�A���_�ݥh,�O
���q���t�s�N����,�b�s�q���۳̱��񯫪�����,�|�P���O��
�y�᭻,�A�ɨ��۲M�n��,���B�b���Y.
�_��O���㩱�����S.
LONG
        );

        set("exits",
        ([
//       "north" : __DIR__ + "aliter",
        "west"  : __DIR__ + "nroad3",
        "east"  : __DIR__ + "nroad1",
        ])
        );

        set("objects", ([
                __DIR__ + "npc/wind_elf":1,
                __DIR__ + "npc/elf_citizen":1,
                __DIR__ + "npc/cat":2,
                ]));

        setup();
}
