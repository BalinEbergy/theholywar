// matt.c
// written by Mondale
// ���S

inherit NPC;

void create()
{
        int HP= 200000, MP= 25000, SP= 6000;

        set_name("[m[1m���S[m", ({ "matt" }));
        set("lv", 80);
        set("class","warrior");
        set("ra","�s��");
        set("gender","�k");
        set("describe","�@��y���ص۪��C,�y�W�S���b�I�������k�l.");
        set("pre","[m[33m�v[1m��[36m�C[32m�t[m");

        set("age", 423);
        set("element","lightning");
        set("mind", 400);

        set("mixHP", HP);
        set("currentHP", HP);
        set("mixMP", MP);
        set("currentMP", MP);
        set("mixSP", SP);
        set("currentSP", SP);
        set("dem", 6000);
        set("def", 35000);
        set("mdem", 40000);
        set("mdef", 40000);
        set("spe",2000);
        set("realmx", 0);
        set("exp", 100000);

        set("chat_chance", 20);
        set("chat_msg", ({
                "\n���S�ΥL�̲`�I�������ݵۧA.\n"
                }));
        setup();
        setup();
}

void init()
{
        ::init();
        if( !previous_object()
                || !userp(previous_object()))
                return;
        call_out("action", 1, previous_object());
}

void action( object obj)
{
        command("say �Q�n�ǲ߷Ҫ��N�h���ޯ��???");
        command("say ��ڴN�i�H�F...^^...");
        setup();
}
