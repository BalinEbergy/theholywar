// map_baluster.c
// Hudson

#include <element.h>

inherit ITEM;

void create()
{
    set_name("[1;32m巴路斯特地圖[m", ({ "map of baluster", "map" }));
    set("long", @LONG
           [1;33mH   E[m                E - 邪惡之路
           |   |                H - 神聖之路
       [1;33mA[m   +   +                A - 防具店
       |   |   |                S - 元老院
   [1;33mM[m +-+-+ +-[1;33mS[m-+                M - 魔法道具店
   | |   |   |                  C - 商業公會
   +-+   +-[1;33mC[m +                  L - 圖書館
   | |   |   |                  T - 道具店
   + + [1;33mT[m-+ +-+-+   [1;33mL[m            B - 銀行
   | |   | | | |   |            R - 餐廳
 +-+ +-+-+-+-[1;32m@[m-+-+-+            I - 旅店
 | |   |   | | |                W - 武器店
 + +-[1;33mR[m +-[1;33mB[m +-+-+                M.F - 楓之森林
 | |   |     |
 + +-+-+-[1;33mI[m   +                  @ - 艾爾芙水晶 魔法公會
 |     |     |
 +-[1;33mW[m   +-+-+-+-+ [1;33mM.F.[m
 |         |
 +-+-+-+-+-+
     |
  [1;32mBaluster[m
LONG
        );
    set("unit", "張");
    set("lv", 50);
    set("element", EARTH);
    set("epower", 50);
    set("value", 3);
    set("color", 1);

    set_weight( 5);
}
