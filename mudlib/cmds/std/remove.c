// remove.c

int main( object me, int argc, string *argv)
{
    object obj;
    string *equipp= ({
        "eq_light", "head", "neckr", "neckl", "body", "handr", "handl",
        "handb", "bracer", "bracel", "ringr", "ringl", "boot"
        });
    string position;

    if( argc< 2)
        write("你想要卸下什麼呢?\n");
    else if( !stringp( position= find_string( argv[1], equipp)) ||
        !objectp( obj= me->query_temp( position)))
        write("這個部分沒有裝備.\n");
    else if( !obj->unequip())
        write("你無法卸下這個裝備.\n");
    else message_vision("$N把"+ obj->name()+ "卸了下來.\n", me);

    return 1;
}

int help( object me)
{
    write( @HELP
指令格式: remove <部位>

照明: eq_light
頭部: head
項鍊: neckr, neckl
身體: body
手部: handr, handl, handb
手腕: bracer, bracel
戒指: ringr, ringl
腳部: boot

這個指令可以讓你卸下身上裝備的物品.
HELP
        );

    return 1;
}
