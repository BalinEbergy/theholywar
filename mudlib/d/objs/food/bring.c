// happy.c
// written by Mondale

#include <element.h>

inherit FOOD;

void create()
{
    object obj;
    set_name("攜型套餐\", ({ "bringing bar", "bringing", "bar" }));
    set("long", @LONG
與歡樂麵同為可保存很久的食物,量多味美,是旅行者的最愛~~
LONG
        );
    set("unit", "份");
    set("lv", 22);
    set("value", 16);

    set_amount( 7);

    set_weight( 20);
}
