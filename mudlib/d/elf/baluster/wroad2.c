// wroad2.c
// written by Hudson

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;32m�ڸ����S��D[m");
        set("long",@LONG
�o��O�s�������䪺��D,�|�P�ͪ��ۭZ�������,�[�W���
���y���q���y�L,��W�[�F���̪��۵M��,�ө����H��,���F
�~���~,�N�O�ǥ��}�]�k���H,�αR�|�۵M���ȫȤF.
LONG
        );

        set("exits",
        ([
        "south" : __DIR__ + "wroad6",
        "west"  : __DIR__ + "wroad3",
        "east"  : __DIR__ + "wroad1",
        ])
        );

        set("objects", ([
                __DIR__ + "npc/dog":1,
                __DIR__ + "npc/cat":3,
                ]));

        setup();
}
