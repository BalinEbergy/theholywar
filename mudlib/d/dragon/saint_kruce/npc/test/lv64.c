// lv64.c
// written by zz

inherit NPC;

void create()
{
        int HP= 10, MP= 0, SP= 100;

        set_name("蚌殼", ({ "lv64"}));
        set("lv", 64);
        set("ra","animal");
        set("gender","男");
        set("describe","一個蚌殼.");
        set("pre","隱藏在石頭之間的");
        set("age", 1);
        set("element","earth");
        set("mind", 0);

        set("mixHP", HP);
        set("currentHP", HP);
        set("mixMP", MP);
        set("currentMP", MP);
        set("mixSP", SP);
        set("currentSP", SP);
        set("dem", 1);
        set("def", 1);
        set("mdem", 7);
        set("mdef", 2);
        set("spe", 1);
        set("exp", 72960);

}
