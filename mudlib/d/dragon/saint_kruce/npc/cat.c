// cat.c
// written by zz

#include <class.h>
#include <element.h>
#include <race.h>

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
        int HP= 2000, MP= 0, SP= 1000;

        set_name("小貓", ({ "cat"}));
        set("lv", 8);
        set("race","animal");
        set("gender","動物");
        set("describe","一隻小貓.");
        set("pre","躲在一旁的");
        set("age", 3);
        set("element",EARTH);
        set("mind", 10);

        set("maxHP", HP);
        set("currentHP", HP);
        set("maxMP", MP);
        set("currentMP", MP);
        set("maxSP", SP);
        set("currentSP", SP);
        set("dem", 65);
        set("def", 40);
        set("mdem", 25);
        set("mdef", 25);
        set("spe", 50);
        set("exp", 1000);

        set("chat_chance", 7);
        set("chat_msg", ({
                (: random_move :),
                "小貓懶懶的伸著懶腰.\n",
                "小貓在你腳邊磨來磨去.\n",
                "'喵~~~喵~~~.'\n"
                }));
        setup();
}
