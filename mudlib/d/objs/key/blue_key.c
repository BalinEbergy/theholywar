// blue_key.c
// written by Bother

inherit ITEM;

void create()
{
        set_name("藍色的鑰匙", ({ "blue key", "blue", "key" }));
        set("long", @LONG
一把深藍的鑰匙
LONG
                );
        set("unit", "把");
        set("weight", 15);
        set("lock_name", "asdf");
        set("lv", 1);
        set("element", "holy");
        set("epower", 1);

        set("unsac", 1);
}
