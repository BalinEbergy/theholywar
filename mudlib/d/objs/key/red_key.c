// red_key.c
// written by Bother

inherit ITEM;

void create()
{
        set_name("紅色的鑰匙", ({ "red key", "red", "key" }));
        set("long", @LONG
一把血紅的鑰匙
LONG
                );
        set("unit", "把");
        set("weight", 12);
        set("lock_name", "qwer");
        set("lv", 1);
        set("element", "holy");
        set("epower", 1);

        set("unsac", 1);
}
