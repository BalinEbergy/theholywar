// life_music.c
// written by Bother

#include <skill.h>

void create() { seteuid( getuid()); }

int main( object me, object *obj)
{
        int i, p;
        object corpse, *invs;

        obj= all_inventory( environment( me));
        p= sizeof( obj);
        while( p--) {
        if( obj[p]->query("ghost")) 
        {
                if( !corpse= obj[p]->query_temp("corpse"))
                        corpse= new(CORPSE_OB);
                corpse->move( environment( me));
                message_vision("\n天空上降下$n的屍體, $N唱出一段聖樂, 瞬間散發出[1;33m萬道光芒!!![m", me, obj[p]);

                invs= all_inventory( corpse);
                i= sizeof( invs);
                while( i--)
                {
                        invs[i]->move( obj[p]);
                        obj[p]->add("under", invs[i]->query("weight"));
                }
                destruct( corpse);
                if( obj[p]->query("money")>= obj[p]->query("lv"))
                        obj[p]->add("money", -obj[p]->query("lv"));
                else if( obj[p]->query("exp")>= obj[p]->query("lv")* 48)
                        obj[p]->add("exp", -obj[p]->query("lv")* 48);
                else obj[p]->add("maxage", -1);
        
                obj[p]->set("currentHP", obj[p]->query("mixHP"));
                obj[p]->set("currentMP", obj[p]->query("mixMP"));
                obj[p]->set("currentSP", obj[p]->query("mixSP"));
                obj[p]->delete("corenv");
                obj[p]->delete("ghost");
                obj[p]->delete("invis");
        
                tell_object( obj[p], "\n你覺得身體又有了熟悉的感覺...\n");
                write("\n"+ obj[p]->name()+ "重生了.\n");

                return 1;
        }
        }
}

string name( int select)
{
        switch( select)
        {
                case 1: return "生命之樂";
                case 2: return "life music";
                case 3: return "life_music";
        }
}

int type() { return SONG; }
int lv() { return 60; }
int attackful()
{
        return 0;
}

string mes( object me)
{
        return "演唱出一段[1;33m迷幻[m的音樂,天空上射下無數[1;37m光芒[m,你覺得四周一片溫暖.";
}

int usesp() { return 120; }
int usemp() { return 80; }
int successful() { return 90; }
int delay() { return 6; }
int all() { return 0; }

int check( object me)
{
        return 1;
}
