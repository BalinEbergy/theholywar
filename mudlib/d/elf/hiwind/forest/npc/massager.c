// massager.c
// written by hiwind

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
    int HP= 2000000, MP= 5000, SP= 50000;

    set_name("[1;31m��[m[1;37m���Ϫ�[m", ({ "massager" }));
    set("lv", 75);
    set("class",WIZARD);
    set("race",ELF);
    set("describe","�����Ϫ�.");
    set("age", 100);
    set("element", WIND);
    set("goodat", EARTH);
    set("weakat", THUNDER);
    set("mind", 800);
    set("gender", 1);

    set("maxHP", HP);
    set("currentHP", HP);
    set("maxSP", SP);
    set("currentSP", SP);
    set("strength", 5000);
    set("def", 20000);
    set("mdem", 40000);
    set("mdef", 10000);
    set("spi", 25);
    set("speed", 1800);
    set("exp", 10000);

    set("chat_chance", 10);
    set("chat_msg", ({
        "�����Ϫ̴d�˪��C���Y.\n"
        }));
    setup();
}
void init()
{
    ::init();
    if( !previous_object()
        || !userp(previous_object()))
        return;
    call_out("action", 10, previous_object());
}

void action( object obj)
{
    if( !obj || !present(obj) || this_object()->is_fighting()) return;
    if( obj->is_fighting()) kill_ob(obj);
    if( obj->query("mind")<0) kill_ob(obj);
    command("say ���L�V�ӶV�p�F,�j�a���󤣬ñ��O?");
    setup();
}
