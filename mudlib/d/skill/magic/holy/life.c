// life.c
// written by Hudson

#include <skill.h>

void create() { seteuid( getuid()); }

int main( object me, object obj)
{
        int i;
        object corpse, *invs;

        if( !obj->query("ghost")) return 0;

        if( !corpse= obj->query_temp("corpse"))
                corpse= new(CORPSE_OB);
        corpse->move( environment( me));
        message_vision("\n天空上降下$n的屍體, $N唸完一串聖咒, 瞬間散發出[1;33m萬道光芒!!![m", me, obj);

        invs= all_inventory( corpse);
        i= sizeof( invs);
        while( i--)
        {
                invs[i]->move( obj);
                obj->add("under", invs[i]->query("weight"));
        }
        destruct( corpse);

        if( obj->query("money")>= obj->query("lv"))
                obj->add("money", -obj->query("lv"));
        else if( obj->query("exp")>= obj->query("lv")* 48)
                obj->add("exp", -obj->query("lv")* 48);
        else obj->add("maxage", -1);

        obj->set("currentHP", obj->query("mixHP"));
        obj->set("currentMP", obj->query("mixMP"));
        obj->set("currentSP", obj->query("mixSP"));
        obj->delete("corenv");
        obj->delete("ghost");
        obj->delete("invis");

        tell_object( obj, "\n你覺得身體又有了熟悉的感覺...\n");
        write("\n"+ obj->name()+ "復生了.\n");
        if( userp( me))
        {
                me->add("mind", obj->query("lv")/ 19);
                if( me->query("mind")> 1000) me->set("mind", 1000);
        }

        return 1;
}

string name( int select)
{
        switch( select)
        {
                case 1: return "甦生";
                case 2: return "life";
                case 3: return "life";
        }
}

int type() { return HOLY; }
int lv() { return 80; }
int attackful()
{
        return 0;
}

string mes( object me)
{
        return "唸出一段神聖的咒語,天空上射下一道[1;37m渾白色的光芒[m,你覺得四周一片溫暖.";
}

int usesp() { return 0; }
int usemp() { return 100; }
int successful() { return 90; }
int delay() { return 5; }
int all() { return 0; }

int check( object me)
{
        if( me->query("mind")< 800)
                return notify_fail("\n你無法放出如此神聖的力量...\n");

        return 1;
}
