// lake_5.c
// written by hiwind

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;36m��y[m");
        set("long",@LONG
[1;36m�p��[m�����Y�N�O[1;36m��y[m,�M�ӥѩ���`�O�j���۲H�H��[1;36m����[m,��
�H�`�O���H�ݱo���I�ҽk���M,��F��W,�~�o�{�����O�p����
�M��,�N�ϩ���l�@��.
LONG
        );

        set("exits",
        ([
        "west" : __DIR__ + "lake_2",
        "east" : __DIR__ + "lake_7",
        "south" : __DIR__ + "lake_4",
        "north" : __DIR__ + "lake_6",
        ])
        );

        set("objects",
        ([
                __DIR__ + "npc/fish1":1,
                __DIR__ + "npc/fish2":1,
        ])
        );
        set("no_fight", 0);
        setup();
}
