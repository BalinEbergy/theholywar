//c-2,-3.c
//written by zz

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;37m騎士隊總部營區[m");
        set("long",@LONG
這裡是皇家騎士隊總部的營區.皇家騎士隊是一支強大的軍隊,
每一個騎士都勇猛善戰.所有騎士都是從這個總部訓練出來的,
你看見四周都是正在接受訓練的見習騎士,看他們有模有樣的,
自己也開始手養了,趕快找個老師學習吧！
LONG
        );
        set("exits", ([
                "south" : __DIR__ + "c-2,-4.c" ,
                "east" : __DIR__ + "c-1,-3.c" ,
                ]));
        setup();
}
