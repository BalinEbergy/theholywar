// test.c
// written by Bother

#include <equip.h>
#include <element.h>

inherit ITEM;

void create()
{
        object obj;
        set_name("看不見的報時器", ({ "look_time" }));
        set("long", @LONG
一個看不見的報時器.
LONG
                );
        set("unit", "個");
        set("lv", 1);
        set("value", 0);
        set("weight", 100000);
        set("no_sac", 1);
        call_out("come", 10, obj);
        call_out("lll", 5, obj);
}
void come(object obj)
{
        tell_room(environment( this_object()), this_object()->short()+ "靠港了\n");
        call_out("go", 20, obj);
}
void go(object obj)
{       tell_room(environment( this_object()), short()+ "出發了\n");
        call_out("come", 10, obj);
} 
void lll(object obj)
{
        write("123");
}
