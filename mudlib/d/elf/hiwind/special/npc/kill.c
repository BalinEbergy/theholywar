// kill.c
// written by hiwind

inherit NPC;

void create()
{
        int HP= 600000, MP= 1000 ,SP= 50000;

        set_name("殺手", ({ "killer" }));
        set("lv", 100);
        set("class","warrior");
        set("ra","human");
        set("describe", "全身都染滿鮮血的殺手.");
        set("age", 40);
        set("mind", -1000);
        set("element", "earth");
        set("gender", "男");

        set("mixHP", HP);
        set("currentHP", HP);
        set("mixMP", MP);
        set("currentMP", MP);
        set("mixSP", SP);
        set("currentSP", SP);
        set("str", 65);
        set("dem", 60000);
        set("vir", 30);
        set("def", 25000);
        set("magic", 10);
        set("mdem", 5000);
        set("mdef", 10000);
        set("spirit", 15);
        set("speed", 30);
        set("spe", 3000);
        set("realmx", 200);
        set("exp", 0);

        seteuid( getuid());

        carry_object( OB_SWORD+ "sword")->wear();

        setup();
}
void init()
{
        if( !previous_object()
                || !userp(previous_object()))
                return;
        call_out("action", 10, previous_object());
        kill_ob( previous_object());
}

void action( object obj)
{
        if( !obj || !present(obj) || this_object()->is_fighting()) return;
        if( obj->is_fighting()) kill_ob(obj);

        command("hihi 讓你見識迴天斷龍斬的威力吧....");
}
