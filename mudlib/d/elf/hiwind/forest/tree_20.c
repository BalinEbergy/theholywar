//tree_20.c
//write by hiwind

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;37m�L��[m");
        set("long",@LONG
�A�u���i�J�F[1;31m��[m�L,�}�}���L���j�o[1;31m��[m���p�R�̯뭸
��,�𮧤@�U�a!!�o�O�p�����R�������r,���\����[1;31m��[m
�L.�߰_�@��[1;31m��[m�����O���a.
LONG
        );

        set("exits",
        ([
        "west" : __DIR__ + "tree_10",
        "south" : __DIR__ + "tree_19",
        "north" : __DIR__ + "tree_21",
        "east" : __DIR__ + "tree_26",
        ])                                                                      
        );
        set("objects",
        ([
                __DIR__ + "npc/rabbit":2,
                __DIR__ + "npc/shrewmouse":1,
                __DIR__ + "npc/bird":2,
                __DIR__ + "npc/wolf":1,
        ])
        );

        set("no_fight", 0);
        setup();
}
