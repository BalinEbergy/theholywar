// wolf_bark.c
// written by Bother

#include <skill.h>

string name( int i);
void die( object me, object victim);

int main( object me, object obj)
{
        int damage, m_mdem, o_mdef, m_lv, o_lv, k;
        string w_name= "琴";

        m_mdem= me->query("mdem")+ me->query("mdem_eff");
        o_mdef= me->query("mdef")+ me->query("mdef_eff");
        m_lv= me->query("lv");
        o_lv= obj->query("lv");
        k= me->skill_level( HARP)/ 25;

        damage= ( m_mdem* m_lv* k) / ( o_mdef* o_lv);
        damage= damage/ 4* 3+ random( damage/ 2);
        if( damage< 0)
                damage= random( 20);
        if( damage> 1000000)
                damage= random( 500000);

        message_vision("[1;35m$N撥動著手中的琴逐漸加速[m\n", me);        
        obj->add("currentHP", -damage);
        return COMBAT_D->skill_msg( me, obj, "豺狼的怒吼", w_name, damage);
}

string name( int select)
{
        switch( select)
        {
                case 1: return "豺狼的怒吼";
                case 2: return "wolf bark";
                case 3: return "wolf_bark";
        }
}

int type() { return HARP; }
int lv() { return 60; }
int attackful()
{
        return 1;
}

string mes( object me)
{
        return "的手指逐漸加速,途然間停了下來,你的[1;37m精神[m鬆懈了下來\n"
                +"在這個時候,他握著"+ me->query_temp("handb")->name()
                +"\n傳出一陣[1;33m怒吼[m,使你嚇了一跳\n";
}

int usesp() { return 120; }
int usemp() { return 40; }
int successful() { return 100; }
int delay() { return 4; }
int all() { return 1; }
string element() { return "holy"; }

int check( object me)
{
        return 1;
}
