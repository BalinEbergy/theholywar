// skill.c

inherit F_DBASE;
inherit F_NAME;

int usehp( object me) { return query("usehp"); }
int usemp( object me) { return query("usemp"); }
int usesp( object me) { return query("usesp"); }
int delay( object me) { return query("delay"); }
int check( object me) { return 1; }
int successful( object me) { return query("successful"); }
