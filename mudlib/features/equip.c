// equip.c

#include <equip.h>
#include <race.h>

static int eqqed= 0;
static int _both= 0;

int equipped() { return eqqed; }
int both() { return _both; }

void set_both()
{
    _both= 1;
}

void add_attribute( object obj, int sign)
{
    int i;
    string *att;

    att= this_object()->query("eff");

    i= sizeof( att);
    while( i--)
        obj->add_temp( att[i],
            sign*
            this_object()->query( att[i])*
            ( obj->query("race")== HUMAN? 6: 5)/ 5);
}

int equip()
{
    int type;
    object obj, tmp;
    string *position= ({
        "eq_light", "head", "neck", "body", "hand", "brace", "ring", "boot" });
    string *eq_msg= ({
        "照明", "頭部", "項鍊", "身體", "手部", "手腕", "戒指", "腳部" });

    if( equipped())
        return 0;

    obj= environment();
    type= this_object()->query("wear_type");
    if( type== HAND)
    {
        if( this_object()->query("weapon") && objectp( obj->query_temp("weapon")))
        {
            write("請先解除武器裝備.\n");
            return 0;
        }

        if( obj->query_temp("handb"))
        {
            write("請先解除手部裝備.\n");
            return 0;
        }

        if( both())
        {
            if( obj->query_temp("handr") || obj->query_temp("handl"))
            {
                write("請先解除手部裝備.\n");
                return 0;
            } else {
                obj->set_temp("handb", this_object());
                this_object()->set_temp("position", "handb");
            }
        } else if( !obj->query_temp("handr"))
        {
            obj->set_temp("handr", this_object());
            this_object()->set_temp("position", "handr");
        } else if( !obj->query_temp("handl"))
        {
            obj->set_temp("handl", this_object());
            this_object()->set_temp("position", "handl");
        } else {
            write("請先解除手部裝備.\n");
            return 0;
        }
    } else if( type== NECK || type== BRACE || type== RING)
    {
        if( !obj->query_temp( position[type]+ "r"))
        {
            obj->set_temp( position[type]+ "r", this_object());
            this_object()->set_temp("position", position[type]+ "r");
        } else if( !obj->query_temp( position[type]+ "l"))
        {
            obj->set_temp( position[type]+ "l", this_object());
            this_object()->set_temp("position", position[type]+ "l");
        } else {
            write("請先解除"+ eq_msg[type]+ "裝備.\n");
            return 0;
        }
    } else {
        if( !obj->query_temp( position[type]))
        {
            obj->set_temp( position[type], this_object());
            this_object()->set_temp("position", position[type]);
        } else {
            write("請先解除"+ eq_msg[type]+ "裝備.\n");
            return 0;
        }
    }

    eqqed= 1;
    if( this_object()->query("weapon"))
        obj->set_temp("weapon", this_object());

    add_attribute( obj, 1);

    return 1;
}

varargs int unequip( int forced)
{
    object obj;

    eqqed= 0;
    obj= environment();
    if( this_object()->query("weapon"))
        obj->delete_temp("weapon");
    obj->delete_temp( this_object()->query_temp("position"));
    this_object()->delete_temp("position");

    add_attribute( obj, -1);
    
    return 1;
}
