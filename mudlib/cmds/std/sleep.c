// sleep.c
// written by Hudson

int main( object me, int argc, string *argv)
{
    if( me->is_ghost() || me->is_fighting())
        write("你現在無法睡覺.\n");
    else if( me->query("rest")> 1)
        write("你已經在睡覺了.\n");
    else if( me->query("meditation"))
    {
        message_vision("$N就地進入冥想狀態...\n", me);
        me->set("rest", 3);
    }
    else {
        message_vision("$N就地躺下,不一會兒便睡著了.\n", me);
        me->set("rest", 2);
    }

    return 1;
}

int help( object me)
{
    write(@HELP
指令格式: sleep

這個指能讓玩家進入睡眠狀態,若學會冥想,則睡眠會以冥想代替.
睡眠時,各項屬性都會恢復較快,但無法行使某些動作.
HELP
    );

    return 1;
}
