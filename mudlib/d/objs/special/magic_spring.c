// magic_spring.c
// written by Hudson

inherit ITEM;

void create()
{
        set_name("魔法泉水", ({ "magic spring", "magic", "spring" }));
        set("long", @LONG
一個用魔法造出的泉水.
LONG
                );
        set("unit", "個");
        set("d_water", 1);
        set("no_get", 1);
        set("unsac", 1);
}

void disappear( object obj)
{
        message_vision("$N消失了.", obj);
        destruct( obj);
}
