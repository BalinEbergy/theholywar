//forest_3.c
//write by hiwind

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;37m�L��[m");
        set("long",@LONG
�A�i�J�F�t�@�Ӵ�[1;32m�L[m,�}�}���L���j�o�𸭦p�R�̯뭸��,��
���@�U�a!!�uť���䦳�𸭳Q���j�o�F�F�@�T,�Pı�Q�����P
,�𮧤@�U�a!
LONG
        );

        set("exits",
        ([
        "east" : __DIR__ + "forest_6",
        "south" : __DIR__ + "forest_2",
        ])
        );

        set("objects",
        ([
                __DIR__ + "npc/rabbit":1,
                __DIR__ + "npc/shrewmouse":1,
                __DIR__ + "npc/bird":2,
        ])
        );

        set("no_fight", 0);
        setup();
}
