//test4.c
//written by zz

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;37mtest4[m");
        set("long",@LONG
test Lv41-50
LONG
        );
        set("exits", ([
                "north" : __DIR__ + "test3.c" ,
                "south" : __DIR__ + "test5.c" ,
                ]));
        set("objects",  ([
                __DIR__ + "npc/test/lv41" :1 ,
                __DIR__ + "npc/test/lv42" :1 ,
                __DIR__ + "npc/test/lv43" :1 ,
                __DIR__ + "npc/test/lv44" :1 ,
                __DIR__ + "npc/test/lv45" :1 ,
                __DIR__ + "npc/test/lv46" :1 ,
                __DIR__ + "npc/test/lv47" :1 ,
                __DIR__ + "npc/test/lv48" :1 ,
                __DIR__ + "npc/test/lv49" :1 ,
                __DIR__ + "npc/test/lv50" :1 ,
        ]));
        set("no_recall", 1);
        setup();
}
