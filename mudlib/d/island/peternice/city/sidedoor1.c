// sidedoor1.c
// written by peternice

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;36m米德加德城門口[m");
        set("long",@LONG
　　眼前望去的，是一些些座落於平原上的民房。驚訝於所見之城，
木造的建築每每及是，配合著平原上的綠地，此情此景儼然便融合於
其後的山林之中，不同於一般的城市，少了給人隔離冷漠的高牆石木，
取而代之的是只有一般人高的圍籬作區別，該是信任吧，融合於自然之
中的感覺。
LONG
        );

        set("exits",
        ([
        "north" : __DIR__ + "sidedoor2",
        ])
        );
}
