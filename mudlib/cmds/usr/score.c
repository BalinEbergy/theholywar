// score.c
// written by Hudson

// #include <selfskill.h>
#include <element.h>
// string selfskill( int check);

int main( object me, int argc, string *argv)
{
    int temp;
    string str;
    mapping dbase;

    seteuid( getuid( me));
    dbase= me->query_entire_dbase();

    str= sprintf("\n"
"%s (%s) %d 歲 %s  LV %d %s",
dbase["name"], me->query_id(), dbase["age"],
dbase["title"], dbase["lv"], dbase["gender"]? "男": "女");

/*    if( dbase["selfsk")!= 0)
        str+= sprintf(" [%s]", selfskill( dbase["selfsk")));*/

    str+= sprintf(
"\n\n種族 [%s] 職業 [%s] 屬性 [%s][%s]\n",
D_STATUS->ra( dbase["race"]), D_STATUS->job( dbase["class"]),
D_STATUS->ele( dbase["element"], 1), D_STATUS->mind( dbase["mind"]));

//        str+= sprintf("體重 [%d]", dbase["weight"));
    if( dbase["nick"]) str+= " 頭銜 [" + dbase["nick"] + "[m]";

    str+= sprintf(
"\n"
"生命 [%s%5d[m/[1;37m%5d[m] [7m%s\n"
"法力 [%s%5d[m/[1;36m%5d[m] [44m%s\n"
"體力 [%s%5d[m/[1;33m%5d[m] [42m%s\n"
"\n"
"力量 [1;31m%2d[m 攻擊力 [1;31m%5d[m | 體格 [1;37m%2d[m 防禦力 [1;37m%5d[m\n"
"智慧 [1;34m%2d[m 魔  力 [1;34m%5d[m | 精神 [1;33m%2d[m 靈  力 [1;33m%5d[m\n"
"敏捷 [1;36m%2d[m 速  度 [1;36m%5d[m | 學習點數 [1;35m%d[m 額外點數 [1;35m%d[m\n"
"\n"
"承受的重量 [ [1;31m%4d[m/ [31m%4d[m] 饑餓度 [ [1;33m%3d[m/ [33m%3d[m]\n"
"使用的魔靈 [ [1;34m%4d[m/ [34m%4d[m] 口渴度 [ [1;36m%3d[m/ [36m%3d[m]\n"
"身上物品數 [ [1;32m%4d[m/ [32m%4d[m]\n"
"\n"
"經驗值 [1;36m%d[m/ [36m%d[m 財產 [1;33m%d[m\n"
"%s\n"
"\n",
D_STATUS->color( temp= dbase["currentHP"]* 100/ dbase["maxHP"]),
dbase["currentHP"], dbase["maxHP"], D_STATUS->prin( temp),
D_STATUS->color( temp= dbase["currentMP"]* 100/ dbase["maxMP"]),
dbase["currentMP"], dbase["maxMP"], D_STATUS->prin( temp),
D_STATUS->color( temp= dbase["currentSP"]* 100/ dbase["maxSP"]),
dbase["currentSP"], dbase["maxSP"], D_STATUS->prin( temp),
dbase["str"], dbase["strength"]+ me->query_temp("dem_eff")+
    ( objectp( me->query_temp("weapon"))? me->query_temp("weapon")->query("damage"): 0),
dbase["con"], dbase["armor"]+ me->query_temp("def_eff"),
dbase["int"], dbase["magic"]+ me->query_temp("mdem_eff"),
dbase["spi"], dbase["spi"]+ me->query_temp("mdef_eff"),
dbase["dex"], dbase["speed"]+ me->query_temp("spe_eff"),
dbase["lpoint"], dbase["addpoint"],
me->query_encumbrance(), me->query_max_encumbrance(),
dbase["currentHungry"], dbase["maxHungry"],
dbase["munder"], dbase["magic"],
dbase["currentThirsty"], dbase["maxThirsty"],
me->query_carried_object(), me->query_max_carried_object(),
dbase["exp"], D_LEVEL->query_exp( dbase["lv"]), dbase["money"],
dbase["describe"]);

    write( str);

    return 1;
}
/*
string magic( object ob)
{
        int temp, defand, *magic_d= ({ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 });
        string str;

        str= "[1;37;44m你現在所受的影響                                                               [m\n\n";

// meditation
        if( dbase["sleep")!= 2)
                str+= "[36m";
        else str+= "[1;37m";
        str+= sprintf("%15s", "冥想[m");
// earth shield
        if( !( temp= dbase["e_shieldt")))
                str+= "[36m";
        else if( temp< 5)
                str+= "[1;31m";
        else if( temp< 20)
                str+= "[1;33m";
        else str+= "[1;37m";
        str+= sprintf("%15s", "土盾術[m");
// blind
        if( !( temp= dbase["blindt")))
                str+= "[36m";
        else if( temp< 5)
                str+= "[1;31m";
        else if( temp< 20)
                str+= "[1;33m";
        else str+= "[1;37m";
        str+= sprintf("%15s", "失明[m");
// holy power
        if( !( temp= dbase["h_powert")))
                str+= "[36m";
        else if( temp< 5)
                str+= "[1;31m";
        else if( temp< 20)
                str+= "[1;33m";
        else str+= "[1;37m";
        str+= sprintf("%15s", "聖靈力[m");
// cosmo wall
        if( !( temp= dbase["c_wallt")))
                str+= "[36m";
        else if( temp< 5)
                str+= "[1;31m";
        else if( temp< 20)
                str+= "[1;33m";
        else str+= "[1;37m";
        str+= sprintf("%15s", "空間障礙[m");
// sky shield
        if( !( temp= dbase["s_shieldt")))
                str+= "[36m";
        else if( temp< 5)
                str+= "[1;31m";
        else if( temp< 20)
                str+= "[1;33m";
        else str+= "[1;37m";
        str+= sprintf("%15s", "天之盾[m");
        str+= "\n";

// magic shield
        if( !( temp= dbase["m_shieldt")))
                str+= "[36m";
        else if( temp< 5)
                str+= "[1;31m";
        else if( temp< 20)
                str+= "[1;33m";
        else str+= "[1;37m";
        str+= sprintf("%15s", "魔法風盾[m");
// light prison
        if( !( temp= dbase["l_prisont")))
                str+= "[36m";
        else if( temp< 3)
                str+= "[1;31m";
        else if( temp< 6)
                str+= "[1;33m";
        else str+= "[1;37m";
        str+= sprintf("%15s", "光明之牢[m");
// wining music
        if( !( temp= dbase["wining_mt")))
                str+= "[36m";
        else if( temp< 3)
                str+= "[1;31m";
        else if( temp< 6)
                str+= "[1;33m";
        else str+= "[1;37m";
        str+= sprintf("%15s", "勝利之樂[m");
// curse sound
        if( !( temp= dbase["curse_st")) || !( temp= dbase["curse_dem_st")) || 
                !( temp= dbase["curse_def_st")) || !( temp= dbase["curse_mdem_st")) ||
                !( temp= dbase["curse_mdef_st")) || !( temp= dbase["curse_spe_st")) ||
                !( temp= dbase["no_recall")))
                str+= "[36m";
        else if( temp< 3)
                str+= "[1;31m";
        else if( temp< 6)
                str+= "[1;33m";
        else str+= "[1;37m";
        str+= sprintf("%15s", "詛咒之音[m");
// holy blest
        if( !( temp= dbase["holy_bt")))
                str+= "[36m";
        else if( temp< 3)
                str+= "[1;31m";
        else if( temp< 6)
                str+= "[1;33m";
        else str+= "[1;37m";
        str+= sprintf("%15s", "神聖的祝福[m");
        str+= "\n";

str+= sprintf("\n[1;36;44m你現在所受影響的效果                                                           [m\n\n");

        defand= dbase["defand_eff");
        str+= sprintf("[1;37m%10s[35m%8d[m", "防護力", defand);

        magic_d[HOLY-1]+=       0;
        magic_d[EVIL-1]+=       0;
        magic_d[LIGHT-1]+=      0;
        magic_d[WIND-1]+=       dbase["wind_eff");
        magic_d[COSMOS-1]+=     0;
        magic_d[THUNDER-1]+=    dbase["lightning_eff");
        magic_d[FIRE-1]+=       dbase["fire_eff");
        magic_d[WATER-1]+=      dbase["water_eff");
        magic_d[EARTH-1]+=      dbase["earth_eff");
        magic_d[DARK-1]+=       0;

        str+= sprintf("[1;37m%10s[36m%8d[m", "聖防護力", magic_d[HOLY-1]);
        str+= sprintf("[1;37m%10s[36m%8d[m", "冥防護力", magic_d[EVIL-1]);
        str+= sprintf("[1;37m%10s[36m%8d[m", "光防護力", magic_d[LIGHT-1]);

        str+="\n";

        str+= sprintf("[1;37m%10s[36m%8d[m", "風防護力", magic_d[WIND-1]);
        str+= sprintf("[1;37m%10s[36m%8d[m", "天防護力", magic_d[COSMOS-1]);
        str+= sprintf("[1;37m%10s[36m%8d[m", "雷防護力", magic_d[THUNDER-1]);
        str+= sprintf("[1;37m%10s[36m%8d[m", "火防護力", magic_d[FIRE-1]);

        str+="\n";

        str+= sprintf("[1;37m%10s[36m%8d[m", "水防護力", magic_d[WATER-1]);
        str+= sprintf("[1;37m%10s[36m%8d[m", "地防護力", magic_d[EARTH-1]);
        str+= sprintf("[1;37m%10s[36m%8d[m", "闇防護力", magic_d[DARK-1]);

        str+= "\n";

        str+= "\n";

        return str;
}

string selfskill( int check)
{
        switch( check)
        {
                case LIGHTN:     return "[1;37m閃[m";
                case DEFI:
                case DEFII:     return "[31m擋[m";
                case SPIRIT:    return "[36m魂[m";
                case HOLYSELF:  return "[1;37m聖[m";
                case DESTORY:   return "[1;35m破[m";
                case REGAN:     return "[1;32m回[m";
                case LUCKY:     return "[1;33m運[m";
                case CACHET:    return "[33m封[m";
                case SLOW:      return "[32m遲[m";
                case EVILSWORD: return "[1;31m妖[m";
                case INV:       return "[35m隱[m";
                case FLY:       return "[1;36m翔[m";
                case DOWN:      return "[1;42;37m擊[m";
                case INPOSSIBLE:return "[1;33m蹟[m";
                case THR:       return "[1;37;46m行[m";
                case LEARN:     return "[1;37;44m學[m";
                case TRANS:     return "[36m送[m";
                case SUMMONS:   return "[1;33m召[m";
                case GODEN:     return "[1;33;44m神示錄[m";
        }
}
*/
int help()
{
    write( @HELP
指令格式: score

可查看自己狀態
HELP
    );

    return 1;
}
