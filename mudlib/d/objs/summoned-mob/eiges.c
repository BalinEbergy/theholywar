// eiges.c
// written by Hudson
// 炎蟲 亞吉斯

#include <element.h>

inherit SUM_MOB;

void create()
{
    set_name("亞吉斯", ({ "eiges" }));
    set("race", SAINTRE);
    set("gender", 1);
    set("describe","一隻全身散發金色光芒的飛蟲.");
    set("pre","炎蟲");

    set("age", 1000);
    set("element", FIRE);
    set("fly", 1);

    setup();

    call_out("disappear", 100);
}
