//lakeside_18.c
//write by hiwind

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1m�߾��W[m");
        set("long",@LONG
�o��s��[1;36m��[m,�A�@�Q�즹���T�h�F�@�U,�P��O[1;31m��[m�L��¶,�o�O
��[1;36m��[m,�O�W�Ѫ���N�w����...�A�]���T���M,���L�o��O�@��
�ר��i�ʪ��n�a��.
�j�۵M���𮧤��_����¶�b�A����,�uı�o��~�κZ.�u�����B
���@�y��,���U�N�O�y�J�ߴ򪺻��դ��e,�@���M�����p�˽w�w
�ѥ_�V�n�Ө�.
LONG
        );

        set("exits",
        ([
        "west" : __DIR__ + "lakeside_14",
        "north" : __DIR__ + "river/river_9",
        "south" : __DIR__ + "lakeside_17",
        ])
        );

        set("objects",
        ([
                __DIR__ + "npc/frog":1
        ])
        );

        set("no_fight", 0);
        setup();
}
