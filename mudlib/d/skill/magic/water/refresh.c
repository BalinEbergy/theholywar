// refresh.c
// written by Hudson

#include <skill.h>

int main( object me, object obj)
{
        int refpoint= 100, ori= 5;

        ori= ori* me->skill_level( 8)* me->query("magic")/ 50;
        refpoint+= ori;
        if( me->query("element")== "water")
                refpoint*= 1.2;
        if( obj->query("element")== "fire")
                refpoint*= 0.8;

        tell_object( obj, "\n你覺得全身充滿了精力!\n");

        return COMBAT_D->heal_func( obj, "SP", refpoint);
}

string name( int select)
{
        switch( select)
        {
                case 1: return "體力恢復術";
                case 2: return "refresh";
                case 3: return "refresh";
        }
}

int type() { return WATER; }
int lv() { return 15; }
int attackful()
{
        return 0;
}

string mes( object me)
{
        return "手上聚集了些[1;32m霧氣[m.";
}

int usesp() { return 0; }
int usemp() { return 50; }
int successful() { return 75; }
int delay() { return 2; }
int all() { return 0; }
string element() { return "water"; }

int check( object me)
{
        return 1;
}
