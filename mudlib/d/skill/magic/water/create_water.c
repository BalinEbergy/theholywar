// create_water.c
// written by Hudson

#include <skill.h>

void create() { seteuid( getuid()); }

int main( object me, object obj)
{
        object spring;

        spring= new( OB_SPECIAL+ "magic_spring");
        spring->move( environment( me));
        call_out( "disappear", me->skill_level( WATER)* 10, spring);

        message_vision("一個魔法泉水出現在$N的眼前!!\n", me);

        return 1;
}

void disappear( object spring)
{
        spring->disappear( spring);
}

string name( int select)
{
        switch( select)
        {
                case 1: return "水創造術";
                case 2: return "create water";
                case 3: return "create_water";
        }
}

int type() { return WATER; }
int lv() { return 5; }
int attackful()
{
        return 0;
}

string mes( object me)
{
        return "喃喃唸著一串咒語,四周的濕度漸漸提升...";
}

int usesp() { return 0; }
int usemp() { return 150; }
int successful() { return 70; }
int delay() { return 2; }
int all() { return 0; }
string element() { return "water"; }

int check( object me)
{
        return 1;
}
