// cure.c
// written by Hudson

#include <skill.h>

int main( object me, object obj)
{
        int curepoint= 110, ori= 10;

        ori= ori* me->skill_level( 8)* me->query("magic")/ 50;
        curepoint+= ori;
        if( me->query("element")== "water")
                curepoint= curepoint* 1.2;
        if( obj->query("element")== "fire")
                curepoint= curepoint* 0.8;

        tell_object( obj, "\n你覺得身體的傷恢復了!\n");

        return COMBAT_D->heal_func( obj, "HP", curepoint);
}

string name( int select)
{
        switch( select)
        {
                case 1: return "恢復術";
                case 2: return "cure";
                case 3: return "cure";
        }
}

int type() { return WATER; }
int lv() { return 1; }
int attackful()
{
        return 0;
}

string mes( object me)
{
        return "手上聚集了些[1;36m霧氣[m.";
}

int usesp() { return 0; }
int usemp() { return 22; }
int successful() { return 80; }
int delay() { return 2; }
int all() { return 0; }
string element() { return "water"; }

int check( object me)
{
        return 1;
}
