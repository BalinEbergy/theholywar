// center.c
// written by Hudson

#include <room.h>

inherit ROOM;

void create()
{
        set("short","*[1;37m........*[m");
        set("long",@LONG
........
LONG
        );

        set("exits",
        ([
        "east" : __DIR__ + "testn4e2",
        "south" : __DIR__ + "testn3e1"
        ])
        );
