// sckused.c
// written by Hudson
// 時古斯都

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
    int HP= 330000, MP= 39000, SP= 4300;

    set_name("時古斯都", ({ "sckused" }));
    set("lv", 88);
    set("class",WIZARD);
    set("race",ELF);
    set("gender",1);
    set("describe","一位身上散發著強烈邪氣的巫師.");
    set("pre","邪冥法師");

    set("age", 600);
    set("element",EVIL);
    set("mind", -999);

    set("maxHP", HP);
    set("currentHP", HP);
    set("maxMP", MP);
    set("currentMP", MP);
    set("maxSP", SP);
    set("currentSP", SP);
    set("strength", 20000);
    set("armor", 30000);
    set("magic", 43000);
    set("spi", 33);
    set("speed", 2500);
    set("exp", 260000);

    setup();
}

void init()
{
    object obj;

    if( !(obj= previous_object())
        || !userp( obj))
        return;

    call_out("ki", 10, obj);
    call_out("action", 20, obj);
}

void action( object obj)
{
    if( !obj || !present(obj) || this_object()->is_fighting()) return;
    command("say 邪念是一切萬物運作的原因,因為有了念,才有行為...記著吧...");
}

void ki( object obj)
{
    if( obj->is_fighting()) kill_ob( obj);
}
