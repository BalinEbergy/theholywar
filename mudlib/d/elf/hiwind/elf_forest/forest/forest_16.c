//forest_16.c
//write by hiwind

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;37m�L��[m");
        set("long",@LONG
���}�F�L�����D,���i�F[1;33;44m���F[m[1;32m�˪L[m���L��,���B��[1;31m��[m[1;32m�L[m������
�j���ۦP,���M���O�p������,����o�Q�����A��.�ѩ�L�즳
�ǭZ�K,�ҥH�����u��q�@�ǵ}�\�����_���S�F�X��,�򤧫e
���L�������,�𽮤U�N��o�Q�������D.
�_��O�L�����D.
LONG
        );

        set("exits",
        ([
        "west" : __DIR__ + "forest_12",
        "east" : __DIR__ + "forest_21",
        "south" : __DIR__ + "forest_15",
        "north" : __DIR__ + "forest_17",
        ])
        );

        set("objects",
        ([
                __DIR__ + "npc/eagle":1,
                __DIR__ + "npc/deer":1,
                __DIR__ + "npc/magic":1,
        ])
        );
        set("no_fight", 0);
        setup();
}
