// ngroad2.c
// written by Hudson

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;32m�ڸ����S�t�D[m");
        set("long",@LONG
�o��O�ڸ����S�t�D,�b��,�D��������Ӥ�V,���F�O���c��
��,����h�O���t����,�W��h�O�ڸ����S�̰�����,���Ѱ|.
LONG
        );

        set("exits",
        ([
        "east"  : __DIR__ + "evilroad1",
        "up"    : __DIR__ + "mageguild/senatec",
        "south" : __DIR__ + "ngroad1",
        "west"  : __DIR__ + "holyroad1",
        ])
        );
        set("objects", ([
                __DIR__ + "npc/wind_elf": 1,
                OB_OTHER + "fountain": 1,
                ]));

        setup();
}
