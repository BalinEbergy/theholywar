// regenesis.c
// written by hiwind

#include <skill.h>

inherit FIRE_MAGIC;

void create()
{
    set("name", "�D������");
    set("id", "regenesis");
    set("lv", 20);
    set("attack", 0);
    set("usemp", 1);
    set("delay", 2);
    set("successful", 50);
    set("cost", 40);
    set("exp", 42);

    set("special_process", 1);

    setup();
}

int main( object me, object obj)
{
    if( random( 99) > 99)
    {
        tell_object( obj, "[1;37m����[m[1;5;31m�P��[m�]��ۧA,���Mı�o[1;37m�N�ӤO[m�����P���ܿ�.\n");

        obj->die( me);
    }
    else
    { 
        tell_object( obj, "[1;37m����[m[1;5;31m�P��[m�]��ۧA,[1;31m�P�K[m�ϧA�P��F�ŷx.\n");

        obj->set("currentHP", obj->query("maxHP"));
        obj->set("currentMP", obj->query("maxMP"));
        obj->set("currentSP", obj->query("maxSP"));
    }
}

string mes( object me)
{
    return "$N��W���_�o�X[1;31m����[m, ����, �@��[1;31;47m�����[m���D�ӥX.";
}
