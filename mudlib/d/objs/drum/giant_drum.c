// giant_drum.c
// written by Hudson

#include <equip.h>
#include <element.h>

inherit EQUIP;

void create()
{
        set_name("巨人之鼓", ({ "giant drum", "giant", "drum" }));
        set("long", @LONG
一個極巨大的鼓,表面似乎是由世界木所製,是由巨人族所做的大鼓.
LONG
                );
        set("lv", 80);
        set("unit", "個");
        set("element", EARTH);
        set("epower", 80);
        set("weight", 170);
        set("value", 100);

        set("wear_type", HAND);
        set("weapon", DRUM);
        set("mdef_eff", 5321);
        set("eff", ({ "mdef_eff", }));

        set_both();
}
