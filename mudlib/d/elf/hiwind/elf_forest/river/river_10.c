// river_10.c
// written by hiwind

#include <room.h>


inherit ROOM;

void create()
{
        set("short","[1;36m�p��[m");
        set("long",@LONG
�o�O�@���M�����p��,�O���F�˪L���@�Ӵ�y���u��,�u���e��
�w�w�o�V�F��y�h,�γ\�]���O�������Y,�|�P���۲H�H����
,�ϱo����a���a�g����.
���B�e�����䳣�������Z�Ӫ��m,�n�_�賣�O��a.
LONG
        );

        set("exits",
        ([
        "west" : __DIR__ + "river_11",
        "east" : __DIR__ + "river_9",
        "south" : "d/elf/hiwind/elf_forest/grass_2",
        "north" : "d/elf/hiwind/elf_forest/grass_3",
        ])
        );

        set("objects",
        ([
                __DIR__ + "npc/fish1":1
        ])
        );
        set("no_fight", 0);
        setup();
}
