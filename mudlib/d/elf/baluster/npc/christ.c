// christ.c
// written by Hudson
// 卡利斯特

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
    int HP= 280000, MP= 36000, SP= 4000;

    set_name("卡利斯特", ({ "christ" }));
    set("lv", 88);
    set("class", WIZARD);
    set("race", ELF);
    set("gender", 1);
    set("describe","一位半閉著雙眼的老人.");
    set("pre","神聖法師");

    set("age", 472);
    set("element", HOLY);
    set("mind", 999);

    set("maxHP", HP);
    set("currentHP", HP);
    set("maxMP", MP);
    set("currentMP", MP);
    set("maxSP", SP);
    set("currentSP", SP);
    set("strength", 7000);
    set("armor", 27000);
    set("magic", 41000);
    set("spi", 30);
    set("speed", 2200);
    set("exp", 247000);

    setup();
}

void init()
{
    object obj;

    if( !(obj= previous_object())
        || !userp( obj))
        return;

    call_out("ki", 10, obj);
    call_out("action", 20, obj);
}

void action( object obj)
{
    if( !obj || !present(obj) || this_object()->is_fighting()) return;
    command("say 別忘了, 神對這個世界的恩賜是什麼... 呵呵...");
}

void ki( object obj)
{
    if( obj->is_fighting()) kill_ob( obj);
}
