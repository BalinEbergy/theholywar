// call_li.c
// written by Hudson

#include <skill.h>

void create() { seteuid( getuid()); }

int main( object me, object obj)
{
        object lightning;

        lightning= new( OB_SPECIAL+ "lightning");
        lightning->set("lv", me->skill_level( SUMMON));
        lightning->move( environment( me));
        lightning->set("epower", lightning->query("lv"));
        call_out( "disappear", me->skill_level( SUMMON)* 10, lightning);

        tell_room( environment( me), "天空上劈下數道閃電!!\n");
        environment( me)->set("lightning", lightning->query("lv"));

        return 1;
}

void disappear( object lightning)
{
        lightning->disappear( lightning);
}

string name( int select)
{
        switch( select)
        {
                case 1: return "雷召喚術";
                case 2: return "thunder";
                case 3: return "call_li";
        }
}

int type() { return SUMMON; }
int lv() { return 1; }
int attackful()
{
        return 0;
}

string mes( object me)
{
        return "口中唸著古老的咒語...";
}

int usesp() { return 0; }
int usemp() { return 10; }
int successful() { return 100; }
int delay() { return 1; }
int all() { return 0; }
string element() { return "lightning"; }

int check( object me)
{
        int i= 1;
        object lightning;

        while( lightning= present("lightning "+ i, environment( me)))
        {
                if( lightning->query("epower"))
                {
                        tell_object( me, "\n已經有雷電存在了...\n");
                        return 0;
                }
                i++;
        }

        return 1;
}
