// makas.c
// written by Hudson

inherit NPC;

void create()
{
        int HP=80000, MP= 0, SP= 8000;

        set_name("測試者", ({ "makas" }));
        set("lv", 50);
        set("class","warrior");
        set("ra","elf");
        set("gender","男");
        set("describe","米契爾道具店巴路斯特總店的老闆.");
        set("pre", "米契爾道具店老闆");

        set("age", 325);
        set("element", "wind");
        set("mind", 0);

        set("mixHP", HP);
        set("currentHP", HP);
        set("mixSP", SP);
        set("currentSP", SP);
        set("dem", 10000);
        set("def", 8000);
        set("mdem", 1200);
        set("mdef", 5000);
        set("spe", 990);
        set("exp", 2500);

        set("trading", 1);
        set("buy_mind", "-999~1000");
        set("selling", ({
                OB_OTHER + "meat.c",
                OB_OTHER + "bread.c",
                }));

        setup();
}
