// lightningdoor.c
// written by Hudson

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;33;44m�p����[m");
        set("long",@LONG
�o��u�O�@�Ӽ��x���a��,�P��L�]�k�����@�˪��O,�o�ణ
�F�@��j�k�v���~,�٦��\�h���p���F�̦b�o��]�Ӷ]�h��,
�A�@����L��,���T�b�F�@�U,�u���p�k�v�U���˯��۹�A��
�ۤ�,���G�Ʊ�A���n���N�L�̪��x��,�|���W�O�@�D�s����
�Ϯסйp����.
LONG
        );

        set("exits",
        ([
        "up" : "d/elf/baluster/holyroad3",
        ])
        );

        set("objects", ([
                "d/elf/baluster/npc/wallson": 1,
                "d/elf/baluster/npc/lightning_elf": 3,
                "d/elf/baluster/npc/fire_elf": 2,
                "d/elf/baluster/npc/water_elf": 4,
                "d/elf/baluster/npc/wind_elf": 3,
                "d/elf/baluster/npc/earth_elf": 1,
                ]));

        setup();
}
