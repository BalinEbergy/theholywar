// wind.c
// written by hiwind

#include <skill.h>

int main( object me, object obj)
{
        int demage, dem, def, me_spe, i, obj_spe, damage, str_vir, hit, wind, me_spe_eff;
        string weapon_name;

        weapon_name=( me->query_temp( me->query_temp("bla")))->query("name");
        dem= me->query("dem")+ me->query("dem_eff");
        def= obj->query("def")+ obj->query("def_eff");
        me_spe= ( me->query("spe")+ me->query("spe_eff"));
        me_spe_eff= ( me->query("spe")+ me->query("spe_eff"))* 2.5;
        obj_spe= obj->query("spe")+ obj->query("spe_eff");
        str_vir= me->query("str")-obj->query("vir");

        i= (me->query("speed")/ 10);

        demage= ( ( dem-( def* obj_spe/ me_spe))/3 
                -obj->query("defand_eff")+ random( me_spe) );

        me->set("winds_time", 0);

        if( i< 2)
        {
                message_vision("$N的速度過於緩慢,導致$N的身影全被$n識破.\n", me, obj);
                obj->add("currentHP", damage/10);
                return damage;
        } else
        {

                if( me->query("dragon_time")!= 3)
                {
                        message_vision("$N迅速向$n衝去,其速度之快,使得$n無法判斷$N的身影.\n", me, obj);
                } else
                {
                        message_vision("$N以極為快速的刀法不斷在$n面前狂砍,$n完全無法抵禦.\n", me, obj);
                }

                while( i--)
                {
                        if( random( me_spe)< obj_spe)
                        {
                                message_vision("$N手持"+ weapon_name+ 
                                "奮力向$n砍去,但被$n閃掉了.\n", me, obj);
                        } else
                        {
                                if( demage> 1)
                                        damage= demage;
                                else
                                        damage= random( me_spe)/ 2.5;

                                obj->add("currentHP", -damage);
                                me->add("winds_time", 1);
                COMBAT_D->skill_msg( me, obj, "疾風斬", weapon_name, damage);
                        }
                }

                if( me->query("blade_lv")== 100)
                {
                        if( me->query("dragon_time")!= 3)
                        {
                                i= damage* me->query("winds_time");
                                me->set("me_i_winds", i);
                                return -1;
                        } else
                        {
                                wind= me->query("me_i_winds")
                                + damage* me->query("winds_time");
                                me->delete("blade_lv");
                                me->delete("no_fight");
                                obj->delete("no_fight");
return COMBAT_D->skill_msg( me, obj, "狂龍暴怒", weapon_name, wind);                 
                        }
                } else
                {
                        if( me->query("gender")== "男")
message_vision("[1;36m$N說道: [1;33m哈!哈!你以為結束了嗎??去死吧....[m\n", me);
                        else
message_vision("[1;35m$N說道: [1;33m哈!哈!你以為結束了嗎??去死吧....[m\n", me);

                        hit= dem- obj->query("defand_eff");
                        obj->add("currentHP", -hit);
                        return hit;
                }
        }
}

string name( int select)
{
        switch( select)
        {
                case 1: return "疾風無影斬";
                case 2: return "wind";
                case 3: return "wind";
        }
}

int lv() { return 45; }
int attackful()
{
        return 1;
}

int type() { return BLADE; }
int usesp() { return 0; }
int usemp() { return 0; }
int successful() { return 100; }
int delay() { return 0; }
int all() { return 0; }
int check( object me)
{
        int me_sp, speed, vir;

        speed= me->query("speed");
        vir= me->query("vir");
        me_sp= 50 + 10* speed/ vir;

        if( me->query("blade_lv")== 100)
                return 1;
        else
        {
                if( me->query("currentSP")< me_sp)
                {
                        write("\n你[5;33m沒力[m了....\n");
                        me->delete_temp("bla");
                        return 0;
                } else
                {
                        me->add("currentSP", -me_sp);
                        if( me->query("gender")== "男")
                        {
message_vision("[1;36m$N說道: [1;33m讓你見識見識鬼魅的速度.[m\n", me);
                        return 1;
                        } else
                        {
message_vision("[1;35m$N說道: [1;33m讓你見識見識鬼魅的速度.[m\n", me);
                        return 1;
                        }
                }
        }
}
void temp(object me)
{
printf(".");
}
