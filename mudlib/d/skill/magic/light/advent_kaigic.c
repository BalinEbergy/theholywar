// advent_kaigic.c
// written by Hudson

#include <skill.h>

string tspace( int i);
void mess( object me, int i);

int main( object me, object obj)
{
        int default_d= 3500, demage, eff, mdem, mdef;
        string *effects;

        mdem= me->query("mdem")+ me->query("mdem_eff");
        mdef= obj->query("mdef")+ obj->query("mdef_eff");

// 傷害
        default_d= default_d+ default_d* me->skill_level( LIGHT)/ 20;

        if( me->query("element")== "light")
                demage= default_d* mdem* 1.2/ mdef;
        else
                demage= default_d* mdem/ mdef;

        if( obj->query("element")== "dark") demage= demage* 1.2;

        demage= demage* ( 90+ random( 30))/ 100*( 100- eff)/ 100;

        if( demage< 0) demage= -demage;

        obj->add("currentHP", -demage);
// ------

// 技能禁錮
        effects= obj->query("effect");
        obj->set("l_prison", 1);
        obj->set("l_prisont", 10);
        if( effects)
                effects+= ({ "l_prison" });
        else effects= ({ "l_prison" });
        obj->set("effect", effects);
// ------

        message_vision("[1;37m在無限的光芒裡[m,突然射出了數道[1;33m猛烈的光柱,瞬間貫穿了$N的身體!!!!!![m\n", obj);

        return demage;
}

string name( int select)
{
        switch( select)
        {
                case 1: return "光神降臨";
                case 2: return "advent of Kaigic";
                case 3: return "advent_kaigic";
        }
}

int type() { return LIGHT; }
int lv() { return 100; }
int attackful()
{
        return 1;
}

string mes( object me)
{
        return "唸完咒語,天空上隱隱的顯現[1;33m光之魔法陣[m...\n"+
                "在一瞬間,[1;5;36m無數道耀眼的光芒[m[1;37m從天上射了下來!!![m\n"+
                "四周的大氣震動著,混雜著龐大的,無窮的魔力,就在轉瞬的極限[1;5;33m爆炸!!!![m";
}

int usesp() { return 50; }
int usemp() { return 250; }
int successful() { return 100; }
int delay() { return 0; }
int all() { return 1; }
string element() { return "light"; }

int check( object me, object obj)
{
        string str;

        if( me->query("mind")< 0)
        {
                write("\n你太邪惡了.\n");
                return 0;
        }

        if( !me->query_temp("advent_kaigic_check"))
        {
                str= "[33m$N口中喃喃的唸著咒語－\n";
                str+= tspace( 5);
                str+= "                        [1;37m比黎明還要耀眼的東西...\n[m";
                str+= tspace( 5);
                message_vision( str, me);
                mess( me, 1);
                me->set_temp("advent_kaigic", obj);
                me->set("delay", time()+ 100);
                environment( me)->set("unmoveable", 1);
                return 0;
        } else
        {
                me->delete_temp("advent_kaigic");
                environment( me)->delete("unmoveable");
                return 1;
        }
}

void mess( object me, int i)
{
        string str= "";
        object env;

        env= environment( me);
        str+= tspace( 5);
        switch( i)
        {
                case 1:
                        if( !me || me->query("ghost"))
                                env->delete("unmoveable");
                        str+= "                        [1;37m比信仰還要神聖的東西...\n[m";
                        str+= tspace( 5);
                        tell_room( env, str);
                        call_out("mess", 1, me, 2);
                        break;
                case 2:
                        if( !me || me->query("ghost"))
                                env->delete("unmoveable");
                        str+= "                        [1;37m讓我與您的力量結合...\n[m";
                        str+= tspace( 5);
                        tell_room( env, str);
                        call_out("mess" 1, me, 3);
                        break;
                case 3:
                        if( !me || me->query("ghost"))
                                env->delete("unmoveable");
                        str+= "                                [1;37m光[m\n";
                        str+= tspace( 5);
                        tell_room( env, str);
                        mess( me, 4);
                        break;
                case 4:
                        if( !me || me->query("ghost"))
                                env->delete("unmoveable");
                        str+= "                                [1;37m神[m\n";
                        str+= tspace( 5);
                        tell_room( env, str);
                        call_out("mess", 1, me, 5);
                        break;
                case 5:
                        if( !me || me->query("ghost"))
                                env->delete("unmoveable");
                        str+= "                                [1;33m降[m\n";
                        str+= tspace( 5);
                        tell_room( env, str);
                        mess( me, 6);
                        break;
                case 6:
                        if( !me || me->query("ghost"))
                                env->delete("unmoveable");
                        str+= "                                [1;33m臨[m\n";
                        str+= tspace( 5);
                        tell_room( env, str);
                        me->set_temp("advent_kaigic_check", 1);
                        if( !me->is_fighting())
                        ( COMMAND_DIR+ "std/cast.c")->main( me,
                                "advent of kaigic to "+
                                ( me->query_temp("advent_kaigic"))->query("id"));
                        else
                        ( COMMAND_DIR+ "std/cast.c")->main( me,
                                "advent of kaigic");
                        me->delete_temp("advent_kaigic_check");
                        me->del("delay");
        }
}

string tspace( int i)
{
        string str= "";

        while( i--)
                str+= "\n";

        return str;
}
