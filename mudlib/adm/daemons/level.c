// level.c

int query_exp( int lv)
{
    int value;

    value= lv* 200+ lv* lv* 50+ lv* 50* exp( lv* 0.088)* log( lv* 100);

    return value;
}

void level_up( object me)
{
    float ratio;
    int hp, mp, sp, str, arm, mag, spe, lv;
    mapping dbase;
    string msg;
    
    dbase= me->query_entire_dbase();

    lv= dbase["lv"];
    dbase["exp"]-= query_exp( lv++);
    ratio= log( 100)- log( 101- lv)+ 1;

    hp= ( dbase["con"]* 9/ 10+ random( dbase["con"]/ 5)+ 1)* ratio* 1.4;
    mp= ( dbase["spi"]* 9/ 10+ random( dbase["spi"]/ 5)+ 1)* ratio;
    sp= ( dbase["dex"]* 9/ 10+ random( dbase["dex"]/ 5)+ 1)* ratio;

    str= ( dbase["str"]* 9/ 10+ random( dbase["str"]/ 5)+ 1)* ratio* 0.7;
    arm= ( dbase["con"]* 9/ 10+ random( dbase["con"]/ 5)+ 1)* ratio* 0.5;
    mag= ( dbase["int"]* 9/ 10+ random( dbase["int"]/ 5)+ 1)* ratio* 0.6;
    spe= ( dbase["dex"]* 9/ 10+ random( dbase["dex"]/ 5)+ 1)* ratio* 0.7;

    msg= sprintf(
        "[1;37;44m你升級了!![m\n"
        "生命提升   - [1;37m%5d[m      法力提升   - [1;36m%5d[m\n"
        "體力提升   - [1;33m%5d[m      攻擊力提升 - [1;31m%5d[m\n"
        "防禦力提升 - [1;37m%5d[m      魔力提升   - [1;34m%5d[m\n"
        "速度提升   - [1;36m%5d[m      額外點數   - [1;35m%5d[m\n"
        "\n", hp, mp, sp, str, arm, mag, spe, !( lv% 5));

    dbase["lv"]++;
    dbase["maxHP"]+= hp;
    dbase["maxMP"]+= mp;
    dbase["maxSP"]+= sp;
    dbase["strength"]+= str;
    dbase["armor"]+= arm;
    dbase["magic"]+= mag;
    dbase["speed"]+= spe;
    dbase["addpoint"]+= !( lv% 5);

    tell_object( me, msg);
}

void add_exp( object me, int vlv, int exp)
{
    int lv, reduce;

    lv= me->query("lv");

    if( lv== 100) return;

    if( lv>= vlv) reduce= ( lv- vlv)* 2;
    else reduce= vlv- lv;

    if( reduce> 10) reduce= 10;

    exp= exp* ( 10- reduce)/ 10;

    me->add("exp", exp);
    tell_object( me, "你得到了 [1;33;44m" + exp + "[m 點經驗值~~~\n");

    while( me->query("exp")>= query_exp( me->query("lv")))
    {
        if( me->query("lv")!= 100)
            level_up( me);
        else me->set("exp", 0);
    }
}
