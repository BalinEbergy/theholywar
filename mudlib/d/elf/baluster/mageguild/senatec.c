// senatec.c
// written by Hudson

#include <room.h>

inherit ROOM;

void create()
{
    set("short","[1;32m巴路斯特元老院[m");
    set("long",@LONG
這裡就是整個巴路斯特的最高首府,[1;32m元老院[m,待在這兒的,都是
巴路斯特中德高望重的長老們,你見到四周牆壁上精靈的圖騰
,感覺到無比的肅穆莊重.
下方是魔法師公會會所.
LONG
    );

    set("exits", 
    ([
    "down"  : __DIR__ + "mageguildc",
    ])
    );

    set("objects", ([
        "/d/elf/baluster/npc/dio":1,
        "/d/elf/baluster/npc/thearn":1,
        "/d/elf/baluster/npc/suenus":1,
        ]));

    setup();
}
