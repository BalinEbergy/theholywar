//test8.c
//written by zz

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;37mtest8[m");
        set("long",@LONG
test Lv81-90
LONG
        );
        set("exits", ([
                "north" : __DIR__ + "test7.c" ,
                "south" : __DIR__ + "test9.c" ,
                ]));
        set("objects",  ([
                __DIR__ + "npc/test/lv81" :1 ,
                __DIR__ + "npc/test/lv82" :1 ,
                __DIR__ + "npc/test/lv83" :1 ,
                __DIR__ + "npc/test/lv84" :1 ,
                __DIR__ + "npc/test/lv85" :1 ,
                __DIR__ + "npc/test/lv86" :1 ,
                __DIR__ + "npc/test/lv87" :1 ,
                __DIR__ + "npc/test/lv88" :1 ,
                __DIR__ + "npc/test/lv89" :1 ,
                __DIR__ + "npc/test/lv90" :1 ,
        ]));
        set("no_recall", 1);
        setup();
}
