// wizard.c

#define WIZLIST "/adm/daemons/wizlist"

private mapping wiz_status= ([]);

void create()
{
    int i;
    string *list, wiz_name, wiz_lv;

    seteuid( getuid());
    list= explode( read_file( WIZLIST), "\n");
    wiz_status= allocate_mapping( sizeof( list));
    for( i= 0; i< sizeof( list); i++)
        if( list[i][0]== '#'
            || sscanf( list[i], "%s %d", wiz_name, wiz_lv)!= 2) continue;
        else wiz_status[ wiz_name]= wiz_lv;
}

int wiz_level( object ob)
{
    int lv;

    if( lv= wiz_status[ ob->query("id")])
        return lv;
    else if( userp( ob))
        return 1;
    else return 0;
}
