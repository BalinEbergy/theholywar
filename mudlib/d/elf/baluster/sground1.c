// sground1.c
// written by Hudson

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;32m巴路斯特廣場[m");
        set("long",@LONG
這裡是巴路斯特廣場,四周是由淡綠色的石板所鋪成,你覺得
精神舒服許多,往來的精靈們神態悠然,讓你覺得這裡的一切
都是那麼的祥和.
LONG
        );

        set("exits",
        ([
	"east"	: __DIR__ + "sground3",
        "north" : __DIR__ + "center",
        "south" : __DIR__ + "sgroad1",
	"west"	: __DIR__ + "sground2",
        ])
        );

        set("objects", ([
                __DIR__ + "npc/kasall": 1,
                ]));
        setup();
}
