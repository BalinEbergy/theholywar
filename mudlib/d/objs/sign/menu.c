// menu.c
// written by Hudson

#include <element.h>

inherit ITEM;

void create()
{
    set_name("���", ({ "menu" }));
    set("long", @LONG
�ݢ�������������������������������������������������������
��        -- [1;32m�t�w���\�] Sainter's Restaurant[m --         ��
��                                                      ��
�� 1. ������ (Beef)             2 �H��            $  3  ��
�� 2. �t��²��M�\ (Quick Bar)  4 �H��            $  6  ��
�� 3. �w���� (Happy Noodles)    3 �H��   ���G��   $  6  ��
�� 4. �⫬�M�\ (Bringing Bar)   7 �H��   ���G��   $ 16  ��
��                                                      ��
�㢤������������������������������������������������������
LONG
        );
    set("unit", "��");
    set("element", WIND);
    set("epower", 88);

    set("no_get", 1);
    set("unsac", 1);
}
