// prac.c
// written by hiwind

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
    int HP= 100000, MP= 2500,SP= 5000;

    set_name("狂魔術士", ({ "practicer" }));
    set("lv", 50);
    set("class",WIZARD);
    set("race",HUMAN);
    set("describe","法師.");
    set("age",200);
    set("element", HOLY);
    set("mind", 500);
    set("gender", 1);

    set("maxHP", HP);
    set("currentHP", HP);
    set("maxMP", MP);
    set("currentMP", MP);
    set("maxSP", SP);
    set("currentSP", SP);
    set("strength", 2000);
    set("def", 800);
    set("mdem", 4000);
    set("mdef", 500);
    set("spi", 20);
    set("speed", 6000);
    set("exp", 5000);
    setup();
}
void init()
{
    ::init();
    if( !previous_object()
        || !userp(previous_object()))
        return;
    call_out("action", 10, previous_object());
}

void action( object obj)
{
    if( !obj || !present(obj) || this_object()->is_fighting()) return;
    if( obj->is_fighting()) kill_ob(obj);
    if( obj->query("mind")<-500) kill_ob(obj);
    command("say 這裡嚴禁邪惡的再度入侵,等級過低不要冒然動手");
    setup();
}
