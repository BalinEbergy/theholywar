// eagle.c
// written by hiwind

inherit NPC;

void create()
{
        int HP= 100000, MP= 1000,SP= 3500;

        set_name("�@�����N", ({ "eagle" }));
        set("lv", 55);
        set("describe","�N.");
        set("age", 5);
        set("element", "air");
        set("mind", 0);

        set("mixHP", HP);
        set("currentHP", HP);
        set("mixMP", MP);
        set("currentMP", MP);
        set("mixSP", SP);
        set("currentSP", SP);
        set("dem", 5000);
        set("def", 250);
        set("mdem", 200);
        set("mdef", 400);
        set("spirit", 20);
        set("spe", 600);
        set("exp", 2500);
        setup();
        set("chat_chance", 25);
        set("chat_msg", ({
                (: this_object(), "random_move" :)}));

}
