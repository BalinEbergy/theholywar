// sroad13.c
// written by Hudson

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;32m�ڸ����S��D[m");
        set("long",@LONG
�o�̬O�ڸ����S�ө����E���a,���D�㩱,�ȩ�,�Ψ��㩱,�\
�U����.���F�_�訫�i��F�ӷ~���|���ڸ����S���|.�|�P��
�����,���F�̪��@�ɵ��A�@���w�ֲ��M���P.
LONG
        );
        set("exits",
        ([
        "west"  : __DIR__ + "sroad14",
        "east"  : __DIR__ + "sroad12",
        ])
        );

        set("objects", ([
                __DIR__ + "npc/earth_elf": 1,
                __DIR__ + "npc/elf_citizen": 1,
                __DIR__ + "npc/water_elf": 1,
                ]));

        setup();
}
