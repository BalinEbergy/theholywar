// big_axe.c
// written by Hudson

#include <equip.h>
#include <element.h>

inherit EQUIP;

void create()
{
    set_name("大斧", ({ "big axe", "big", "axe" }));
    set("long", @LONG
一把普通的大斧.
LONG
        );
    set("lv", 23);
    set("unit", "把");
    set("element", EARTH);
    set("value", 50);

    set("wear_type", HAND);
    set("weapon", AXE);
    set("dem_eff", 500);
    set("spe_eff", -10);
    set("eff", ({ "dem_eff", "spe_eff" }));

    set_weight( 130);
}
