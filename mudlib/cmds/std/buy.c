// buy.c
// written by Bother
// rewritten by Hudson

void create() { seteuid( getuid()); }

int main( object me, int argc, string *argv)
{
    int i, p, j, k, long;
    object obj, *objs, *dirs, item; // Hudson
    string str= "", ch, pa= " ", pass= "";
    string arg;

    if( me->is_ghost())
    {
        write("你沒有辦法做到.\n");
        return 1;
    }
    if( argc< 2)
    {
        write("你想要買什麼?\n");
        return 1;
    }

    arg= argv[1];

    objs= all_inventory( environment( me));
    i= sizeof( objs);

    while( i--)
    {
        if( objs[i]->query("trading"))
        {
            if( stringp( ch= objs[i]->query("buy_mind")))
            {
                sscanf( ch, "%d~%d", j, k);
                if( me->query("mind")< j && j!= -1000)
                    return notify_fail("\n你太邪惡了,我不做你的生意.\n");
                if( me->query("mind")> k && k!= 1000)
                    return notify_fail("\n你不屬於我的陣營,對不起了.\n");
            }
            if( arg== "list")
            {
                if( dirs= all_inventory( objs[i]))
                {
                    p= sizeof( dirs);
                    while( p--)
                    if( !dirs[p]->equipped())
                    {
                        if( dirs[p]->query("color"))
                        {
                            pass= "";
                            for( long= 0; long< dirs[p]->query("color"); long++)
                            {
                                pass+= pa;
                            }
                            str+= "[36m["+ 
    sprintf("%3d", dirs[p]->query("lv"))+ "][m"+ "\t"+ 
    sprintf("%-30s", dirs[p]->name()+ "("+ dirs[p]->query("id")+")"+ pass)+ 
    "[1;33m["+ sprintf("%7d", dirs[p]->query("value"))+ "]"+ "\n[m";
                        } 
                        else
                            str+= "[36m["+ 
    sprintf("%3d", dirs[p]->query("lv"))+ "][m"+ "\t"+ 
    sprintf("%-30s", dirs[p]->name()+ "("+ dirs[p]->query("id")+")")+ 
    "[1;33m["+ sprintf("%7d", dirs[p]->query("value"))+ "]"+ "\n[m";
                    }
                }
                if( dirs= objs[i]->query("selling"))
                {
                    p= sizeof( dirs);
                    while( p--)
                        if( dirs[p]->query("color"))
                        {
                            pass= "";
//                            long= dirs[p]->query("color");
                            for( long= 0; long< dirs[p]->query("color"); long++)
                            {
                                pass+= pa;
                            }
                            str+= "[36m["+ 
    sprintf("%3d", dirs[p]->query("lv"))+ "][m"+ "\t"+ 
    sprintf("%-30s", dirs[p]->name()+ "("+ dirs[p]->query("id")+")"+ pass)+ 
    "[1;33m["+ sprintf("%7d", dirs[p]->query("value"))+ "]"+ "\n[m";
                        }
                        else
                            str+= "[36m["+ 
    sprintf("%3d", dirs[p]->query("lv"))+ "][m"+ "\t"+ 
    sprintf("%-30s", dirs[p]->name()+ "("+ dirs[p]->query("id")+")")+
    "[1;33m["+ sprintf("%7d", dirs[p]->query("value"))+ "]"+ "\n[m";
                }
                if( str!= "")
                {
                    write("\n[1;44m這裡有賣:[m"+
                    "\n等級\t名稱\t\t\t\t價錢\n"+ str+ "\n");
//                    me->start_more("\n"+ str+ "\n");
                    return 1;
                }   
                else {
                    write("這裡沒東西可以買喔!\n");
                    return 1;
                }
            }
            else
            {
                dirs= objs[i]->query("selling");
                for( p=0; p< sizeof( dirs); p++)
                {
                    if( arg== dirs[p]->query("id"))
                    {
                        if( me->query("money")< dirs[p]->query("value"))
                        {
                            objs[i]->command("tell "+ me->query("id")+ " 你沒有足夠的錢喔.");
                            return 1;
                        }
                        if( me->query_max_encumbrance()- me->query_encumbrance()<
                            dirs[p]->weight())
                        {
                            write("你拿不動這個東西.\n");
                            return 1;
                        } else
                        {
                            me->add("money", -dirs[p]->query("value"));
// Hudson
                            item= new(( string) dirs[p]);
                            item->move( me);
// end... 問題解決 ...
                            message_vision( "\n$N從"+objs[i]->query("name")+ "那兒買了"+ dirs[p]->query("unit")+ dirs[p]->name()+ "("+ dirs[p]->query("id")+")"+ "\n", me);
                            return 1;
                        }
                    }
                }
                    if( obj= present( arg, objs[i]))
                    {
                        if( obj->equipped())
                        {
                            objs[i]->command("tell "+ me->query("id")+ " 這裡沒有賣這個東西.\n");
                            return 1;
                        } else
                        {
                            me->add("money", -obj->query("value"));
                            obj->move( me);
                            message_vision( "\n$N從"+objs[i]->query("name")+ "那兒買了"+ obj->query("unit")+ obj->name()+ "("+ obj->query("id")+")"+ "\n", me);
                            return 1;
                        }
                    }
                    write("這裡沒有賣這個東西.\n");
                    return 1;
            }
        }
    }

    write("這裡不能買東西.\n");
    return 1;
}

int help( object me)
{
    write( @HELP
指令格式 : buy [物品] | <list>

當這格有商人的時候你可以利用這個指令買東西,或打list可以知道商人所賣的東西.
但當你正營和商人衝突時他不會賣東西給你的.
HELP
  );

  return 1;
}
