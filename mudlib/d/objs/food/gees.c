// gees.c
// written by Hudson

#include <element.h>

inherit FOOD;

void create()
{
    set_name("吉斯果", ({ "gees" }));
    set("long", @LONG
艾爾芙大陸東側的農產,一種可保存很久的水果,味甜而甘.
LONG
        );
    set("unit", "顆");
    set("lv", 10);
    set("value", 5);

    set_weight( 20);
}
