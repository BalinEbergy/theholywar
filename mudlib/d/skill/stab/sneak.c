// sneak.c
// written by Hudson

#include <skill.h>

int main( object me, object obj)
{
        int damage, def_eff, dem_eff, sp, dem, def, str, spirit;
        string weapon_name;

        dem= me->query("spe")+ me->query("spe_eff");
        def= obj->query("spe")+ me->query("spe_eff");
        spirit= me->query("spirit");

        if( random( 100)> (( dem* 2- def)* 50/ dem+ spirit))
        {
                message_vision("\n可是沒有命中$N!\n", obj);
                return -1;
        }

        def_eff= obj->query("defand_eff");
        def= obj->query("def")+ obj->query("def_eff");
        dem_eff= me->query("damage_eff");
        dem= me->query("dem")+ me->query("dem_eff");
        str= me->query("str")/ 40+ 1;
        weapon_name=( me->query_temp( me->query_temp("stb")))->query("name");

        damage= dem*( 350+ me->skill_level( STAB))* str/ 400- random( dem)/ 10-
                ( def* 2/ 7+ def_eff- dem_eff);
        if( damage< 0) damage= 0;

        obj->add("currentHP", -damage);

        message_vision(damage+ "\n$N趁敵人漏出破綻的時候,將全身的力量傳到手中的"
                + weapon_name+ ",\n像一條巨蟒猛然向$n竄去!\n", me, obj);

        return damage;
}

string name( int select)
{
        switch( select)
        {
                case 1: return "巨蟒突竄";
                case 2: return "stab sneak";
                case 3: return "sneak";
        }
}

int type() { return STAB; }
int lv() { return 30; }
int attackful()
{
        return 1;
}

string mes( object me)
{
        return "的手臂不斷積蓄力量.";
}

int usesp() { return 30; }
int usemp() { return 0; }
int successful() { return 100; }
int delay() { return 3; }
int all() { return 0; }
string element() { return ""; }

int check( object me)
{
        if( me->query_temp("stb") != "handb")
        {
                write("\n你單手持槍,無法使出全力!\n");
                me->delete_temp("stb");
                return 0;
        }
        if( me->query("str")< 30)
        {
                write("\n你力不從心.\n");
                me->delete_temp("stb");
                return 0;
        }
        if( me->query("currentSP")< me->query("mixSP")/ 10)
        {
                write("\n你[5;33m沒力[m了....\n");
                me->delete_temp("stb");
                return 0;
        }
        return 1;
}
