// strength
// written by Mondale

#include <skill.h>

string name( int i);

int main( object me, object obj)
{
        string w_name= me->query_temp( me->query_temp("sla"))->name();
        int damage, dam_all = me->query("dem") + me->query("dem_eff");
        int def_all = obj->query("def") + obj->query("def_eff");
        int k,l,m;
        object env;
        env= environment( me);
        if (dam_all <= 10000)
        {
        damage = dam_all * 3 - (def_all /2) + random (50 * me->skill_level(SLASH));
        } else if (dam_all <=20000)
        {
        damage = dam_all * 2.5 - (def_all /2) + random (50 * me->skill_level(SLASH)) + 5000;
        } else if (dam_all <= 30000)
        {
        damage = dam_all * 2 - (def_all /2) + random (50 * me->skill_level(SLASH)) + 15000;
        } else
        {
        damage = dam_all * 1.5 - (def_all /2) + random (50 * me->skill_level(SLASH)) + 30000;
        }
        message_vision("\n$N��ܤ@�n�G�u[31;1;5m$n!!![0m",me,obj);
        m = random(5);
        switch ( m)
        {
        default:
                tell_room(env,"�ĨĻ{�R�a!!�Y�ڳo�@�C!!�v\n");
                break;
        case 1:
                tell_room(env,"����[35;1m�U�a��[0m�h�a!!�v\n");
                break;
        case 2:
                tell_room(env,"���n���,[32;1;5m�h���a[0m!!�v\n");
                break;
        case 3:
                tell_room(env,"���ڱq�o�ӥ@�ɤW�����a!!�v\n");
                break;
        case 4:
                tell_room(env,"[1;37m���a!!![0m�v\n");
                break;
        case 5:
                tell_room(env,"�A���J�F!!���ڥh��[36;1m�F��[0m�a!!�v\n");
                break;
        }
        message_vision("$N���@�n�j��...",me);
        if (random(15) + me->query("spirit") + me->query("int") > 3*(obj->query("spirit") + random(15)))
                {
                message_vision("�ϱo$N�ߨ���í�^�˦b�a." 
                + "$n�X���κɥ��O�@�C�A�U.\n",obj,me );
                l = 20;
                }
                else if (random(15) + me->query("spirit") + me->query("int") > 2*(obj->query("spirit") + random(15)))
                        {
                        message_vision("�ϱo$N�b�F�@�b..."
                        + "$n�X���κɥ��O�@�C�A�U.\n",obj,me );
                        l = 10;
                        }
                                else
                                {
                                message_vision("��$N�����S���v�T...\n",obj);
                                l = 0;
                                }
        if ( random(100) > 50 + l + (me->query("speed") / 5))
        {
        message_vision("$N�κɥ��O���@�C�Q$n�{�L�F!!\n",me,obj);
        return -1;
        }
        if (damage < 0 ) damage = 0;
        obj->add("currentHP", -damage);
        return COMBAT_D->skill_msg( me, obj, name( 1), w_name, damage);
}

string name( int select)
{
        switch( select)
        {
                case 1: return "�O�A�s�e";
                case 2: return "strength";
                case 3: return "strength";
        }
}

int lv() { return 10; }
int attackful()
{
        return 1;
}

string mes( object me)
{
        return "�`�`���l�F�@�f��,�N���C���|�L�Y.";
}

int type() { return SLASH; }
int usesp() { return 20; }
int usemp() { return 0; }
int successful() { return 100; }
int delay() { return 3; }
int all() { return 0; }
int check( object me)
{
        return 1;
}
