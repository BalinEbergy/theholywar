// eater.c
// written by Hudson

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

string *race= ({ HUMAN, OGRE, DRAGON });
string *element= ({ WIND, EARTH, WATER, DARK, FIRE });

void create()
{
    int HP= 23000, MP= 200, SP= 2000;

    set_name("食客", ({ "eater" }));
    set("lv", 27);
    set("class",WARRIOR);
    set("race", race[ random( 2)]);
    set("gender",1);
    set("describe","正大快朵頤的食客.");

    set("age", 70+ random( 70));
    set("element", element[ random( 4)]);
    set("mind", 17);

    set("maxHP", HP);
    set("currentHP", HP);
    set("maxMP", MP);
    set("currentMP", MP);
    set("maxSP", SP);
    set("currentSP", SP);
    set("strength", 1000);
    set("armor", 912);
    set("magic", 600);
    set("spi", 3);
    set("speed", 1000);
    set("exp", 2000);

    setup();
}

void init()
{
    object obj;

    if( !(obj= previous_object()) || !userp( obj))
        return;

    call_out("ki", 1, obj);
}

void ki( object obj)
{
    if( obj->is_fighting()) kill_ob( obj);
}
