// black_box.c
// written by Bother

inherit BOX;

void create()
{
        set_name("黑色的箱子", ({ "black box", "black", "box" }) );
        set("long", @LONG
一個全身烏黑的箱子
LONG
                );
        set("lv", 1);
        set("unit", "個");
        set("weight", 3000);
        set("element", "dark");
        set("epower", 10);
        set("lock", "1");
        set("lock_name", "1");
        set("box", ({
                OB_OTHER + "meat.c",
                OB_KEY + "red_key.c",
        }));

        set("time", 20);
        set("money", 10);
        set("value", 1);
        set("unsac", 1);
        set("no_get", 1);

        setup();
}
