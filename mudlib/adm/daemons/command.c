// command.c

#include <command.h>

static string *wiz;
static string *usr;
static string *std;

string find_command( string, object);

void create()
{
    seteuid( getuid());
    wiz= get_dir( WIZ_DIR)- ({ "."});
    usr= get_dir( USR_DIR)- ({ "."});
    std= get_dir( STD_DIR)- ({ "."});
}

int exec_command( string str, object obj)
{
    int i, j, k, size;
    string verb, file;
    string *argv;

    if( sizeof( argv= explode( str, " ")))
        verb= argv[0];
    else verb= str;

    // Deal with quote
    argv-= ({ "" });
    size= sizeof( argv);

    for( i= 1, k= 1; i< size; i++, k++)
    {
        if( argv[i][0]== '"')
        {
            argv[k]= argv[i][ 1.. sizeof( argv[i])- 1];
            while( i++< size- 1)
            {
                argv[k]+= " "+ argv[i];
                if( argv[i][ sizeof( argv[i])- 1]== '"')
                {
                    argv[k]= argv[k][ 0.. sizeof( argv[k])- 2];
                    break;
                }
            }
        } else argv[k]= argv[i];
    }
    if( k< size) argv= argv[ 0.. k- 1];

    file= find_command( verb, obj);

    if( file== "N")
        return 0;
    else if( file== "P")
    {
        tell_object( obj, "你現在在睡覺喔!\n");
        return 1;
    } else return file->main( obj, sizeof( argv), argv);
}

string find_command( string verb, object obj)
{
    int i, level;
    string *cmds, command, path, temp;

    command= "N";

    for((( level= D_WIZARD->wiz_level( obj))> 1? level= 3: level++),
        i= 0; i< level; i++)
    {
        switch( i) {
            case 2: cmds= wiz; break;
            case 1: cmds= usr; break;
            case 0: cmds= std; break;
        }

        temp= find_string( verb, cmds);

        if( !temp) continue;

        if( command== "N" || sizeof( command)> sizeof( temp))
        {
            command= temp;
            switch( i) {
                case 2: path= WIZ_DIR; break;
                case 1: path= USR_DIR; break;
                case 0: path= STD_DIR; break;
            }
        }
    }

    if( obj->query("rest")> 1 && path== STD_DIR)
        return "P";

    if( command== "N") return command;
    else return path+ command;
}
