// elf_lance.c
// written by Hudson

#include <equip.h>
#include <element.h>

inherit EQUIP;

void create()
{
        set_name("精靈之槍", ({ "elf lance", "elf", "lance" }));
        set("long", @LONG
一把泛著綠色光芒的長槍.
LONG
                );
        set("lv", 80);
        set("unit", "把");
        set("element", WIND);
        set("epower", 87);
        set("value", 100);

        set("wear_type", HAND);
        set("weapon", LANCE);
        set("dem_eff", 5000);
        set("spe_eff", 27);
        set("eff", ({ "dem_eff", "spe_eff" }));

        set_weight( 100);
}
