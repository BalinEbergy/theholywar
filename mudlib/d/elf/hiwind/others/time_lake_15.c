// time_lake_3.c
// written by hiwind

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;5;31m�۷�[m[1;37m��[m[1;36m���[m");
        set("long",@LONG
�A�i�J�F���,�M�ӥu���@�W�Q�����R���k�l���C�Y���_.����
�n�ϧA��ı�o�s��,���T�Q�����@�u���O.
LONG
        );

        set("exits",
        ([
        "west" : __DIR__ + "time_lake_11",
        "up" : "d/elf/hiwind/hiwind",
        ])
        );

        set("objects",
        ([
                __DIR__ + "npc/girl_3":1
        ])
        );

        set("no_fight", 0);
        setup();
}
