// lv1blade.c
// written by Hudson

#include <equip.h>
#include <element.h>

inherit EQUIP;

void create()
{
    set_name("見習短刀", ({ "novice blade" }));
    set("long", @LONG
一把木柄小短刀.
LONG
        );
    set("lv", 1);
    set("unit", "把");
    set("element", COSMOS);
    set("value", 5);

    set("wear_type", HAND);
    set("weapon", BLADE);
    set("damage", 30);
    set("def_eff", 10);
    set("hit_eff", 200);
    set("eff", ({ "hit_eff", "def_eff" }));

    set_weight( 40);
}
