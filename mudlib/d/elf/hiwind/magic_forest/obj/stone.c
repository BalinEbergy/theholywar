// stone.c
// written by hiwind

inherit ITEM;

void create()
{
        set_name("古老石碑", ({ "statue" }));
        set("long", @LONG
聖冥傳說:
如果聖被冥所擊敗,那世界將被毀滅,我精靈族亦不保,而上天
為了懲罰世界上的無知與兇殘,所以在1000年後,天地將失衡,
大戰必開始,然而上蒼並不是要消滅我們,所以他給予我們一
唯一的機會,將有精靈族之英雄降世,但由於擔心最強之魔法
失傳,所以刻此碑.
<唯有死能生,當聖所衰弱時,火燄能使重生.>
LONG
                );
        set("unsac", 1);
        set("no_get", 0);
        set("weight", 4000);
}
