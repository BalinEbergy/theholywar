// sac.c
// written by Hudson

int main( object me, int argc, string *argv)
{
    object obj;

    if( argc< 2)
        write("你要奉獻什麼呢?\n");
    else if( me->is_ghost())
        write("你無法奉獻.\n");
    else if( !( obj= find( argv[1], environment( me))) || obj->is_character())
        write("你找不到這樣物品.\n");
    else if( obj->query("unsac"))
        write("這樣物品無法奉獻...\n");
    else {
        if( obj->query("playercor"))
            if( obj->query("playercor")!= me->query("id"))
            {
                write("這不是你的屍體.\n");
                return 1;
            }
        message_vision("$N把$n奉獻給天神,天神賜給$N一些[1;33m金幣[m.\n", me, obj);
        me->add("money", obj->query("value")/ 100);
        destruct( obj);
    }

    return 1;
}
