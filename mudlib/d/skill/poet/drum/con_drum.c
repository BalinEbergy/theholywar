// con_drum.c
// written by Hudson

#include <skill.h>

int main( object me, object obj)
{
        int default_d= 10, m_mdef, o_mdef, damage, lv;

        m_mdef= me->query("mdef")+ me->query("mdef_eff");
        o_mdef= obj->query("mdef")+ obj->query("mdef_eff");
        lv= me->skill_level( DRUM);
        default_d= default_d+ default_d *lv/ 50;

        damage= default_d* m_mdef/ o_mdef;
        lv= lv/ 33+ 2;
        while( lv--)
        {
                damage= damage/ 4* 3+ random( damage/ 2);

                obj->add("currentHP", -damage);
                COMBAT_D->skill_msg( me, obj, "連環擊", "none", damage);
                if( obj->query("currentHP")<= 0)
                        return -1;
        }

        return -1;
}

string name( int select)
{
        switch( select)
        {
                case 1: return "連環擊";
                case 2: return "continuous drum";
                case 3: return "con_drum";
        }
}

int type() { return DRUM; }
int lv() { return 2; }
int attackful()
{
        return 1;
}

string mes( object me)
{
        return "揮舞著雙手的鼓棒,在"+ me->query_temp("handb")->name()
                + "上連續敲出序曲的聲音~";
}

int usesp() { return 10; }
int usemp() { return 0; }
int successful() { return 90; }
int delay() { return 0; }
int all() { return 1; }

int check( object me)
{
        if( me->query_temp("drum"))
        {
                write("\n你上一個動作還沒完成...\n");
                return 0;
        }

        me->set_temp("drum", 1);
        call_out("drum_dis", 2, me);

        return 1;
}

void drum_dis( object me)
{
        if( me->query_temp("drum")== 1)
                me->delete_temp("drum");
}
