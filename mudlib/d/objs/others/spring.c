// spring.c
// written by Bother

#include <element.h>

inherit ITEM;

void create()
{
    set_name("泉水", ({ "spring" }));
    set("long", @LONG
一條水柱直沖上天際.
LONG
        );
    set("unit", "個");
    set("element", WATER);
    set("epower", 20);
    set("no_get", 1);
    set("unsac", 1);

    set("drinkable", 1);
}
