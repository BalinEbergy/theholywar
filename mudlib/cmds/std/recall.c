// recall.c

int main( object me, int argc, string *argv)
{
    seteuid( getuid());

    write("你祈求天神傳送...\n");
    if( me->move( D_TRANS->transport_room( me->query("recallroom"))))
        write("有錯誤產生.\n");
    else me->set("currentSP", me->query("currentSP")/ 2);

    return 1;
}

int help( object me)
{
    write( @HELP
指令格式: recall 或 /

可祈求天神傳送你至起始點.
HELP
    );

    return 1;
}
