// confucius.c
// written by zz
// 至聖先師 孔夫子

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
    int
        HP       =  300000,
        MP       =  260000,
        SP       =   15000,
        LV       =   150,
        damage     =   24000,
        defence    =   30000,
        magic_damage   =   39000,
        magic_defence  =   47000,
        speed      =  2100,
        exp      =   53000;

    set_name("孔夫子", ({ "confucius"}));
    set("pre", "至聖先師");
    set("describe", "一位留著白色長鬚的老者,全身散發著智慧的光芒");
    set("class", GOD);
    set("race", GODBEING);
    set("gender", 1);
    set("age", 350);
    set("element", HOLY);
    set("mind", 1000);

    set("teacher", ({
        0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
        11, 12, 13, 14, 15, 16,
        20, 21, 22, 23,
        30,
        }));
    set("favorite", ({
        "knight", "warrior", "mage", "ruronin",
        "summoner", "e_maker", "poet", "god",
        }));
    set("teach_mind", "-1000 ~ 1000");
    set("lv_up", "[1;36m孔夫子說道: [1;7;5;41;30mLV UP!!![m [1;33mCongratulation![m\n");
    set("no_enu_money", "[1;36m孔夫子說道: [33m沒錢也要給我一束脩啊![m\n");
    set("teach_message", "[1;36m孔夫子說道: [33m我什麼都會...你想學什麼?");

    set("lv", LV);
    set("maxHP", HP);
    set("currentHP", HP);
    set("maxMP", MP);
    set("currentMP", MP);
    set("maxSP", SP);
    set("currentSP", SP);
    set("dem", damage);
    set("def", defence);
    set("mdem", magic_damage);
    set("mdef", magic_defence);
    set("spe", speed);
    set("exp", exp);

    setup();
}
