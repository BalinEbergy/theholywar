// earth_shield.c
// written by Hudson

#include <equip.h>
#include <element.h>

inherit EQUIP;

void create()
{
        set_name("奧格之盾", ({ "ogre shield", "ogre", "shield" }));
        set("long", @LONG
一面深棕色的盾,上面刻著一行字:

        [31m奧格.大地之盾[m
LONG
           );
        set("lv", 100);
        set("unit", "面");
        set("element", EARTH);
        set("epower", 100);
        set("weight", 1000);

        set("wear_type", HAND);
        set("def_eff", 10000);
        set("defand_eff", 20000);
        set("eff", ({ "def_eff", "defand_eff" }));
}
