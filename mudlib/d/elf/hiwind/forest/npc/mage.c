// magic.c
// written by hiwind

#include <class.h>
#include <element.h>
#include <race.h>

inherit NPC;

void create()
{
    int HP= 8000, MP= 2000, SP= 1000;

    set_name("[1;31m��[m�����F", ({ "magic" }));
    set("lv", 20);
    set("class",WIZARD);
    set("race",ELF);
    set("describe","���F.");
    set("age", 25);
    set("element", COSMOS);
    set("mind", 400);

    set("maxHP", HP); 
    set("currentHP", HP);
    set("maxMP", MP);
    set("currentMP", MP);
    set("maxSP", SP);
    set("currentSP", SP);
    set("strength", 900);
    set("armor", 650);
    set("magic", 600);
    set("spi", 10);
    set("speed", 600);
    set("exp", 1000);

    setup();
}
