// _.c
// written by koyamakyo
// 底線

inherit NPC;

void create()
{
        int HP= 0, MP= 0, SP= 0;

        set_name("底線", ({ "_" }));
        set("lv", 0);
        set("class","職業");
        set("ra","種族");
        set("gender","性別");
        set("describe","敘述");
        set("pre","形容");

        set("age", 0);
        set("element","屬性");
        set("mind", 陣營);

        set("mixHP", HP);
        set("currentHP", HP);
        set("mixMP", MP);
        set("currentMP", MP);
        set("mixSP", SP);
        set("currentSP", SP);
        set("dem", 0);
        set("def", 0);
        set("mdem", 0);
        set("mdef", 0);
        set("spe", 0);
        set("realmx", 0);
        set("exp", 0);

        set("chat_chance", 0);
        set("chat_msg", ({
                (: this_object(), "random_move" :),
                "\n動作\n"
                }));
        setup();
}

void init()
{
        ::init();
        if( !previous_object()
                || !userp(previous_object()))
                return;

}
