// holyroad3.c
// written by Hudson

#include <room.h>

inherit ROOM;

void create()
{
        set("short","[1;37m���t����[m");
        set("long",@LONG
���t��������o��,�|�P��{�F���ӤJ�f,�o��N�O���t�k�v
�̪��𮧤��B�F.
LONG
        );

        set("exits",
        ([
        "east"  : __DIR__ + "airdoor",
        "down"  : __DIR__ + "lightning/lightningdoor",
        "up"    : __DIR__ + "holydoor",
        "north" : __DIR__ + "lightdoor",
        "south" : __DIR__ + "holyroad2",
        "west"  : __DIR__ + "winddoor",
        ])
        );

        set("objects", ([
                __DIR__ + "npc/holygiant": 1,
                ]));

        setup();
}
